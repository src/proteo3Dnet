This repository contains the data and instructions to deploy the ***Proteo3Dnet*** protocol.

Starting from a flat list of proteins as output from interacomics experiments (e.g. AP-MS), this protocol aims at integrating information of the multimeric protein 3D structures available in the Protein Data Bank (PDB), as well as information related to Short Linear Motifs (SLiMs) and information from the BioGRID.

It relies on 3 steps:

**1. hh_enhanced_search:** make use of HHsearch to identify for each protein of the input the best corresponding structure of the PDB (https://www.rcsb.org/)

**2. network_analysis:** given the best structures, perform an analysis of the PDB entries so as to identify complexes of resolved structure likely to involve the proteins of the flat list

**3. elm_biogrid_analysis:** enrich previous annotations by searching possible transient interactions involving SLiMs, as integrated in the Eukaryotic Linear Motif resource (http://elm.eu.org/)


Requirements
============
- Linux OS (tested with Ubuntu 16.04.5 LTS)
- Docker (tested version: 18.06.0-ce)
- An internet connection
- Python 2
- Local version of the PDB
- Local version of the UniProt database (https://www.uniprot.org/)
- Local version of the HHsuite pdb70 database (available here: http://wwwuser.gwdg.de/%7Ecompbiol/data/hhsuite/databases/hhsuite_dbs/)

:warning: All other dependencies (e.g. HHsearch, MAFFT, PSIPRED, or BLAST) are automatically downloaded and installed, thanks to the Docker recipes.


Installation
============
1. Open hh_enhanced_search/src/step1/findTemplate.ini and replace the paths for the PDB, UniProt, and HHsuite pdb70 databases, with your own values.

2. Build the 3 Docker images  
    From the hh_enhanced_search/ directory:  
    `docker build -t hh_enhanced_search .`

    From the network_analysis/ directory:  
    `docker build -t network_analysis .`

    From the elm_biogrid_analysis/ directory:  
    `docker build -t limip .`

:warning: Depending on your Docker installation, these commands may require superuser or root privileges


Usage
=====
`wrapper_proteo3Dnet.py [-h] -i INFILE [-s SPECIES] -o OUTPUTDIR`

Arguments are:  
    
    -i INFILE     Input file: either multi-fasta, list of UniProtAC, or
                UniProtID (valid file extensions are: .txt, .dat, .data,
                .fasta, .fas, .fa, .faa, .mpfa, .fst, or no file extension)
    -s SPECIES    Filter the results based on the given organism name (UniProt
                format, e.g. HUMAN, DROME, YEAST, MOUSE)
    -o OUTPUTDIR  Name of the directory that will be created to store the
                results

Examples:  
`./wrapper_proteo3Dnet.py -i example/example_UniProtID.dat -s HUMAN -o MyOutputDir`

`./wrapper_proteo3Dnet.py -i example/proteasome.dat -o ProteasomeOutputDir`

`./wrapper_proteo3Dnet.py -i example/pragmin.dat -o PragminOutputDir`
