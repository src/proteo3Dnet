import os
import sys
#--------------------------------------------------------
# ----------------------- META DATA ---------------------
#--------------------------------------------------------
__author__ = 'IFB - Institut Francaise de Bioinformatique'
__dev__ = 'Reys Victor : reys[at]cbs.cnrs.fr | victor.reys[at]france-bioinformatique.fr'
__version__ = '1.6'

# -----------------------------------------------------------
# ---------------- INTERNAL DATABASES -----------------------
# -----------------------------------------------------------
# OWN DATABASES
# Path to ELM_interaction_domains data
ELM_INTERACTION_DOMAIN = '/LIMIP/databases/OWN_DATABASES/OWN_ELM_INTER_DOMAIN.json'
# Path to ELM_validated_instances data
ELM_VALIDE_INSTANCES = '/LIMIP/databases/OWN_DATABASES/OWN_ELM_VALIDE_INSTANCES.tsv'
# PATH to ELM classes and motifs regex
ELM_CLASSES = '/LIMIP/databases/OWN_DATABASES/OWN_ELM_CLASSES.json'
# Path to Uniprot_data path
UNIPROT_DATA = '/LIMIP/databases/OWN_DATABASES/OWN_FULL_REVIEWED_UNIPROT.json'
# Path to uniprot_names
UNIPROT_NAMES = '/LIMIP/databases/OWN_DATABASES/UNIPROT_NAMES.json'
# BioGrid ID database
BIOGRID_ID = '/LIMIP/databases/OWN_DATABASES/OWN_BIOGRID_ID.json'
# BioGrid Interactions data
BIOGRID_DATA = '/LIMIP/databases/OWN_DATABASES/OWN_BIOGRID_DATA.json'
# PDB associated with each PFam
PFAM_DATA = '/LIMIP/databases/OWN_DATABASES/OWN_PFAM_DATA.json'

# Do we with to retrieve FASTA from internal database ? 
# Yes because FULL_REVIEWED_UNIPROT.tab contains FASTA sequence also !
INTERNAL_UNIPROT = True

INTERNAL_PDB = '/DATABASE/PDB/pdb{PDB_ID}.ent.Z' 


# -----------------------------------------------------------
# ------------------ RAW  DATABASES -------------------------
# -----------------------------------------------------------
# REAL / RAW DATABASES
# Path to ELM_interaction_domains data
ELM_INTERACTION_DOMAIN_RAW = '/LIMIP/databases/RAW_DATABASES/ELM_interaction_domain_up_to_date.tsv'
# Path to ELM_validated_instances data
ELM_VALIDE_INSTANCES_RAW = '/LIMIP/databases/RAW_DATABASES/elm_interactions.tsv'
# PATH to ELM classes and motifs regex
ELM_CLASSES_RAW = '/LIMIP/databases/RAW_DATABASES/elm_classes.tsv'
# Path to Uniprot_data path
UNIPROT_DATA_RAW = '/LIMIP/databases/RAW_DATABASES/FULL_REVIEWED_UNIPROT.tab'
# Biogrid ID
BIOGRID_ID_RAW = '/LIMIP/databases/RAW_DATABASES/BIOGRID_ID.tab.zip'
# BioGrid interactions
BIOGRID_DATA_RAW = '/LIMIP/databases/RAW_DATABASES/BIOGRID_DATA.tab2.zip'
# Path of Pfam_data file
PFAM_DATA_RAW = '/LIMIP/databases/RAW_DATABASES/pdb_pfamA_reg.txt.gz'
# Directory where to generate results
OUTPUT_DIR = '/LIMIP/databases/USERS_RESULT/'
# ------------------------------------------------------------------
# --------------------- USED SOFTWARE APP PATH ---------------------
# ------------------------------------------------------------------

# IUpred data :  Must end with '/' and point IUpred_data dir
IUPRED_DATA = '/LIMIP/databases/iupred_data/'

# -----------------------------------------------------------
# ---------------- AVAILABLE HARDWARE -----------------------
# -----------------------------------------------------------
# Total usable cpu after launching main programm
TOTAL_CPU = 3


# -------------------------------------------------------------------
# --------------------- SCRIPT PARAMETERS ---------------------------
# -------------------------------------------------------------------
# Access to webserver_path data
WEBSERVER_PATH_CONFIG = '/LIMIP/cgi/webserver_path'
# Rscript Cluster path
CLUSTER_RSCRIPT_PATH = '/LIMIP/LiMIP/LiMIP_dev/DistMatrixCluster.R'
# Tmp dir
TMP_DIR = '/LIMIP/tmp/'
# Connexion file name
CONNEXION_FILE_BASENAME = 'ELM_MOTIF_CONNEXION_TABLE.tsv'

# Unfound analysis basename
UNFOUND_ANALYSIS_BASENAME = 'UNFOUND_ANALYSIS.csv'
# ELM_CLUSTERS BASNAME
ELM_CLUSTER_BASENAME = 'ELM_CLUSTERS.json'
# UNIPROT-PFAM CLUSTER BASNE
PFAM_CLUSTER_BASENAME = 'UNIPORTPFAM_CLUSTERS.json'
# BioGrid Extention Basename
BIO_EXTEND_BASENAME = 'BIOGRID_PARTNERS_EXTENTION.txt'
# BioGrid TSV SubDB generation Basename
BIO_SUBDB_BASENAME = 'BIOGRID_CONNEXION_TALBE.tsv'
BIO_SUBDB_JSON_BASENAME = 'BIOGRID_SUBDB_EXTRACTION.json'

#----------------------------------------------------------
# ------------- DATABSE UPDATES DIRECT LINKS --------------
#----------------------------------------------------------
# File containing all UniprotIds associated with respective PFamIDs
# HELP : https://www.uniprot.org/help/api_queries
# Carefull : ' ' must be changed to %20 or + to work
UNIPROT_DL = 'https://www.uniprot.org/uniprot/?query=reviewed:yes&format=tab&columns=id,database(PDB),database(Pfam),database(PDBsum),entry%20name,organism,protein%20names,genes,feature(DOMAIN%20EXTENT),sequence&sort=score'

# Files containing all ELM data requiered for the analysis
# Interaction domain are links between classes and Pfam
ELM_INTERACTION_DOMAIN_DL = 'http://elm.eu.org/infos/browse_elm_interactiondomains.tsv'
# List of classes and regex
ELM_CLASSES_DL = 'http://elm.eu.org/elms/elms_index.tsv'
# All experimentaly validated instances (experimentaly seen ELM class + Pfam)
ELM_EXP_INTERACTIONS_DL = 'http://elm.eu.org/interactions/as_tsv'

# BioGrid 
BIOGRID_DATA_DL = 'https://downloads.thebiogrid.org/Download/BioGRID/Release-Archive/BIOGRID-3.5.177/BIOGRID-ALL-3.5.177.tab2.zip'
BIOGRID_ID_DL = 'https://downloads.thebiogrid.org/Download/BioGRID/Release-Archive/BIOGRID-3.5.177/BIOGRID-IDENTIFIERS-3.5.177.tab.zip'

# PFam
# File containing all PDBIds associated with respective PFamID
PFAM_PDB_DL = 'ftp://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/database_files/pdb_pfamA_reg.txt.gz'
