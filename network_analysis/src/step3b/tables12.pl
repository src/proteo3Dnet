#! /usr/bin/env perl

use strict;
use warnings;

my @mslist = readfile($ARGV[0]);
my @annotations = readfile($ARGV[1]);
my $output_file1 = "TABLE1.tsv";
my $output_file2 = "TABLE2.tsv";
my $max_all_templates = 30;
my $max_title = 80;

open (OUT1, '>', $output_file1) or die "Error: cannot write $output_file1\n\t$!\n";
print OUT1 "#Input\tUniProt_name\tTemplate\tOrganism\tSeq id (%)\tSequence_coverage\tProtein_structure_title\tMethod\tResolution\tOther_templates\n";
open (OUT2, '>', $output_file2) or die "Error: cannot write $output_file2\n\t$!\n";
print OUT2 "#Input\tUniProt_name\tTemplate\tProtein_structure_title\tStoichiometry\tSeq id (%)\tSequence_coverage\tOrganism\n";

my %hash;
my %hash2; # HOMOMULTIMERS
my %hash_all_templates;

foreach my $msline (@mslist){
    chomp $msline;
    my ($ac, $id) = split("\t", $msline);

    unless (exists $hash{$ac}){
        $hash2{$ac} = [];
        $hash{$ac} = [];
        $hash_all_templates{$ac} = [];
    }

    foreach my $line2 (@annotations){
        chomp $line2;
        my ($uniprot, $pdb6char, $proba, $id_percent, $resol1, $coverage, $species, $title, $method, $resol2, $stochio) = split("\t", $line2);
        if (length $title > $max_title){
            $title = substr($title, 0, $max_title);
            $title.="...";
        }
        if ($ac eq $uniprot or $id eq $uniprot){
            my $newline = "$id\t$pdb6char\t$species\t$id_percent\t$coverage\t$title\t$method\t$resol2";
            push(@{$hash2{$ac}}, [$pdb6char, $stochio, $id_percent, $species, $coverage, $title]);
            push(@{$hash{$ac}}, [$newline, $id_percent]);
            push(@{$hash_all_templates{$ac}}, $pdb6char);
        }
    }
}

my $input_number = 1;
foreach my $msline (@mslist){
    my ($ac, $id) = split("\t", $msline);
    foreach my $key (keys %hash){
        if ($key eq $ac){
            my @array = @{$hash{$key}};
            my @sorted_array = sort { $b->[1] <=> $a->[1] } @array;
            if ($sorted_array[0][0]){
                my @all_templates = @{$hash_all_templates{$key}};
                my $number_of_other_templates = $#all_templates + 1;
                my $all_templates_str = join(" ", @all_templates);
                if (length $all_templates_str > $max_all_templates){
                    $all_templates_str = substr($all_templates_str, 0, $max_all_templates);
                    $all_templates_str.="...";
                }
                $all_templates_str = "(n=$number_of_other_templates) $all_templates_str";
                print OUT1 "$input_number\t$sorted_array[0][0]\t$all_templates_str\n";
            }
            else{
                print OUT1 "$input_number\t$id\t-\t-\t-\t-\t-\t-\t-\t-\n";
            }
        }
    }
    $input_number++;
}

$input_number = 1;
foreach my $msline (@mslist){
    my ($ac, $id) = split("\t", $msline);
    foreach my $key (keys %hash2){
        if ($key eq $ac){
            my @array = @{$hash2{$key}};
            my @sorted_array = sort { $b->[2] <=> $a->[2] } @array;

            my $homomultimer_found = 0;
            my $index = 0;
            for (my $i=0; $i<=$#sorted_array; $i++){
                if ($sorted_array[$i][1] =~ m/Homo /){
                    $homomultimer_found = 1;
                    $index = $i;
                    last;
                }
            }

            if ($homomultimer_found){
                my ($pdb4char, $chain) = split('_', $sorted_array[$index][0]);
                my $homomultimer_state = $sorted_array[$index][1];
                my $seq_identity = $sorted_array[$index][2];
                my $species = $sorted_array[$index][3];
                my $coverage = $sorted_array[$index][4];
                my $title = $sorted_array[$index][5];
                $homomultimer_state =~ s/> /(/;
                $homomultimer_state.=")";
                print OUT2 "$input_number\t$id\t$pdb4char\t$title\t$homomultimer_state\t$seq_identity\t$coverage\t$species\n";
            }
            else{
                print OUT2 "$input_number\t$id\t-\t-\t-\t-\t-\t-\n";
            }
        }
    }
    $input_number++;
}

close OUT1;
close OUT2;

sub readfile{
    my $filename = shift;
    open(IN, '<', $filename) or die "\e[1;31mError\e[0m while opening $filename:\n\t$!\n";
    my @file = <IN>;
    close IN;
    return @file;
}

