FROM ubuntu:xenial
MAINTAINER Guillaume Postic <guillaume.postic@univ-paris-diderot.fr>

RUN apt-get -qy update && \
apt-get -qy install \
  r-base
#  python3

# Notes:
# - python3 only installs Python 3.5, but Python 2.7 is installed through r-base
# - python3 is not required for building the image, but it is for running "findTemplate_ms2m.py"
# - software-properties-common is for adding Oracle JRE repository
# - ruby-full is because some MAFFT scripts uses Ruby

# Set timezone
ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Useful tools when running Docker in interactive mode
  # Removing vim-tiny is Ubuntu-specific:
RUN apt-get remove -qy vim-tiny && \
apt-get -qy install \
  wget \
  vim \
  htop \
  screen \
  bash-completion

# Extend bash history
RUN sed -i 's/HISTSIZE\=1000/HISTSIZE\=1000000/' /root/.bashrc && sed -i 's/HISTFILESIZE\=2000/HISTFILESIZE\=2000000/' /root/.bashrc
# Modify .bashrc for (improved) autocompletion
RUN sed -i '/^#.*bash_completion/s/^#//' /root/.bashrc && sed -i '$ s/^#//' /root/.bashrc
# Change the default shell in screen to bash
RUN echo "shell \"/bin/bash\"" > /root/.screenrc

# Vim: default syntax highlighting + highlight search
RUN echo "colorscheme default" > /root/.vimrc
RUN echo "set hlsearch" >> /root/.vimrc

################################################################################

# Data are copied to home
COPY src/step*.pl /home/
COPY src/dsv2datatables_m2m.pl /home/
COPY src/step3b /home/step3b
COPY src/step4 /home/step4
COPY src/step5 /home/step5
COPY data/ /home/databases

###############################################################################

# Cleanup

RUN apt-get autoremove -qy && \
apt-get clean && \
rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
