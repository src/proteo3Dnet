# Step-by-step guidelines

Here are described the format of (i) the input files for **_Proteo3Dnet_** and (ii) the generated output files.  
An example command is provided, along with the results it produces (downloadable archive below).  
For the installation of Proteo3Dnet, please refer to the [README.md](https://gitlab.rpbs.univ-paris-diderot.fr/src/proteo3Dnet/blob/master/README.md) file.



Input
-----

The input `-i INFILE` for Proteo3Dnet can consist in either
- a list of UniProt identifiers, one per line:
  - either UniProtAC (e.g. O15350)
  - or UniProtID (e.g. P73_HUMAN)
- multiple FASTA sequences in which, for each sequence, the first line starts with a ">", followed by a valid UniProt identifier. For example, it can be:
  - ">P73_HUMAN"
  - or ">O15350"
  - or ">sp|O15350|P73_HUMAN ..."

**Valid file extensions are: .txt, .dat, .data, .fasta, .fas, .fa, .faa, .mpfa, .fst, or no file extension.**

`-s SPECIES`: Filter the results based on the given organism name (UniProt format, e.g. HUMAN, DROME, YEAST, MOUSE).  
`-f FOLD_CHANGE`: List (column) of enrichment values, one for each input protein. :warning: [work in progress]  
###### Note: although taken as input by the current version, the fold change values will have no impact on the results produced.



Example command
---------------
The following command runs the Proteo3Dnet analysis on the 19 significant candidate partners from the Zip2 interactome ([DOI: 10.1101/gad.308510.117](https://doi.org/10.1101/gad.308510.117)).
```
./wrapper_proteo3Dnet.py -i example/zip2.dat -o Zip2OutputDir -f example/zip2_enrichment.dat
```
**The results produced by the above command can be downloaded [here](https://gitlab.rpbs.univ-paris-diderot.fr/src/proteo3Dnet/blob/master/example/Zip2OutputDir.zip).**



Output
------

The Proteo3Dnet tool generates 5 files:
- `3Dcomplexes.tsv`
- `graph.json`
- `homologs.tsv`
- `homo-oligomers.tsv`
- `SLiMs_analysis.txt`

**The content of each column in these files is described below.**



### File: 3Dcomplexes.tsv

This tab-separated values (TSV) file reports the detection of protein complexes gathering candidates from the input flat list.  
It has two types of headers: one for the complex itself, and then one for each multimeric structure gathering (strictly or by homology) the candidate partners.

#### Columns
1. **Complex ID**: Identifier given to the complex (e.g. c001, c002, etc.).
2. **Max avg id%**: The maximum sequence identity among the multimeric PDB structures found.
3. **Inputs found**: The number of the input proteins from the flat list gathered in the complex.
4. **Structures found**: The number of multimeric PDB files found.
5. **Max completeness**: The maximum completeness value (see below) among the multimeric PDB structures found.
6. **Max undetected**: The maximum number of undetected partners (see below) among the multimeric PDB structures found.
7. **Parent complexes**: IDs of complexes that entirely contain this one.
8. **Child complexes**: IDs of complexes that are entirely contained into this one.


For each complex, the PDB files found are listed under the second header.

#### Columns
1. **Template**: The PDB code (4 characters) of the multimeric template.
2. **Organism ID**: The name of the organism.
3. **Avg id**: The sequence identity (%) between the input protein and the template, averaged over all chains of the multimeric structure.
4. **Completeness**: The ratio (%) between the number of input proteins gathered and the number of chains in the template structure.
5. **Proteins found:chain (seq id%)**: The detailed sequence identity for each "input protein vs template chain" pair.
6. **Undetected partners**: The protein chains of the template structure that have no match within the input flat list of proteins.
7. **Protein structure title**: The title of the PDB structure.
8. **Method**: The experimental method used to determine the structure (X-ray, NMR, cryo-EM).
9. **Resolution**: The structural resolution (in Å).
10. **Stoichiometry**: The stoichiometry of the multimeric template structure.
11. **Number of chains**: The number of chains of the multimeric template structure.
12. **Names of chains**: The names of the chains of the multimeric template structure.



### File: graph.json

This JSON-formatted file provides a graph description of the PPI network that is compatible with Cytoscape.js (https://js.cytoscape.org/).



### File: homologs.tsv

This tab-separated values (TSV) file reports the results of the search for remote homologies.

#### Columns
1. **Input**: The number of the input protein as in the flat list.
2. **UniProtID**: The UniProt entry name (https://www.uniprot.org/help/entry_name).
3. **UniProtAC**: The UniProt accession (https://www.uniprot.org/help/accession_numbers).
4. **3D complexes**: The name of the complexes (e.g. c001; see above) where the input protein is found.
5. **Best template**: The protein chain that is ranked first in the homology-based detection of template structures.
6. **Organism**: The organism of the best template.
7. **Seq id (%)**: The sequence identity of the best template.
8. **Seq coverage**: The sequence coverage of the best template.
9. **Structure title**: The title of the PDB structure.
10. **Method**: The experimental method used to determine the structure (X-ray, NMR, cryo-EM).
11. **Resolution**: The structural resolution (in Å).
12. **Other templates**: Alternative templates, i.e. ranked lower in the homology search.



### File: homo-oligomers.tsv

This tab-separated values (TSV) file reports the detection of homo-oligomeric states.

#### Columns
1. **Input**: The number of the input protein as in the flat list.
2. **UniProtID**: The UniProt entry name (https://www.uniprot.org/help/entry_name).
3. **UniProtAC**: The UniProt accession (https://www.uniprot.org/help/accession_numbers).
4. **Oligomeric template**: The best homo-oligomeric template structure in the homology search.
5. **Stoichiometry**: The stoichiometry of the best homo-oligomeric template structure.
6. **Structure title**: The title of the PDB structure.
7. **Seq id (%)**: The sequence identity of the best homo-oligomeric template.
8. **Seq coverage**: The sequence coverage of the best homo-oligomeric template.
9. **Organism**: The organism of the template structure.



### File: SLiMs_analysis.txt

This space-separated text file reports the results of the [ELM](http://elm.eu.org/)-based analysis, which also integrates results from [BioGRID](https://thebiogrid.org/).

#### Columns
1. **ELM_CLASS**: The identifier of the ELM class (e.g. LIG_HP1_1).
2. **MATCHED_MOTIF**: The part of the input protein sequence that matches the class RegEx.
3. **MOTIF_UNICODE**: The UniProtAC of the input protein that has the ELM motif.
4. **MOTIF_STARTS**: The start position of the motif on the protein sequence.
5. **MOTIF_ENDS**: The end position of the motif on the protein sequence.
6. **ASSOCIATED_PFAM**: The Pfam domain (e.g. PF01393) associated with the above matched motif.
7. **MATCHED_PFAM_UNICODE**: The UniProtAC of the input protein that has the Pfam domain.
8. **EXPERIMENTAL_EVIDENCE**: (Boolean value) experimental evidence of the SLiM-based interaction, as documented in ELM.
9. **ELM_PROBA**: The "Pattern Probability" of the motif, as documented in ELM.
10. **STRICT_DISORDER**: (Boolean value) is the motif located within an intrinsically disordered regions, as predicted by [IUPred]( https://doi.org/10.1002/pro.3334) ? (yes=1; no=0)
11. **SHORT_IUPRED_AVG_SCORE**: IUPred score for the short disordered segments.
12. **LONG_IUPRED_AVG_SCORE**: IUPred score for the long disordered segments.
13. **SHORT_IUPRED_GLOB_DOM**: IUPred score for the short structured domains.
14. **LONG_IUPRED_GLOB_DOM**: IUPred score for the long structured domains.
15. **ANCHOR2_AVG_SCORE**: ANCHOR2 score, as computed by IUPred, for predicting disordered binding regions.
16. **BIOGRID_INTERACTIONS**: Occurrences of this SLiM-based PPI in BioGRID, when excluding 'Far Western', 'Co-fractionation', 'Co-localization', 'Biochemical Activity', and 'High Throughput' experimental systems.
17. **BIOGRID_EXFILTERED**: Occurrences of this SLiM-based PPI in BioGRID, for all experimental systems.