import os
import sys
import json
import time
import LMIP_script_config

def get_biogrid_ID(target_uniprot, biogrid_ID):
    if target_uniprot in biogrid_ID.keys():
        return biogrid_ID[target_uniprot]
    return False

def reverse_biogrid_id(ID, biogrid_ID):
    try:
        return list(biogrid_ID.keys())[list(biogrid_ID.values()).index(ID)]
    except:
        return 'Unfound'

def gen_biogrid_tsv_subdb(uniprot_list, biogrid_data = False, biogrid_ID = False):
    # IMPORT databases
    if biogrid_ID == False:    
        with open(LMIP_script_config.BIOGRID_ID) as filin_id:
            biogrid_ID = json.load(filin_id)
            
    if biogrid_data == False:        
        with open(LMIP_script_config.BIOGRID_DATA) as filin_data:
            biogrid_data = json.load(filin_data)
            
    # Set returned tsv var
    tsv = []
    
    # Loop over entries
    for uniprot in uniprot_list:
        # Retrieve BioGrid ID
        bid = get_biogrid_ID(uniprot, biogrid_ID)
        if bid != False:
            # Loop over Interactors
            for iter_index, interactorB in enumerate(biogrid_data[bid]['InteractorB']):
                # Set empty row
                tsv_row = [uniprot, reverse_biogrid_id(interactorB, biogrid_ID)]
                # Set empty exp_data mapping
                exp_data_map = []
              # Low/Verified Experimental data > 0
                if biogrid_data[bid]['Low'][iter_index] > 0:
                    for pmid in biogrid_data[bid]['PMID_Low'][iter_index].keys():
                        exp_data_map.append('{0}:{1}'.format(pmid, biogrid_data[bid]['PMID_Low'][iter_index][pmid]))
                tsv_row.append(';'.join(exp_data_map))
                # Add data to tsv
                tsv.append('\t'.join(tsv_row))
                
    return '\n'.join(tsv)+'\n'
                    

def extend_results(connexion_table, uniprot_list, biogrid_data = False, biogrid_ID = False, uniprot_names = False, uniprot_data = False):

# IMPORT databases
    if biogrid_ID == False:    
        with open(LMIP_script_config.BIOGRID_ID) as filin_id:
            biogrid_ID = json.load(filin_id)
            
    if biogrid_data == False:        
        with open(LMIP_script_config.BIOGRID_DATA) as filin_data:
            biogrid_data = json.load(filin_data)
       
    if uniprot_names == False:
        with open(LMIP_script_config.UNIPROT_NAMES) as filin:
            uniprot_names = json.load(filin)  

    if uniprot_data == False:
        with open(LMIP_script_config.UNIPROT_DATA) as filin:
            uniprot_data = json.load(filin)
            
            
# Import connexion file
    with open(connexion_table, 'r') as filin:
        full_file = filin.readlines()
    
       
# Import wished CONNEXIONs from connexion table
    elm_pfam_match = []
    for i, row in enumerate(full_file):
        # Get header
        if i == 0:
            headers = row.strip().split('\t')
            
            # Get header index
            try:
                for index, head in enumerate(headers):
                    if head == 'EXPERIMENTAL_EVIDENCE':
                        ELM_VALID_INDEX = index
                    elif head == 'ELM_PROBA':
                        ELM_PROBA_INDEX = index
                    elif head == 'SHORT_IUPRED_AVG_SCORE':
                        SAVG_INDEX = index
                    elif head == 'LONG_IUPRED_AVG_SCORE':
                        LAVG_INDEX = index
                    elif head == 'SHORT_IUPRED_GLOB_DOM':
                        SDOM_AVG_INDEX = index
                    elif head == 'LONG_IUPRED_GLOB_DOM':
                        LDOM_AVG_INDEX = index
                    elif head == 'ANCHOR2_AVG_SCORE':
                        ANCHOR_INDEX = index
                    elif head == 'BIOGRID_INTERACTIONS':
                        bg_low_index = index
                    elif head == 'BIOGRID_EXFILTERED':
                        bg_high_index = index
                    elif head == 'STRICT_DISORDER':
                        STRICT_DIS_INDEX = index
                    elif head == 'ELM_CLASS':
                        elm_class_index = index
                    elif head == 'ASSOCIATED_PFAM':
                        match_pfam_index = index
                    elif head == 'MATCHED_PFAM_UNICODE':
                        match_pfam_unicode_index = index
                    elif head == 'MOTIF_UNICODE':
                        elm_motif_unicode_index = index
                    elif head == 'MOTIF_STARTS':
                        elm_motif_starts = index                   
                    elif head == 'MOTIF_ENDS':
                        elm_motif_ends = index
                    elif head == 'MATCHED_MOTIF':
                        elm_motif_match_index = index    
                        
                error_flag = False           
                for index in [ELM_VALID_INDEX, ELM_PROBA_INDEX, SAVG_INDEX, LAVG_INDEX, SDOM_AVG_INDEX, LDOM_AVG_INDEX, ANCHOR_INDEX, bg_low_index, STRICT_DIS_INDEX, bg_high_index, elm_class_index, match_pfam_index, match_pfam_unicode_index, elm_motif_unicode_index, elm_motif_starts, elm_motif_ends, elm_motif_match_index]:
                    if type(index) != type(int()):
                        error_flag = True
                        break
                 
                if error_flag:       
                    print('not good') 
                        
            except Exception as err:
                print(Exception, err)
        
        # Get data    
        else:    
            act_row = row.strip().split('\t')
            elm_pfam_match.append(act_row)

    # Free memory
    del full_file
    
# Sort ELM-match by UniProt and Set UNICODE partner list
    elm_match_uniprot = {}
    
    # Loop over uniprot list, and set dic Keys and empty ELM values
    for uniprot in uniprot_list:
        if not uniprot in elm_match_uniprot.keys():
            elm_match_uniprot[uniprot] = {'ELM_PARTNERS':[], 'BIOGRID_PARTNERS':[], 'ELM_INTER_BIOGRID':[]}
    
    #uniprot_list = list(elm_match_uniprot.keys())
    # Look for BioGrid IDs!
    biogrid_list = []
    for uniprotcode in uniprot_list:
        biogrid_list.append(get_biogrid_ID(uniprotcode, biogrid_ID))
    
             
    # Loop over filtered match
    for match in elm_pfam_match:
        # Retrieve BioGrid ID
        motif_partner_biogrid_id = biogrid_list[uniprot_list.index(match[match_pfam_unicode_index])]
        # Add a partner if not already in    
        if not motif_partner_biogrid_id in elm_match_uniprot[match[elm_motif_unicode_index]]['ELM_PARTNERS']:
            elm_match_uniprot[match[elm_motif_unicode_index]]['ELM_PARTNERS'].append(motif_partner_biogrid_id)
            
       
    
    
    
    
        
# Loop over unicodes, and set list of missing_partners
    # Loop over unicodes
    for uniprot in uniprot_list:
        if biogrid_list[uniprot_list.index(uniprot)] == False:
            continue
        # Loop over BioGrid Partners
        uniprot_biogrid_data = biogrid_data[biogrid_list[uniprot_list.index(uniprot)]]
        for index in range(len(uniprot_biogrid_data['InteractorB'])):
            # If this partner is not in low throuput
            if uniprot_biogrid_data['Low'][index] == 0:
                continue
            
            # If  biogrid partner in list of ELM_match_partners
            if uniprot_biogrid_data['InteractorB'][index] in elm_match_uniprot[uniprot]['ELM_PARTNERS']:
                # Add if not already added
                if not uniprot_biogrid_data['InteractorB'][index] in elm_match_uniprot[uniprot]['ELM_INTER_BIOGRID']:
                    elm_match_uniprot[uniprot]['ELM_INTER_BIOGRID'].append(uniprot_biogrid_data['InteractorB'][index])
            
            # Add bioparneer to biogrid partners   
            elm_match_uniprot[uniprot]['BIOGRID_PARTNERS'].append(uniprot_biogrid_data['InteractorB'][index])
                    
                    
    output_biogrid_extention = []
    total_BioGrid_partners = []
    total_new_partners_only = []  
           
# Loop over Uniprot and set results ->  find Uniprot_ID and HF_Uniprot_name
    # Loop over uniprot
    for uniprot in uniprot_list:
        # Loop over possible BioGrid interactors with this uniprot
        for bio_partner in elm_match_uniprot[uniprot]['BIOGRID_PARTNERS']:
            # Add new interactor in All possible proteomic interactors
            if not bio_partner in total_BioGrid_partners:
                total_BioGrid_partners.append(bio_partner)
            # Add new interactor in new_patners_only
            if not bio_partner in elm_match_uniprot[uniprot]['ELM_INTER_BIOGRID'] and not bio_partner in total_new_partners_only:
                total_new_partners_only.append(bio_partner)
         
     # Mapp BioGrid names to Uniprot Names
        # Loop over lists
        for key in ['ELM_INTER_BIOGRID', 'BIOGRID_PARTNERS', 'ELM_PARTNERS']:
            # Gen Uniprot_map_key
            uniprot_map_key = key+'_UNIPROTMAP'
            # Set uniprot mapping list
            elm_match_uniprot[uniprot][uniprot_map_key] = []
            
            # Find name for each biogrid id
            for biog_id in elm_match_uniprot[uniprot][key]:
            # Look for uniprot correspondance
                uniprot_correspondance = reverse_biogrid_id(biog_id, biogrid_ID)
                elm_match_uniprot[uniprot][uniprot_map_key].append(uniprot_correspondance)
                 
        # Set new row
        new_row = '{0}:TOTAL_BIOGRID_PARTNERS(LOW)={1}:IDS={2}:TOTAL_ELM={3}:IDS={4}:ELM_INTER_BIOGRID={5}:IDS={6}'.format(uniprot, len(elm_match_uniprot[uniprot]['BIOGRID_PARTNERS_UNIPROTMAP']), ','.join(elm_match_uniprot[uniprot]['BIOGRID_PARTNERS_UNIPROTMAP']),
len(elm_match_uniprot[uniprot]['ELM_PARTNERS_UNIPROTMAP']), ','.join(elm_match_uniprot[uniprot]['ELM_PARTNERS_UNIPROTMAP']),
len(elm_match_uniprot[uniprot]['ELM_INTER_BIOGRID_UNIPROTMAP']), ','.join(elm_match_uniprot[uniprot]['ELM_INTER_BIOGRID_UNIPROTMAP']))
        output_biogrid_extention.append(new_row)
    
    
    output_biogrid_extention.insert(0, 'TOTAL POSSIBLE PARTNERS (BIOGRID) : {0}'.format(len(total_BioGrid_partners)))
    output_biogrid_extention.insert(1, 'TOTAL POSSIBLE NEW PARTNERS (BIOGRID - ELM) : {0}'.format(len(total_new_partners_only)))
    
    return '\n'.join(output_biogrid_extention) 
   
if __name__ == "__main__":
    # arg[1] == Connexion file path
    # arg[2] == list of uniprot
    with open(sys.argv[2], 'r') as filin:
        unfound_ana_header = filin.readline()
    uniprot_list = unfound_ana_header.strip().split(';')[1:]
    extend_results(sys.argv[1], uniprot_list)