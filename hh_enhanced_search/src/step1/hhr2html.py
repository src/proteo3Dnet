#!/usr/bin/env python

import re,os

class hhr2html:
    # genere un fichier html a partir d'un fichier hhr
    def __init__(self, file_in, db, label, path, fst_file, mode = "standard"):
        self.fileIn = file_in
        self.db = db
        self.label = label
        self.qlen = 0
        self.fileLine = (open(file_in)).readlines()
        self.res = []
        self.restxt = ""
        self.align=[]
        self.alitxt = ""
        self.path = path
        self.params = ""
        self.infoseqs={}
        self.pos = {}
        self.fst_file = fst_file
        
        self.CMD_SCRIPT = "/usr/bin/Rscript --vanilla "

        if mode == "pairwise_interevdock":
            self.mode = "pairwise_interevdock"
            self.dic_delim = {} # used to get the delimitations in the template
        elif mode == "standard":
            self.mode = "standard"
        

# premiere partie du fichier
    def set_res(self): 
        start = [i for i in range(len(self.fileLine)) if self.fileLine[i][0:7] == "Command"]
        end = [i for i in range(len(self.fileLine)) if self.fileLine[i][0:3] == "No "]
        self.res = self.fileLine[start[0]+2:end[0]-1]
        lines_fst = open(self.fst_file, "rt").readlines()
        fst_seq = ""
        for l in lines_fst:
            if l[0] == ">":
                continue
            else:
                fst_seq += l.strip()
        #self.qlen = int((re.search("^Match_columns (\d+)", self.fileLine[1])).group(1))
        self.qlen = len(fst_seq)
        self.params = [self.label, self.qlen, (re.search("^Neff          (\d+\.\d+)", self.fileLine[3]).group(1)),(open(self.fst_file)).readlines()[1][0:10]+"..."+(open(self.fst_file)).readlines()[1][self.qlen-10:-1]]


    def get_res(self):
        return self.res

    def addHtml2res(self):
        regex_nb = '^ *[0-9]+ '
        if self.mode == "pairwise_interevdock":
            # regex PDB adapted for upper case PDB codes
            regexPDB = re.compile("[0-9][A-Za-z0-9]{3}_[A-Za-z0-9]")
            regexPDBdelim = re.compile("(\d+)-(\d+)\s*\(\d+\)")
        else:
            regexPDB = re.compile("[0-9][a-z0-9]{3}_[A-Za-z0-9]")
        regexSCOP = re.compile("(d[0-9]([a-z]|[0-9]){4}(_|[0-9])) +([a-z]\.[0-9]+\.[0-9]+\.[0-9]+)")
        regexPFAMA = re.compile("PF[0-9]+")
        regexCKOG = re.compile("[CK]OG[0-9]+")
        regexSUPFAM = re.compile("SUPFAM[0-9]+")
        regexTIGRFAM = re.compile("TIGR[0-9]+")
        regexPANTHER = re.compile("PTHR[0-9]+")
        regexCD = re.compile("cd[0-9]+")
        regexPIRSF = re.compile("PIRSF[0-9]+")

        
        self.restxt += '     <div class="ct2">\n'
        self.restxt += '      <pre><span style="font-weight:bold;">'+self.res[0]+'</span>'
        for l in self.res[1:]:
            match_nb = re.search(regex_nb,l, re.M)
            match_PDB = regexPDB.search(l)
            if self.mode == "pairwise_interevdock":
                match_PDBdelim = regexPDBdelim.search(l)
            match_SCOP = regexSCOP.search(l)
            match_PFAMA = regexPFAMA.search(l)
            match_CKOG = regexCKOG.search(l)
            match_SUPFAM = regexSUPFAM.search(l)
            match_TIGRFAM = regexTIGRFAM.search(l)
            match_PANTHER = regexPANTHER.search(l)
            match_CD = regexCD.search(l)
            match_PIRSF = regexPIRSF.search(l)
            
            if match_nb:
                l = l.replace(match_nb.group(),'<a href="#No'+(match_nb.group()).strip()+'"><span style="font-weight:bold;">'+match_nb.group()+'</span></a>',1)
                latest_match_nb_detected = match_nb.group().strip()
            if match_PDB:
                id_PDB=match_PDB.group().split("_")
                
                if self.mode == "pairwise_interevdock":
                    # We get the delimitations in the matched template
                    if match_PDBdelim:
                        start,end = match_PDBdelim.group(1),match_PDBdelim.group(2)
                    else:
                        start,end = "1","10000"
                    self.dic_delim["%s"%(latest_match_nb_detected)] = [id_PDB[0].upper(),id_PDB[1],start,end]
                    
                l = l.replace(match_PDB.group(),'<a href=\'http://rcsb.org/pdb/explore.do?structureId='+id_PDB[0].upper()+'\' target="_blank"><span style="font-weight:bold;">'+match_PDB.group()+'</span></a>')
                self.restxt += l
            elif match_SCOP:
                id_SCOP = match_SCOP.group(1)[1:5]
                l = l.replace(match_SCOP.group(1),'<a href=\'http://www.ncbi.nlm.nih.gov/sites/entrez?SUBMIT=y&db=structure&orig_db=structure&term='+id_SCOP.upper()+'\' target="_blank"><span style="font-weight:bold;">'+match_SCOP.group(1)+'</span></a>')
                l = l.replace(match_SCOP.group(4),'<a href=\'http://scop.mrc-lmb.cam.ac.uk/scop/search.cgi?sid='+match_SCOP.group(1)+'&lev=fa\' target="_blank"><span style="font-weight:bold;">'+match_SCOP.group(4)+'</span></a>')
                self.restxt += l
            elif match_PFAMA:
                l = l.replace(match_PFAMA.group(),'<a href=\'http://pfam.sanger.ac.uk/family/'+match_PFAMA.group()+'\' target="_blank"><span style="font-weight:bold;">'+match_PFAMA.group()+'</span></a>')
                self.restxt += l
            elif match_CKOG:
                l = l.replace(match_CKOG.group(),'<a href=\'http://www.ncbi.nlm.nih.gov/Structure/cdd/cddsrv.cgi?uid='+match_CKOG.group()+'\' target="_blank"><span style="font-weight:bold;">'+match_CKOG.group()+'</span></a>')
                self.restxt += l
            elif match_SUPFAM:
                l = l.replace(match_SUPFAM.group(),'<a href=\'http://supfam.cs.bris.ac.uk/SUPERFAMILY/cgi-bin/model.cgi?model='+(match_SUPFAM.group()).split("SUPFAM")[1]+'\' target="_blank"><span style="font-weight:bold;">'+match_SUPFAM.group()+'</span></a>')
                self.restxt += l
            elif match_TIGRFAM:
                l = l.replace(match_TIGRFAM.group(),'<a href=\'http://cmr.jcvi.org/cgi-bin/CMR/HmmReport.cgi?hmm_acc='+match_TIGRFAM.group()+'\' target="_blank"><span style="font-weight:bold;">'+match_TIGRFAM.group()+'</span></a>',1)
                self.restxt += l
            elif match_PANTHER:
                l = l.replace(match_PANTHER.group(),'<a href=\'http://www.pantherdb.org/panther/family.do?clsAccession='+match_PANTHER.group()+'\' target="_blank"><span style="font-weight:bold;">'+match_PANTHER.group()+'</span></a>')
                self.restxt += l
            elif match_CD:
                l = l.replace(match_CD.group(),'<a href=\'http://www.ncbi.nlm.nih.gov/Structure/cdd/cddsrv.cgi?uid='+match_CD.group()+'\' target="_blank"><span style="font-weight:bold;">'+match_CD.group()+'</span></a>')
                self.restxt += l
            elif match_PIRSF:
                l = l.replace(match_PIRSF.group(),'<a href=\'http://pir.georgetown.edu/cgi-bin/ipcSF?id='+match_PIRSF.group()[3:]+'\' target="_blank"><span style="font-weight:bold;">'+match_PIRSF.group()+'</span></a>')
                self.restxt += l

            elif self.db == "UNIPROT20" or self.db == "NR20" or self.db == "SMART" or self.db == "CD" or self.db == "CATH":
                self.restxt += l
                
        self.restxt +='       </pre>\n'
        self.restxt += '     </div>\n'
        return self.restxt

# fin premiere partie

# seconde partie du fichier
    def set_align(self): 
        
        start = [i for i in range(len(self.fileLine)) if self.fileLine[i][0:3] == "No "]
        end = [i for i in range(len(self.fileLine)) if self.fileLine[i][0:5] == "Done!"]
        self.align = self.fileLine[start[0]:end[0]]
               
        cpt = 1
        nom=""
        for l in self.align:
            if self.mode == "pairwise_interevdock":
                match_prob_E = re.search("^>(.+?):AUTOPDB;\s*Probability=(.+?);\s*Identities=(.+?);\s*Resolution=(.+?);\s*MatchedRegionTemplate=(-?\d+-\d+);\s*(.+)",l)
            else:
                match_prob_E = re.search("^Probab=(.+)\s+E-value=(.+)\s+Score=.+",l)
            match_nom = re.search("^>(.+)",l)
            if match_nom:
                nom = match_nom.group(1)
            if match_prob_E:
                if self.mode == "pairwise_interevdock":
                    self.infoseqs[str(cpt)] = [match_prob_E.group(1),match_prob_E.group(2),match_prob_E.group(3),
                                               match_prob_E.group(4),match_prob_E.group(5),match_prob_E.group(6)]
                else:
                    self.infoseqs[str(cpt)] = [match_prob_E.group(1),match_prob_E.group(2),nom]

                cpt += 1

    def color_ss(self, chaine):
        
        if len(chaine) == 1:
            if chaine.upper() == "E":
                return '<span style="color:blue">'+chaine+'</span>'
            elif chaine.upper() == "H":
                return '<span style="color:red">'+chaine+'</span>'
            else:
                return '<span style="color:black">'+chaine+'</span>'
        
        cpt_e = ""
        cpt_h = ""
        cpt_c = ""
        new_chaine = ""
    
        ch = chaine[0]
        i = 1
        cpt = ch
        while i < len(chaine):
            while chaine[i].upper() == ch.upper():
                cpt += chaine[i]
                i += 1
                if i >= len(chaine):
                    break
            
            if len(cpt) > 0 and cpt[0].upper() == "E":
                cpt_e = cpt.replace(cpt, '<span style="color:blue">'+cpt+'</span>')
                new_chaine += cpt_e
                if i+1 <=  len(chaine):
                    ch = chaine[i]
                cpt = ""
            if len(cpt) > 0 and cpt[0].upper() == "H":
                cpt_h = cpt.replace(cpt, '<span style="color:red">'+cpt+'</span>')
                new_chaine += cpt_h
                if i+1 <= len(chaine):
                    ch = chaine[i]
                cpt = ""
            if len(cpt) > 0 and cpt[0].upper() == "C":
                cpt_c = cpt.replace(cpt, '<span style="color:black">'+cpt+'</span>')
                new_chaine += cpt_c
                if i+1 <= len(chaine):
                    ch = chaine[i]
                cpt = ""
            if len(cpt) > 0 and cpt[0].upper() != "C" and cpt[0].upper() != "E" and cpt[0].upper() != "H":
                cpt_c = cpt.replace(cpt, '<span style="color:black">'+cpt+'</span>')
                new_chaine += cpt_c
                if i+1 <= len(chaine):
                    ch = chaine[i]
                cpt = ""

        return new_chaine

    def color_seq(self, chaine):
        dico_aa = {"MILV" : "Lime", "RK" : "red", "H": "orange", "APGSET-" : "black",\
                   "DE" : "blue", "YFW" : "green", "QN" : "fuchsia", "C" : "saddlebrown"}
        new_chaine = ""
        for aa in chaine:
            for i in dico_aa.keys():
                if aa.upper() in i:
                    new_chaine += '<span style="color:'+dico_aa[i]+'">'+aa+'</span>'
                    break
        return new_chaine


    def addHtml2align(self):
        regexHit = re.compile("^No [0-9]+")
        if self.mode == "pairwise_interevdock":
            regexPDB = re.compile("[0-9][A-Za-z0-9]{3}_[A-Za-z]")
            regexPDBdelim = re.compile("MatchedRegionTemplate=(\d+)-(\d+)")
        else:
            regexPDB = re.compile("[0-9][a-z0-9]{3}_[A-Z]")
        regexSCOP = re.compile("(>d[0-9]([a-z]|[0-9]){4}(_|[0-9])) +([a-z]\.[0-9]+\.[0-9]+\.[0-9]+)")
        regexSS = re.compile("^[QT] (ss_pred|ss_dssp) +([A-Za-z\-]+)")
        regexSEQ = re.compile("^(Q "+self.label+"|T (.*)) +[0-9]+ +([A-Za-z\-]+)")
        regexPFAMA = re.compile(">(PF[0-9]+)")
        regexCKOG = re.compile(">([CK]OG[0-9]+)")
        regexSUPFAM = re.compile(">(SUPFAM[0-9]+)")
        regexTIGRFAM = re.compile(">(TIGR[0-9]+)")
        regexPANTHER = re.compile(">(PTHR[0-9]+)")
        regexCD = re.compile(">(cd[0-9]+)")
        regexPIRSF = re.compile(">(PIRSF[0-9]+)")
        regexMatchedRegion = re.compile("(-?\d+)-(-?\d+)")
        
        self.alitxt += '     <div class="ct2">\n'
        self.alitxt += '      <pre>'
        in_pairwise_ali_section = True
        for l in self.align:
            match_Hit = regexHit.search(l)
            match_PDB = regexPDB.search(l)
            match_SCOP = regexSCOP.search(l)
            match_ss = regexSS.search(l)
            match_SEQ = regexSEQ.search(l)
            match_PFAMA = regexPFAMA.search(l)
            match_CKOG = regexCKOG.search(l)
            match_SUPFAM = regexSUPFAM.search(l)
            match_TIGRFAM = regexTIGRFAM.search(l)
            match_PANTHER = regexPANTHER.search(l)
            match_CD = regexCD.search(l)
            match_PIRSF = regexPIRSF.search(l)

            if self.mode == "pairwise_interevdock":
                match_PDBdelim = regexPDBdelim.search(l)  
                if l[0:20] == "Done fasta alignment":
                     in_pairwise_ali_section = False
                     l = '<br><br><span style="font-weight:bold;color:black;">'\
                     + '############## END OF PAIRWISE ALI SECTION ##############'\
                     + '<br><br><br><br>'\
                     + '####### START OF PROFILES PAIR ALIGNMENTS SECTION #######</span><br><br>'

            if match_Hit:
                if self.mode == "pairwise_interevdock":
                    hit_number = match_Hit.group().split()[1]
                    PDBcode = self.dic_delim[hit_number][0]
                    PDBchain = self.dic_delim[hit_number][1]
                    self.infoseqs[hit_number][4]
                    matchedregion = regexMatchedRegion.search(self.infoseqs[hit_number][4])
                    start = matchedregion.group(1)
                    end = matchedregion.group(2)
                    #start = self.infoseqs[hit_number][4].split('-')[0]
                    #end = self.infoseqs[hit_number][4].split('-')[1]
                    href_link_to_3D = 'href=\'http://3dmol.csb.pitt.edu/viewer.html?pdb=%s' %(PDBcode)\
                                  + '&select=all&style=cartoon'\
                                  + '&select=chain:%s&style=cartoon:color~pink'%(PDBchain)\
                                  + '&select=resi:%s-%s;chain:%s&style=cartoon:colorscheme~redCarbon'%(start,end,PDBchain)
                    
                    if in_pairwise_ali_section:
                        # We are in the pairwise fasta section 
                        Help_message = '<br><span style="font-weight:bold;font-style:italic;color:orange;">Copy & paste the pair ali below into InterEvDock2 (back to form and paste in the Advanced mode \'Protein x sequence template alignment\' box)</span>'
                        Navigate_pair_vs_hhr = '<a href="#%shhr"><span style="font-style:italic;">Check the detailed profile-profile alignment</span></a>'%(match_Hit.group()).replace(' ','')
                        Html_tag     = (match_Hit.group()).replace(' ','')
                    else:
                        # We are in profile-profile detailed section
                        Help_message = ''
                        Navigate_pair_vs_hhr = '<a href="#%s"><span style="font-style:italic;">Move to pairwise alignment</span></a>'%(match_Hit.group()).replace(' ','')
                        Html_tag     = (match_Hit.group()).replace(' ','')+"hhr"
                        
                        
                    l = l.replace(match_Hit.group(),'<br><a name="%s"><span style="font-weight:bold;color:red;">##### %s #####</span></a>'%(Html_tag,match_Hit.group())\
                                  + '<br><a href="#top"><span style="font-style:italic;">Back to top of page</a> '\
                                  + '<br>%s'%(Navigate_pair_vs_hhr)\
                                  + '<br><a %s \' target="_blank"><span style="font-style:italic;">View 3D template structure (matched region->red, rest of chain->pink, other chains->grey)</span></a>' %(href_link_to_3D)\
                                  + Help_message\
                                  )
                else:
                    l = l.replace(match_Hit.group(),'<a name="'+(match_Hit.group()).replace(' ','')+'"><span style="font-weight:bold;color:black;">'+match_Hit.group()+'</span></a>\n      </pre>\n      <p>')
            if match_PDB:
                id_PDB=match_PDB.group().split("_")
                l = l.replace(match_PDB.group(),'<a href=\'http://rcsb.org/pdb/explore.do?structureId='+id_PDB[0].upper()+'\' target="_blank"><span style="font-weight:bold;">'+match_PDB.group()+'</span></a>')
            elif match_SCOP:
                id_SCOP = match_SCOP.group(1)[2:6]
                l = l.replace(match_SCOP.group(1),'><a href=\'http://www.ncbi.nlm.nih.gov/sites/entrez?SUBMIT=y&db=structure&orig_db=structure&term='+id_SCOP.upper()+'\' target="_blank"><span style="font-weight:bold;"'+match_SCOP.group(1)+'</span></a>')
                l = l.replace(match_SCOP.group(4),'<a href=\'http://scop.mrc-lmb.cam.ac.uk/scop/search.cgi?sid='+match_SCOP.group(1)[1:]+'&lev=fa\' target="_blank"><span style="font-weight:bold;">'+match_SCOP.group(4)+'</span></a>')
            elif match_PFAMA:
                l = l.replace(match_PFAMA.group(1),'<a href=\'http://pfam.sanger.ac.uk/family/'+match_PFAMA.group(1)+'\' target="_blank"><span style="font-weight:bold;">'+match_PFAMA.group(1)+'</span></a>')
            elif match_CKOG:
                l = l.replace(match_CKOG.group(1),'<a href=\'http://www.ncbi.nlm.nih.gov/Structure/cdd/cddsrv.cgi?uid='+match_CKOG.group(1)+'\' target="_blank"><span style="font-weight:bold;">'+match_CKOG.group(1)+'</span></a>')
            elif match_SUPFAM:
                l = l.replace(match_SUPFAM.group(1),'<a href=\'http://supfam.cs.bris.ac.uk/SUPERFAMILY/cgi-bin/model.cgi?model='+(match_SUPFAM.group(1)).split("SUPFAM")[1]+'\' target="_blank"><span style="font-weight:bold;">'+match_SUPFAM.group(1)+'</span></a>')
            elif match_TIGRFAM:
                l = l.replace(match_TIGRFAM.group(1),'<a href=\'http://cmr.jcvi.org/cgi-bin/CMR/HmmReport.cgi?hmm_acc='+match_TIGRFAM.group(1)+'\' target="_blank"><span style="font-weight:bold;">'+match_TIGRFAM.group(1)+'</span></a>',1)
            elif match_PANTHER:
                l = l.replace(match_PANTHER.group(1),'<a href=\'http://www.pantherdb.org/panther/family.do?clsAccession='+match_PANTHER.group(1)+'\' target="_blank"><span style="font-weight:bold;">'+match_PANTHER.group(1)+'</span></a>')
            elif match_CD:
                l = l.replace(match_CD.group(1),'<a href=\'http://www.ncbi.nlm.nih.gov/Structure/cdd/cddsrv.cgi?uid='+match_CD.group(1)+'\' target="_blank"><span style="font-weight:bold;">'+match_CD.group(1)+'</span></a>')
            elif match_PIRSF:
                l = l.replace(match_PIRSF.group(1),'<a href=\'http://pir.georgetown.edu/cgi-bin/ipcSF?id='+match_PIRSF.group(1)[3:]+'\' target="_blank"><span style="font-weight:bold;">'+match_PIRSF.group(1)+'</span></a>')
            
            
            if match_ss:
                l = l.replace(match_ss.group(2),self.color_ss(match_ss.group(2)))
            if match_SEQ and not re.search("Consensus",l):
                l = l.replace(match_SEQ.group(3),self.color_seq(match_SEQ.group(3)))

            if (re.compile("^>")).search(l):
                l = l.replace('|','|<wbr>')
                if self.mode == "pairwise_interevdock":
                    l = '><span style="font-weight:bold;color:black;">'+l[1:-1]+'</span>\n'
                else:
                    l = l.replace('<pre>','<p>')
                    l = l.replace('</pre>','</p>')
                    l += '      </p>\n      <pre>'
            self.alitxt += l
            #if match_nom:
                #nom = match_nom.group(1)
        self.alitxt += '      </pre>\n'
        self.alitxt += '     </div>\n'
        return self.alitxt

    # fin seconde partie

    # Contenu html
    def header(self):
        head = '\
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n\
 <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >\n\
  <head>\n\n\
  <title> Alignment results</title>\n\n\
   <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15" />\n\n\
   <style type="text/css">\n\
     <!-- \n\
         a {\n\
            color:#1874C4;\n\
            text-decoration:none;\n\
           }\n\
         a:hover {\n\
            color:#874C4;\n\
            text-decoration:none;\n\
                 }\n\
         #container {\n\
            width: 80%;\n\
            padding: 20px;\n\
            background: #ffffff;\n\
                    }\n\
         .ct1 {\n\
            position:absolute;\n\
            width: 70%;\n\
            padding-left: 20px;\n\
            padding-top: 30px;\n\
            margin: 5 px;\n\
            font-weight: bold;\n\
         .ct2 {\n\
            position:relative;\n\
            width: 70%;\n\
            padding: 20px;\n\
            margin: 5 px;\n\
            background: #ffffff;\n\
         }\n\
         pre, p { \n\
            margin:0;\n\
            padding:0;\n\
           }\n\
     --> \n\
   </style>\n\n\
  </head>\n\n\
   <body style="font-family:monospace;font-size:11px;line-height:18px;">\n\n\
    <div id="container">\n\
    <div class="ct1">\n\
    <a name="top"></a>\
    <h1> ALIGNMENT RESULTS FOR '+self.label+': </h1>\
    </div>\n' 
        return head

    def footer(self):
        return '\
    </div>\n\
   </body>\n\n\
 </html>'

# fin contenu html

    def get_html(self):
        return self.header()+self.img_html()+self.addHtml2res()+self.addHtml2align()+self.footer()

# image
    def create_image(self):
        #img_txt = open(self.label+".txt","wt")
        nameF = os.path.basename(self.fileIn)
        nameF = os.path.splitext(nameF)[0]
        img_txt = open(self.path + "/" +nameF+".txt","wt")
        img_txt.write("1,%s,0,1,%s,%s,%s,%s,%s\n"%(self.label,self.qlen,self.qlen,self.qlen,self.qlen,self.qlen))
        for i in self.res[1:]:
            #match = re.search("^\s*([0-9]+)\s+([A-Za-z0-9_]+\|?[A-Za-z0-9_]+).+\s([0-9]{,4}\.[0-9]{1})\s+[0-9E\.\-]+.*\s+[0-9E\.\-].*\s[0-9]+\.[0-9]+\s+.*[0-9]+\.[0-9]+\s+[0-9]+\s+([0-9]+\-[0-9]+)\s*.+\s([0-9]+\-[0-9]+)\s*\(([0-9]+)\)",i)
            match = re.search("^\s*([0-9]+)\s+([A-Za-z0-9_]+\|?[A-Za-z0-9_]+).+\s([0-9]{,4}\.[0-9]{1})\s+([0-9E\.\-]+.*)\s+([0-9E\.\-].*)\s[0-9]+\.[0-9]+\s+.*[0-9]+\.[0-9]+\s+[0-9]+\s+([0-9]+\-[0-9]+)\s+\s([0-9]+\-[0-9]+)\s*\(([0-9]+)\)",i)
            if match:
                img_txt.write("%d,%s: %s,%s,%s,%s,%s,%s,%1.1e,%1.1e\n"%(int(match.group(1))+1,match.group(1),match.group(2),match.group(3),(match.group(6)).split("-")[0],(match.group(6)).split("-")[1],(match.group(7)).split("-")[0],(match.group(7)).split("-")[1], float(match.group(4)), float(match.group(5))))
                self.pos[str(int(match.group(1))+1)] = [(match.group(6)).split("-")[0],(match.group(6)).split("-")[1]]
        img_txt.close()

        # In case we are running this script in InterEvDock2, we changed the parameters of the R script generating the png  
        # cmd = "%s %s/%s.txt %s/%s.png >> %s/%s.txt" %(Config.HHLIB,self.path,nameF,self.path,nameF,self.path,nameF)
        #Rfilepath = os.path.join(self.path,"genere_png.R")
        Rfilepath = os.path.join(self.path,"genere_png_"+nameF+".R") # GP2018 XXX
        self.generateRscript(Rfilepath)
        cmd = self.CMD_SCRIPT + Rfilepath + " %s/%s.txt %s/%s.png >> %s/%s.txt"%(self.path,nameF,self.path,nameF,self.path,nameF)
        os.system(cmd)
    
        
    
    def img_html(self):
        nameF = os.path.basename(self.fileIn)
        nameF = os.path.splitext(nameF)[0]
        print nameF
        print self.path
        fich_txt = open(self.path + "/" +nameF+".txt","rt").readlines()
        ordre = (fich_txt[-2][:-1].split(" "))
        pos = (fich_txt[-1][:-1].split(" "))

        map = '     <div>\n      <map name="hhmap">\n'
        x1 = int(1.0*(748.0/self.qlen))+ 24
        x2 = int(self.qlen*(748.0/self.qlen))+ 27
        y1 = 92
        y2 = y1 + 12
        map += '<area shape="rect" coords="%d,%d,%d,%d" href="#query" title="Query=%s" />\n' %(x1,y1,x2,y2,self.label)
        y = {}
        for i in ordre:
            if y == {}:
                y[str(i)] = y1 + 16
            if y != {} and i not in y.keys():
                y[i] = y[str(int(i)-1)] + 15

        sPosKeys = self.pos.keys()
        sPosKeys.sort()
        
        # BUG REPORT: RG(CEA) had to correct Geraldine's script. There was a bug in the way indexes were used
        index_for_match=[]
        for i in range(2,len(pos)+2):
            index_for_match.append(pos.index(str(i)))

        for i in range(len(pos)):
            x1 = int(float(self.pos[str(pos[index_for_match[i]])][0])*(748.0/self.qlen))+ 24
            x2 = int(float(self.pos[str(pos[index_for_match[i]])][1])*(748.0/self.qlen))+ 27
            y1 = y[ordre[index_for_match[i]]]
            y2 = y1 + 11
            if self.mode == "pairwise_interevdock":
                lmap = [jj for jj in self.infoseqs[str(i+1)]]
                map += '<area shape="rect" coords="%d,%d,%d,%d" href="#No%d" title="%s; Prob=%s Id=%s Resol=%s Delim=%s; %s" />\n' %(x1,y1,x2,y2,i+1,lmap[0],lmap[1],lmap[2],lmap[3],lmap[4],lmap[5])
            else:
                map += '<area shape="rect" coords="%d,%d,%d,%d" href="#No%d" title="Prob=%s%% E=%s %s" />\n' %(x1,y1,x2,y2,i+1,self.infoseqs[str(i+1)][0],self.infoseqs[str(i+1)][1],self.infoseqs[str(i+1)][2])

        map += '      </map>\n      <p><img src="%s.png" border="0" alt="hhhits" usemap="#hhmap"></p>\n' %nameF
        #if self.hh == "HHB":
        map += '     </div>\n     <div class="ct2">\n     <pre>\n<a name="query"><span style="font-weight:bold;color:black;">Query </span></a>%s (seq=%s Len=%d Neff=%s)\n<b>Parameters </b>db=%s' %(self.label,self.params[-1], self.qlen, self.params[2],self.db)
        map += '</pre>\n'
        map += '     </div>\n\n'
        return map
    
    def write_html(self,filein):
        """
        """
        nHTML = os.path.basename(".".join(hhr_file.split(".")[:-1]))
        HTMLFILE = open(w+"/"+nHTML+".html", "wt")
        HTMLFILE.write(html_file.get_html())
        HTMLFILE.close()
        
    def generateRscript(self,Rfilepath):
        """write a copy of the R script 'genere_png.R' 
        """
        Rfile = open(Rfilepath,"wt")
        Rscript = """#!/usr/bin/env Rscript

#### FONCTIONS
f = function(vect){
   pos = -as.numeric(vect[1])   
   lab = as.character(vect[2])
   x1 = as.numeric(vect[4])
   y1 = as.numeric(vect[5])
   prob = as.numeric(vect[3])
   deb = as.numeric(vect[6])
   fin = as.numeric(vect[7])
   hh_seq = as.numeric(vect[8])
   pt_size=0.5
   if (y1<400 && y1>=100){
    pt_size=0.2
   }
   else if (y1<100 && y1>=60){
    pt_size=0.1
   }else if (y1<60){
   pt_size=0.05
   }

   if (pos == -1){
     points(seq(x1,y1,pt_size),rep(pos,length(seq(x1,y1,pt_size))),pch=21, bg="1", col="1", cex=1.2);
     text((y1+x1)/2,pos-0.02,label=toupper(lab),cex=0.6, font=2, col="white");
     text(x1, pos, label=x1, pos=3, cex=0.6,)
     text(y1, pos, label=y1, pos=3, cex=0.6,)
   }else{
     points(seq(x1,y1,pt_size),rep(pos,length(seq(x1,y1,pt_size))) ,pch=23, bg=f_col(prob),col=f_col(prob));
     if( fin == hh_seq) {
        points(y1,pos ,pch=21, bg=f_col(prob), col=f_col(prob));
     }
     if( deb == 1) {
        points(x1,pos ,pch=21, bg=f_col(prob), col=f_col(prob));
     }
        text((y1+x1)/2,pos-0.02,label=toupper(lab), col="white", cex=0.6, font=2);
   }
}

f_chev = function(elem1,elem2){
   elem1[1] = as.numeric(elem1[1])
   elem2[1] = as.numeric(elem2[1])
   elem1[4] = as.numeric(elem1[4])
   elem2[4] = as.numeric(elem2[4])
   elem1[5] = as.numeric(elem1[5])
   elem2[5] = as.numeric(elem2[5])
   if (elem1[1] < elem2[1]) return(c(elem2[1],1));
   if (elem1[4] > (elem2[5] + 3) ) return(c(elem2[1],0));
   if (elem1[5] < (elem2[4] - 3) ) return(c(elem2[1],0));
   return(c(elem1[1]+1,1));     
}

f_col = function(num){
  myPal = colorRampPalette( c("red","orange","green","darkblue") )
  mycol = myPal( 8 )
  if (num >= 90) return(mycol[1])
  else if (num >= 80) return(mycol[2])
  else if (num >= 70) return(mycol[3])
  else if (num >= 60) return(mycol[4])
  else if (num >= 50) return(mycol[5])
  else if (num >= 40) return(mycol[6])
  else if (num >= 20) return(mycol[7])
  else return(mycol[8])
}


#### MAIN
args <- commandArgs(TRUE)
t = read.table(args[1], sep=",")

t[,2]=as.character(t[,2])
t[,10]=t[,1]
t[2:length(t[,1]),1] = rep(t[2,1],length(t[,1])-1)
t_tmp2 = cbind(t, t[,5]-t[,4]+1)
colnames(t_tmp2) = c("V1","V2","V3","V4","V5","V6","V7","V8", "V9", "V10", "V11")
t_tmp3 = t_tmp2[2:length(t_tmp2[,1]),]
t = rbind(t_tmp2[1,],t_tmp3)

for ( i in 2:length(t[,1]) ){
  if ((i+1) < (length(t[,1])+1)){
    for ( j in (i+1):(length(t[,1])) ){
       res = f_chev(t[i,],t[j,])
       res = as.vector(unlist(res))
       t[j,1] = res[1]
    }
  }
}
t = t[order(t$V1),]

cpt = 1
i = 1
while( i <= length(t[,1])){
  if (i == 1) {
    t[i,1] = 1
  }else{
     if (t[i-1,1] == t[i,1]){
       t[i,1] = cpt
     }else{
       cpt = cpt + 1
       t[i,1] = cpt
    }
  }
  i = i + 1
}

my_mai=c(0.2,0,0.2,0)
if ( t[1,5]<20){
  my_mai=c(0.1,0,0.1,0)
}else if(t[1,5]<100 && max(t[,1]) >= 50 ){
  my_mai=c(0.45,0,0.45,0)
}else if ( max(t[,1]) >= 99){
  my_mai=c(0.05,0,0.05,0)
}else if (max(t[,1])>=97){
  my_mai=c(0.1,0,0.1,0)
}

my_asp = 0.02026*t[1,5]+0.01641
png(args[2],width=800,height=(10+max(t[,1]))*15,pointsize=20)
par(mai=my_mai)
plot(c(t[1,3],t[1,5]),c(1,-max(t[,1])),type="n",axe="F",xlab=NA,ylab=NA,asp=my_asp)
apply(t,1, f)
dev.off()

cat(t[2:length(t[,1]),1], file = args[1], append="T")
cat("\\n", file = args[1], append="T")
cat(t[2:length(t[,1]),10], file = args[1], append="T")
cat("\\n", file = args[1], append="T")
"""
        Rfile.write(Rscript)
        Rfile.close()
        return
# fin image
if __name__ == "__main__":
    test = hhr2html("/home/guerois/tmp/test_hhsearch.hhr", "PDB","XXX", "/home/guerois/tmp","/home/guerois/tmp/2994632.fasta",mode="pairwise_interevdock")
    # args for hhr2html : file_in, db, label, path, fst_file)


    test.set_res()
    test.set_align()
    test.create_image()
    fout = open("/home/guerois/tmp/test_2994632.html","w")
    fout.write(test.get_html())
    fout.close()
    
