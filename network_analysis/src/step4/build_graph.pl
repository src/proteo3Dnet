#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;

my @global_html_header;
my @global_html_bottom;
my @global_js;
my @global_json;

sub main{
    my $table1_file = '';
    my $table3_file = '';
    my $job_title = '';
    my $fold_change_file = '';
    my $layout = '';
    my $max_undetected = 50;
    my $max_biogrid_nodes = 10;
    my $elm_table_file = '';
    my $biogrid_table_file = '';
    my $anchor_threshold = 1.0;
    my $conversion_table_file = '';
 
    my $help = (@ARGV) ? 0 : 1;

    GetOptions(
        'table1=s' => \$table1_file,
        'table3=s' => \$table3_file,
        'job_title=s' => \$job_title,
        'fold_change=s' => \$fold_change_file,
        'elm_table=s' => \$elm_table_file,
        'biogrid_table=s' => \$biogrid_table_file,
        'anchor_threshold=s' => \$anchor_threshold,
        'layout=s' => \$layout,
        'max_undetected=s' => \$max_undetected,
        'max_biogrid_nodes=s' => \$max_biogrid_nodes,
        'conversion_table=s' => \$conversion_table_file,
        'help' => \$help
    );
    helper() if ($help);

    check_arguments($layout, $table1_file, $table3_file, $job_title, $conversion_table_file);
 
    # DEFINE COUNTERS
    my $input_edges_counter = 0;
    my $missing_edges_counter = 0;
    my $structural_edges_counter = 0;
 
 
    # XXX UTILITE DE strength ??????? XXX
 
 
    # NOTE: This program takes TABLE1 as an input to get (i) the input list of proteins and (ii) the input organism
    my @table1 = readfile($table1_file);
    my @initial_list;
    my $input_species = ''; # NOTE: VERY IMPORTANT: code from the second half of the UniProtID FIXME
    shift(@table1); # remove header
    for (my $i=0; $i<=$#table1; $i++){
        my @cols = split("\t", $table1[$i]);
        my @A = split('_', $cols[1]);
        $input_species = $A[1];
        push(@initial_list, $A[0]);
    }


    # CHECK FOLD CHANGE DATA
    my @fold_change_table;
    if ($fold_change_file){
        @fold_change_table = readfile_strip($fold_change_file);
        if ($#fold_change_table > $#initial_list){
            print "Warning: more fold change values than UniProt codes: fold change not taken into account\n";
        }
        elsif ($#fold_change_table < $#initial_list){
            print "Warning: less fold change values than UniProt codes: fold change not taken into account\n";
        }
        else{
            for (my $i=0; $i<=$#fold_change_table; $i++){
                if ($fold_change_table[0] !~ m/^-?\d+(\.\d+)?$/){
                    my $line_num = $i+1;
                    print "Warning: invalid fold change value (at line $line_num: $fold_change_table[0]): fold change not taken into account\n";
                    @fold_change_table = ();
                    last;
                }
            }
        }
    }

    # IF FOLD CHANGE RANGE NOT OK, TRY TO TRANSFORM IT
    if ($#fold_change_table > -1){
        if (!range_ok(\@fold_change_table)){
            my @log2 = log_fold_change(\@fold_change_table, 2);
            my @loge = log_fold_change(\@fold_change_table, 2.71828);
            my @log10 = log_fold_change(\@fold_change_table, 10);

            if (range_ok(\@log2)){
                @fold_change_table = @log2;
            }
            elsif (range_ok(\@loge)){
                @fold_change_table = @loge;
            }
            elsif (range_ok(\@log10)){
                @fold_change_table = @log10;
            }
            else{
                print "Warning: invalid range of fold change values: fold change not taken into account\n";
                @fold_change_table = ();
            }
        }
    }

 
 
    
    my @arr_table3 = readfile($table3_file);
    my $linenum = 0;
    
    # Loading the $hash_complex_to_entries: key = complex name (c1, c2, ...) ; value = (reference to) array of input UniProt codes
    # Loading the $hash_complex_to_missings: key = complex name (c1, c2, ...) ; value = (reference to) array of missing UniProt codes
    # Loading the $hash_complex_to_pdbs: key = complex name (c1, c2, ...) ; value = (reference to) array of PDB codes
    my %hash_complex_to_entries;
    my %hash_pdbANDentry_to_identities;
    my %hash_complex_to_missings;
    my %hash_complex_to_pdbs;
    my %hash_pdb_to_missings;
    my @array_for_score_calculation;
    foreach my $line (@arr_table3){
        if ($line =~ m/^(c\d+)/){
            my $complexe_name = $1;
            $hash_complex_to_entries{$complexe_name} = [];
	    $hash_complex_to_pdbs{$complexe_name} = [];
            my $i = $linenum+1;
            my @arr_miss;
            while ($arr_table3[$i] !~ m/^c\d+/ and $i <= $#arr_table3){
                if ($arr_table3[$i] =~ m/^\d/){
                    my @A = split("\t", $arr_table3[$i]);

                    push(@{$hash_complex_to_pdbs{$complexe_name}}, $A[0]);

                    my $organism_id = $1 if ($A[1] =~ m/\((\w+)\)/);
                    my $avg_id = $A[2];
                    my $completeness = $A[3];

                    my @arr_chains_found = split(/\)/, $A[4]);
		    my $j = 0;
		    while ($j <= $#arr_chains_found){
			if ($arr_chains_found[$j] =~ m/(\w+):(\w+) \((\d+)/){
                            my $entry = $1;
                            my $chain = $2;
                            my $identity = $3;
                            $hash_pdbANDentry_to_identities{"$A[0]:$entry"} = $identity;
                            unless (grep(/^$entry$/, @{$hash_complex_to_entries{$complexe_name}})){ # NOTE: Important because the first line (occurence) is sufficient
                                if ($entry ne 'undefinedID'){
                                    push(@{$hash_complex_to_entries{$complexe_name}}, $entry);
                                }
                                else{
                                    print STDERR "\e[1;31mWARNING: $A[0]\_$chain UniProtID is undefinedID\e[0m\n";
                                }
                            }
			}
			$j++;
                    }

                    # IF OPTION FOR TREATING UNDETECTED PARTNERS IS ACTIVATED
                    my @arr_compuls = split(' ', $A[5]);
    
                    if ($arr_compuls[0] ne '-'){
                        foreach my $line (@arr_compuls){
                            if ($line =~ m/:(\w+)/){
                                my $missing_partner = $1;
                                unless ($missing_partner eq 'undefinedID'){
                                    if ($organism_id eq $input_species or $organism_id eq 'BOVIN'){
                                        unless (grep(/^$missing_partner$/, @arr_miss)){
                                            push(@arr_miss, $missing_partner);
                                        }
                                        unless (exists $hash_pdb_to_missings{$A[0]}){
                                            $hash_pdb_to_missings{$A[0]} = [];
                                        }
                                        push(@{$hash_pdb_to_missings{$A[0]}}, $missing_partner);
                                        push(@array_for_score_calculation, [$missing_partner, $avg_id, $completeness, $complexe_name]);
                                    }
                                }
                                else{
                                    print STDERR "\e[1;31mWARNING: $A[0]'s undetected = undefinedID\e[0m\n";
                                }
                            }
                        }
                    }
                }
                $i++;
            }
            $hash_complex_to_missings{$complexe_name} = [@arr_miss];
        }
        $linenum++;
    }


    # AND NOW: THE INVERSE!
    # Filling the $hash_entry_to_complexes: value = a UniProt entry; key = (reference to) array of complexes names (c1, c2, ...)
    my %hash_entry_to_complexes;
    foreach my $key (keys %hash_complex_to_entries){
        my @members = @{$hash_complex_to_entries{$key}};
        foreach my $member (@members){
            unless (exists $hash_entry_to_complexes{$member}){
                $hash_entry_to_complexes{$member} = [];
            }
	    push (@{$hash_entry_to_complexes{$member}}, $key);
	    #print "push (@ { hash_entry_to_complexes { $member } }, $key);\n";
        }
    }



 
    # DEFINE THE SIZES
    my $font_size;
    my $node_size;
    my $border_width_variation;
    my $repulsion = 10000; # rather than 1 million
    $font_size = '110px';
    $node_size = '200';
    $border_width_variation = '10';
    #$repulsion = 169731 * log($total_nodes); # So it is 1000000 for 362 nodes (totally arbitrary!)
    #$repulsion = sprintf("%.0f", $repulsion); # FIXME it has to be round e.g. 1000000
    my $cy_layout;
    if ($layout eq "cose"){
        $cy_layout = "                       layout: { name: 'cose', nodeRepulsion: function( node ){ return '$repulsion'; }, nodeOverlap: 10000, animate: false, numIter: 2000 },\n";
    }
    elsif ($layout eq "circle"){
        $cy_layout = "                       layout: { name: 'circle' },\n";
    }
    
    # DEFINE THE COLORS
    my $input_node_color = 'deepskyblue';
    my $missing_node_color = 'gray';
    my $missing_edge_color = 'gray';
    my $elm_edge_color = 'darkblue';
    my $biogrid_edge_color = 'chartreuse';
    my $negative_fold_change_color = 'crimson';
    my $biogrid_node_color = 'chartreuse';
 
 
    print_head($job_title);
    print_js_head($cy_layout, $font_size, $input_node_color, $missing_edge_color, $missing_node_color, $negative_fold_change_color, $elm_edge_color, $biogrid_node_color, $biogrid_edge_color);
 
 
#    # DEFINE COLORS FOR THE NEW EDGES (only between input nodes)
#    my $color_index = 0;
#    #foreach my $key (sort keys %hash_complex_to_entries){ # FOR EACH COMPLEX (c1, c2, ...)
#    foreach my $key ( sort { scalar(@{$hash_complex_to_entries{$b}}) <=> scalar(@{$hash_complex_to_entries{$a}}) } keys %hash_complex_to_entries ) {
#	color_edges($hash_complex_to_entries{$key}, $color_names[$color_index], $key);
#        $color_index++;
#        if ($color_index > $#color_names){
#            $color_index = 0;
#    	#die "\e[1;31mERROR: Please define more different colors\e[0m\n";
#        }
#    }

    push(@global_js, "                           {selector: '\.id95edge', style: {'line-color': 'blue', 'width': '40', 'opacity': '0.33'}},\n");
    push(@global_js, "                           {selector: '\.id80edge', style: {'line-color': 'green', 'width': '40', 'opacity': '0.33'}},\n");
    push(@global_js, "                           {selector: '\.id50edge', style: {'line-color': 'gold', 'width': '40', 'opacity': '0.33'}},\n");
    push(@global_js, "                           {selector: '\.id30edge', style: {'line-color': 'red', 'width': '40', 'opacity': '0.33'}},\n");
    push(@global_js, "                           {selector: '\.id0edge', style: {'line-color': 'black', 'width': '40', 'opacity': '0.33'}},\n");
    push(@global_js, "                           {selector: ':selected',style: {'background-color': 'magenta','line-color': 'magenta'}},\n"); # THIS HAS TO BE DEFINED LAST

    push(@global_js, "                       ],\n                       // END OF style: BLOCK\n\n");
    


    # DEFINE THE NODES
    push(@global_js,
    "                       elements: ");


    # XXX START JSON XXX
    
    push(@global_json,
    "                       {\n",
    "                           nodes: [\n");
    
    
    
    my %hash_created_nodes; # NOTE: In order to avoid creating two identical nodes
    
    # Step 1: Input nodes
    for(my $i = 0; $i <= $#initial_list; $i++){
        chomp $initial_list[$i];
        unless(exists $hash_created_nodes{$initial_list[$i]}){ # XXX This should not be necessary
            my $logfc_class = '';
            my $input_node_size = $node_size;
            if ($#fold_change_table > -1){
                $input_node_size = $fold_change_table[$i]*100;
                if ($input_node_size > 0){
                    $logfc_class = 'logfc_positives';
                }
                else{
                    $input_node_size*=-1;
                    $logfc_class = 'logfc_negatives';
                }
            }

	    # DEFINE CLASSES FOR EACH INPUT NODE: TO WHICH COMPLEX(ES) DOES IT BELONG?
	    my @matching_complexes;
	    if (exists $hash_entry_to_complexes{$initial_list[$i]}){
                @matching_complexes = sort @{$hash_entry_to_complexes{$initial_list[$i]}}; # NOTE: the sort is for reproductibility
            }

	    my $classes = "['input_nodes',";
            foreach my $matching_complex (@matching_complexes){
                $classes.="'$matching_complex',";
            }
            if ($logfc_class){
                $classes.="'$logfc_class',";
            }
	    $classes.="]";

            push(@global_json, "                                    { data: { id: '$initial_list[$i]', size: $input_node_size, name: '$initial_list[$i]' }, classes: $classes },\n");
            $hash_created_nodes{$initial_list[$i]} = 1;
        }
    }
    
    # Step 2: Missing nodes
    push(@global_json, "\n", "                                    // MISSING NODES\n");
    foreach my $key (sort keys %hash_complex_to_missings){
        my @missing_partners = @{$hash_complex_to_missings{$key}};
        for(my $i=0; $i <= $#missing_partners; $i++){
            # BELOW: BECAUSE THE SAME UNDETECTED PARTNER MAY COME FROM DIFFERENT COMPLEXES
            if (!exists $hash_created_nodes{$missing_partners[$i]}){
                push(@global_json, "                                    { data: { id: '$missing_partners[$i]', size: $node_size, name: '$missing_partners[$i]' }, classes: 'missing_nodes' },\n");
                $hash_created_nodes{$missing_partners[$i]} = 1;
            }
        }
    }





    my %hash_biogrid_node_to_inputs;
    my @top_biogrid_nodes;
    if (-f $biogrid_table_file and -e $biogrid_table_file){
        push(@global_json, "\n", "                                    // BIOGRID NODES\n");
        @top_biogrid_nodes = additional_biogrid_nodes($biogrid_table_file, $max_biogrid_nodes, $node_size, \%hash_created_nodes, \%hash_biogrid_node_to_inputs, $input_species);
    }
    else{
        print "Note: BioGRID analysis returned no results\n";
    }






    # IF NO MISSING NODE HAS BEEN PUSHED, LAST LINE WILL BE THE JS COMMENT: THEN DO NOT chomp
    if ($global_json[$#global_json] !~ m/\/\//){ # FIXME
        chomp $global_json[$#global_json];
    }

    # DEFINE THE EDGES
    push(@global_json, "],\n",
    "                           // END OF elements: { nodes BLOCK\n\n",
    "                           edges: [\n");
 




    # Step: Edges with input partners
    my %hash_input_edges_to_pdbs; # Key = UniProt1-UniProt2; Value = (ref to) an array of PDB codes
    foreach my $key (sort keys %hash_complex_to_entries){
    #foreach my $key ( sort { scalar(@{$hash_complex_to_entries{$b}}) <=> scalar(@{$hash_complex_to_entries{$a}}) } keys %hash_complex_to_entries ) {
        compute_input_edges($hash_complex_to_entries{$key}, $key, \$structural_edges_counter, \%hash_input_edges_to_pdbs, \%hash_complex_to_pdbs, \%hash_pdbANDentry_to_identities);
    }





    push(@global_json, "\n", "                                    // STRUCTURAL EDGES\n");
    foreach my $input_edge (sort keys %hash_input_edges_to_pdbs){
        my $classes = "['structural_edges',";
	my ($nodeA, $nodeB) = split('-', $input_edge);
        my @sorted = sort { $b->[1] <=> $a->[1] } @{$hash_input_edges_to_pdbs{$input_edge}};
        my $conservation_class = '';
        if ($sorted[0][1] > 95){
            $conservation_class = "'id95edge'";
        }
        elsif ($sorted[0][1] > 80){
            $conservation_class = "'id80edge'";
        }
        elsif ($sorted[0][1] > 50){
            $conservation_class = "'id50edge'";
        }
        elsif ($sorted[0][1] > 30){
            $conservation_class = "'id30edge'";
        }
	else{
            $conservation_class = "'id0edge'";
        }
	$classes.="$conservation_class,";
        foreach my $array_ref (@sorted){
            my $pdb_code = ${$array_ref}[0];
            my $avg_seqid = ${$array_ref}[1];
            $classes.="'$pdb_code ($avg_seqid%)',"; # XXX NOTE: These PDB codes have a seq id% next to them
        }
        $classes.="]";
        # BELOW : DO NOT USE AN id FOR EACH EDGE
        push(@global_json, "                                    { data: { source: '$nodeA', target: '$nodeB', strength: 5 }, classes: $classes },\n");
    }







    # Step: Edges with missing partners
    push(@global_json, "\n", "                                    // MISSING EDGES\n");
    my %hash_edges_to_pdbs; # Key = UniProt1-UniProt2; Value = (ref to) an array of PDB codes
    foreach my $key (sort keys %hash_complex_to_entries){
        compute_missing_edges($hash_complex_to_entries{$key}, $key, \$missing_edges_counter,  \%hash_complex_to_missings, \%hash_edges_to_pdbs, \%hash_complex_to_pdbs, \%hash_pdb_to_missings);
    }

    foreach my $edge (sort keys %hash_edges_to_pdbs){
        my $classes = "['missing_edges',";
	my ($nodeA, $nodeB) = split('-', $edge);
        foreach my $pdb_code (@{$hash_edges_to_pdbs{$edge}}){
            $classes.="'$pdb_code',";
        }
        $classes.="]";
        # BELOW : DO NOT USE AN id FOR EACH EDGE
        push(@global_json, "                                    { data: { source: '$nodeA', target: '$nodeB', strength: 5 }, classes: $classes },\n");
    }






    my %hash_ac2id = load_ac2id($conversion_table_file); # Only for the input proteins
    my %hash_elm_edge_to_classes;
    my $count_elm_edges = 0;

    if (-f $elm_table_file and -e $elm_table_file){
        # XXX XXX Redondance interdite avec les input nodes
        my @elm_table = readfile($elm_table_file);
        shift (@elm_table);
        compute_elm_edges(\%hash_elm_edge_to_classes, \$count_elm_edges, $anchor_threshold, \@elm_table, \%hash_ac2id);

        # For each edge
        push(@global_json, "\n", "                                    // ELM EDGES\n");
        foreach my $key (sort keys %hash_elm_edge_to_classes){
            my $classes = "['elm_edges',";
            my ($nodeA, $nodeB) = split('-', $key);
    
            foreach my $class (@{$hash_elm_edge_to_classes{$key}}){
                $classes.="'$class',";
            }
            $classes.="]";
            push(@global_json, "                                    { data: { source: '$nodeA', target: '$nodeB', strength: 5 }, classes: $classes },\n");
        }
    }
    else{
        print "Note: ELM analysis returned no results\n";
    }

    # NOTE: By definition, BioGRID edges are all new, as they connect BioGRID supplementary nodes, which remain unconnected at this stage
    for (my $i=0; $i<=$#top_biogrid_nodes; $i++){
        my $biogrid_ac = $top_biogrid_nodes[$i][0];
        my $biogrid_id = $top_biogrid_nodes[$i][1];
        my ($biogrid_id_code, $biogrid_id_species) = split('_', $biogrid_id);
        foreach my $input_ac (@{$hash_biogrid_node_to_inputs{$biogrid_ac}}){
            my $input_id = $hash_ac2id{$input_ac};
            my ($input_id_code, $input_id_species) = split('_', $input_id);
            push(@global_json, "                                    { data: { source: '$input_id_code', target: '$biogrid_id_code', strength: 5 }, classes: 'biogrid_edges' },\n");
        }
    }





    write_json($job_title);

    # TRANSFERT JSON TO MAIN JS, WITH FILTERING OF TOP UNDETECTED NODES
    my @top_undetected = find_top_undetected(\@array_for_score_calculation, $max_undetected);
    my @accepted_nodes; # NOTE: COMBINES INPUT LIST AND TOP UNDETECTED + ELM
    foreach my $node (@initial_list){
        chomp $node; # FIXME unnecessary
        push(@accepted_nodes, $node);
    }
    foreach my $node (@top_undetected){
        push(@accepted_nodes, $node);
    }
    foreach my $ele (@global_json){
        if ($ele =~ m/missing_nodes/){
            if ($ele =~ m/name: '(\w+)' /){
                my $node_name = $1;
                if (grep /^$node_name$/, @top_undetected){ # NOTE: TOP UNDETECTED NODES HERE
                    push(@global_js, $ele);
                }
            }
        }
        elsif ($ele =~ m/missing_edges/){
            if ($ele =~ m/source: '(\w+)', target: '(\w+)',/){
                my $node1 = $1;
                my $node2 = $2;

                if ((grep /^$node1$/, @accepted_nodes) and (grep /^$node2$/, @accepted_nodes)){
                    push(@global_js, $ele);
                }
            }
        }
        else{
            push(@global_js, $ele);
        }
    }



 
    print_js_bottom($border_width_variation, $input_species); # FIXME Use of this variable?
 
    print_html_bottom($input_node_color, $missing_node_color, $missing_edges_counter, $missing_edge_color, $structural_edges_counter, \%hash_complex_to_entries, $#fold_change_table, $negative_fold_change_color, $elm_edge_color, $biogrid_node_color, $biogrid_edge_color);
    open (OUTHTML, '>', "$job_title.html") or die "Error while creating .html output\n\t$!\n";
    print OUTHTML @global_html_header;
    print OUTHTML "\n<script src='$job_title.js'></script>\n";
    print OUTHTML @global_html_bottom;
    close OUTHTML;
    
    open (OUTJS, '>', "$job_title.js") or die "Error while creating .js output\n\t$!\n";
    print OUTJS @global_js;
    close OUTJS;
}

main();

################################################################################
#                                SUBROUTINES                                   #
################################################################################


#sub color_edges{
#    # NOTE: NOT IN USE
#    my @list_of_uniprotID = @{$_[0]};
#    my $color = $_[1];
#    my $complexe_name = $_[2];
#    for(my $i = 0; $i <= $#list_of_uniprotID; $i++){
#        chomp $list_of_uniprotID[$i];
#        for(my $j = $i+1; $j <= $#list_of_uniprotID; $j++){
#            chomp $list_of_uniprotID[$j];
#            push(@global_js,
#                    "                           {selector: '#$complexe_name","x$list_of_uniprotID[$i]x$list_of_uniprotID[$j]', style: {'line-color': '$color', 'width': '40', 'opacity': '0.33'}},\n"
#	        );
#        }
#    }
#}


sub additional_biogrid_nodes{
    my $biogrid_table_file = $_[0];
    my $max_biogrid_nodes = $_[1];
    my $node_size = $_[2];
    my $hash_created_nodes_ref = $_[3];
    my $hash_biogrid_node_to_inputs_ref = $_[4];
    my $input_species = $_[5];

    my @output; # Top BioGRID nodes
    my %hash_biogridnode_to_occurrences;
    load_biogrid_hashes($biogrid_table_file, \%hash_biogridnode_to_occurrences, $hash_biogrid_node_to_inputs_ref);
    my $count = 0;
    # For each biogrid node *sorted by occurrences*
    foreach my $biogrid_ac (sort {$hash_biogridnode_to_occurrences{$b} <=> $hash_biogridnode_to_occurrences{$a}} sort keys %hash_biogridnode_to_occurrences){ # The second sort is for reproducibility
        # Search the UniProtID matching with this UniProtAC
        my $uniprotid = get_id_from_ac($biogrid_ac);
        if ($uniprotid){
            my ($id_code, $id_species) = split('_', $uniprotid);
            # NOTE: ONLY ADD BIOGRID NODES OF THE PROPER SPECIES
            if ($id_species eq $input_species){
                unless (exists ${$hash_created_nodes_ref}{$id_code}){
                    push(@global_json, "                                    { data: { id: '$id_code', size: $node_size, name: '$id_code' }, classes: 'biogrid_nodes' },\n");
                    push(@output, [$biogrid_ac, $id_code]);
                    ${$hash_created_nodes_ref}{$id_code} = 1;
                    $count++;
                    if ($count == $max_biogrid_nodes){
                        last; # This break is here, because the search into the huge flat file to convert UniProt AC to ID takes time:
                              # Not every BioGRID node is processed
                              # FIXME Future version of Victor's method should directly provide both AC and ID
                    }
                }
            }
        }
    }
    return @output;
}


sub load_biogrid_hashes{
    my $input_file = $_[0];
    my $hash_biogridnode_to_occurrences_ref = $_[1];
    my $hash_biogrid_node_to_inputs_ref = $_[2];

    open (IN, '<', $input_file) or die "Error: $0: cannot read $input_file\n";
    while (my $line = <IN>){
        chomp $line;
        if ($line =~ m/^(\w+):TOTAL_BIOGRID_PARTNERS\(LOW\)=\d+:IDS=(.+):TOTAL_ELM/){
            my $input_node = $1;
            my @uniprots = split(',', $2);
            foreach my $uniprot (@uniprots){
                if ($uniprot ne "Unfound"){
                    unless (exists ${$hash_biogrid_node_to_inputs_ref}{$uniprot}){
                        ${$hash_biogrid_node_to_inputs_ref}{$uniprot} = [];
                    }
                    push(@{${$hash_biogrid_node_to_inputs_ref}{$uniprot}}, $input_node);

                    if (exists ${$hash_biogridnode_to_occurrences_ref}{$uniprot}){
                        ${$hash_biogridnode_to_occurrences_ref}{$uniprot}++;
                    }
                    else{
                        ${$hash_biogridnode_to_occurrences_ref}{$uniprot} = 1;
                    }
                }
            }
        }
    }
    close IN;
}


sub compute_elm_edges{
    my $hash_elm_edge_to_classes_ref = $_[0];
    my $count_elm_edges_ref = $_[1];
    my $anchor_threshold = $_[2];
    my %hash_ac2id = %{$_[4]};

    # This hash will contained a reduced (filtered) version of the raw table
    my %hash_unique_elm;
    
    foreach my $line (@{$_[3]}){ # @elm_table
        chomp $line;
        my ($elm_class, $motif, $partner_motif, $start,
            $end, $pfam, $partner_pfam, $motif_exp,
            $elm_proba, $strict_disorder, $short_iupred, $long_iupred,
            $short_glob, $long_glob, $anchor, $biogrid_inter,
            $biogrid_filtered) = split("\t", $line);
    
        if ($partner_motif ne $partner_pfam){
            if ($biogrid_inter or $anchor > $anchor_threshold){
                my @key_arr = sort($elm_class, $partner_motif, $pfam, $partner_pfam);
                my $key = join("-", @key_arr);
                if (exists $hash_unique_elm{$key}){
                    my @A = split("\t", $hash_unique_elm{$key});
                    if ($anchor > $A[14]){ # NOTE: Filtering based on the ANCHOR2 score
                        $hash_unique_elm{$key} = $line;
                    }
                }
                else{
                    $hash_unique_elm{$key} = $line;
                }
            }
        }
    }


    # Now that the hash is ready, parse it    
    foreach my $key (sort keys %hash_unique_elm){
        my $line = $hash_unique_elm{$key};
        my ($elm_class, $motif, $partner_motif, $start,
            $end, $pfam, $partner_pfam, $motif_exp,
            $elm_proba, $strict_disorder, $short_iupred, $long_iupred,
            $short_glob, $long_glob, $anchor, $biogrid_inter,
            $biogrid_filtered) = split("\t", $line);

        my ($partner_motif_id, $partner_motif_species) = split("_", $hash_ac2id{$partner_motif});
        my ($partner_pfam_id, $partner_pfam_species) = split("_", $hash_ac2id{$partner_pfam});
 
        my ($partnerA, $partnerB) = sort($partner_motif_id, $partner_pfam_id);

        my $source = ($biogrid_inter) ? 'BioGRID' : 'ANCHOR2';
    
        unless (exists ${$hash_elm_edge_to_classes_ref}{"$partnerA-$partnerB"}){
            ${$hash_elm_edge_to_classes_ref}{"$partnerA-$partnerB"} = [];
            $$count_elm_edges_ref++;
        }
    
        $anchor = sprintf("%.1f", $anchor);
        push(@{${$hash_elm_edge_to_classes_ref}{"$partnerA-$partnerB"}}, "$partner_motif_id:$elm_class $partner_pfam_id:$pfam ($source)");
    }
}



sub compute_input_edges{ # FIXME
    my @list_of_uniprotID = @{$_[0]};
    my $complexe_name = $_[1];
    my $structural_edges_counter_ref = $_[2];
    my $hash_input_edges_to_pdbs_ref = $_[3];
    my $hash_complex_to_pdbs_ref = $_[4];
    my $hash_pdbANDentry_to_identities_ref = $_[5];

    # Nested loop to establish edges between the members of the complex
    for(my $i = 0; $i <= $#list_of_uniprotID; $i++){
        chomp $list_of_uniprotID[$i];
        for(my $j = $i+1; $j <= $#list_of_uniprotID; $j++){
            chomp $list_of_uniprotID[$j];
            my @input_partners = ($list_of_uniprotID[$j], $list_of_uniprotID[$i]);
            my $input_edge_name = join('-', sort @input_partners); # NOTE: the sort is very important

            unless (exists ${$hash_input_edges_to_pdbs_ref}{$input_edge_name}){
                ${$hash_input_edges_to_pdbs_ref}{$input_edge_name} = [];
            }

            # Edges between input members
            # FIND THE PDBs ASSOCIATED WITH THE CURRENT COMPLEX
            foreach my $pdb_code (@{${$hash_complex_to_pdbs_ref}{$complexe_name}}){
                my $seqidj = ${$hash_pdbANDentry_to_identities_ref}{"$pdb_code:$list_of_uniprotID[$j]"};
                my $seqidi = ${$hash_pdbANDentry_to_identities_ref}{"$pdb_code:$list_of_uniprotID[$i]"};
		my $avg_seqid = ($seqidj+$seqidi)/2;
                $avg_seqid = sprintf("%.1f", $avg_seqid);
                push(@{${$hash_input_edges_to_pdbs_ref}{$input_edge_name}}, [$pdb_code, $avg_seqid]);
            }
            $$structural_edges_counter_ref++;
        }
    }
}




sub compute_missing_edges{ # FIXME
    my @list_of_uniprotID = @{$_[0]};
    my $complexe_name = $_[1];
    my $missing_edges_counter_ref = $_[2];
    my $hash_complex_to_missings_ref = $_[3];
    my $hash_edges_to_pdbs_ref = $_[4];
    my $hash_complex_to_pdbs_ref = $_[5];
    my $hash_pdb_to_missings_ref = $_[6];

    # Nested loop to establish edges between the members of the complex
    for(my $i = 0; $i <= $#list_of_uniprotID; $i++){
        chomp $list_of_uniprotID[$i];
        if (exists ${$hash_complex_to_missings_ref}{$complexe_name}){
            foreach my $missing_partner (@{${$hash_complex_to_missings_ref}{$complexe_name}}){
                chomp $missing_partner;
                my @partners = ($missing_partner, $list_of_uniprotID[$i]);
                my $edge_name = join('-', sort @partners); # NOTE: the sort is very important
    
                unless (exists ${$hash_edges_to_pdbs_ref}{$edge_name}){
                    ${$hash_edges_to_pdbs_ref}{$edge_name} = [];
                }
 
                # Edges between members
                # FIND THE PDBs ASSOCIATED WITH THE CURRENT COMPLEX
                foreach my $pdb_code (@{${$hash_complex_to_pdbs_ref}{$complexe_name}}){
                    if (grep(/^$missing_partner$/, @{${$hash_pdb_to_missings_ref}{$pdb_code}})){
                        push(@{${$hash_edges_to_pdbs_ref}{$edge_name}}, $pdb_code);
                    }
                }
                $$missing_edges_counter_ref++;
            }
        }
    }
    foreach my $pdb_code (@{${$hash_complex_to_pdbs_ref}{$complexe_name}}){
        if (exists ${$hash_pdb_to_missings_ref}{$pdb_code}){
            foreach my $missing1 (@{${$hash_pdb_to_missings_ref}{$pdb_code}}){
                foreach my $missing2 (@{${$hash_pdb_to_missings_ref}{$pdb_code}}){
                    if ($missing1 ne $missing2){
                        my @partners = ($missing1, $missing2);
                        my $edge_name = join('-', sort @partners); # NOTE: the sort is very important
            
                        unless (exists ${$hash_edges_to_pdbs_ref}{$edge_name}){
                            ${$hash_edges_to_pdbs_ref}{$edge_name} = [];
                        }
                        push(@{${$hash_edges_to_pdbs_ref}{$edge_name}}, $pdb_code);
                        $$missing_edges_counter_ref++;
                    }
                }
            }
        }
    }
}




sub print_head{
    my $job_title = $_[0];

    push(@global_html_header, "<!DOCTYPE>\n",
    "<html>\n",
    "  <head>\n",
    "    <title>$job_title</title>\n",
    "    <meta name='viewport' content='width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1'>\n",
    "    <meta content='text/html;charset=utf-8' http-equiv='Content-Type'>\n",
    "    <meta content='utf-8' http-equiv='encoding'>\n",
    #"    <script src='https://code.jquery.com/jquery-3.0.0.min.js'></script>\n",
    "    <script src='vendor/jquery-3.0.0.min.js'></script>\n",
    #"    <script src='https://unpkg.com/cytoscape/dist/cytoscape.min.js'></script>\n",
    "    <script src='vendor/cytoscape.min.js'></script>\n",
    #"    <!-- for testing with local version of cytoscape.js -->\n",
    #"    <!--<script src='../cytoscape.js/build/cytoscape.js'></script>-->\n",
    #"    <script src='https://cdn.rawgit.com/cytoscape/cytoscape.js-cose-bilkent/2.0.4/cytoscape-cose-bilkent.js'></script>\n",
    #"    <script src='vendor/cytoscape-cose-bilkent.js'></script>\n",
    "    <link href='vendor/cytoscape.js-panzoom.css' rel='stylesheet' type='text/css' />\n",
    "    <link href='font-awesome-4.0.3/css/font-awesome.css' rel='stylesheet' type='text/css' />\n",
    "    <script src='vendor/cytoscape-panzoom.js'></script>\n",
    "    <script src='vendor/cytoscape-view-utilities.js'></script>\n",
    "    <script src='vendor/cytoscape-qtip.js'></script>\n",
    "    <script src='vendor/jquery.qtip.js'></script>\n",
    "    <link rel='stylesheet' type='text/css' href='vendor/jquery.qtip.css'>\n",
    "    <link rel='stylesheet' type='text/css' href='css/mystyle.css'>\n",
    "    <style>\n",
    "      body {\n",
    "      font-family: helvetica neue, helvetica, liberation sans, arial, sans-serif;\n",
    "      font-size: 14px;\n",
    "      }\n",
    "      #cy {\n",
    "      z-index: 999;\n",
    "      width: 85%;\n",
    "      height: 85%;\n",
    "      float: left;\n",
    "      }\n",
    "      h1 {\n",
    "      opacity: 0.5;\n",
    "      font-size: 1em;\n",
    "      font-weight: bold;\n",
    "      }\n",
    "    </style>\n");
}


sub print_js_head{
    my ($cy_layout, $font_size, $input_node_color, $missing_edge_color, $missing_node_color, $negative_fold_change_color, $elm_edge_color, $biogrid_node_color, $biogrid_edge_color) = splice(@_, 0, 9);

    push(@global_js,
    "      document.addEventListener('DOMContentLoaded', function(){\n",
    "                   var cy = window.cy = cytoscape({\n",
    "                       container: document.getElementById('cy'),\n",
    "                       maxZoom: 0.2,\n",
    "                       minZoom: 0.06,\n",
$cy_layout,
    #"                       layout: { name: 'circle' },\n",
    #"                       layout: { name: 'cose-bilkent' },\n", # XXX Alternative layout
    #"                       layout: { name: 'cose', nodeRepulsion: function( node ){ return '$repulsion'; }, animate: false, numIter: 100 },\n",
    #"                       layout: { name: 'cose', nodeOverlap: '5000000000' },\n",
    "                       style: [\n",
    "                           {\n",
    "                               selector: 'node',\n",
    "                               style: {\n",
    "                                   'content': 'data(name)',\n",
    "                                   'border-color': 'black',\n",
    "                                   'font-weight': 'bold',\n",
    "                                   'font-size': '$font_size',\n",
    "                                   'color': '#262626',\n",
    "                                   'text-valign': 'center',\n",
    "                                   'height': 'data(size)',\n",
    "                                   'width': 'data(size)',\n",
    #"                                   'z-compound-depth': 'bottom',\n",
    "                                   'background-color': '$input_node_color'\n",
    #"                                   'shape-polygon-points': [ 0, -1, 1, 1, -1, 1 ]\n",
    "                               }\n",
    "                           },\n",
    "                           {\n",
    "                               selector: 'edge',\n",
    "                               style: {\n",
    "                               }\n",
    "                           },\n",
    "      \n",
    "                           {selector: '.input_nodes',style: {}},\n",
    "                           {selector: '.structural_edges',style: {}},\n",
    "                           {selector: '.missing_edges',style: {'z-compound-depth': 'bottom','line-color': '$missing_edge_color'}},\n",
    "                           {selector: '.elm_edges',style: {'z-compound-depth': 'bottom','line-color': '$elm_edge_color'}},\n",
    "                           {selector: '.biogrid_edges',style: {'z-compound-depth': 'bottom','line-color': '$biogrid_edge_color'}},\n",
    "                           {selector: '.missing_nodes',style: {'background-color': '$missing_node_color'}},\n",
    "                           {selector: '.biogrid_nodes',style: {'background-color': '$biogrid_node_color'}},\n",
    "                           {selector: '.logfc_positives',style: {}},\n",
    "                           {selector: '.logfc_negatives',style: {'background-color': '$negative_fold_change_color'}},\n\n");
}


sub print_js_bottom{
    my $border_width_variation = $_[0];
    my $input_species = $_[1];

    push(@global_js, "                           ]\n",
    "                           // END OF elements: { edges BLOCK\n\n",
    "                       }\n",
    "                       // END OF elements: BLOCK\n\n",
    "                   });\n",
    "                   // END OF cytoscape BLOCK\n\n"
    );


#// just use the regular qtip api but on cy elements
push(@global_js,
    "                   cy.elements('node').qtip({\n",
    "                     content: function(){ return '<a href=\"https://www.uniprot.org/uniprot/'+ this.id() +'_$input_species'+'\" target=_blank>UniProt</a><br><a href=vendor/molart.html?'+ this.id() +'_$input_species'+' target=_blank>MolArt</a>' },\n",
    "                     position: {\n",
    "                       my: 'top center',\n",
    "                       at: 'bottom center'\n",
    "                     },\n",
    "                     style: {\n",
    "                       classes: 'qtip-bootstrap',\n",
    "                       tip: {\n",
    "                         width: 16,\n",
    "                         height: 8\n",
    "                       }\n",
    "                     },\n",
    "                     show: { event: 'cxttap' }\n",
    "                   });\n",
    "\n",
    "                   cy.elements('edge').qtip({\n",
    "                     content: function(){\n",
    "                         var str_pdbs_list = '';\n",
    "                         var str_elm_list = '';\n",
    "                         var number_of_pdbs = 0;\n",
    "                         var number_of_elm = 0;\n",
    "                         this['_private'].classes.forEach(function( myclass ){\n",
    "                             if (myclass.match(/^\\d/) && !myclass.match(/:/)){\n",
    "                                 if (myclass.match(/\\(/)){\n",
    "                                     words = myclass.split(' (');\n",
    "                                     pdbcode=words[0];\n",
    "                                     percentage='('+words[1];\n",
    "                                     str_pdbs_list+='<a href=https://www.rcsb.org/structure/'+pdbcode+' target=_blank>'+pdbcode+'</a> '+percentage+'<br>';\n",
    "                                 }\n",
    "                                 else{\n",
    "                                     str_pdbs_list+='<a href=https://www.rcsb.org/structure/'+myclass+' target=_blank>'+myclass+'</a><br>';\n",
    "                                 }\n",
    "                                 number_of_pdbs++;\n",
    "                             }\n",
    "                             else if (myclass.match(/BioGRID|ANCHOR2/)){ \n",
    "                                 partners = myclass.split(' ')\n",
    "                                 elm_codes = partners[0].split(':')\n",
    "                                 pfam_codes = partners[1].split(':')\n",
    "                                 str_elm_list+=elm_codes[0]+':<a href=http://elm.eu.org/elms/'+elm_codes[1]+' target=_blank>'+elm_codes[1]+'</a> '+pfam_codes[0]+':<a href=https://pfam.xfam.org/family/'+pfam_codes[1]+' target=_blank>'+pfam_codes[1]+'</a><br><br>';\n",
    "                                 number_of_elm++;\n",
    "                             }\n",
    "                         });\n",
    "                         if (number_of_pdbs > 0){\n",
    "                             str_pdbs_list=\"Seen in \"+number_of_pdbs+\" PDB:<br><div style='height:34px; overflow-y:scroll;'>\"+str_pdbs_list+\"</div>\";\n",
    "                             return str_pdbs_list;\n",
    "                         }\n",
    "                         else if (number_of_elm > 0){\n",
    "                             str_elm_list=\"Seen in \"+number_of_elm+\" ELM:<br><div style='height:34px; overflow-y:scroll;'>\"+str_elm_list+\"</div>\";\n",
    "                             return str_elm_list;\n",
    "                         }\n",
    "                         else{\n",
    "                             biogrid_node = this['_private'].data.target;\n",
    "                             message = \"More interactions with \"+biogrid_node+\" on <a href=https://thebiogrid.org/ target=_blank>BioGRID</a>\";\n",
    "                             return message;\n",
    "                         }\n",
    "                     },\n",
    "                     position: {\n",
    "                       my: 'top center',\n",
    "                       at: 'bottom center'\n",
    "                     },\n",
    "                     style: {\n",
    "                       classes: 'qtip-bootstrap',\n",
    "                       tip: {\n",
    "                         width: 16,\n",
    "                         height: 8\n",
    "                       }\n",
    "                     },\n",
    "                     show: { event: 'cxttap' }\n",
    "                   });\n");



# this.data('invented_field_name')



push(@global_js, "      \n",
    "                   var api = cy.viewUtilities({\n",
    "                       neighbor: function(node){\n",
    "                           return node.closedNeighborhood();\n",
    "                       },\n",
    "                       neighborSelectTime: 1000\n",
    "                   });\n",
    "                   \n",
    #"                   var layout = cy.layout({\n",
    #"                     name: 'cose-bilkent',\n",
    #"                     randomize: false,\n",
    #"                     fit: false\n",
    #"                   });\n",
    "      \n",
#"const nodes = cy.nodes();\n", # XXX To manually fetch the node positions in the web browser console
#"//nodes.layout(layout);\n",
#"console.log(nodes.map(node => node.position()));\n",
    "      \n",
    "                   // Increase border width to show nodes with hidden neighbors\n",
    "                   function thickenBorder(eles){\n",
    "                     eles.forEach(function( ele ){\n",
    "                       var defaultBorderWidth = Number(ele.css('border-width').substring(0,ele.css('border-width').length-2));\n",
    "                       ele.css('border-width', defaultBorderWidth + $border_width_variation);\n",
    "                     });\n",
    "                     return eles;\n",
    "                   }\n",
    "                   // Decrease border width when hidden neighbors of the nodes become visible\n",
    "                   function thinBorder(eles){\n",
    "                     eles.forEach(function( ele ){\n",
    "                       var defaultBorderWidth = Number(ele.css('border-width').substring(0,ele.css('border-width').length-2));\n",
    "                       ele.css('border-width', defaultBorderWidth - $border_width_variation);\n",
    "                     });\n",
    "                     return eles;\n",
    "                   }\n",
    "      \n",
    "                   function count_selected_nodes(){\n",
    "                       document.getElementById('number_of_selected_nodes').innerHTML = cy.nodes(':selected').size();\n",
    "                   }\n",
    "\n",
    "                   function display_selected_nodes(){\n",
    "                       var str_of_selected = '';\n",
    "                       var counter = 0;\n",
    "                       cy.nodes(':selected').forEach(function( mynode ){\n",
    "                           str_of_selected+=mynode.data('id')+'; ';\n",
    "                           counter++;\n",
    "                           if (counter > 4){ // NOTE: add a newline after 4 node names\n",
    "                               str_of_selected+='&#13;&#10;';\n",
    "                               counter = 0;\n",
    "                           }\n",
    "                       });\n",
    "                       //console.log(str_of_selected);\n",
    "                       document.getElementById('selected_names').innerHTML = str_of_selected;\n",
    "                   }\n",
    "\n",
    "                   function display_corresponding_complexes(){\n",
    "                       var arr_of_selected = [];\n",
    "                       var str_of_selected = '';\n",
    "                       if (cy.nodes(':selected').size() > 0){\n",
    "                           cy.nodes(':selected').forEach(function( myobject ){\n",
    "                               myobject['_private'].classes.forEach(function( myclass ){\n",
    "                                   if (!arr_of_selected.includes(myclass)){\n",
    "                                       // DO NOT DISPLAY OTHER CLASSES THAN COMPLEXES\n",
    "                                       if (myclass.substring(0,1) === 'c' && !arr_of_selected.includes(myclass)){\n",
    "                                           arr_of_selected.push(myclass);\n",
    "                                       }\n",
    "                                   }\n",
    "                               });\n",
    "                           });\n",
    "                           str_of_selected = arr_of_selected.join('; ');\n",
    "                       }\n",
    "                       document.getElementById('corresponding_complexes').innerHTML = str_of_selected;\n",
    "                       document.getElementById('number_of_corresponding_complexes').innerHTML = arr_of_selected.length;\n",
    "                   }\n",
    "\n",
    "                   cy.on('select', 'node', function (event) {\n",
    "                       count_selected_nodes();\n",
    "                       display_selected_nodes();\n",
    "                       display_corresponding_complexes();\n",
    "                   });\n",
    "                   cy.on('unselect', 'node', function (event) {\n",
    "                       count_selected_nodes();\n",
    "                       display_selected_nodes();\n",
    "                       display_corresponding_complexes();\n",
    "                   });\n",
    "\n",
    "                   cy.on('unselect', '.input_nodes', function (event) {\n",
    "                       document.getElementById('select_node_type1').checked = false;\n",
    "                   });\n",
    "\n",
    "\n",
    "                   cy.on('unselect', '.missing_nodes', function (event) {\n",
    "                       document.getElementById('select_node_type2').checked = false;\n",
    "                   });\n",
    "\n",
    "\n",
    "                   cy.on('unselect', '.biogrid_nodes', function (event) {\n",
    "                       document.getElementById('select_node_type3').checked = false;\n",
    "                   });\n",
    "\n",
    "\n",
    "\n",
    "                   \$('#select_connected_nodes').click(function () {\n",
    "                       cy.edges(':selected').connectedNodes().select();\n",
    "                   });\n",
    "\n",
    "                   \$('#select_connected_edges').click(function () {\n",
    "                       cy.nodes(':selected').connectedEdges().select();\n",
    "                   });\n",
    "\n",
    "                   \$('#hide').click(function () {\n",
    "                       api.disableMarqueeZoom();\n",
    "                       var nodesWithHiddenNeighbor = cy.edges(':hidden').connectedNodes(':visible');\n",
    "                       thinBorder(nodesWithHiddenNeighbor);\n",
    "                       api.hide(cy.\$(':selected'));\n",
    "                       nodesWithHiddenNeighbor = cy.edges(':hidden').connectedNodes(':visible');\n",
    "                       thickenBorder(nodesWithHiddenNeighbor);\n",
    "                       //if(document.getElementById('layout').checked){\n",
    "                       //  layout.run();  \n",
    "                       //}\n",
    "                   });\n"
);



push(@global_js, "      \n",
    "      \n",
    "                   \$('#showAll').click(function () {\n",
    "                       api.disableMarqueeZoom();\n",
    "                       var nodesWithHiddenNeighbor = cy.edges(':hidden').connectedNodes(':visible');\n",
    "                       thinBorder(nodesWithHiddenNeighbor);\n",
    "                       api.show(cy.elements());\n",
    "                       //if(document.getElementById('layout').checked){\n",
    "                       //  layout.run();  \n",
    "                       //}\n",
    "                   });\n",
    "      \n",
    "                   \$('#showHiddenNeighbors').click(function () {\n",
    "                       api.disableMarqueeZoom();\n",
    "                       var nodesWithHiddenNeighbor = cy.edges(':hidden').connectedNodes(':visible');\n",
    "                       thinBorder(nodesWithHiddenNeighbor);\n",
    "                       api.show(cy.nodes(':selected').neighborhood().union(cy.nodes(':selected').neighborhood().parent()));\n",
    "                       nodesWithHiddenNeighbor = cy.edges(':hidden').connectedNodes(':visible');\n",
    "                       thickenBorder(nodesWithHiddenNeighbor);\n",
    "                       //if(document.getElementById('layout').checked){\n",
    "                       //  layout.run();  \n",
    "                       //}                    \n",
    "                   });\n",
    "      \n",
    "                   var arr_selected_complexes = [];\n",
    "                   \$('#complex_select').click( function(){\n",
    "                       if (\$('#exclusive_complex').is(':checked')){\n",
    "                           cy.nodes(':selected').forEach(function( myobject ){\n",
    "                               myobject['_private'].classes.forEach(function( myclass ){\n",
    "                                   if (myclass.substring(0,1) === 'c'){\n",
    "                                       myobject.unselect();\n",
    "                                   }\n",
    "                               });\n",
    "                           });\n",
    "                           //cy.nodes(':selected').unselect();\n",
    "                           arr_selected_complexes = [];\n",
    "                       }\n",
    "                       var complex_name = document.getElementById('complex_select').value;\n",
    "                       if (complex_name === 'all_complexes'){\n",
    "                           cy.nodes().forEach(function( myobject ){\n",
    "                               myobject['_private'].classes.forEach(function( myclass ){\n",
    "                                   if (myclass.substring(0,1) === 'c'){\n",
    "                                       myobject.select();\n",
    "                                   }\n",
    "                               });\n",
    "                           });\n",
    "                       }\n",
    "                       else if (complex_name){\n",
    "                           cy.\$('.'+complex_name).select();\n",
    "                           if (!arr_selected_complexes.includes(complex_name)){\n",
    "                               arr_selected_complexes.push(complex_name);\n",
    "                           }\n",
    "                       }\n",
    "                       else{\n",
    "                           arr_selected_complexes = [];\n",
    "                           cy.nodes(':selected').forEach(function( myobject ){\n",
    "                               myobject['_private'].classes.forEach(function( myclass ){\n",
    "                                   if (myclass.substring(0,1) === 'c'){\n",
    "                                       myobject.unselect();\n",
    "                                   }\n",
    "                               });\n",
    "                           });\n",
    "                           //cy.nodes(':selected').unselect();\n",
    "                       }\n",
    "                       document.getElementById('selected_complexes').innerHTML = arr_selected_complexes.join('; ');\n",
    "                       //console.log(uniprot_name);\n",
    "                       //if(document.getElementById('layout').checked){\n",
    "                       //  layout.run();\n",
    "                       //}\n",
    "                   });\n",
    "      \n",
    "                   \$('#free_select').click( function(){\n",
    "                       // GET INPUT NODE NAMES\n",
    "                       var uniprot_str = document.getElementById('uniprot_text_area').value;\n",
    "                       var regex=new RegExp(\"[ ,;]+\", \"g\");\n",
    "                       var uniprot_arr = uniprot_str.split(regex);\n",
    "                       uniprot_arr.forEach(function(uniprot_name) {\n",
    "                         //console.log(uniprot_name);\n",
    "                         if (uniprot_name){\n",
    "                             cy.\$('#'+uniprot_name).select();\n",
    "                         }\n",
    "                       });\n",
    "                       //if(document.getElementById('layout').checked){\n",
    "                       //  layout.run();\n",
    "                       //}\n",
    "                   })\n",
    "      \n",
    "                   // CHECK BOX 1\n",
    "                   \$('#select_node_type1').click( function(){\n",
    "                       var node_type_selected = document.getElementById('select_node_type1').value;\n",
    "                       var thisCheck = \$(this);\n",
    "                       if (thisCheck.is (':checked')){\n",
    "                         cy.\$('.'+node_type_selected).select();\n",
    "                       }\n",
    "                       else{\n",
    "                         cy.\$('.'+node_type_selected).unselect();\n",
    "                       }\n",
    "                       //if(document.getElementById('layout').checked){\n",
    "                       //  layout.run();\n",
    "                       //}\n",
    "                   })\n",
    "      \n",
    "                   // CHECK BOX 2\n",
    "                   \$('#select_node_type2').click( function(){\n",
    "                       var node_type_selected = document.getElementById('select_node_type2').value;\n",
    "                       var thisCheck = \$(this);\n",
    "                       if (thisCheck.is (':checked')){\n",
    "                         cy.\$('.'+node_type_selected).select();\n",
    "                       }\n",
    "                       else{\n",
    "                         cy.\$('.'+node_type_selected).unselect();\n",
    "                       }\n",
    "                       //if(document.getElementById('layout').checked){\n",
    "                       //  layout.run();\n",
    "                       //}\n",
    "                   })\n",
    "      \n",
    "                   // CHECK BOX 3\n",
    "                   \$('#select_node_type3').click( function(){\n",
    "                       var node_type_selected = document.getElementById('select_node_type3').value;\n",
    "                       var thisCheck = \$(this);\n",
    "                       if (thisCheck.is (':checked')){\n",
    "                         cy.\$('.'+node_type_selected).select();\n",
    "                       }\n",
    "                       else{\n",
    "                         cy.\$('.'+node_type_selected).unselect();\n",
    "                       }\n",
    "                       //if(document.getElementById('layout').checked){\n",
    "                       //  layout.run();\n",
    "                       //}\n",
    "                   })\n",
    "      \n",
    "                   \$('#zoomToSelected').click( function(){ \n",
    "                       api.disableMarqueeZoom();   \n",
    "                       var selectedEles = cy.\$(':selected');\n",
    "                       api.zoomToSelected(selectedEles);\n",
    "                       //if(document.getElementById('layout').checked){\n",
    "                       //  layout.run();  \n",
    "                       //}   \n",
    "                   })\n",
    "      \n",
    "                   \$('#select_neighbors').click( function(){\n",
    "                       cy.nodes(':selected').neighborhood().select();\n",
    "                       // FIXME below is to get rid of errors\n",
    "                       //if(document.getElementById('layout').checked){\n",
    "                       //  layout.run();\n",
    "                       //}\n",
    "                   })\n",
    "      \n",
    "                   \$('#invert_selection').click( function(){\n",
    "                       unselected_nodes = cy.nodes(':unselected');\n",
    "                       cy.nodes(':selected').unselect();\n",
    "                       unselected_nodes.select();\n",
    "                       // FIXME below is to get rid of errors\n",
    "                       //if(document.getElementById('layout').checked){\n",
    "                       //  layout.run();\n",
    "                       //}\n",
    "                   })\n",
    "      \n",
    #"                   \$('#marqueeZoom').click( function(){\n",
    #"                       api.enableMarqueeZoom();   \n",
    #"                       if( document.getElementById('layout').checked){\n",
    #"                           layout.run();\n",
    #"                       }\n",
    #"                   })\n",
    #"      \n",
    "                   var tappedBefore;\n",
    "                   cy.on('tap', 'node', function (event) {\n",
    "                     var node = this;\n",
    "                     var tappedNow = node;\n",
    "                     setTimeout(function () {\n",
    "                       tappedBefore = null;\n",
    "                     }, 300);\n",
    "                     if (tappedBefore && tappedBefore.id() === tappedNow.id()) {\n",
    "                       tappedNow.trigger('doubleTap');\n",
    "                       tappedBefore = null;\n",
    "                     } else {\n",
    "                       tappedBefore = tappedNow;\n",
    "                     }\n",
    "                   });\n",
    "                   \n",
    "                   cy.on('doubleTap', 'node', function (event) {\n",
    "                       var nodesWithHiddenNeighbor = cy.edges(':hidden').connectedNodes(':visible');\n",
    "                       thinBorder(nodesWithHiddenNeighbor);\n",
    "                       api.show(cy.nodes(':selected').neighborhood().union(cy.nodes(':selected').neighborhood().parent()));\n",
    "                       nodesWithHiddenNeighbor = cy.edges(':hidden').connectedNodes(':visible');\n",
    "                       thickenBorder(nodesWithHiddenNeighbor);\n",
    "                       if(document.getElementById('layout').checked){\n",
    "                         layout.run();  \n",
    "                       }  \n",
    "                   });\n",
    "      \n",
    "                   \$('#highlightSelection').click(function () {\n",
    "                       api.disableMarqueeZoom();\n",
    "                       if(cy.\$(':selected').length > 0)\n",
    "                         api.highlight(cy.\$(':selected'));\n",
    "                   });\n",
    "      \n",
    "                   \$('#removeHighlights').click(function () {\n",
    "                       api.disableMarqueeZoom();\n",
    "                       api.removeHighlights();\n",
    "                   });\n",
"cy.panzoom({});\n",
    "      });\n");
}

sub print_html_bottom{ # FIXME COLORS FOR EDGES NOT USED

    my ($input_node_color, $missing_node_color, $missing_edges_counter, $missing_edge_color, $structural_edges_counter, $hash_complex_to_entries_ref, $fold_change_last_index, $negative_fold_change_color, $elm_edge_color, $biogrid_node_color, $biogrid_edge_color) = splice(@_, 0, 11);

    push(@global_html_bottom,
    "  </head>\n",
    "  <body>\n",
#    "    <b>Move/Unselect:</b> <a style='color: darkorange'>Left click</a> |\n",
#    "    <b>Zoom:</b> <a style='color: darkorange'>Scroll wheel</a> |\n",
#    "    <b>Select area:</b> <a style='color: darkorange'>Shift + Left click</a> |\n",
#    "    <b>Get info:</b> <a style='color: darkorange'>Right click</a><br/>\n",
    "    <b>Select nodes by names:</b> <input type='text' id='uniprot_text_area' placeholder='UniProt IDs with any separator'> <button id='free_select' type='button'>Select</button>&emsp;\n",
    "    <b>Select a complex:</b> <select id='complex_select' multiple='multiple' size=2 style='vertical-align: text-top;'>\n");

    # DROP DOWN LIST OF COMPLEXES TO SELECT
    push(@global_html_bottom, "                               <option value=''>None</option>\n");
    push(@global_html_bottom, "                               <option value='all_complexes'>All</option>\n");
    foreach my $key (sort keys %{$hash_complex_to_entries_ref}){
        push(@global_html_bottom, "                               <option value='$key'>$key</option>\n");
    }

    # 
    push(@global_html_bottom,
    "                           </select>\n",
#    "    <textarea readonly cols='20' rows='8' style='height: 17px; vertical-align: middle;' id='selected_complexes' placeholder='Selected complexes'></textarea>\n",
    "    <input type='checkbox' id='exclusive_complex' style='vertical-align: middle; opacity: 0;' onclick='return false;' checked='checked'>\n",
    "    <br>\n",
    "    <b>Select nodes by sources:</b>\n");


    if ($fold_change_last_index > -1){
        push(@global_html_bottom,
        "    Input <a style='color: $input_node_color'>&#x2B24</a> <a style='color: $negative_fold_change_color'>&#x2B24</a> <input type='checkbox' value='input_nodes' id='select_node_type1' style='vertical-align: middle;'>&emsp;\n");
    }
    else{
        push(@global_html_bottom,
        "    Input <a style='color: $input_node_color'>&#x2B24</a> <input type='checkbox' value='input_nodes' id='select_node_type1' style='vertical-align: middle;'>&emsp;\n");
    }

    push(@global_html_bottom,
    "    Undetected <a style='color: $missing_node_color'>&#x2B24</a> <input type='checkbox' value='missing_nodes' id='select_node_type2' style='vertical-align: middle;'>&emsp;\n",
    "    BioGRID <a style='color: $biogrid_node_color'>&#x2B24</a> <input type='checkbox' value='biogrid_nodes' id='select_node_type3' style='vertical-align: middle;'>\n",
    "\n",
    "    <a href='#' class='tooltip' style='text-decoration: none;'>\n",
    "        <img src='img/imark.png' style='width:20px;height:20px;vertical-align:middle;'>\n",
    "        <span>\n",
    "            <img class='callout' src='img/callout.gif'>\n",
    "            <div style='text-align: justify; word-break: normal;'>\n",
    "                In addition to the input proteins, the best partners from the 3D-based analysis (n=50 max.) and from the BioGRID analysis (n=10 max.) are included into the graph representation.\n",
    "            </div>\n",
    "        </span>\n",
    "    </a>\n",
    "\n",
    "    <br>\n",
    "    <b>Legend: </b> Complex edges (seq. id.):\n",
    "    <hr style='border: 4px solid blue;'> ≥95%;\n",
    "    <hr style='border: 4px solid green;'> ≥80%;\n",
    "    <hr style='border: 4px solid gold;'> ≥50%;\n",
    "    <hr style='border: 4px solid red;'> ≥30%;\n",
    "    <hr style='border: 4px solid black;'> ≥0%;\n",
    "\n",
    "    <a href='#' class='tooltip' style='text-decoration: none;'>\n",
    "        <img src='img/imark.png' style='width:20px;height:20px;vertical-align:middle;'>\n",
    "        <span>\n",
    "            <img class='callout' src='img/callout.gif'>\n",
    "            <div style='text-align: justify; word-break: normal;'>\n",
    "                Edge colors are a function of sequence identity of the pairs of structures from which the interaction has been inferred.\n",
    "            </div>\n",
    "        </span>\n",
    "    </a>\n",
    "\n",
    "    &emsp;Other edges: <hr style='border: 1px solid gray; opacity:1.0'> Undetected;\n",
    "    <hr style='border: 1px solid $elm_edge_color; opacity:1.0'> ELM;\n",
    "    <hr style='border: 1px solid $biogrid_edge_color; opacity:1.0'> BioGRID;\n",
    "\n",
    "    <a href='#' class='tooltip' style='text-decoration: none;'>\n",
    "        <img src='img/imark.png' style='width:20px;height:20px;vertical-align:middle;'>\n",
    "        <span>\n",
    "            <img class='callout' src='img/callout.gif'>\n",
    "            <div style='text-align: justify; word-break: normal;'>\n",
    "                Undetected and BioGRID edges connect nodes of the corresponding types. ELM edges connect input nodes.\n",
    "            </div>\n",
    "        </span>\n",
    "    </a>\n",
    "\n",
    "    <br>\n",
    "    <b>Extend selection:</b>\n",
    "    Nodes: <button id='invert_selection' type='button'>Invert</button>\n",
    "    <button id='select_neighbors' type='button'>Neighbors</button>\n",
    "\n",
    "    <a href='#' class='tooltip' style='text-decoration: none;'>\n",
    "        <img src='img/imark.png' style='width:20px;height:20px;vertical-align:middle;'>\n",
    "        <span>\n",
    "            <img class='callout' src='img/callout.gif'>\n",
    "            <div style='text-align: justify; word-break: normal;'>\n",
    "                By clicking two times on \"Neighbors\", you can get the \"neighbors of the neighbors\" of the selected node.\n",
    "            </div>\n",
    "        </span>\n",
    "    </a>\n",
    "\n",
#    "    <b>Nodes:</b> MS ($input_nodes_counter) <a style='color: $input_node_color'>&#x2B24</a> <button id='hide_input_nodes' type='button'>H</button><button id='show_input_nodes' type='button'>S</button>&emsp;\n",
#    "    Missing ($missing_nodes_counter) <a style='color: $missing_node_color'>&#x2B24</a> <button id='hide_missing_nodes' type='button'>H</button><button id='show_missing_nodes' type='button'>S</button>&emsp;\n",
#    "    <b>Edges:</b> Missing ($missing_edges_counter) <a style='color: $missing_edge_color; font-weight: 900;'>&#8213;</a> <button id='hide_missing_edges' type='button'>H</button><button id='show_missing_edges' type='button'>S</button>&emsp;\n",
#    "    Structural ($structural_edges_counter) &#127752; <button id='hide_structural_edges' type='button'>H</button><button id='show_structural_edges' type='button'>S</button><br>\n",
    "    &emsp;Edges: <button id='select_connected_nodes' type='button'>Connected nodes</button>\n",
    "\n",
    "    <a href='#' class='tooltip' style='text-decoration: none;'>\n",
    "        <img src='img/imark.png' style='width:20px;height:20px;vertical-align:middle;'>\n",
    "        <span>\n",
    "            <img class='callout' src='img/callout.gif'>\n",
    "            <div style='text-align: justify; word-break: normal;'>\n",
    "                Here, from a selected edge, you can further select the two connected nodes.\n",
    "            </div>\n",
    "        </span>\n",
    "    </a>\n",
    "\n",
#    "    <button id='select_connected_edges' type='button'>Connected edges</button><br>\n",
    "    <br>\n",
    "    <br><p id='number_of_selected_nodes'>0</p> <p style='color: gray;'>nodes selected:</p> \n",
    "    <textarea readonly cols='40' rows='8' style='height: 17px; vertical-align: middle;' id='selected_names' placeholder='Selected UniProt IDs'></textarea>&emsp;\n",
    "    <p style='color: gray;'>belonging to</p> <p id='number_of_corresponding_complexes'>0</p> <p style='color: gray;'>complexes:</p>\n",
    "    <textarea readonly cols='20' rows='8' style='height: 17px; vertical-align: middle;' id='corresponding_complexes' placeholder='Names of complexes'></textarea>\n",
    "    <br>\n",
    "    <b>Action:</b>\n",
    "    <button id='zoomToSelected' type='button'>Focus</button>&emsp;\n",
    "    <button id='hide' type='button'>Hide</button>\n",
    "    <button id='showHiddenNeighbors' type='button'>Show</button>\n",
    "    <button id='showAll' type='button'>Reset</button>\n",
    "\n",
    "    <a href='#' class='tooltip' style='text-decoration: none;'>\n",
    "        <img src='img/imark.png' style='width:20px;height:20px;vertical-align:middle;'>\n",
    "        <span>\n",
    "            <img class='callout' src='img/callout.gif'>\n",
    "            <div style='text-align: justify; word-break: normal;'>\n",
    "                Here, you can \"Hide\" selected nodes or edges. Nodes with hidden connections have a black border. By selecting such nodes, you can \"Show\" the hidden connections. Finally, you can \"Reset\" the display.\n",
    "            </div>\n",
    "        </span>\n",
    "    </a>\n",
    "\n",
    "    &emsp;Highlights: <button id='highlightSelection' type='button'>Activate</button>\n",
    "    <button id='removeHighlights' type='button'>Cancel</button>\n",
    "\n",
    "    <a href='#' class='tooltip' style='text-decoration: none;'>\n",
    "        <img src='img/imark.png' style='width:20px;height:20px;vertical-align:middle;'>\n",
    "        <span>\n",
    "            <img class='callout' src='img/callout.gif'>\n",
    "            <div style='text-align: justify; word-break: normal;'>\n",
    "                Warning: the \"Highlights\" function may slow down your web browser when nodes are too numerous.\n",
    "            </div>\n",
    "        </span>\n",
    "    </a>\n",
    "\n",
    "    <br>\n",
    "    <br>\n",
    "    <div id='cy' style='border: 1px solid lightgray;'></div>\n",
    "    <div id='cy'></div>\n",
    "  </body>\n",
    "</html>\n");
}


sub find_top_undetected{
    my $array_for_score_calculation_ref = $_[0];
    my $max_undetected = $_[1];

    my @aoa_undetected_score;
    my $total = 0;
    my $sum = 0;
    my $last_one = '';
    my @sorted_array_for_score_calculation = sort { $a->[0] cmp $b->[0] } @{$array_for_score_calculation_ref};
    push(@sorted_array_for_score_calculation, ['foo', 1234, '1234', 'baz']); # DUMMY PUSH
    # CALCULATE THE SCORE FOR EACH MISSING NODE
    for (my $i=0; $i<=$#sorted_array_for_score_calculation; $i++){
        my $undetected = $sorted_array_for_score_calculation[$i][0];
        my $avg_id = $sorted_array_for_score_calculation[$i][1];
        my $completeness = $sorted_array_for_score_calculation[$i][2];
        my $complexe_name = $sorted_array_for_score_calculation[$i][3];

        if ($undetected ne $last_one and $last_one){
            my $score = $sum/$total;
            push(@aoa_undetected_score, [$last_one, $score]);
            $total = 0;
            $sum = 0;
        }

        my $product = $completeness*$avg_id;
        $sum+=$product;
        $total++;
        $last_one = $undetected;
    }

    my @sorted_aoa_undetected_score = sort { $b->[1] <=> $a->[1] } @aoa_undetected_score;
    my @top_undetected;
    for (my $i=0; $i<=$#sorted_aoa_undetected_score; $i++){
        push(@top_undetected, $sorted_aoa_undetected_score[$i][0]);
        if ($i == $max_undetected){
            last;
        }
    }

    return @top_undetected;
}


sub write_json{
    my $job_title = $_[0];

    my $str_json = join('', @global_json);
    my @arr_json = split("\n", $str_json);
    $arr_json[$#arr_json] =~ s/,$//;

    open(OUT, '>', "$job_title.json") or die "ERROR in $0: cannot write $job_title.json\n";
    foreach my $ele (@arr_json){
        $ele =~ s/,]/]/g;
        $ele =~ s/'/"/g;
        $ele =~ s/nodes:/"nodes":/g;
        $ele =~ s/data:/"data":/g;
        $ele =~ s/id:/"id":/g;
        $ele =~ s/size:/"size":/g;
        $ele =~ s/name:/"name":/g;
        $ele =~ s/classes:/"classes":/g;
        $ele =~ s/edges:/"edges":/g;
        $ele =~ s/source:/"source":/g;
        $ele =~ s/target:/"target":/g;
        $ele =~ s/strength:/"strength":/g;
        unless ($ele =~ m/\/\//){
            print OUT "$ele\n";
        }
    }
    print OUT "]}";
    close OUT;
}


sub range_ok{
    my $min = 0;
    my $max = 0;

    foreach my $value (@{$_[0]}){
        chomp $value; # FIXME unnecessary?
        if ($value > $max){
            $max = $value;
        }
        if ($value < $min){
            $min = $value;
        }
    }
    if ($max > 30 or $min < -30){
        return 0; # RANGE NOT OK
    }
    else{
        return 1; # RANGE OK
    }
}


sub load_ac2id{
    my %hash;
    my @arr = readfile($_[0]);
    foreach my $line (@arr){
        chomp $line;
        my ($ac, $id) = split("\t", $line);
        $hash{$ac} = $id;
    }
    return %hash;
}


sub log_fold_change{
    my @output;
    my $logbase = $_[1];
    foreach my $value (@{$_[0]}){
        chomp $value;
        my $new = log($value)/log($logbase);
        push(@output, $new);
    }
    return @output;
}


sub check_arguments{
    my ($layout, $table1_file, $table3_file, $job_title, $conversion_table_file) = splice(@_, 0, 5);

    unless (-e $conversion_table_file and -f $conversion_table_file){
        die "Error: missing --conversion_table argument\n";
    }

    if (!$layout){
        die "Error: missing --layout argument: 'circle' or 'cose'\n";
    }
    
    if (!$table1_file){
        die "Error: missing --table1 argument\n";
    }
    if (!$table3_file){
        die "Error: missing --table3 argument\n";
    }
    if (!$job_title){
        die "Error: missing --job_title argument\n";
    }
}


sub readfile{
    my $filename = shift;
    open(IN, '<', $filename) or die "\e[1;31mError\e[0m while opening $filename:\n\t$!\n";
    my @file = <IN>;
    close IN;
    return @file;
}


sub readfile_strip{
    my $filename = shift;
    open(IN, '<', $filename) or die "\e[1;31mError\e[0m while opening $filename:\n\t$!\n";
    my @file;
    while (my $line = <IN>){
        chomp $line;
        $line =~ s/^\s+//;
        $line =~ s/\s+$//;
        $line =~ s/^\t+//;
        $line =~ s/\t+$//;
        push(@file, $line);
    }
    close IN;
    return @file;
}


sub get_id_from_ac{
    my $ac = $_[0];
    system "wget -q https://www.uniprot.org/uniprot/$ac.txt";
    my $id = '';

    open (IN, '<', "$ac.txt") or die "Error: cannot read $ac.txt\n";
    while (my $line = <IN>){
        if ($line =~ m/^ID\s+(\w+_\w+)/){
            $id = $1;
            last;
        }
    }
    close IN;
    system "rm $ac.txt";

    if (!$id){
        system "wget -q https://www.uniprot.org/uniprot/$ac";

        open (IN, '<', "$ac") or die "Error: cannot read $ac\n";
        while (my $line = <IN>){
            if ($line =~ m/alternateName">\((\w+_\w+)\)/){
                $id = $1;
                last;
            }
        }
        close IN;
        system "rm $ac";
        if ($id){
            print "Warning: UniProtAC $ac ($id) seems obsolete\n";
        }
        else{ # NOTE: here, it is a simple warning, because it is only about biogrid nodes
            print "Warning: $ac is an invalid UniProtAC\n";
        }
    }

    return $id;
}


sub helper {
    print
      "\n",
      "\tOptions:\n",
      "\t--table1                     TABLE1\n",
      "\t--table3                     TABLE3\n",
      "\t--elm_table                  ELM_MOTIF_CONNEXION_TABLE.txt\n",
      "\t--biogrid_table              BIOGRID_PARTNERS_EXTENTION.txt (with the typo!)\n", # XXX XXX
      "\t--anchor_threshold           Minimal value (0.0 to 1.0) for the ANCHOR2 score (default=1.0)\n",
      "\t--job_title                  Self explanatory\n",
      "\t--fold_change                Fold change\n",
      "\t--layout                     Layout (circle or cose); note: CoSE for Compound Spring Embedder\n",
      "\t--conversion_table           The .acid file to convert UniProt AC to ID\n",
      "\t--max_undetected             Max number of undetected (3D) nodes in the graph (JSON has everything)\n",
      "\t--max_biogrid_nodes          Max number of BioGRID nodes in the graph (JSON has everything)\n",
      "\t--help                       This help\n",
      "\n";
    exit 1;
}
