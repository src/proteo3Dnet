#!/usr/bin/env python
# -*- coding: utf-8 -*-


def main():
    HHCLUSTERBANK = "/home/databases/pdb70_clu_7158679.tsv" # HHsearch's pdb70_clu.tsv, with size appended
    MUSTANGBANK = "/home/databases/MUSTANG"
    SEQRESBANK = "/home/databases/SEQATM_PASS1.txt" # Only residues with 3D coordinates (rather than pdb_seqres.txt)
    PDBHTML = "/home/databases/PDBhtml.tsv"

    input_fasta_file, species, outputdir = get_args()

    if os.path.isdir(outputdir):
        sys.exit("Error: cannot create output directory %s: already exists" % outputdir)

    print("1/6 Checking input conformity...")

    # Check input file extension
    p1 = re.compile("\.fasta$|\.fas$|\.fa$|\.faa$|\.mpfa$|\.fst$|\.txt$|\.dat$|\.data$")
    p2 = re.compile("\.\w+$")
    extension1 = p1.search(input_fasta_file)
    extension2 = p2.search(input_fasta_file)
    if extension2 and not extension1: # Files with no extension are accepted
        print("Error: invalid input file extension (query)")
        sys.exit(1)

    # Put the input file into a list (one line = one element)
    temp_input = ffile2list(input_fasta_file) # NOTE: \n, \s, \t are removed

    # Check organism validity
    if (species and species != "None"):
        check_species(species)

    # Check whether input is a list of FASTA sequences or a list of UniProt codes
    is_fasta = is_multifasta(temp_input)

    # If it is not a FASTA, then it must be a UniProt AC or ID (otherwise: die!)
    if not is_fasta:
        if not is_uniprot_ac(temp_input) and not is_uniprot_id(temp_input):
            print("Error: invalid format of the input")
            sys.exit(1)

    # Check that protein sequences do not contain non-alphabetical characters
    if is_fasta:
        check_alphabetical(temp_input)

    # Get list of input names
    names = find_input_names(temp_input, is_fasta)

    # Reject single input
    if len(names) < 2:
        print("Error: at least 2 input proteins are required to detect an interaction")
        sys.exit(1)

    # Reject too numerous inputs
    if len(names) > 400:
        print("Warning: the number of input proteins is >400 (n=%s): the graph representation will not be displayed and must be downloaded for local viewing" % str(len(names)))

    # If input is not a FASTA, download the corresponding sequences
    if not is_fasta:
        download_sequences('downloaded_multi.fasta', names)
        input_fasta_file = 'downloaded_multi.fasta'

    # Read the input multi-FASTA file into a list, where each element is: sequence name + sequence itself (including all carriage returns)
    input_fasta_str = os.popen('cat ' + input_fasta_file).read()
    input_fasta_list = input_fasta_str.split('>')
    input_fasta_list.pop(0)

    # Split the list into n files, where n is the input number of tasks
    # FILENAME.fasta => 0_FILENAME.fasta, 1_FILENAME.fasta, 2_FILENAME.fasta, ...
    number_of_tasks = len(input_fasta_list)
    chunks = chunk(input_fasta_list, number_of_tasks)

    output_list_filename = "ALL_SUBLISTS" # FIXME Arbitrary name
    write_sublists_and_listoflists(output_list_filename, input_fasta_file, chunks)

    hhsearch_dir = os.path.splitext(input_fasta_file)[0]
    hhdbfile = os.path.splitext(input_fasta_file)[0]+'.hhdb'
    mafft_dir = os.path.splitext(input_fasta_file)[0]+'_MAFFT_OUTPUT'

    print("2/6 Fetching information for each input protein...")
    # XXX This step has to be performed, because it produces the .acid and .ac files
    os.system('docker run --rm -v $(pwd):/mnt/ network_analysis /home/step1.pl /mnt/%s %s 2> step1_network_analysis.err > step1_network_analysis.out' % (input_fasta_file, species))

    # If this first step yields errors: print and stop the program
    step1_network_analysis_err = os.popen('cat step1_network_analysis.err').read()

    # Print warnings yielded by step1
    step1_network_analysis_out = os.popen('cat step1_network_analysis.out').read()

    print("3/6 Running HHsearch... (this may take some time, depending on (i) the number of input proteins and (ii) the number of CPUs used by HHsearch)")
    file1 = open(output_list_filename, 'r') 
    Lines = file1.readlines()
    for line in Lines:
        os.system('docker run --rm -v $(pwd):/mnt/ -v /home/alcor/mounted_banks/:/scratch/banks/ hh_enhanced_search /home/step1/wrapper_findTemplate.pl /mnt/%s > /dev/null 2>&1' % line.strip())
    file1.close()

    # Move the results into a directory (newly created, unless it already exists)
    # FILENAME.fasta => FILENAME/
    if (not os.path.isdir(hhsearch_dir)):
        os.mkdir(hhsearch_dir)
    mv_target = '*_'+hhsearch_dir+'*'
    os.system('mv %s %s 2> /dev/null' % (mv_target, hhsearch_dir)) 
 
    # Analyze the HHsearch results
    # (i) Search additional templates into the clusters
    # (ii) Annotate these templates
    print("4/6 Performing advanced cluster search...")
    hhdb_filename = hhsearch_dir+'.hhdb'

    os.popen('ls ' + hhsearch_dir + '/*.out > ALL_OUT')
    os.system("sed 's/^/\/mnt\//' ALL_OUT > ALL_SUBOUT")
    os.system('docker run --rm -v $(pwd):/mnt/ hh_enhanced_search /bin/bash -c "/home/step2/extract_HHsearch_data.pl /mnt/%s /mnt/%s %s %s %s > /dev/null 2>&1; mv /*.alntmp /*.maffterr /*.mafftout /*.hhaln /*.msa* /*.multifasta /mnt/*_MAFFT_OUTPUT/; mv /*.hhdb /mnt/%s"' % ('ALL_SUBOUT', hhsearch_dir, HHCLUSTERBANK, MUSTANGBANK, SEQRESBANK, hhdb_filename))

    print("5/6 Complex identification...")
    os.system('docker run --rm -v $(pwd):/mnt/ network_analysis /bin/bash -c "/home/step3b/annotate_hhdb_file.pl /mnt/%s %s > /dev/null 2>&1; mv *.hhdb /mnt/"' % (hhdbfile, PDBHTML))

    acidfile = os.path.splitext(input_fasta_file)[0]+'.acid'
    acfile = os.path.splitext(input_fasta_file)[0]+'.ac'
    annotatedfile = 'annotated.hhdb'
    os.system('docker run --rm -v $(pwd):/mnt/ network_analysis /bin/bash -c "/home/step3b/tables12.pl /mnt/%s /mnt/%s; mv *.tsv /mnt/"' % (acidfile, annotatedfile))
    os.system('docker run --rm -v $(pwd):/mnt/ network_analysis /bin/bash -c "/home/step3a.pl -i /mnt/%s -p %s -r /mnt/%s --homology_table /mnt/TABLE1.tsv --oligomery_table /mnt/TABLE2.tsv > /dev/null 2>&1; mv complexes* *.tsv /mnt/ 2> /dev/null"' % (acidfile, PDBHTML, hhdbfile))

    print("6/6 ELM/BioGRID analysis...")
    output1 = "BIOGRID_PARTNERS_EXTENTION.txt"
    output2 = "ELM_MOTIF_CONNEXION_TABLE.txt"
    output3 = "UNFOUND_ANALYSIS.csv"
    results_path = "/LIMIP/databases/USERS_RESULT/"
    user_tag = "foo"
    cat1 = " && cat "+results_path+user_tag+"/"+output1+" > "+output1
    cat2 = " && cat "+results_path+user_tag+"/"+output2+" > "+output2
    cat3 = " && cat "+results_path+user_tag+"/"+output3+" > "+output3
 
    cmd = '/bin/bash -c "/LIMIP/LiMIP/LiMIP.py -i '+'/mnt/'+acfile+' -u '+user_tag+cat1+cat2+cat3+'; mv BIOGRID_PARTNERS_EXTENTION.txt ELM_MOTIF_CONNEXION_TABLE.txt UNFOUND_ANALYSIS.csv /mnt/"'
    os.system('docker run --rm -v $(pwd):/mnt/ limip '+cmd)

    # Build graph
    os.system('docker run --rm -v $(pwd):/mnt/ network_analysis /bin/bash -c "/home/step4/build_graph.pl -a 0.95 --max_undetected 50 -c /mnt/%s --biogrid_table /mnt/BIOGRID_PARTNERS_EXTENTION.txt -e /mnt/ELM_MOTIF_CONNEXION_TABLE.txt --table1 /mnt/TABLE1.tsv --table3 /mnt/complexes_concatenated.tsv --job_title graph --layout cose > graph_builder.out 2> /dev/null; mv graph* /mnt/; cp -r /home/step4/vendor/ /home/step4/font-awesome-4.0.3/ /home/step4/img/ /home/step4/css/ /mnt/"' % acidfile)

    # Moving to output directory and removing useless files
    os.system('docker run --rm -v $(pwd):/mnt/ network_analysis /bin/bash -c "cd /mnt/; mkdir %s; mv graph.* TABLE* complexes_concatenated.tsv ELM_MOTIF_CONNEXION_TABLE.txt css font-awesome-4.0.3 img vendor %s/; rm -r uniprot_download.report step1_network_analysis.* ALL_SUBLISTS ALL_OUT ALL_SUBOUT *.tsv *.txt *.csv graph* annotated.hhdb %s* complexes* > /dev/null 2>&1"' % (outputdir, outputdir, os.path.splitext(input_fasta_file)[0]))

    # Renaming
    os.system('docker run --rm -v $(pwd):/mnt/ network_analysis /bin/bash -c "cd /mnt/%s; mv TABLE1.tsv homologs.tsv; mv TABLE2.tsv homo-oligomers.tsv; mv ELM_MOTIF_CONNEXION_TABLE.txt SLiMs_analysis.txt; mv complexes_concatenated.tsv 3Dcomplexes.tsv"' % (outputdir))

    print("Job completed normally.")


def get_args():
    parser = argparse.ArgumentParser(description = 'Wrapper of the Proteo3Dnet method')
    parser.add_argument('-i', dest='infile', type=str, help="Input file: either multi-fasta, list of UniProtAC, or UniProtID (valid file extensions are: .txt, .dat, .data, .fasta, .fas, .fa, .faa, .mpfa, .fst, or no file extension)", required=True)
    parser.add_argument('-s', dest="species", type=str, help="Filter the results based on the given organism name (UniProt format, e.g. HUMAN, DROME, YEAST, MOUSE)")
    parser.add_argument('-o', dest="outputdir", type=str, help="Name of the directory that will be created to store the results", required=True)

    if len(sys.argv) < 2:
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()

    return args.infile, args.species, args.outputdir


def download_sequences(concatenated_fasta_name, names):
    for name in names:
        name.rstrip('\n')
        # Now download and concatenate
        exit_code = os.system("wget https://www.uniprot.org/uniprot/%s.fasta -O %s.upfas >> uniprot_download.report 2>&1" % (name, name))
        if exit_code == 0:
            os.system("cat %s.upfas >> %s" % (name, concatenated_fasta_name))
        else:
            print("Warning: cannot download %s.fasta: input protein discarded" % name)
        os.system("rm %s.upfas 2> /dev/null" % name)

 
def find_input_names(raw_input_list, is_fasta):
    output_list = []
    for line in raw_input_list:
        if is_fasta:
            p1 = re.compile("^>(\w+)$") # UniProt ID
            p2 = re.compile("^>\w\w\|([A-Za-z0-9]+)-?\d*\|\w+_\w+") # UniProt FASTA header
            p3 = re.compile("^>([A-Za-z0-9]+)-?\d*$") # UniProt AC
            p4 = re.compile("^>")
            name1 = p1.search(line)
            name2 = p2.search(line)
            name3 = p3.search(line)
            name4 = p4.search(line)
            if name1:
                output_list.append(name1.group(1))
            elif name2:
                output_list.append(name2.group(1))
            elif name3:
                output_list.append(name3.group(1))
            elif name4:
                print("Error: the following line is not a valid FASTA identifier:\n%s\n" % line)
                sys.exit(1)
        else:
            p = re.compile("(^\w+$|^[A-Za-z0-9]+)-?\d*$")
            name = p.search(line)
            if name:
                output_list.append(name.group(1))
            else:
                print("Error: the following line is not a valid UniProt code:\n%s\n" % line)
                sys.exit(1)
    return output_list


def is_multifasta(array):
    for line in array:
        if line[0] == '>' and array[0][0] != '>':
            print("Error: invalid input: if multi-fasta, first character must be '>'\n")
            sys.exit(1)

    previous_line = 'foo'
    for line in array:
        if line[0] == '>' and previous_line[0] == '>':
            print("Error: invalid input: two consecutive lines starting with '>'\n")
            sys.exit(1)
        previous_line = line

    # AND NOW...
    if array[0][0] == '>':
        return 1 # is multi-fasta
    else:
        return 0 # is multi-codes UniProt AC or ID


def is_uniprot_ac(array):
    for code in array:
        if "_" in code:
            return False
    return True

def is_uniprot_id(array):
    for code in array:
        if not "_" in code:
            return False
    return True


def chunk(xs, n):
    ''' Split a list xs into n chunks
    '''
    L = len(xs)
    assert 0 < n <= L
    s = L//n
    return [xs[p:p+s] for p in range(0, L, s)]


def ffile2list(file_name):
    output_lines = []
    try:
        FHi = open( file_name, 'r')
        for line in FHi:
            line = line.strip() # NOTE: strip leading and trailing \s \t \n etc.
            if len(line) > 0:
                output_lines.append(line)
        FHi.close()
    except:
        sys.exit('Error while reading {}'.format(file_name) )

    return output_lines


def write_sublists_and_listoflists(output_list_filename, input_fasta_file, chunks): 
    f1 = open(output_list_filename, 'w')
    for i in range(len(chunks)):
        list_name = str(i)+'_'+input_fasta_file
        f1.write("%s\n" % list_name)
        f2 = open(list_name, 'w')
        for sequence in chunks[i]:
            sequence = '>'+sequence
            f2.write(sequence)
        f2.close()
    f1.close()


def check_species(organism):
    print("Selected organism: %s" % organism)
    p1 = re.compile("^\w+$") # FIXME NOT SURE IF SPECIES MAY HAVE NUMBERS
    p2 = re.compile("_")
    name1 = p1.search(organism)
    name2 = p2.search(organism)
    if name2 or not name1:
        sys.exit("%s is not a valid organism name" % organism)


def check_alphabetical(temp_input):
    p1 = re.compile("^[a-zA-Z]+$")
    for line in temp_input:
        if line[0] != '>':
            match1 = p1.search(line)
            if not match1:
                sys.exit("Error: input protein sequence contains non-alphebetical character(s)")



if __name__ == '__main__':
    import sys, os, argparse, re
    main()
