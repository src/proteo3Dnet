#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;

# XXX THIS IS THE DETECTION OF COMPLEXES XXX

my $homology_table = ''; # GLOBAL FIXME
my $oligomery_table = ''; # GLOBAL FIXME

sub main{
    my $input_list;
    my $PDBhtml_file = '';
    my $template_shared = 1; # Allow N input proteins to share the same template
    my $resultsHH = '';
    my $local = 0;

    my $help = (@ARGV) ? 0 : 1;

    GetOptions(
        'template_shared=s' => \$template_shared,
        'input=s' => \$input_list,
        'pdbhtml=s' => \$PDBhtml_file,
        'results_hh=s' => \$resultsHH,
        'homology_table=s' => \$homology_table,
        'oligomery_table=s' => \$oligomery_table,
        'local' => \$local,
        'help' => \$help
    );
    helper() if ($help);

    $template_shared--;
    if (!$resultsHH){
        print "\e[42m--results_hh: no .hhdb file given: 3D-only mode activated\e[0m\n";
    }

    my $path_dsv2datatables = ($local) ? "./dsv2datatables_m2m.pl" : "/home/dsv2datatables_m2m.pl";
    unless (-e "$path_dsv2datatables" and -f "$path_dsv2datatables"){
        die "Error: cannot find $path_dsv2datatables (see --local option)\n";
    }

    handle_errors($template_shared, $resultsHH);

    my @input_file = readfile($input_list);
    my @PDBhtml = readfile($PDBhtml_file);

    my @input_ac = get_ac_column(\@input_file);
    my @input_id = get_id_column(\@input_file);

    my %hash_local_id_to_ac;
    for (my $i=0; $i<=$#input_id; $i++){
        $hash_local_id_to_ac{$input_id[$i]} = $input_ac[$i];
    }

    my $output_file = "complexes.tsv";


    my ($code, $organism_id) = split('_', $input_id[0]);


    my %hash_hhresults_tsv;
    open(IN, '<', $resultsHH) or die "ERROR: cannot read $resultsHH\n";
    while (my $line = <IN>){
        chomp $line;
        my ($ac, $pdb6char, $proba, $id_percent, $resol, $coverage);
#O89100  1AD5_A  99.6    37      2.6     134
        ($ac, $pdb6char, $proba, $id_percent, $resol, $coverage) = split("\t", $line);
        # If user has identified the FASTA sequence by using the UniProt ID (entry name)
        # then convert it to a UniProt AC
        if ($ac =~ m/\w+_\w+/){
            for (my $i=0; $i<=$#input_id; $i++){
                if ($input_id[$i] eq $ac){
                    $ac = $input_ac[$i];
                }
            }
        }

        unless (exists $hash_hhresults_tsv{$ac}){
            $hash_hhresults_tsv{$ac} = [];
        }
        push(@{$hash_hhresults_tsv{$ac}}, [$pdb6char, $id_percent, $coverage]);
    }
    close IN;

    my %hash_6char_to_ac;
    my %hash_ac_to_id;
    my %hash_PDBhtml;
    fill_with_PDBhtml(\%hash_6char_to_ac, \%hash_ac_to_id, \%hash_PDBhtml, \@PDBhtml);

    # Search for the PDB (4 char) that gather several input proteins (defined by either their UniProt AC or chain name)
    my %hash_pdb4char_to_list;

    # For every existing PDB+chain (>140000)
    foreach my $pdb6char (sort keys %hash_6char_to_ac){
        my $ac = $hash_6char_to_ac{$pdb6char};
        # If the corresponding AC is in the input list
        if ( grep(/^$ac$/, @input_ac) ){
            # If the template found is not an obsolete PDB
            if (exists $hash_6char_to_ac{$pdb6char}){
                my $ac_template = $hash_6char_to_ac{$pdb6char};
                fill_hashes($pdb6char, \%hash_pdb4char_to_list, $ac, $ac_template, $template_shared, '100', '10000'); # XXX 10000 is used as a coverage value for native structures
            }
        }
    }

    # If .hhdb file given
    if ($resultsHH){
        # Add HHsearch results
        for (my $i=0; $i<=$#input_ac; $i++){ # For each input UniProt
            my $ac = $input_ac[$i];
            my $id = $input_id[$i];
            # Search templates in the hash_hhresults_tsv
            # if exists...
            if (exists $hash_hhresults_tsv{$ac}){
                my @AoA_unsorted = @{$hash_hhresults_tsv{$ac}};
                # NOTE: 18 Sep 2019: Sort the content from the *.hhdb file
                my @AoA = sort { $b->[1] <=> $a->[1] } @AoA_unsorted;
    
                for (my $i=0; $i<=$#AoA; $i++){
                    # If the template found is not an obsolete PDB
                    my $pdb6char = $AoA[$i][0];
                    my $id_percent = $AoA[$i][1];
                    my $coverage = $AoA[$i][2];
                    if (exists $hash_6char_to_ac{$pdb6char} and $id_percent > 0){ # XXX The .hhdb file sometimes contains "pseudo-results" for which id% and coverage are null
                        my $ac_template = $hash_6char_to_ac{$pdb6char};
                        fill_hashes($pdb6char, \%hash_pdb4char_to_list, $ac, $ac_template, $template_shared, $id_percent, $coverage);
                    }
                }
                # Completing the hash (since it was only filled based on the PDB)
                $hash_ac_to_id{$ac} = $id;
            }
            else{
                print STDERR "\e[1;33m$ac ($id) has no structure in the HHsearch results\e[0m\n";
            }
        }
    }


    foreach my $pdb4char (sort keys %hash_pdb4char_to_list){
        # XXX Sort by id%
        my @ac_and_pdb6char = sort { $b->[3] <=> $a->[3] } @{$hash_pdb4char_to_list{$pdb4char}};

        # Reset
        $hash_pdb4char_to_list{$pdb4char} = [];

        my @all_ac_templates;
        for (my $i=0; $i <= $#ac_and_pdb6char; $i++){
            my @values = @{$ac_and_pdb6char[$i]};
            my @occurrences = grep(/^$values[2]$/, @all_ac_templates); # search the UniProt AC of the template
            if ($#occurrences < $template_shared){
                push(@all_ac_templates, $values[2]);
                push(@{$hash_pdb4char_to_list{$pdb4char}}, $ac_and_pdb6char[$i]);
            }
        }
    }



    # Classify each PDB found, based on the UniProt ACs that it gathers
    my %hash_complex_id_to_PDBlist;
    foreach my $pdb4char (sort keys %hash_pdb4char_to_list){
        # XXX The sort below is important; the array of arrays is sorted by AC (col. 0), hence the 'cmp' operator, instead of <=>
        my @ac_and_pdb6char = sort { $a->[0] cmp $b->[0] } @{$hash_pdb4char_to_list{$pdb4char}};
        my $complex_size = $#ac_and_pdb6char + 1;
        # XXX Here, you can filter the complexes that will be detected based on their size
        if ($complex_size > 1){
            my @ac_found = map $_->[ 0 ], @ac_and_pdb6char;
            my $complex_identifier = join('_', @ac_found);
            unless (exists $hash_complex_id_to_PDBlist{$complex_identifier}){
                $hash_complex_id_to_PDBlist{$complex_identifier} = [];
            }
            push(@{$hash_complex_id_to_PDBlist{$complex_identifier}}, $pdb4char);
        }
    }

    # Find redundant complexes: regarding the UniProt AC
    my %redundant_groups = find_redundant_groups(\%hash_complex_id_to_PDBlist);

    parse_results(\%hash_complex_id_to_PDBlist, \%hash_ac_to_id, \%hash_pdb4char_to_list, \%hash_PDBhtml, \%redundant_groups, $output_file, \%hash_local_id_to_ac);

    system "mkdir complexes/";
    my @tsv_files = `ls *.tsv`;
    foreach my $tsv_file (@tsv_files){
        unless ($tsv_file =~ m/^complexes_concatenated\.tsv$/){
            chomp $tsv_file;
            my $rootname = $1 if ($tsv_file =~ m/^(\w+)\.tsv$/);
            my $html_title;
            if ($tsv_file =~ m/^TABLE1/){
                $html_title = 'Homology detection';
                $rootname = 'Homology_detection';
            }
            elsif ($tsv_file =~ m/^TABLE2/){
                $html_title = 'Homo-oligomeric states';
                $rootname = 'Homo-oligomeric_states';
            }
            else{
                $html_title = 'Complexes';
            }

            system "$path_dsv2datatables --input $tsv_file --output $rootname.html --title $html_title --separator tab --species $organism_id";
            if ($rootname =~ m/^c\d\d\d$/){
                system "mv $rootname.html complexes/";
            }
        }
    }
}

sub parse_results{
    my $hash_complex_id_to_PDBlist_ref = $_[0];
    my $hash_ac_to_id_ref = $_[1];
    my $hash_pdb4char_to_list_ref = $_[2];
    my $ref_hash_PDBhtml = $_[3];
    my $redundant_groups_ref = $_[4];
    my $output_file = $_[5];
    my $hash_local_id_to_ac_ref = $_[6];

    # For each complex
    my $index95 = 0;
    my $index80 = 0;
    my $index50 = 0;
    my $index30 = 0;
    my $index0 = 0;
    my @table95 = (); # Col0=the string to print; Col1=the size of the complex; Col2=the sequence identity
    my @table80 = (); # Complexes with Avg. Id. % > 80 and < 100
    my @table50 = ();
    my @table30 = ();
    my @table0 = ();

    foreach my $complex_id (sort keys %{$hash_complex_id_to_PDBlist_ref}){
        # INITIALIZE
        my $print_string1 = '';
        my $print_string2 = '';
        my $print_string3 = '';

        my @list_of_pdb = @{${$hash_complex_id_to_PDBlist_ref}{$complex_id}};
        my @acs = split('_', $complex_id);

        # STEP 1
        # Print information about the complex
        my $complex_size = $#acs+1;
        my $number_of_pdb = $#list_of_pdb + 1;
        $print_string1="cxx";

        my $subcomplex_of = '';
        foreach my $group (keys %{$redundant_groups_ref}){
            foreach my $redundant_group (@{${$redundant_groups_ref}{$group}}){
                if ($complex_id eq $redundant_group){
                    $subcomplex_of.="$group ";
		    last;
                }
	    }
        }
        $subcomplex_of=~s/ $//;



        my $has_subcomplexes = '';
        foreach my $group (keys %{$redundant_groups_ref}){
            if ($complex_id eq $group){
                foreach my $redundant_group (@{${$redundant_groups_ref}{$group}}){
                    $has_subcomplexes.="$redundant_group ";
	        }
            }
        }
        $has_subcomplexes=~s/ $//;



	if ($subcomplex_of){
            $print_string1.=" ($complex_id is a subcomplex of $subcomplex_of)";
        }
        else{
            $print_string1.=" ($complex_id is not a subcomplex)";
        }

        if ($has_subcomplexes){
            $print_string1.=" (has subcomplexes: $has_subcomplexes)";
        }
        else{
            $print_string1.=" (has not any subcomplexes)";
        }


	#if (grep /^$complex_id$/, @{$redundant_groups_ref}){
	#    $print_string1.=" (Subcomplex)";
	#}

        $print_string1.=" n=$complex_size inputs found in $number_of_pdb structures\n";
	#$print_string1.="GROUP OF $complex_size INPUT PROTEINS (labeled p1, p2, ...) FOUND TOGETHER IN $number_of_pdb STRUCTURES\n";

        # STEP 2
        # Print the partner labels (p1, p2, ...)
	#for (my $i=0; $i<=$#acs; $i++){
	#    my $partner_number = $i+1;
	#    $print_string2.="p$partner_number:${$hash_ac_to_id_ref}{$acs[$i]}\n";
	#}

        $print_string2.="Template\tOrganism ID\tAvg id\tCompleteness\tProteins found:chain (seq id%)\tUndetected partners\tProtein structure title\tMethod\tResolution\tStoichiometry\tNumber of chains\tNames of chains\n";

        # STEP 3
        # For each PDB found
        my $best_average = 0;
        my $best_completeness = 0;
        my $most_undetected = 0;
        foreach my $pdb4char (@list_of_pdb){
            my ($proteins, $organism, $title, $method, $resolution, $assembly, $stochio, $pseudo_stochio, $non_proteins) = get_information($pdb4char, $ref_hash_PDBhtml);

            # [$ac, $pdb6char, $ac_template, $id_percent, $coverage]
            my @ac_and_pdb6char = sort { $a->[0] cmp $b->[0] } @{${$hash_pdb4char_to_list_ref}{$pdb4char}}; # XXX sort is important; the 2d array is sorted by AC (col. 0), hence the 'cmp' operator instead of <=>

            my @id_percent_found = map $_->[ 3 ], @ac_and_pdb6char;
            my $average = average(\@id_percent_found); # For each PDB found, the avg id / chain is calculated
            if ($average > $best_average){
                $best_average = $average;
            }

            # XXX COLUMN #0: PDB ID
            $print_string3.="$pdb4char\t";
	    
	    my $organism_code = '-';
            my @proteins_array = split(' ', $proteins);
            my @ac_found = map $_->[ 2 ], @ac_and_pdb6char;
            my @missing_chains;
            my @missing_ac;
            foreach my $protein (@proteins_array){
                my @A = split('=', $protein);
                my @chains = split(',', $A[0]);
                my ($acc, $entry) = split(':', $A[1]);
                if ($entry =~ m/_/){
                    #my ($half_id, $species) = split('_', ${$hash_ac_to_id_ref}{$acc}); # XXX BELOW SHOULD BE THE SAME... FIXME TO TEST...
                    my ($half_id, $species) = split('_', $entry);
                    $organism_code = $species;
                    unless ( grep(/^$acc$/, @ac_found) ){
                        push(@missing_chains, "$chains[0]:$half_id");
                        push(@missing_ac, $acc);
                    }
                }
                else{ # THESE ARE undefinedID or undefinedAC
                    print STDERR "\e[43mWARNING: [Prog: $0] PDB: $pdb4char: AC=$acc ID=$entry\e[0m\n";
                }
            }

	    # XXX COLUMN #2: Species
            if ($organism =~ m/\(/){
                my @A = split(/\(/, $organism);
                $organism = $A[0];
            }
            if ($organism =~ m/^(\w+ \w+)/){
                $organism = $1;
	    }
            $print_string3.="$organism ($organism_code)\t";

	    # XXX COLUMN #1: AVERAGE SEQID%
	    $print_string3.="$average\t";


            # XXX COLUMN #xx: COMPLETENESS
            my $completeness = (($#ac_found+1)/($#ac_found+1+$#missing_ac+1))*100;
            $completeness = sprintf("%.1f", $completeness);
            $print_string3.="$completeness\t";


            if ($completeness > $best_completeness){
                $best_completeness = $completeness;
            }
            if ($#missing_ac+1 > $most_undetected){
                $most_undetected = $#missing_ac+1;
            }


            my @pdb6char_found = map $_->[ 1 ], @ac_and_pdb6char;
            my @chains_found;
            for (my $i=0; $i<=$#pdb6char_found; $i++){
                my @A = split("_", $pdb6char_found[$i]);
                push(@chains_found, $A[1]);
            }

            # XXX COLUMN #6: partner-to-chain mapping
            for (my $i=0; $i<=$#chains_found; $i++){
                my $partner_number = $i+1;
		#$print_string3.="p$partner_number:$chains_found[$i] ($id_percent_found[$i])";
                
                my $protein_found = ${$hash_ac_to_id_ref}{$acs[$i]};
                if ($protein_found =~ m/_/){
                    my @A = split('_', $protein_found);
                    $protein_found = $A[0];
		}
                $print_string3.="$protein_found:$chains_found[$i] ($id_percent_found[$i])";
                if ($i < $#chains_found){
                    $print_string3.=" ";
                }
                else{
                    $print_string3.="\t";
                }
            }

            # XXX COLUMN #7: missing partners
            my $missing_chains_str = join(" ", @missing_chains);

            if (!$missing_chains_str){
                $missing_chains_str = '-';
            }
            $print_string3.="$missing_chains_str\t";

            # XXX COLUMN #10: Protein_structure_title
            $print_string3.="$title\t";

            # XXX COLUMN #8: Method
            if ($method =~ m/SOLUTION NMR/){
                $method = 'NMR';
            }
            elsif ($method =~ m/X-RAY DIFFRACTION/){
                $method = 'X-RAY';
            }
            elsif ($method =~ m/ELECTRON MICROSCOPY/){
                $method = 'CRYO-EM';
            }
            $print_string3.="$method\t";

            # XXX COLUMN #9: Resolution
            $print_string3.="$resolution\t";


	    # XXX COLUMN #3: Stoichiometry
            $stochio =~ s/> /(/;
            $stochio.=')';
            if ($stochio !~ m/^Hetero/){
                print STDERR "\e[1;31mWARNING: STOICHIOMETRY: $stochio\e[0m\n";
	    }
	    else{
                $stochio =~ s/^Hetero //;
	    }
            $print_string3.="$stochio\t";

            # XXX COLUMN #4: Number_of_protein_chains
            my $number = $#proteins_array+1;
            $print_string3.="$number\t";


            # XXX COLUMN #5: Names_of_chains

            my $chains_list;
            foreach my $protein (@proteins_array){
                #A=Q9HC62:SENP2_HUMAN B=P61956:SUMO2_HUMAN ;
                my @A = split('=', $protein);
                my @chains = split(',', $A[0]);

                if ($#chains > 0){
                    $chains_list.="(";
                    foreach my $chain (@chains){
                        $chains_list.="$chain,";
                    }
		    $chains_list.=") ";
                }
                else{
                    $chains_list.="$chains[0] ";
                }
            }
            $chains_list =~ s/,\)/\)/g;

            # Treating nucleic acids
            my $nucleic_acids = "";
            if ($non_proteins){
                $non_proteins =~ s/ /;/g;
                $nucleic_acids = " (Nucl. Acids: $non_proteins)";
            }

            $print_string3.="$chains_list$nucleic_acids\t";

            $print_string3.="\n";
        }
        $best_average = sprintf("%.1f", $best_average);
        $best_completeness = sprintf("%.1f", $best_completeness);

        $print_string3.="\n";

        # ASSEMBLE
        my $print_string = $print_string1.$print_string2.$print_string3;




        # FIXME TODO CLEANER WAY FOR BELOW
        my $temp_string = '';
        my @tempAoA;
        my @temp_array = split("\n", $print_string3);
        foreach my $line (@temp_array){
            my @Z = split("\t", $line);
            push(@tempAoA, [$Z[2], $line]); # col 2 is avg_id
        }
        my @temp_sorted = sort { $b->[0] <=> $a->[0] } @tempAoA;
        for (my $i=0; $i<=$#temp_sorted; $i++){
            $temp_string.="$temp_sorted[$i][1]\n";
        }

        $print_string = $print_string1.$print_string2.$temp_string."\n";



        my @complex_information = ($print_string, $complex_size, $best_average, $best_completeness, $most_undetected);
        if ($best_average > 95){fill_results_tables(\@table95, \$index95, \@complex_information);}
        elsif ($best_average > 80){fill_results_tables(\@table80, \$index80, \@complex_information);}
        elsif ($best_average > 50){fill_results_tables(\@table50, \$index50, \@complex_information);}
        elsif ($best_average > 30){fill_results_tables(\@table30, \$index30, \@complex_information);}
        else{fill_results_tables(\@table0, \$index0, \@complex_information);}
    }

    my $complex_number = 0;
    my @results_to_print;
    my %hash_complex_id_to_number;
    sort_and_push(\@table95, \@results_to_print, \$complex_number, \%hash_complex_id_to_number);
    sort_and_push(\@table80, \@results_to_print, \$complex_number, \%hash_complex_id_to_number);
    sort_and_push(\@table50, \@results_to_print, \$complex_number, \%hash_complex_id_to_number);
    sort_and_push(\@table30, \@results_to_print, \$complex_number, \%hash_complex_id_to_number);
    sort_and_push(\@table0, \@results_to_print, \$complex_number, \%hash_complex_id_to_number);
    open(my $fh_out, '>', $output_file) or die "Error: cannot create $output_file\n\t$!\n";
    open(COMCON, '>', "complexes_concatenated.tsv") or die "Error: cannot create complexes_concatenated.tsv\n\t$!\n";
    print $fh_out "Complex ID\tMax avg id%\tInputs found\tStructures found\tMax completeness\tMax undetected\tParent complexes\tChild complexes\n";
    for (my $i=0; $i<=$#results_to_print; $i++){
        if ($results_to_print[$i] =~ m/^(c\d+) \(Max avg id = (\d+\.\d+)\) \(Max completeness = (\d+\.\d+)\) \(Max undetected = (\d+)\) \(.+ a subcomplex(.*)\) \(has.*subcomplexes(.*)\) n=(\d+) .+ in (\d+) structures/){
            close OUT;
            my $complex_num = $1;
            open(OUT, '>', "$complex_num.tsv") or die "ERROR: can't write $complex_num.tsv\n";
            my $max_avg = $2;
            my $max_completeness = $3;
            my $max_undetected = $4;
            my $master_complexes = $5;
            my $child_complexes = $6;
	    my $n_inputs = $7;
	    my $n_pdbs = $8;
            $master_complexes =~ s/^ of //; # NOTE: This has to be put after fetching regex matches
            $child_complexes =~ s/^: //;

            my $new_masters = '';
            my @parent_complex_ids = split(" ", $master_complexes);
            foreach my $complex_id (@parent_complex_ids){
                $new_masters.="$hash_complex_id_to_number{$complex_id}, ";
            }
            $new_masters =~ s/, $//;

            my $new_children = '';
            my @child_complex_ids = split(" ", $child_complexes);
            foreach my $complex_id (@child_complex_ids){
                $new_children.="$hash_complex_id_to_number{$complex_id}, ";
            }
            $new_children =~ s/, $//;

            unless ($i == 0){
                print COMCON "\n";
            }
            if (!$new_masters){
                $new_masters = '-';
            }
            if (!$new_children){
                $new_children = '-';
            }
            print COMCON "Complex ID\tMax avg id%\tInputs found\tStructures found\tMax completeness\tMax undetected\tParent complexes\tChild complexes\n";
	    print COMCON "$complex_num\t$max_avg\t$n_inputs\t$n_pdbs\t$max_completeness\t$max_undetected\t$new_masters\t$new_children\n";
	    print $fh_out "$complex_num\t$max_avg\t$n_inputs\t$n_pdbs\t$max_completeness\t$max_undetected\t$new_masters\t$new_children\n";
        }
	else{
            print COMCON "$results_to_print[$i]\n";
            print OUT "$results_to_print[$i]\n";
        }
    }
    close $fh_out;
    close COMCON;


    # XXX MODIFYING TABLE1
    my %hash_input_to_complex_numbers;
    foreach my $key (sort keys %hash_complex_id_to_number){
        my @input_acs = split('_', $key);
        my $compl_num = $hash_complex_id_to_number{$key};
        foreach my $input_ac (@input_acs){
            my $input_id = ${$hash_ac_to_id_ref}{$input_ac};
            if (exists $hash_input_to_complex_numbers{$input_id}){
                push(@{$hash_input_to_complex_numbers{$input_id}}, $compl_num);
            }
            else{
                $hash_input_to_complex_numbers{$input_id} = [];
                push(@{$hash_input_to_complex_numbers{$input_id}}, $compl_num);
            }
        }
    }

    my @old_table1 = readfile($homology_table);
    shift @old_table1; # Remove header
    open(OUT, '>', $homology_table) or die "Error: cannot write $homology_table\n";
    print OUT "Input\tUniProtID\tUniProtAC\t3D complexes\tBest template\tOrganism\tSeq id (%)\tSeq coverage\tStructure title\tMethod\tResolution\tOther templates\n";
    foreach my $line (@old_table1){
        chomp $line;
        my @A = split("\t", $line);

        # FIXME PATCH FOR EMPTY DATA
        for(my $i=0; $i<=$#A; $i++){
            if (!$A[$i]){
                $A[$i] = 'NA';
            }
        }

        my $organism = $A[3];
        if ($organism =~ m/^(\w+ \w+) \(/){
            $organism = $1;
        }

        my $method = $A[7];
        if ($method =~ m/SOLUTION NMR/){
            $method = 'NMR';
        }
        elsif ($method =~ m/X-RAY DIFFRACTION/){
            $method = 'X-RAY';
        }
        elsif ($method =~ m/ELECTRON MICROSCOPY/){
            $method = 'CRYO-EM';
        }


        my $input_ac = ${$hash_local_id_to_ac_ref}{$A[1]};
        my $complexes_found = '-';
        if ($hash_input_to_complex_numbers{$A[1]}){
            $complexes_found = join(', ', @{$hash_input_to_complex_numbers{$A[1]}});
        }
        $complexes_found =~ s/, $//;
        print OUT "$A[0]\t$A[1]\t$input_ac\t$complexes_found\t$A[2]\t$organism\t$A[4]\t$A[5]\t$A[6]\t$method\t$A[8]\t$A[9]\n";
    }
    close OUT;


    my @old_table2 = readfile($oligomery_table);
    shift @old_table2; # Remove header
    open(OUT, '>', $oligomery_table) or die "Error: cannot write $oligomery_table\n";
    print OUT "Input\tUniProtID\tUniProtAC\tOligomeric template\tStoichiometry\tStructure title\tSeq id (%)\tSeq coverage\tOrganism\n";
    foreach my $line (@old_table2){
        chomp $line;
        my @A = split("\t", $line);

        # FIXME PATCH FOR EMPTY DATA
        for(my $i=0; $i<=$#A; $i++){
            if (!$A[$i]){
                $A[$i] = 'NA';
            }
        }

        my $organism = $A[7];
        if ($organism =~ m/^(\w+ \w+) \(/){
            $organism = $1;
        }

        my $input_ac = ${$hash_local_id_to_ac_ref}{$A[1]};
        print OUT "$A[0]\t$A[1]\t$input_ac\t$A[2]\t$A[4]\t$A[3]\t$A[5]\t$A[6]\t$organism\n";
    }
    close OUT;
}


sub sort_and_push{
    my $table_ref = $_[0];
    my $results_to_print_ref = $_[1];
    my $complex_number_ref = $_[2];
    my $hash_complex_id_to_number_ref = $_[3];
    # SORT BY COMPLEX INFORMATION: 1: $complex_size; 2: $best_average
    my @sorted_table = sort { $b->[1] <=> $a->[1] || $b->[2] <=> $a->[2] } @{$table_ref};

    for (my $i=0; $i<=$#sorted_table; $i++){
        $$complex_number_ref++;
        my $length = 3 - length($$complex_number_ref); # NOTE: HERE, ENSURING A 3-CHAR-LONG COMPLEX NUMBER
        my $formated_number = "0"x$length;
        $formated_number.=$$complex_number_ref;
        $sorted_table[$i][0] =~ s/cxx/c$formated_number (Max avg id = $sorted_table[$i][2]) (Max completeness = $sorted_table[$i][3]) (Max undetected = $sorted_table[$i][4])/;
        if ($sorted_table[$i][0] =~ m/\).+\((\w+) .+ a subcomplex/){
            ${$hash_complex_id_to_number_ref}{$1} = "c$formated_number";
        }
        if ($sorted_table[$i][0] =~ m/\n/){
            my @A = split("\n", $sorted_table[$i][0]);
            foreach my $element (@A){
                push(@{$results_to_print_ref}, $element);
            }
        }
        else{
            push(@{$results_to_print_ref}, $sorted_table[$i][0]); # NOTE: This prints all the lines: C001... + every line starting by a PDB
        }
    }
}


sub fill_results_tables{
    my $table_ref = $_[0];
    my $index_ref = $_[1];
    my $complex_information_ref = $_[2];
    ${$table_ref}[$$index_ref][0] = ${$complex_information_ref}[0]; #print_string
    ${$table_ref}[$$index_ref][1] = ${$complex_information_ref}[1]; #complex_size
    ${$table_ref}[$$index_ref][2] = ${$complex_information_ref}[2]; #best_average
    ${$table_ref}[$$index_ref][3] = ${$complex_information_ref}[3]; #best_completeness
    ${$table_ref}[$$index_ref][4] = ${$complex_information_ref}[4]; #most_undetected
    $$index_ref++;
}


sub fill_with_PDBhtml{
    my $hash_6char_to_ac_ref = $_[0];
    my $hash_ac_to_id_ref = $_[1];
    my $hash_PDBhtml_ref = $_[2];
    my $arr_PDBhtml_ref = $_[3];


    foreach my $line (@{$arr_PDBhtml_ref}){
# 9MSI; A=P19614:ANP12_ZOAAM ; Zoarces americanus; TYPE III ANTIFREEZE PROTEIN ISOFORM HPLC 12 T18N; X-RAY DIFFRACTION; 2.6; 1; Monomer > A; ; ;
        unless ($line =~ m/STATUS OBSOLETE/){
            my @A = split(';', $line);
            my $pdb4char = $A[0];
            ${$hash_PDBhtml_ref}{$pdb4char} = $line;
        
            my @B = split(' ', $A[1]);
            foreach my $chain (@B){ # e.g. A=P19614:ANP12_ZOAAM
                my @C = split('=', $chain);
                my @chain_names = split(',', $C[0]);
                foreach my $chain_name (@chain_names){
                    my @D = split(':', $C[1]);
                    my $ac = $D[0];
                    my $id = $D[1];

                    ${$hash_6char_to_ac_ref}{"$pdb4char\_$chain_name"} = $ac;
                    ${$hash_ac_to_id_ref}{$ac} = $id;

                    # XXX This should not happen (otherwise, it means that something's wrong with the PDBhtml database)
#                    if (!$ac){die "\e[1;31mDATABASE FATAL ERROR:\e[0m $pdb4char\_$chain_name has no AC!\n";}
#                    if (!$id){die "\e[1;31mDATABASE FATAL ERROR:\e[0m $pdb4char\_$chain_name has no ID!\n";}
                }
            }
        }
    }
}


sub get_ac_column{
    my @input_file = @{$_[0]};
    my @input_ac;
    foreach my $line (@input_file){
        chomp $line;
        my ($ac, $id) = split("\t", $line);
        push(@input_ac, $ac);
    }

    return @input_ac;
}


sub get_id_column{
    my @input_file = @{$_[0]};
    my @input_id;
    foreach my $line (@input_file){
        chomp $line;
        my ($ac, $id) = split("\t", $line);
        push(@input_id, $id);
    }

    return @input_id;
}


sub find_redundant_groups{
    my %redundant_groups;

    my $hash_complex_id_to_PDBlist_ref = $_[0];
    
    foreach my $groupA (sort keys %{$hash_complex_id_to_PDBlist_ref}){
        my @A = split('_', $groupA);
        foreach my $groupB (sort keys %{$hash_complex_id_to_PDBlist_ref}){
            my @B = split('_', $groupB);
            my $also_in_B = 0;
            foreach my $itemA (@A){
                foreach my $itemB (@B){
                    if ($itemA eq $itemB){
                        $also_in_B++;
                    }
                }
            }
            # LITTERALLY: IF ALL MEMBERS OF THE GROUP_A ARE ALSO IN GROUP_B, AND GROUP_B LARGER THAN GROUP_A
            if ($also_in_B == $#A+1 and $#A < $#B){
                push(@{$redundant_groups{$groupB}}, $groupA); # NOTE: THIS MEANS THAT GROUP_A IS A SUBCOMPLEX OF GROUP_B
            }
            if ($also_in_B > $#A+1){
                die "IMPOSSIBLE!!\n"; # FIXME to remove
            }
        }
    }
    
    return %redundant_groups;
}


sub average{
    my @set = @{$_[0]};
    my $len = $#set+1;
    my $total = 0;

    foreach my $value (@set){
        $total+=$value;
    }

    my $average = $total/$len;
    $average = sprintf("%.1f", $average);

    return $average;
}


sub fill_hashes{
    my $pdb6char = $_[0];
    my $hash_pdb4char_to_list_ref = $_[1];
    my $ac = $_[2];
    my $ac_template = $_[3];
    my $template_shared = $_[4];
    my $id_percent = $_[5];
    my $coverage = $_[6];

    my @A = split('_', $pdb6char);
    my $pdb4char = $A[0];

    unless (exists ${$hash_pdb4char_to_list_ref}{$pdb4char}){
        ${$hash_pdb4char_to_list_ref}{$pdb4char} = [];
    }

    my @ac_target_list = map $_->[ 0 ], @{${$hash_pdb4char_to_list_ref}{$pdb4char}};
    my @ac_template_list = map $_->[ 2 ], @{${$hash_pdb4char_to_list_ref}{$pdb4char}};

    my @occurrences_AC = grep(/^$ac$/, @ac_target_list);
    if ( $#occurrences_AC < 0 ){ # XXX By using another value than 0, you could treat homomultimers (but you don't want to!)
        my @occurrences_AC_template = grep(/^$ac_template$/, @ac_template_list);
        #if ( $#occurrences_AC_template < $template_shared ){ # XXX Here, a value of 0 prevents having several targets modeled by a single template
            push(@{${$hash_pdb4char_to_list_ref}{$pdb4char}}, [$ac, $pdb6char, $ac_template, $id_percent, $coverage]);
        #}
    }
}


sub readfile{
    my $filename = shift;
    open(IN, '<', $filename) or die "\e[1;31mError\e[0m while opening $filename:\n\t$!\n";
    my @file = <IN>;
    close IN;
    return @file;
}


sub get_information{
    #5AN9; A=P34113:RL3_DICDI B=Q54XI5:RL9_DICDI C=P22685:RLA0_DICDI D=Q54J50:RL12_DICDI E=Q54G86:RL23_DICDI F=Q54J69:RL10_DICDI G=Q54VN6:RL24_DICDI H=P14794:RL40_DICDI I=Q551M2:IF6_DICDI J=Q9Y3A5:SBDS_HUMAN ;
    #Dictyostelium discoideum; Mechanism of eIF6 release from the nascent 60S ribosomal subunit; ELECTRON MICROSCOPY; 3.3; 1; Hetero 10-mer > ABCDEFGHIJ; ; N ;

    my $template = $_[0]; # 4 char

    if (!${$_[1]}{$template}){
        return 0;
    }

    my @data = split(';', ${$_[1]}{$template});
    for (my $i = 0; $i <= $#data; $i++){
        $data[$i] =~ s/^ //;
        $data[$i] =~ s/ $//;
    }

    my $proteins = $data[1];
    my $organism = $data[2];
    my $title = $data[3];
    my $method = $data[4];
    my $resolution = $data[5];
    my $assembly = $data[6];
    my $stochio = $data[7];
    my $pseudo_stochio = $data[8];
    my $non_proteins = $data[9];

    return ($proteins, $organism, $title, $method, $resolution, $assembly, $stochio, $pseudo_stochio, $non_proteins);
}


sub handle_errors{
    my $template_shared = shift;
    my $resultsHH = shift;

    unless (-e $resultsHH and -f $resultsHH){
        die "\e[31mError: $0: --results_hh: file $resultsHH not found or empty\e[0m\n";
    }

    if ($template_shared < 0){
        die "\e[31mError: $0: --template_shared value must be >0\e[0m\n";
    }
}


sub helper {
    print
      "\n",
      "\tOptions:\n",
      "\t--input            TSV file; column 0: UniProt AC; column 1: UniProt ID;\n",
      "\t--pdbhtml          The PDBhtml database (CSV file)\n",
      "\t--template_shared  Allow N input proteins to share the same template (default = 1)\n",
      "\t--results_hh       TSV file (extension = .hhdb) representing the HHsearch results\n",
      "\t                   If not provided, activates the 3D-only mode\n",
      "\t--local            Run on local computer (rather than on cluster)\n",
      "\t--help             This help\n",
      "\n";
    exit 1;
}

main();
