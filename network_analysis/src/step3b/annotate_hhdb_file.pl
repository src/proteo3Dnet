#!/usr/bin/env perl

use strict;
use warnings;


sub fill_with_PDBhtml{
    open(IN, '<', $_[0]) or die "Error: $0: cannot read $_[0]: $!\n";

    while (my $line = <IN>){
        unless ($line =~ m/STATUS OBSOLETE/){
            my @A = split(';', $line);
            my $pdb4char = $A[0];
            my @B = split(' ', $A[1]);
            ${$_[3]}{$pdb4char} = $line;
        
            foreach my $chain (@B){ # e.g. A=P19614:ANP12_ZOAAM
                my @C = split('=', $chain);
                my @chain_names = split(',', $C[0]);
                foreach my $chain_name (@chain_names){
                    my @D = split(':', $C[1]);
                    my $ac = $D[0];
                    my $id = $D[1];

                    ${$_[1]}{"$pdb4char\_$chain_name"} = $ac;
                    ${$_[2]}{$ac} = $id;

                    # XXX This should not happen (otherwise, it means that something's wrong with the PDBhtml database)
#                    if (!$ac){die "\e[1;31mDATABASE FATAL ERROR:\e[0m $pdb4char\_$chain_name has no AC!\n";}
#                    if (!$id){die "\e[1;31mDATABASE FATAL ERROR:\e[0m $pdb4char\_$chain_name has no ID!\n";}
                }
            }
        }
    }

    close IN;
}


sub get_information{
    #5AN9; A=P34113:RL3_DICDI B=Q54XI5:RL9_DICDI C=P22685:RLA0_DICDI D=Q54J50:RL12_DICDI E=Q54G86:RL23_DICDI F=Q54J69:RL10_DICDI G=Q54VN6:RL24_DICDI H=P14794:RL40_DICDI I=Q551M2:IF6_DICDI J=Q9Y3A5:SBDS_HUMAN ;
    #Dictyostelium discoideum; Mechanism of eIF6 release from the nascent 60S ribosomal subunit; ELECTRON MICROSCOPY; 3.3; 1; Hetero 10-mer > ABCDEFGHIJ; ; N ;

    my $template = $_[0]; # 4 char

    if (!${$_[1]}{$template}){
        return 0;
    }

    my @data = split(';', ${$_[1]}{$template});
    for (my $i = 0; $i <= $#data; $i++){
        $data[$i] =~ s/^ //;
        $data[$i] =~ s/ $//;
    }

    my $proteins = $data[1];
    my $organism = $data[2];
    my $title = $data[3];
    my $method = $data[4];
    my $resolution = $data[5];
    my $assembly = $data[6];
    my $stochio = $data[7];
    my $pseudo_stochio = $data[8];
    my $non_proteins = $data[9];

    return ($proteins, $organism, $title, $method, $resolution, $assembly, $stochio, $pseudo_stochio, $non_proteins);
}

sub main{
    if ($#ARGV < 1){
        die "Error: $0: Missing arguments\n";
    }

    my $input_filename = $ARGV[0];
    my $PDBhtml_file = $ARGV[1];
    my $output_filename = 'annotated.hhdb';

    open(OUT, '>', $output_filename) or die "Error: $0: cannot create $output_filename: $!\n";

    my %hash_6char_to_ac;
    my %hash_ac_to_id;
    my %hash_PDBhtml;
    fill_with_PDBhtml($PDBhtml_file, \%hash_6char_to_ac, \%hash_ac_to_id, \%hash_PDBhtml);

    open (IN, '<', $input_filename) or die "Error: $0: cannot read $input_filename: $!\n";
    while (my $line = <IN>){
        chomp $line;
        my ($ac, $pdb6char, $proba, $id_percent, $coverage) = split("\t", $line);
        my ($pdb4char, $chain) = split("_", $pdb6char);
        my ($proteins, $organism, $title, $method, $resolution, $assembly, $stochio, $pseudo_stochio, $non_proteins) = get_information($pdb4char, \%hash_PDBhtml);

        if (exists $hash_PDBhtml{$pdb4char}){
            my $newline = "$line\t$organism\t$title\t$method\t$resolution\t$stochio";
            print OUT "$newline\n";
        }
        else{
            print "\e[33mWarning: $0: no entry for $pdb4char in PDBhtml database\e[0m\n";
        }
    }
    close IN;
    close OUT;
}

main();
