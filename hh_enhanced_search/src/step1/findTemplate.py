#!/usr/bin/env python
'''
Created 27 Nov 2017
@author: Jessica Andreani
This script looks for a suitable PDB template for a given fasta sequence
and creates the pairwise alignment between the query and the template.

Modifications tagged "GP2018", "GP2019"
'''

import os
import sys
import tempfile
import re
import shutil
import math
import commands
#import tools # Was actually not used from the beginning #GP2018

import hhr2html #FIXME THERE ARE DIFFERENCES WITH OTHER VERSIONS...

import ConfigParser
configfile = os.path.abspath("/home/step1/findTemplate.ini") #GP2018
config = ConfigParser.ConfigParser()
config.read(configfile)

# Directory where temporary files should be stored
TMP_DIR = config.get("General","TMP_DIR")

# Tools
BLASTP = config.get("General","BLASTP")    
HHBLITS = config.get("General","HHBLITS")
ADDSS = config.get("General","ADDSS")    
HHSEARCH = config.get("General","HHSEARCH")
#MAFFT = config.get("General","MAFFT")    # Was actually not used #GP2018

# Databases
#BLASTDB = config.get("General","BLASTDB") # GP2018
HHBLITS_UNIPROT20 = config.get("General","HHBLITS_UNIPROT20")
HHSEARCH_PDB = config.get("General","HHSEARCH_PDB")
DFLT_PDB_BANK = config.get("General","DFLT_PDB_BANK")
#MMCIF_PDB_BANK = config.get("General","MMCIF_PDB_BANK") #GP2019

# Thresholds and parameters
BLAST_EVAL = eval(config.get("General","BLAST_EVAL")) # = 0.1
BLAST_ID = eval(config.get("General","BLAST_ID")) #= 35.0
BLAST_COV = eval(config.get("General","BLAST_COV")) #= 50.0
HHSUITE_NCPU = config.get("General","HHSUITE_NCPU") #= "3"
HHSUITE_VMODE = config.get("General","HHSUITE_VMODE") #= "0"
HHSUITE_ALTERNATIVE_ALI = config.get("General","HHSUITE_ALTERNATIVE_ALI") #= "1"

HHBLITS_NITE = config.get("General","HHBLITS_NITE") #= "1"
HHBLITS_COV = config.get("General","HHBLITS_COV") #= "20"
HHSEARCH_PROB = eval(config.get("General","HHSEARCH_PROB")) #= 95.0

RESOLUTION_TRESH_AUTOMODE = eval(config.get("General","RESOLUTION_TRESH_AUTOMODE"))
RESOLUTION_TRESH_MANUALMODE = eval(config.get("General","RESOLUTION_TRESH_MANUALMODE"))

LINE_LENGTH_IN_PAIRFASTA = eval(config.get("General","LINE_LENGTH_IN_PAIRFASTA"))
KEEP_NTEMPLATE_IN_HHR = eval(config.get("General","KEEP_NTEMPLATE_IN_HHR"))

def writeTemplate(hit, template, mmCIF_and_obsolete_search):
    """ Write template chain into file.
    For the InterEvDock2 server, the urllib part
    should probably be replaced by direct retrieval
    of template coordinates from the local PDB (?)
    """
    
    def getPDBChain(inpdb,chain,outpdb):
        if not os.path.exists(inpdb):
            return False
        FHi = open(inpdb, 'r')
        FHo = open(outpdb, 'w')
        isOneLineWritten = False
        for line in FHi:
            if len(line) <= 4:
                continue
            if line[0:4]!="ATOM":
                continue
            if line[21]==chain:
                isOneLineWritten = True
                FHo.write(line)
        FHi.close()
        FHo.close()
        return isOneLineWritten

    try:
        pdb,chain = hit.split("_")
    except ValueError:
        print "Unable to extract chain from file %s" %(hit)
        return False

    if mmCIF_and_obsolete_search: #GP2019 on-the-fly processing of mmCIF and obsolete files
        twoMidChars = pdb[1:3]
        os.popen('wget -P '+TMP_DIR+'/'+' https://files.rcsb.org/pub/pdb/compatible/pdb_bundle/'+twoMidChars.lower()+'/'+pdb.lower()+'/'+pdb.lower()+'-pdb-bundle.tar.gz')

        # IF WAS A MMCIF
        if os.path.isfile(TMP_DIR+'/'+pdb.lower()+'-pdb-bundle.tar.gz'):
            mapfile = os.popen('tar xfO '+TMP_DIR+'/'+pdb.lower()+'-pdb-bundle.tar.gz '+pdb.lower()+'-chain-id-mapping.txt').read()
            mapfile_arr = mapfile.split('\n')
     
            one_char_chain = ''
            bundle = ''
            myregex = r'^\s+(\w)\s+'+re.escape(chain)
     
            for line in mapfile_arr:
                match1 = re.search(r'^(\d.+):',line)
                if match1:
                    bundle = match1.group(1)
            
                match2 = re.search(myregex,line)
                if match2:
                    one_char_chain = match2.group(1)
                    break;
            
            if one_char_chain:
                pdbfile = os.popen('tar xfO '+TMP_DIR+'/'+pdb.lower()+'-pdb-bundle.tar.gz '+bundle).read()
                pdbfile_arr = pdbfile.split('\n')
                OUT = open(template, 'w')
                is_one_line_written = False
                for line in pdbfile_arr:
                    if len(line) <= 4:
                        continue
                    if line[0:4]!="ATOM":
                        continue
                    if line[21]==one_char_chain:
                        is_one_line_written = True
                        OUT.write(line)
                OUT.close()
                return is_one_line_written
            else:
                print("Chain %s does not exist in %s" % (chain, pdb))
                return False
        # IF IT WAS NOT A MMCIF, BUT AN OBSOLETE
        else:
            os.popen('wget -P '+TMP_DIR+'/'+' https://files.rcsb.org/download/'+pdb+'.pdb.gz') # NOTE: Here, either PDB lower- or uppercase works
            if os.path.isfile(TMP_DIR+'/'+pdb+'.pdb.gz'):
                pdbfile = TMP_DIR + "/" + pdb + ".pdb"
                os.system("gunzip -f %s.gz" % pdbfile)
                isFileWritten = getPDBChain(pdbfile,chain,template)
                return isFileWritten
            else:
                print("Cannot download PDB file: %s" % pdb)
                return False
 
    else:
        pdbfile = TMP_DIR + "/" + pdb + ".pdb"
        try:
            shutil.copy2(DFLT_PDB_BANK + "/pdb" + pdb.lower() + ".ent.gz", pdbfile + ".gz")
        except IOError, e:
            print "Unable to copy file. %s" % e
            return False
        os.system("gunzip -f %s.gz" % pdbfile)
        isFileWritten = getPDBChain(pdbfile,chain,template)
        return isFileWritten
 
def getFirstResIndex(pdb):
    fin = open(pdb,'r')
    for line in fin:
        if line[0:4]!="ATOM":
            continue
        try:
            FirstResIndex = int(line[22:26].strip())
        except:
            FirstResIndex = 1
        break
    fin.close()
    return FirstResIndex        
    

# Since Blast was actually not used from the beginning, there was no reason to keep these functions #GP2018
#def parseBlast(blastfile, fastalen, verbose):
#    """ Parse blast output file
#    """
#    FHi = open(blastfile, 'r')
#    foundHit = False
#    foundAli = False
#    #queryseq = ""
#    #templateseq = ""
#    
#    for line in FHi:
#        if not foundHit:
#            m = re.search(r"(pdb\|\w{4}\|\w)\s+.*\s+(\d+.\d)\s+((\d+.\d+)|(\d+e-?\d+))",line)
#            if m:
#                foundHit = True
#                hit = m.group(1)
#                score = m.group(2)
#                evalue = float(m.group(3))
#                print(evalue)
#                if evalue > BLAST_EVAL:
#                    foundHit = False
#                    break
#        elif not foundAli:
#            m = re.search(r">(pdb\|\w{4}\|\w)",line)
#            if m:
#                if m.group(1) == hit:
#                    foundAli = True
#                else:
#                    foundAli = False
#        else:
#            m = re.search(r"\s*Identities = \d+/(\d+) \((\d+)\%\), Positives = \d+/\d+ \((\d+)\%\)",line)
#            if m:
#                id = float(m.group(2))
#                cov = float(m.group(1))/float(fastalen)*100.0
#                if verbose:
#                    print(id)
#                    print(cov)
#                if id < BLAST_ID or cov < BLAST_COV:
#                    foundHit = False
#                # we only consider the first hit
#                break
#                
#            
#            # m = re.search(r"Query\s+\d+\s+(\w+)\s+\d+",line)
#            # if m:
#            #     queryseq += m.group(1)
#            # m = re.search(r"Sbjct\s+\d+\s+(\w+)\s+\d+",line)
#            # if m:
#            #     templateseq += m.group(1)
#            
#    FHi.close()
#
#    if foundHit:
#        return hit.split("|")[1].lower()+"_"+hit.split("|")[2]
#    return None
#        
#
#def searchBlast(fasta, verbose, cleanup, blast_out):
#    """ Search for a template with blastp
#    """
#    cmd = BLASTP + " " +\
#          "-query " + fasta + " " +\
#          "-db " + BLASTDB + " " +\
#          "-out " + blast_out
#
#    if verbose:
#        print(cmd)
#    os.system(cmd)
#
#    fastalen = 0
#    FHi = open(fasta, 'r')
#    for line in FHi:
#        if line[0]==">":
#            continue
#        fastalen += len(line.strip())
#    FHi.close()
#                    
#    hit = parseBlast(blast_out, fastalen, verbose)
#    return hit
#
#
#def realignBlastHit(fasta, hit, template, alignment, verbose, cleanup):
#    """ Realign blastp hit with query using MAFFT
#    and output alignment to file
#    """
#
#    fastaseq = ""
#    FHi = open(fasta, 'r')
#    for line in FHi:
#        if line[0]==">":
#            header = line
#        else:
#            fastaseq += line.strip()
#    FHi.close()
#
#    pdb,chain = hit.split("_")
#    fd, ftmp_path = tempfile.mkstemp(dir=TMP_DIR)
#    os.close(fd)
#    tools.PDB_2_fasta(template, ftmp_path, chain=chain)
#
#    fd, ftmp_path2 = tempfile.mkstemp(dir=TMP_DIR)
#    os.close(fd)
#    FHo = open(ftmp_path2, 'w')
#    FHi = open(ftmp_path, 'r')
#    FHo.write(header)
#    FHo.write("%s\n"%fastaseq)
#    FHo.write(">%s\n"%hit)
#    for line in FHi:
#        if line[0]!=">":
#            FHo.write(line)
#    FHi.close()
#    FHo.close()
#
#    cmd = MAFFT + " --genafpair --maxiterate 1000 --quiet " +\
#          ftmp_path2 + " > " +\
#          alignment
#
#    execmode = 1
#    if verbose:
#        FHi = open(ftmp_path2,'r')
#        print(FHi.readlines())
#        FHi.close()
#        print(cmd)
#        execmode = 2
#    tools.execute(cmd,mode=execmode)
#
#    if cleanup:
#        if os.path.isfile(ftmp_path):
#            os.remove(ftmp_path)
#        if os.path.isfile(ftmp_path2):
#            os.remove(ftmp_path2)
        
def parseFasta(fasta_file):
    header = ""
    sequence = ""
    fin = open(fasta_file,'r')
    line = fin.readline()
    while line:
        if line[0] == ">":
            header = line.strip()
        else:
            sequence += line.strip()
        line = fin.readline()
    return header,sequence

# Commented because not used #GP2019
#def make_dic_delim(dic_hits,Npdb_to_analyze=20):
#    dic_structural_delim = {}
#    fd, ftmp_path = tempfile.mkstemp(dir=TMP_DIR)
#    os.close(fd)
#    counter = 1
#    for hit in dic_hits["list_proba_max_new_sorted"] + dic_hits["list_proba_notmax"]:
#        if not dic_hits[hit]["Valid"]:
#            continue
#        start = int(dic_hits[hit]['TemplateHMM'].split('-')[0])
#        end   = int(dic_hits[hit]['TemplateHMM'].split('-')[1])
#        writeTemplate(dic_hits[hit]['PDBname'], ftmp_path)
#        first_residue = getFirstResIndex(ftmp_path)
#        dic_structural_delim[hit]="%s-%s"%(start+first_residue-1,end+first_residue-1)
#        counter += 1
#        if counter > Npdb_to_analyze:
#            break
#    if os.path.isfile(ftmp_path):
#        os.remove(ftmp_path)
#    return dic_structural_delim

def parse_multitemplate_HHsearch_use_resolution(hhsearchfile, input_fasta, out_pairfile = None, out_hhfile = None, auto_mode = True):
    """ Parse HHsearch output and retrieve template and alignment
    returns hit, queryseq, templateseq 
    """
    FHi = open(hhsearchfile, 'r')
    
    # new_hhr is a file containing only the sorted and selected hits presented 
    #   using the hhsearch output format to be read by the checkpoint 1 page  
    new_hhr_header = ""
    
    line = FHi.readline()
    new_hhr_header += line
    while line:
        line = FHi.readline()
        #if re.search(r"\s+(\d+)\s+(\w{4}_\w)\s.{23}\s\s?(\d+.\d).*",line):
        if re.search(r"\s*(\d+)\s+(\d\w{3}_\w{1,4})\s.{20,23}\s\s?(\d+.\d).*",line): # GP2020 XXX NEW REGEX
            # parse lines such as  "10 3J2S_B Coagulation factor VIII 100.0  3E-100  6E-105  962.2  58.8  642 1710-2351    1-642 (642)"
            break
        new_hhr_header += line
        
    query_header, query_sequence = parseFasta(input_fasta)
    
    # dic_hits are indexed by the original hit numbers
    # sorted and short lists of the hits we keep will refer to these index
    dic_hits = {}
    dic_hits["list_proba_max"] = []
    dic_hits["list_proba_notmax"] = []
    dic_hits["list_proba_max_new_sorted"] = []
    dic_hits["order"] = []
    
    """
    Read the header section
    """
    while line:
        hits = []
        probabilities = []
        #hit = re.search(r"\s*(\d+)\s+(\w{4}_\w)\s.{23}\s\s?(\d+.\d).{34}\s\s*?(\d+-\d+)\s+(\d+-\d+)\s*\((\d+)\)",line)
        hit = re.search(r"\s*(\d+)\s+(\d\w{3}_\w{1,4})\s.{20,23}\s\s?(\d+.\d).{34}\s\s*?(\d+-\d+)\s+(\d+-\d+)\s*\((\d+)\)",line) # GP2020 XXX NEW REGEX
        # some chain names now contain two characters for very large complexes, e.g. 5WLC_SO obtained as hit#10 using query sequence 2ANN_A
        # parse lines such as  " 10 3J2S_B Coagulation factor VIII 100.0  3E-100  6E-105  962.2  58.8  642 1710-2351    1-642 (642)"
        #                      "  1 5KHU_Q Anaphase-promoting comp 100.0 4.1E-58 9.6E-63  503.9  41.8  450    1-450   601-1050(1050)"
        # [1]:Hit index, [2]:PDB name, [3]:Proba, [4]:Query HMM, [5]:Template HMM, [6]:Template Length

        try:
            test = hit.group(1)
        except: 
            break
            #print "Problem parsing the HHSEARCH output file. Might be a problem with the format"
            #return None
        
        #print hit.group(1),hit.group(2),hit.group(3),hit.group(4),hit.group(5),hit.group(6)
        
        list_items   = ['PDBname', 'Proba', 'QueryHMM', 'TemplateHMM', 'TemplateLength']
        list_matches = [hit.group(2),float(hit.group(3)),hit.group(4),hit.group(5),hit.group(6)]
        hit_index = hit.group(1)
        dic_hits[hit_index] = dict(zip(list_items,list_matches))
        dic_hits[hit_index]["Valid"] = True 
        dic_hits[hit_index]["Header"] = line
        if dic_hits[hit_index]["Proba"] < HHSEARCH_PROB:
            break
        dic_hits["order"].append(hit_index)
        if hit_index == "1":
            max_proba = dic_hits["1"]["Proba"]
        if dic_hits[hit_index]["Proba"] == max_proba:
            dic_hits["list_proba_max"].append(hit_index)
        else:
            dic_hits["list_proba_notmax"].append(hit_index)

        line = FHi.readline()
        if line[:2]=="No":
            break
    ####    
    """
    Read the alignment section
    """
    while line:
        if line[:2]=="No":
            break
        line = FHi.readline()
    current_hit = "1"


    #while not current_hit in dic_hits: #GP2019 When HHsearch fails (e.g. ALMS1_HUMAN)
    #    current_hit = str(int(current_hit)+1)
    #if int(current_hit) > 500:
    #    return None, None, None


    dic_hits[current_hit]["AliBlock"] = ""
    while line:
        line = FHi.readline()         
        if line[:2]=="No":
            hit_index = line.split()[1]
            current_hit = hit_index
            if not current_hit in dic_hits:
                break
            dic_hits[current_hit]["AliBlock"] = ""
        else:
            dic_hits[current_hit]["AliBlock"] += line  

    for hit in dic_hits["order"]:
        ali = dic_hits[hit]["AliBlock"]
        patQ    = re.compile(r"^Q\s(?!Consensus)(?!ss_pred)(?!ss_dssp)\S+\s+\d*?\s(\S+)",re.M)
        patT    = re.compile(r"^T\s(?!Consensus)(?!ss_pred)(?!ss_dssp)\S+\s+\d*?\s(\S+)",re.M)
        patDSSP = re.compile(r"^T\sss_dssp\s+\d*?\s(\S+)",re.M)
        patId   = re.compile(r"Identities=(\d+)\%",re.M)
        patEval = re.compile(r"E\Svalue=(\S+)",re.M)
        patRes  = re.compile(r";\s(\S+)\s\{.+\}",re.M)
        patHead = re.compile(r"^>(.+\})",re.M)
        
        listQali    = patQ.findall(ali)
        listTali    = patT.findall(ali)
        listDSSPali = patDSSP.findall(ali)
        listId      = patId.findall(ali) 
        listEval    = patEval.findall(ali)
        listRes     = patRes.findall(ali)
        listHead    = patHead.findall(ali)
        
        dic_hits[hit]["Qali"] = "".join(listQali)
        dic_hits[hit]["TaliSeq"] = "".join(listTali)
        dic_hits[hit]["Theader"] = listHead[0]
        
        # DSSP is not always present. For instance, in case, PDB is only made of CA.
        # We remove structures without DSSP calculated
        if len(listDSSPali) == 0:
            dic_hits[hit]["Valid"] = False
            #os.system("echo '%s' >> NO_DSSP.list" % hit)
            # XXX Prendre ss_seq dans la base => aligner avec MAFFT (!!!!!!!! taliseq doit rester fixe!!!!!!!!!!!!!!)
        else:
            dic_hits[hit]["DSSPali"] = "".join(listDSSPali)
            #os.system("echo '%s' > %s.dsspali" % (dic_hits[hit]["DSSPali"], hit))
            #os.system("echo '%s' > %s.taliseq" % (dic_hits[hit]["TaliSeq"], hit))
        
        if len(listId) == 0:
            dic_hits[hit]["Valid"] = False
        else:
            dic_hits[hit]["Identities"] = listId[0]
        
        if len(listEval) == 0:
            dic_hits[hit]["Valid"] = False
        else:
            dic_hits[hit]["E-value"] = listEval[0]
            
        if len(listRes) == 0:
            dic_hits[hit]["Valid"] = False
        else:
            dic_hits[hit]["str_Resol"] = listRes[0]
            if listRes[0][-1] == "A":
                try:
                    dic_hits[hit]["float_Resol"] = float(listRes[0][:-1]) # If it finishes by Angtrom, we assume it is a float
                    if auto_mode:
                        # In auto-mode, we don't want to build models above 7A
                        if dic_hits[hit]["float_Resol"] > RESOLUTION_TRESH_AUTOMODE: # default 7
                            dic_hits[hit]["Valid"] = False
                    else:
                        # In manual selection mode, user can inspect the interest of using a low resolution structure
                        # But we remove everything which might be over 10
                        if dic_hits[hit]["float_Resol"] > RESOLUTION_TRESH_MANUALMODE: # default 10
                            dic_hits[hit]["Valid"] = False
                except: 
                    dic_hits[hit]["float_Resol"] = 100.0 # Unexpected but for security.
                    dic_hits[hit]["Valid"] = False
            else:
                dic_hits[hit]["float_Resol"] = 100.0 # This is for instance assigned for NMR structures
        
        """
        Here we generate sequences aligned but arranged for subsequent modelling
        so that 1) We can recover the correct indexes for the query sequence
                2) The sequence of the template matches better the sequence of 
        """
        if dic_hits[hit]["Valid"]:
            
            # Pad the QUERY sequence, so that it is full
            NterIndex, CterIndex = dic_hits[hit]["QueryHMM"].split('-')
            padNter = int(NterIndex) - 1
            padCter = len(query_sequence) - int(CterIndex)
            dic_hits[hit]["PaddedQali"] = ""
            dic_hits[hit]["PaddedQali"] += query_sequence[:int(NterIndex)-1]
            dic_hits[hit]["PaddedQali"] += dic_hits[hit]["Qali"]
            dic_hits[hit]["PaddedQali"] += query_sequence[int(CterIndex):]
            
            # We create an alignement for the template part 
            # in which we fuse the Template sequence and the DSSP 
            # (indicating the AA truly observable in the structure)
            dic_hits[hit]["PaddedTaliStruct"] = ""
            for addgap in range(padNter):
                dic_hits[hit]["PaddedTaliStruct"] += "-"
            for ii,AA in enumerate(dic_hits[hit]["TaliSeq"]):
                if dic_hits[hit]["DSSPali"][ii] == "-":
                    dic_hits[hit]["PaddedTaliStruct"] += "-"
                else:
                    dic_hits[hit]["PaddedTaliStruct"] += AA
            for addgap in range(padCter):
                dic_hits[hit]["PaddedTaliStruct"] += "-"
            print "###",hit
            print dic_hits[hit]["PaddedQali"]
            print dic_hits[hit]["PaddedTaliStruct"]
            print "#"
            print dic_hits[hit]["Qali"]
            print dic_hits[hit]["TaliSeq"]
            
    ####
    
    
    """
    Sort the matches of max identity favouring the highest identity first, then evalue and resolution as long as above 7A
    """
    list_hit_to_sort = []
    list_add_after = [] # to keep out the low resolutions from the best sorted
    for hit_index in dic_hits["list_proba_max"]:
        if not dic_hits[hit_index]["Valid"]:
            continue
        if dic_hits[hit_index]["float_Resol"] > RESOLUTION_TRESH_AUTOMODE and dic_hits[hit_index]["float_Resol"] < 100:
            # We remove low resolution but not nmr from the sorting
            list_add_after.append(hit_index)
            continue
        list_hit_to_sort.append([100.0-float(dic_hits[hit_index]["Identities"]),float(dic_hits[hit_index]["E-value"]),dic_hits[hit_index]["float_Resol"],hit_index])
    list_hit_to_sort.sort()
    dic_hits["list_proba_max_new_sorted"] = [x[3] for x in list_hit_to_sort] + list_add_after
    
    """
    Check the existence of the PDB template and Recover the first residue index of the template
    """
    hit_new_index = 1
    fd, ftmp_path = tempfile.mkstemp(dir=TMP_DIR)
    os.close(fd)
    for hit in dic_hits["list_proba_max_new_sorted"] + dic_hits["list_proba_notmax"]:
        if not dic_hits[hit]["Valid"]:
            continue
        start = int(dic_hits[hit]['TemplateHMM'].split('-')[0])
        end   = int(dic_hits[hit]['TemplateHMM'].split('-')[1])

        mmCIF_and_obsolete_search = 0 #GP2019

        isPDBexist = writeTemplate(dic_hits[hit]['PDBname'], ftmp_path, mmCIF_and_obsolete_search)
        if not isPDBexist:
            mmCIF_and_obsolete_search = 1
            isPDBexist = writeTemplate(dic_hits[hit]['PDBname'], ftmp_path, mmCIF_and_obsolete_search)

        if not isPDBexist:
            dic_hits[hit]["Valid"] = False
            continue
        hit_new_index += 1
        if hit_new_index > KEEP_NTEMPLATE_IN_HHR:
            break
        first_residue = getFirstResIndex(ftmp_path)
        # getFirstResIndex returns 1 in case of any problem regarding the pdb read
        dic_hits[hit]['TemplateHMMStrucDelim'] = "%s-%s"%(start+first_residue-1,end+first_residue-1)
    if os.path.isfile(ftmp_path):
        os.remove(ftmp_path)

    """
    Extract match of high identity with proba not maximal due to profile divergence
    """    
    THRESHOLD_HIGH_IDENTITY = 70.0
    THRESHOLD_HIT_LENGTH = 50
    hit_new_index = 1
    dic_hits["list_high_identity_hit"] = []
    for hit in dic_hits["list_proba_max_new_sorted"] + dic_hits["list_proba_notmax"]:
        if not dic_hits[hit]["Valid"]:
            continue
        if float(dic_hits[hit]["Identities"]) >= THRESHOLD_HIGH_IDENTITY:
            start,end = dic_hits[hit]['QueryHMM'].split('-')
            hit_length = eval(end) - eval(start)
            if hit_length >  THRESHOLD_HIT_LENGTH:
                list_value = [100. - float(dic_hits[hit]["Identities"]), float(dic_hits[hit]["E-value"]), hit]
                dic_hits["list_high_identity_hit"].append(list_value)
                
    dic_hits["best_hit_from_identity"] = []
    bestHitExists = False
    if len(dic_hits["list_high_identity_hit"]) > 0:
        dic_hits["list_high_identity_hit"].sort()
        fd, ftmp_path = tempfile.mkstemp(dir=TMP_DIR)
        os.close(fd)
        for best_hit_list in dic_hits["list_high_identity_hit"]:
            # Here we run the same check PDB as before if we need to add this additional entry to the list of best hits
            best_hit = best_hit_list[2]
            if best_hit not in dic_hits["list_proba_max_new_sorted"] + dic_hits["list_proba_notmax"]:

                mmCIF_and_obsolete_search = 0 #GP2019

                isPDBexist = writeTemplate(dic_hits[best_hit]['PDBname'], ftmp_path, mmCIF_and_obsolete_search)
                if not isPDBexist:
                    mmCIF_and_obsolete_search = 1
                    isPDBexist = writeTemplate(dic_hits[best_hit]['PDBname'], ftmp_path, mmCIF_and_obsolete_search)

                if not isPDBexist:
                    dic_hits[best_hit]["Valid"] = False
                    continue
                first_residue = getFirstResIndex(ftmp_path)
                dic_hits[best_hit]['TemplateHMMStrucDelim'] = "%s-%s"%(start+first_residue-1,end+first_residue-1)
            bestHitExists = True
            dic_hits["best_hit_from_identity"].append(best_hit)
            if best_hit in dic_hits["list_proba_max_new_sorted"]:
                dic_hits["list_proba_max_new_sorted"].remove(best_hit)
            if best_hit in dic_hits["list_proba_notmax"]:
                dic_hits["list_proba_notmax"].remove(best_hit)
            if bestHitExists:
                break
        if os.path.isfile(ftmp_path):
            os.remove(ftmp_path)
    
    ####
    
    # Here is the list of templates that will finally be written to output files and displayed to users
    list_hits_to_write = dic_hits["best_hit_from_identity"] + dic_hits["list_proba_max_new_sorted"] + dic_hits["list_proba_notmax"]
    
    """
    Generate the parwise ali file output
    """
    if out_pairfile: # always known in the server mode. Option can be destroyed later
        fout_pair = open(out_pairfile,"w")
        hit_new_index = 1
        for hit in list_hits_to_write:
            if not dic_hits[hit]["Valid"]:
                continue            
            fout_pair.write("# No %d\n"%(hit_new_index))
            fout_pair.write("%s; Delimitation=%s\n"%(query_header,dic_hits[hit]['QueryHMM']))
            fout_pair.write(dic_hits[hit]["PaddedQali"]+"\n")
            fout_pair.write(">%s:AUTOPDB"%(dic_hits[hit]['PDBname']))
            fout_pair.write("; Probability=%.1f"%(dic_hits[hit]['Proba']))
            fout_pair.write("; Identities=%s"%(dic_hits[hit]['Identities'])+"%")
            fout_pair.write("; Resolution=%s"%(dic_hits[hit]['str_Resol']))
            fout_pair.write("; Delimitation=%s\n"%(dic_hits[hit]['TemplateHMM']))
            fout_pair.write(dic_hits[hit]['PaddedTaliStruct']+"\n\n")
            hit_new_index += 1
        fout_pair.close()
        
    """
    Generate a hhsearch output, containing both pair fasta and pair profiles alignments
    """
    if out_hhfile:
        
        fout_hh = open(out_hhfile,"w")
        
        hit_new_index = 1
        
        # First initial standard lines
        fout_hh.write(new_hhr_header)
        
        # Write the summary table
        for hit in list_hits_to_write:
            if not dic_hits[hit]["Valid"]:
                continue  
            length_index = len(str(hit_new_index))
            if length_index <= 3:
                str_hit_new_index = "%3d "%(hit_new_index)
            else:
                str_hit_new_index = "%4d "%(hit_new_index)
            new_summary_line = dic_hits[hit]["Header"][4:].strip()+"\n"
            fout_hh.write(str_hit_new_index + new_summary_line)
            hit_new_index += 1
            if hit_new_index > KEEP_NTEMPLATE_IN_HHR:
                break
        fout_hh.write("\n")
        
        # Write the pair fasta alignments section
        hit_new_index = 1
        for hit in list_hits_to_write:
            if not dic_hits[hit]["Valid"]:
                continue
            fout_hh.write("No %d\n"%(hit_new_index))
            # Write the header of the query
            fout_hh.write("%s; MatchedRegionQuery=%s\n"%(query_header,dic_hits[hit]['QueryHMM']))
            # Write the sequence of the query 
            bloc_padded_ali = ""
            for ii,AA in enumerate(dic_hits[hit]["PaddedQali"]):
                if ii > 0 and ii < len(dic_hits[hit]["PaddedQali"])-1 and ii%LINE_LENGTH_IN_PAIRFASTA == 0:
                    bloc_padded_ali += "\n"
                bloc_padded_ali += AA
            fout_hh.write(bloc_padded_ali+"\n")
            # Write the header of the template
            fout_hh.write(">%s:AUTOPDB"%(dic_hits[hit]['PDBname']))
            fout_hh.write("; Probability=%.1f"%(dic_hits[hit]['Proba']))
            fout_hh.write("; Identities=%s"%(dic_hits[hit]['Identities'])+"%")
            fout_hh.write("; Resolution=%s"%(dic_hits[hit]['str_Resol']))
            # Try to retrieve the correct delimitation if possible
            try:
                fout_hh.write("; MatchedRegionTemplate=%s"%(dic_hits[hit]['TemplateHMMStrucDelim']))
            except KeyError:
                fout_hh.write("; MatchedRegionTemplate=%s"%(dic_hits[hit]['TemplateHMM']))
                
            # Because we write already the resolution in the header we don't want to see twice in the generic header of PDB database
            Theader_no_resol = re.sub("\s+(\S+?)\s+{"," {",dic_hits[hit]["Theader"])
            fout_hh.write("; %s\n"%(Theader_no_resol[7:]))
            # Write the sequence of the template
            bloc_padded_ali = ""
            for ii,AA in enumerate(dic_hits[hit]["PaddedTaliStruct"]):
                if ii > 0 and ii < len(dic_hits[hit]["PaddedTaliStruct"])-1 and ii%LINE_LENGTH_IN_PAIRFASTA == 0:
                    bloc_padded_ali += "\n"
                bloc_padded_ali += AA
            fout_hh.write(bloc_padded_ali+"\n")
            hit_new_index += 1
            if hit_new_index > KEEP_NTEMPLATE_IN_HHR:
                break
        fout_hh.write("Done fasta alignment\n") # tag important for hh2html.py  
        
        # Write the profile-profile alignments section
        hit_new_index = 1
        for hit in list_hits_to_write:
            if not dic_hits[hit]["Valid"]:
                continue
            fout_hh.write("No %d\n"%(hit_new_index))
            fout_hh.write(dic_hits[hit]["AliBlock"])
            hit_new_index += 1
            if hit_new_index > KEEP_NTEMPLATE_IN_HHR:
                break
        fout_hh.write("Done!\n")
        
        fout_hh.close()
    FHi.close()
    
    """
    Send the template and its alignment so that it can be written prior to the next stage
    """
    if auto_mode:
        try:
            for hit in list_hits_to_write:
                if dic_hits[hit]["Valid"]:
                    best_hit = hit
                    break
            #best_hit = list_hits_to_write[0]
            lowcase_bestpdb       = dic_hits[best_hit]['PDBname'].split("_")[0].lower()
            lowcase_bestpdb_chain = dic_hits[best_hit]['PDBname'].split("_")[1]
            query_ali_best        = dic_hits[best_hit]['PaddedQali']
            template_ali_best     = dic_hits[best_hit]['PaddedTaliStruct']
            return lowcase_bestpdb+"_"+lowcase_bestpdb_chain, query_ali_best, template_ali_best
        except (IndexError, UnboundLocalError):
            return None, None, None
    else:
        return None, None, None
   
def searchHH(fasta, alignment, verbose, cleanup, hhsearch_out, auto_mode):
    """ Search for a template with HH-suite.
    If a template is found, output alignment to file.
    auto_mode = True --> Returns only the best template
    auto_mode = False --> Retrieves the ordered list of templates that match the conditions 
    """

    # Build HHblits profile
    fd0, ftmp_path0 = tempfile.mkstemp(dir=TMP_DIR)
    fd, ftmp_path = tempfile.mkstemp(dir=TMP_DIR)
    os.close(fd0)
    os.close(fd)
    
    cmd = HHBLITS + " " +\
          "-i " + fasta + " " +\
          "-d " + HHBLITS_UNIPROT20 + " " +\
          "-n " + HHBLITS_NITE + " " +\
          "-cov " + HHBLITS_COV + " " +\
          "-o " + ftmp_path0 + " " +\
          "-oa3m " + ftmp_path + " " +\
          "-cpu " + HHSUITE_NCPU + " " +\
          "-v " + HHSUITE_VMODE

    if verbose:
        print(cmd)
    os.system(cmd)

    # Add secondary structure predictions to the profile
    fd1, ftmp_path1 = tempfile.mkstemp(dir=TMP_DIR)
    os.close(fd1)
    
    #GP2018: Addition of this devnull variable, because addss.pl is too verbose
    devnull = " > /dev/null 2>&1"
    if verbose:
        devnull = ""

    cmd = ADDSS + " " +\
          ftmp_path + " " +\
          ftmp_path1 + devnull

    if verbose:
        print(cmd)
    os.system(cmd)

    # In case addss hasn't worked, keep the profile without ss
    if not os.path.exists(ftmp_path1) or os.path.getsize(ftmp_path1)==0:
        shutil.copy2(ftmp_path,ftmp_path1)
    
    # Search with this profile against the PDB
    hhsearch_out_preprocess = hhsearch_out + ".preprocessed"
    cmd = HHSEARCH + " " +\
          "-i " + ftmp_path1 + " " +\
          "-d " + HHSEARCH_PDB + " " +\
          "-o " + hhsearch_out_preprocess + " " +\
          "-cpu " + HHSUITE_NCPU + " " +\
          "-v " + HHSUITE_VMODE + " " +\
          "-alt " + HHSUITE_ALTERNATIVE_ALI

    if verbose:
        print(cmd)
    os.system(cmd)
    
    
    hit, queryseq, templateseq = parse_multitemplate_HHsearch_use_resolution(hhsearch_out_preprocess, fasta, out_hhfile = hhsearch_out, auto_mode = auto_mode)


    #FIXME SI DESSOUS = FAUX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX
    #GP2018 The above function actually takes 5 arguments (not 4). However, this has no consequence since the last 3 have predefined values (see below).
    #def parse_multitemplate_HHsearch_use_resolution(hhsearchfile,
    #                                                input_fasta,
    #                                                out_pairfile = None,
    #                                                out_hhfile = None,
    #                                                auto_mode = True):



    if hit:
        FHo = open(alignment, 'w')
        FHo.write(">query\n")
        FHo.write("%s\n"%queryseq)
        FHo.write(">%s\n"%hit)
        FHo.write("%s\n"%templateseq)
        FHo.close()
        
    if cleanup:
        if os.path.isfile(ftmp_path0):
            os.remove(ftmp_path0)
        if os.path.isfile(ftmp_path):
            os.remove(ftmp_path)
        if os.path.isfile(ftmp_path1):
            os.remove(ftmp_path1)
    
    return hit

def cleanup():
    """ Remove intermediate files after execution
    """

    filelist = []
    for f in filelist:
        if os.path.isfile(f):
            os.remove(f)
        
def get_name_from_fasta(fasta_input):
    fin = open(fasta_input).readlines()
    try:
        header = fin[0][1:].strip().split()[0][:30]#.split()[0]
        header_sub = re.sub('[=]','_',header)
    except:
        header_sub = 'QUERY'
    return header_sub
    
    
def createHtml(hhsearch_output_path, label_input, working_path, input_fasta_path, html_output_path):
    """ Call the class hhr2html to generate the html file required for InterEvDock2 displays
    """

    #GP2018: Note: XXX This function also create the hhlog digest (.txt file)

    HTMLout = hhr2html.hhr2html(hhsearch_output_path, "PDB", label_input, working_path, input_fasta_path, mode="pairwise_interevdock")
    # args for hhr2html : file_in, db, label, path, fst_file)

    HTMLout.set_res()
    HTMLout.set_align()
    HTMLout.create_image()
    fout = open(html_output_path,"w")
    fout.write(HTMLout.get_html())
    fout.close()



























def Main():
    import optparse
    usage = "Script to find a suitable template for modelling a given protein sequence.\n"+\
            "First search with blastp against the PDB.\n"+\
            "If nothing matches, build profile with HHblits against uniprot20 and search\n"+\
            "with HHsearch against the PDB."
    parser = optparse.OptionParser(usage)
    parser.add_option('-f', '--fasta', action="store", help='input fasta file')
    parser.add_option('-a', '--alignment', action="store", default="", help='output alignment file')
    parser.add_option('-t', '--template', action="store", default=None, help='output PDB template file')
    #parser.add_option('-b', '--blast_out', action="store", default="blast.out", help='blast output') #GP2018: because BLAST is no longer used
    parser.add_option('-s', '--hhsearch_out', action="store", default="hhsearch.out", help='HHsearch output (if needed)')
    parser.add_option('-l', '--hhsearch_html', action="store", default="hhsearch.html", help='HHsearch output in html format (if needed)')
    parser.add_option('-m', '--auto_mode', action="store_true", default=False, help='Run automode with one best selected template"+\
                                                                                     "and do not let the user select its template')
    # GP2018: commented
    # Two options useful for debugging but not required for normal function
    #parser.add_option('--output_pairali',action="store", default=None, help='Output pairwise alignments for modelling')
    #parser.add_option('--output_hhsearchali',action="store", default=None, help='Output hhsearch for modelling')
 
    parser.add_option('--verbose', action="store_true", default=False, help="print extra info")
    parser.add_option('--cleanup', action="store_true", default=False, help="cleanup temporary files after execution")
    # GP2018: commented
    #parser.add_option('--debug', action="store_true", default=False, help="used to debug the parsing of hhr output files")
 

    if len(sys.argv) == 1:
        print "type -h or --help for more help information"
        sys.exit(1)

    (args, leftover_args) = parser.parse_args(sys.argv)
    if len(leftover_args) > 1: 
        print "Leftover arguments:" + str(leftover_args[1:])

    # We don't use blast anymore
    blasthit = False#searchBlast(args.fasta, args.verbose, args.cleanup, args.blast_out)

# Since blasthit is always 'False', there is no reason to keep the lines below #GP2018
#    if blasthit:
#        isFileWritten = writeTemplate(blasthit, args.template)
#        if isFileWritten:
#            realignBlastHit(args.fasta, blasthit, args.template, args.alignment, args.verbose, args.cleanup)
#            print("Found hit with blast %s"%blasthit)
#        else:
#            print("Could not extract the pdb template identified using blast hit  %s"%blasthit)
#    elif args.auto_mode:
    if args.auto_mode:
        # Auto mode : keeps only the best template from HHsearch analysis
        hhsearchhit = searchHH(args.fasta, args.alignment, args.verbose, args.cleanup, args.hhsearch_out, args.auto_mode)
        
        if hhsearchhit: # GP2019 NOTE: in this variable, the PDB 4 characters are lowercase (unlike hhsearch raw data)
            working_path = os.getcwd()
            label_input = get_name_from_fasta(args.fasta)
            createHtml(args.hhsearch_out, label_input, working_path, args.fasta, args.hhsearch_html) #GP2019

            mmCIF_and_obsolete_search = 0 #GP2019
            
            isFileWritten = writeTemplate(hhsearchhit, args.template, mmCIF_and_obsolete_search)

            if not isFileWritten:
                mmCIF_and_obsolete_search = 1 #GP2019
                isFileWritten = writeTemplate(hhsearchhit, args.template, mmCIF_and_obsolete_search)

            if isFileWritten:
                print("Found hit with hhsearch %s"%hhsearchhit)
            else:
                print("No suitable template found! Cannot proceed with modelling and docking")
        else:
            # Send a warning to the user: no template was found, cannot proceed with docking
            #print("No suitable template found! Cannot proceed with modelling and docking")
            # Misleading error message #GP2018
            print("Error while writing the template structure file: Cannot proceed with modelling and docking")
    else:
        # Run HHsearch and returns the list of templates that match the conditions.
        hhsearchhit = searchHH(args.fasta, args.alignment, args.verbose, args.cleanup, args.hhsearch_out, args.auto_mode)
        
        working_path = os.getcwd()
        label_input = get_name_from_fasta(args.fasta)
        createHtml(args.hhsearch_out, label_input, working_path, args.fasta, args.hhsearch_html) #GP2019
        
        # Send a warning to the user: no template was found, cannot proceed with docking
        #print("No suitable template found! Cannot proceed with modelling and docking")
        # Misleading error message #GP2018
        print("Template search done. Will not proceed with modelling and docking (see selected options)")
 




if __name__ == "__main__":
    Main()
