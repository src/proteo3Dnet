FROM ubuntu:xenial
MAINTAINER Victor REYS <reys@cbs.cnrs.fr>

################################################################################
############################ Basic Ubuntu Xenial Set Up ########################
################################################################################

RUN apt-get -qy update && \
    apt-get -qy install \
        cmake \
        ascii \
        python3

# Set timezone
ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Useful tools when running Docker in interactive mode
  # Removing vim-tiny is Ubuntu-specific:
RUN apt-get remove -qy vim-tiny && \
    apt-get -qy install \
        vim \
        htop \
        screen \
        wget \
        dnsutils \
        bash-completion

# Extend bash history
RUN sed -i 's/HISTSIZE\=1000/HISTSIZE\=1000000/' /root/.bashrc && sed -i 's/HISTFILESIZE\=2000/HISTFILESIZE\=2000000/' /root/.bashrc
# Modify .bashrc for (improved) autocompletion
RUN sed -i '/^#.*bash_completion/s/^#//' /root/.bashrc && sed -i '$ s/^#//' /root/.bashrc
# Change the default shell in screen to bash
RUN echo "shell \"/bin/bash\"" > /root/.screenrc

# Vim: default syntax highlighting + highlight search
RUN echo "colorscheme default" > /root/.vimrc
RUN echo "set hlsearch" >> /root/.vimrc


################################################################################
############################ LiMIP INSTALL #####################################
################################################################################
# Copy scripts and Tree
COPY LIMIP/ /LIMIP

# Add LIMIP tree Directories
RUN mkdir -p /LIMIP/databases/USERS_RESULT \
             /LIMIP/databases/RAW_DATABASES \
             /LIMIP/databases/OWN_DATABASES

RUN chmod -R 777 /LIMIP

################################################################################
################################### CLEAN UP ###################################
################################################################################
RUN apt-get autoremove -qy && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
    
################################################################################
############################ DATABASES SET UP ##################################
################################################################################
#RUN export PYTHONIOENCODING=UTF-8
# Run DB_UPDATER.py to gather raw databases and compute own databases
#RUN python3 /LIMIP/LiMIP/DB_UPDATER.py

################################################################################
############################### TEST INSTALL ###################################
################################################################################
# Run test set to check install
#RUN python3 /LIMIP/LiMIP/LiMIP.py -u install_test -i /LIMIP/tmp/test_input_codeslist.txt -v

#RUN python3 /LIMIP/LiMIP/rLiMIP.py -u install_test_fast -i /LIMIP/tmp/test_input_codeslist.txt -v

# Remove test files
#RUN rm -r /LIMIP/databases/USERS_RESULT/install_test
#RUN rm -r /LIMIP/databases/USERS_RESULT/install_test_fast
#RUN rm /LIMIP/tmp/test_input_codeslist.txt
    
################################################################################
############################ LAUNCHING COMMAND #################################
################################################################################
#ENTRYPOINT [ "/LIMIP/LiMIP/" ]