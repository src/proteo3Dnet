#!/usr/bin/env perl

use strict;
use warnings;

my $mfasta = $ARGV[0]; # multi-FASTA file
my $label;

die "Use: $0 <INPUT MULTI-FASTA>\n" if (!$ARGV[0]);

if ($mfasta =~ m/(.+)\..+$/){
    $label = $1;
}
else{
    die "Please use an input filename with an extension\n";
}

my @mfasfile = readfile($mfasta);


# CLEAN FORMAT MSA
my @seqlist;
my @namelist;
my $seqtemp = '';
my $nametemp = '';
my $i = 0;
my $countline = 0;
foreach my $line (@mfasfile){
    chomp $line;
    if ($line =~ m/^>(.+)/){
        if ($seqtemp){
            $namelist[$i] = $nametemp;
            $seqlist[$i] = $seqtemp;
            $i++;
        }
        $nametemp = $1;
        $seqtemp = '';
    }
    elsif ($countline == $#mfasfile){
        $seqtemp.=$line;
        $namelist[$i] = $nametemp;
        $seqlist[$i] = $seqtemp;
    }
    else{
        $seqtemp.=$line;
    }
    $countline++;
}

my $total = $#seqlist+1;

for (my $j=0; $j <= $#seqlist; $j++){
    my $current = $j+1;
    my $k;
    if (length($j) < 2){
        $k = "0$j";
    }
    else{
        $k = $j;
    }

    # The FASTA name will be used for creating filenames
    # Therefore, any space is replaced by an underscore
    $namelist[$j] =~ s/ /_/g;
    $namelist[$j] =~ s/^_//g;
    $namelist[$j] =~ s/_$//g;

    # The following 8 characters are deleted, since they make findTemplate.py crash
    $namelist[$j] =~ s/,//g;
    $namelist[$j] =~ s/"//g;
    $namelist[$j] =~ s/'//g;
    $namelist[$j] =~ s/\(//g;
    $namelist[$j] =~ s/\)//g;
    $namelist[$j] =~ s/\[//g;
    $namelist[$j] =~ s/\]//g;
    $namelist[$j] =~ s/#//g;

    print "$current/$total: \e[32m$namelist[$j]\e[0m\n";
    writefile("$label\_$k.seq", $namelist[$j], $seqlist[$j]);

    # STDOUT ACCUMULATES IN A .ftlog FILE
    # STDERR IS NOT REDIRECTED
    my $cmd = "python /home/step1/findTemplate.py -f $label\_$k.seq -t $label\_$k\_template.pdb -a $label\_$k.ali -s $label\_$k\_hhsearch.out -l $label\_$k\_hhsearch.html --auto_mode >> $mfasta.ftlog";
    # print "\e[30m$cmd\e[0m\n";
    system $cmd;
}

print "Done\n";

####################
####################



sub readfile{
    my $filename = shift;
    open(IN, '<', $filename) or die "\e[1;31mError\e[0m while opening $filename:\n\t$!\n";
    my @file = <IN>;
    close IN;
    return @file;
}


sub writefile{
    my $filename = shift;
    my $fasname = shift;
    my $sequence = shift;
    open(OUT, '>', $filename) or die "\e[1;31mError\e[0m while opening $filename:\n\t$!\n";
    print OUT ">$fasname\n";
    print OUT $sequence;
    close OUT;
}
