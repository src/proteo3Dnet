#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;

my %hash_seqres; # XXX GLOBAL

sub helper{
    print
      "\n",
      "\tOptions:\n",
      "\t--input            A list of lists of PDB files\n",
      "\t--pdb              Path to local PDB\n",
      "\t--mmcif            Path to mmCIF\n",
      "\t--obsolete_pdb     Path to obsolete PDB\n",
      "\t--obsolete_mmcif   Path to obsolete mmCIF\n",
      "\t--timelimit        Time limit for MUSTANG alignment (default = 120 s)\n",
      "\t--errors_mute      Mute the system errors\n",
      "\t--seqres           The pdb_seqres.txt file\n",
      "\t--mafft_only       Do not use MUSTANG, only MAFFT\n",
      "\t--help             This help\n",
      "\n";
    exit 1;
}


sub parse_maxclus{
    no warnings; # XXX

    my @input = readfile($_[0]);
    my $cluster_name = $_[1];
    my @array; # The output; Subcluster files listed for MUSTANG

    my $i = 0;
    while ($input[$i] !~ m/\d+ Clusters/ and $i <= $#input){
        $i++;
    }

    my $total_clusters = undef;
    if ($input[$i] =~ m/(\d+) Clusters/){
        $total_clusters = $1;
    }

    $i+=3;

    my %hash_clusters; # Key: cluster number according to MaxCluster; Value: the PDB that belong to this cluster

    my $count = 0;
    while ($i <= $#input){
        if ($input[$i] =~ m/\.pdb$/){
            my @A = split(/\s+/, $input[$i]);
            if ($A[4] > 0){
                if (exists $hash_clusters{$A[4]}){
                    push(@{$hash_clusters{$A[4]}}, $A[5]);
                }
                else{
                    $hash_clusters{$A[4]} = [];
                    push(@{$hash_clusters{$A[4]}}, $A[5]);
                    $count++;
                }
            }
        }
        $i++;
    }

    if ($count){
        foreach my $key (keys %hash_clusters){
            my $subcluster_name = "$cluster_name\__$key";
            open(OUT, '>', $subcluster_name) or die "\e[1;41mERROR: can't write $subcluster_name\e[0m\n";
            foreach my $line (@{$hash_clusters{$key}}){
                print OUT "+$line\n"; # XXX Output format for MUSTANG
            }
            close OUT;
            push(@array, $subcluster_name);
        }

        if ( $count != $total_clusters ){
            print "\e[1;31mWARNING: possible problem with the maxcluster output for $cluster_name\e[0m\n";
        }
    }
    else{
        if ( defined $total_clusters ){
            print "\e[1;31mERROR: please check the maxcluster output\n";
        }
    }

    return @array;
}


sub treat_one_cluster_member{
    my ($pdb_path, $mmcif_path, $obsolete_pdb_path, $obsolete_mmcif_path, $mute_errors, $pdbcode, $chain, $actual_cluster_size) = splice(@_, 0, 8);

    my $lcpdbcode = lc($pdbcode);
    my @pdbfile;

    my $old_chain = $chain;

    # Try #1: Fetch PDB file from local PDB
#    if( !(system "cp $pdb_path/pdb$lcpdbcode.ent.gz . $mute_errors && gunzip -f pdb$lcpdbcode.ent.gz $mute_errors") ){
#        print "\e[33m$pdbcode downloaded from the local PDB\e[0m\n";
#        @pdbfile = readfile("pdb$lcpdbcode.ent");
#        system "rm pdb$lcpdbcode.ent";
#    }

    if ( -e "$pdb_path/pdb$lcpdbcode.ent.gz" and -f "$pdb_path/pdb$lcpdbcode.ent.gz" ){
        print "\e[33m$pdbcode downloaded from the local PDB\e[0m\n";
        @pdbfile = `zcat $pdb_path/pdb$lcpdbcode.ent.gz`;
    }

    # Try #2: Fetch obsolete PDB file
#    elsif( !(system "cp $obsolete_pdb_path/$pdbcode.pdb.gz . $mute_errors && gunzip -f $pdbcode.pdb.gz $mute_errors") ){
#        @pdbfile = readfile("$pdbcode.pdb");
#        system "rm $pdbcode.pdb";
#        print "\e[35m$pdbcode downloaded from the RCSB PDB\e[0m\n";
#    }

    elsif ( -e "$obsolete_pdb_path/$pdbcode.pdb.gz" and -f "$obsolete_pdb_path/$pdbcode.pdb.gz"){
        print "\e[35m$pdbcode downloaded from the RCSB PDB\e[0m\n";
        @pdbfile = `zcat $obsolete_pdb_path/$pdbcode.pdb.gz`;
    }

    # Try #3: Fetch obsolete mmCIF
#    elsif( !(system "cp $obsolete_mmcif_path/$pdbcode.pdb . $mute_errors") ){
#        @pdbfile = readfile("$pdbcode.pdb");
#        system "rm $pdbcode.pdb";
#        print "\e[30m$pdbcode is a mmCIF converted with PyMOL\e[0m\n";
#    }

    elsif ( -e "$obsolete_mmcif_path/$pdbcode.pdb" and -f "$obsolete_mmcif_path/$pdbcode.pdb"){
        print "\e[30m$pdbcode is a mmCIF converted with PyMOL\e[0m\n";
        @pdbfile = `cat $obsolete_mmcif_path/$pdbcode.pdb`;
    }

    # Try #4: Fetch mmCIF
#    elsif( !(system "cp $mmcif_path/$lcpdbcode-pdb-bundle.tar.gz . $mute_errors && tar -xzf $lcpdbcode-pdb-bundle.tar.gz $mute_errors") ){
#        my @mapfile = readfile("$lcpdbcode-chain-id-mapping.txt");
# 
#        my $currentbundle;
#        foreach my $line (@mapfile){
#            if ($line =~ m/^(\d.+):/){
#                $currentbundle = $1;
#            }
# 
#            if ($line =~ m/^\s+(\w)\s+$chain/){
#                system "mv $currentbundle $pdbcode.pdb";
#                system "rm $lcpdbcode-pdb-bundle* $lcpdbcode-chain-id-mapping.txt";
#                $chain = $1;
#                last;
#            }
#        }
#        @pdbfile = readfile("$pdbcode.pdb");
#        system "rm $pdbcode.pdb";
#        print "\e[36m$pdbcode is a pre-converted mmCIF from the RCSB PDB\e[0m\n";
#    }
    elsif( -e "$mmcif_path/$lcpdbcode-pdb-bundle.tar.gz" and -f "$mmcif_path/$lcpdbcode-pdb-bundle.tar.gz" ){
        my @mapfile = `tar xfO $mmcif_path/$lcpdbcode-pdb-bundle.tar.gz $lcpdbcode-chain-id-mapping.txt`;
 
        my $currentbundle;
        foreach my $line (@mapfile){
            if ($line =~ m/^(\d.+):/){
                $currentbundle = $1;
            }
 
            if ($line =~ m/^\s+(\w)\s+$chain/){
                $chain = $1;
                last;
            }
        }
        @pdbfile = `tar xfO $mmcif_path/$lcpdbcode-pdb-bundle.tar.gz $currentbundle`;
        print "\e[36m$pdbcode is a pre-converted mmCIF from the RCSB PDB\e[0m\n";
    }

    else{
        print "\e[1;31mDOWNLOAD ERROR: $pdbcode\_$old_chain not included in cluster\e[0m\n";
        return $actual_cluster_size;
    }

    if ($#pdbfile < 0){
        print "\e[1;35mREADING ERROR: $pdbcode\_$old_chain not included in cluster\e[0m\n";
        return $actual_cluster_size;
    }

    # If the file has been successfully copied and read: Extract the chain of interest
    my @filetoprint;

    my %hash_resnum;

    foreach my $line (@pdbfile){
        if ($line =~ m/^ATOM|^HETATM/ && substr($line,21,1) eq $chain){
            my $atom = substr($line, 12, 4);
            $atom =~ s/ //g;
            my $resnum = substr($line, 22, 4);
            $resnum =~ s/ //g;
            $resnum.=$atom;
            unless (exists $hash_resnum{$resnum}){ # XXX For AGLU, BGLU ...
                push(@filetoprint, $line);
            }
            $hash_resnum{$resnum} = 1;
        }
        if ($line =~ m/^ENDMDL/){ # XXX If multimodel PDB file: only takes the first one
            last;
        }
    }

    undef %hash_resnum;

    if ($#filetoprint < 20){ # XXX For the few mmCIF converted with PyMOL
        @filetoprint = ();
        foreach my $line (@pdbfile){
            if ($line =~ m/^ATOM|^HETATM/ && substr($line,72,4) =~ m/$chain/){
                my $atom = substr($line, 12, 4);
                $atom =~ s/ //g;
                my $resnum = substr($line, 22, 4);
                $resnum =~ s/ //g;
                $resnum.=$atom;
                unless (exists $hash_resnum{$resnum}){
                    push(@filetoprint, $line);
                }
                $hash_resnum{$resnum} = 1;
            }
            if ($line =~ m/^ENDMDL/){
                last;
            }
        }
    }

    if ($#filetoprint < 20){ # XXX 20 is arbitrary
        print "\e[1;31mWARNING: $pdbcode: chain $old_chain not found\e[0m\n";
    }
    else{
        # XXX Here, all .pdb file have been removed FIXME NOT TRUE ANYMORE
        open(PDBOUT, '>', "$pdbcode\_$old_chain.pdb") or die "Error while creating file $pdbcode\_$old_chain.pdb:\n\t$!\n";
        foreach my $linetoprint (@filetoprint){
            print PDBOUT $linetoprint;
        }
        close PDBOUT;
        $actual_cluster_size++;
    }

    return $actual_cluster_size;
}


sub run_mustang{
    my ($mute_errors, $subclus, $num_cluster, $dat, $actual_cluster_size, $theo_cluster_size, $subclus_size, $subnum, $total_subclus, $timelimit, $PDBSEQRES, $mafft_only) = splice(@_, 0, 12);

    if ($mafft_only){
        run_mafft($PDBSEQRES, $subclus, $dat, $mute_errors, $actual_cluster_size, $theo_cluster_size, $subnum, $total_subclus, $subclus_size, $num_cluster);
        system "mv $dat.maxclus_* $dat\_dir/ $mute_errors";
        return 0;
    }

    my $start_time = time;
    my $cmd = "timeout $timelimit mustang-3.2.3 -f $subclus -F fasta -o $subclus -s OFF > $subclus.out 2> $subclus.err";
    print "$num_cluster: RUNNING: $cmd ... (n = $actual_cluster_size/$theo_cluster_size structures; subcluster $subnum/$total_subclus: n = $subclus_size) ";

    if(system $cmd){
        my $end_time = time;
        if ( ($end_time-$start_time) > ($timelimit-2) ){
            print "\e[1;33mTIMEOUT!\e[0m\n";
        }
        else{
            print "\e[1;31mFAIL!\e[0m\n";
        }
        system "mv $subclus.out $subclus.err $dat.maxclus_* $dat\_dir/ $mute_errors";

        run_mafft($PDBSEQRES, $subclus, $dat, $mute_errors, $actual_cluster_size, $theo_cluster_size, $subnum, $total_subclus, $subclus_size, $num_cluster);
    }
    else{
        my $end_time = time;
        my $time_exec = $end_time-$start_time;
        print "\e[1;32mSUCCESS!\e[0m ($time_exec s)\n";
        system "mv $dat.maxclus_* $dat\_dir/ $mute_errors";
    }
}


sub run_mafft{
    my ($PDBSEQRES, $subclus, $dat, $mute_errors, $actual_cluster_size, $theo_cluster_size, $subnum, $total_subclus, $subclus_size, $num_cluster) = splice(@_, 0, 10);

    my $multifastafile = "$subclus.mafft_in";
    my $alignfastafile = "$subclus.afasta";
    my $mafft_stderr = "$subclus.mafft_stderr";

    my @cluster_members = readfile($subclus);

    open(OUT, '>>', $multifastafile) or die "Error while writing $multifastafile\n";

    foreach my $cluster_member (@cluster_members){
        if ($cluster_member =~ m/(\d\w\w\w_\w+)\.pdb/){
            my $pdb6char = $1;
            my ($pdb4char, $chain) = split('_', $pdb6char);
            my $lcpdb6char = lc($pdb4char)."_".$chain;
            #system "grep -A1 -m 1 $lcpdb6char $PDBSEQRES > $pdb6char.seqres";
            #system "sed -i 's/$lcpdb6char/$pdb6char/g' $pdb6char.seqres";

            if (exists $hash_seqres{$pdb6char}){
                print OUT ">$pdb6char\n$hash_seqres{$pdb6char}";
            }
            # XXX Treat obsolete
            else{
                print STDERR "\e[1;31mWARNING!!\e[0m No way to get the seqres of $pdb6char\n";
            }
        }
        else{
            die "ERROR: no PDB 6 char found in line:\n$cluster_member\n";
        }
    }

    close OUT;

    my $cmd = "mafft --auto $multifastafile > $alignfastafile 2> $mafft_stderr";
    print "$num_cluster: RUNNING: $cmd ... (n = $actual_cluster_size/$theo_cluster_size structures; subcluster $subnum/$total_subclus: n = $subclus_size) ";
    system $cmd;

    my @lines = readfile($mafft_stderr);
    my $warning_count = 0;
    foreach my $line (@lines){
        if ($line =~ m/Warning/){
            $warning_count++;
        }
    }
    if ($warning_count > 0){
        print "\e[1;31mFAIL!\e[0m\n";
    }
    else{
        print "\e[1;32mSUCCESS!\e[0m\n";
    }

    system "mv $multifastafile $alignfastafile $mafft_stderr $dat\_dir/ $mute_errors";
}


sub check_args{
    my ($input_list, $timelimit, $pdb_path, $mmcif_path, $obsolete_pdb_path, $obsolete_mmcif_path, $PDBSEQRES, $mafft_only) = splice(@_, 0, 8);

    if (!$input_list){
        die "ERROR: Missing input: A list of lists of PDB files\n";
    }
    if (!$pdb_path){
        die "ERROR: path to PDB is missing\n";
    }
    if (!$mmcif_path){
        die "ERROR: path to mmCIF database is missing\n";
    }
    if (!$obsolete_pdb_path){
        die "ERROR: path to obsolete PDB is missing\n";
    }
    if (!$obsolete_mmcif_path){
        die "ERROR: path to obsolete mmCIF database is missing\n";
    }
    if (!$PDBSEQRES){
        die "ERROR: Missing --seqres argument\n";
    }
    unless (-d $pdb_path){
        die "ERROR: --pdb $pdb_path is not a directory\n";
    }
    unless (-d $mmcif_path){
        die "ERROR: --mmcif $mmcif_path is not a directory\n";
    }
    unless (-d $obsolete_pdb_path){
        die "ERROR: --obsolete_pdb $obsolete_pdb_path is not a directory\n";
    }
    unless (-d $obsolete_mmcif_path){
        die "ERROR: --obsolete_mmcif $obsolete_mmcif_path is not a directory\n";
    }
    unless (-e $PDBSEQRES and -f $PDBSEQRES){
        die "ERROR: --seqres $PDBSEQRES is not a file\n";
    }
    if ($timelimit and $mafft_only){
        print STDERR "WARNING: --timelimit useless with --mafft_only\n";
    }
}


sub readfile{
    my $filename = shift;
    open(IN, '<', $filename) or die "\e[1;31mError\e[0m while opening $filename:\n\t$!\n";
    my @file = <IN>;
    close IN;
    return @file;
}


sub main{
    my $timelimit = "";
    my $input_list = "";
    my $pdb_path = "";
    my $mmcif_path = "";
    my $obsolete_pdb_path = "";
    my $obsolete_mmcif_path = "";
    my $mute_errors = "";
    my $PDBSEQRES = "";
    my $mafft_only = "";

    my $help = (@ARGV) ? 0 : 1;

    GetOptions(
        'timelimit=s' => \$timelimit,
        'input=s' => \$input_list,
        'pdb=s' => \$pdb_path,
        'mmcif=s' => \$mmcif_path,
        'obsolete_pdb=s' => \$obsolete_pdb_path,
        'obsolete_mmcif=s' => \$obsolete_mmcif_path,
        'errors_mute' => \$mute_errors,
        'mafft_only' => \$mafft_only,
        'seqres=s' => \$PDBSEQRES,
        'help' => \$help
    );
    helper() if ($help);

    check_args($input_list, $timelimit, $pdb_path, $mmcif_path, $obsolete_pdb_path, $obsolete_mmcif_path, $PDBSEQRES, $mafft_only);

    if (!$timelimit and !$mafft_only){
        $timelimit = 120;
    }

    if ($mute_errors){
        # Mute the system errors that could be produced by mv, rm,or cp commands, because these error are explicitly treated in this script
        $mute_errors = '2> /dev/null';
    }

    my @list_of_dat = readfile($input_list);







##################################
# XXX ACTUALLY USED FOR TREATING THE SEQRES FROM THE RCSB XXX
#my $current_pdb = '';
#open(IN, '<', $PDBSEQRES) or die "Error while reading $PDBSEQRES\n";
#while (my $line = <IN>){
#    if ($line =~ m/^>(\w+) /){
#        my ($pdb4char, $chain) = split('_', $1);
#        $pdb4char = uc($pdb4char);
#        $current_pdb = "$pdb4char\_$chain";
#    }
#    else{
#        if (!$current_pdb){
#            die "FATAL ERROR while loading hash SEQRES\n";
#        }
#        chomp $line;
#        $hash_seqres{$current_pdb} = $line;
#        $current_pdb = '';
#    }
#}
#close IN;
##################################
open(IN, '<', $PDBSEQRES) or die "Error while reading $PDBSEQRES\n";
while (my $line = <IN>){
    chomp $line;
    my ($pdb6char, $sequence) = split(' ', $line);
    $sequence =~ s/!//g; # XXX GAP INFORMATION IS KEPT IN THIS SEQATOM DATABASE (SYMBOL = '!')
    $hash_seqres{$pdb6char} = $sequence;
}
close IN;
##################################





    my $i = 1;
    # For each MSA to create (= for each .dat file = for each cluster)
    foreach my $dat (@list_of_dat){
        print "\n";
        chomp $dat;
        my @datfile = readfile($dat);
        my $actual_cluster_size = 0;
        my $theo_cluster_size = $#datfile+1;
        my $total = $#list_of_dat+1;
        my $num_cluster = "$i/$total";

        # For each PDB file (+chain) in the .dat list
        my @cwd = `pwd`;
        system "mkdir $dat\_dir";
        chdir("$dat\_dir") or die "$!"; # XXX

        foreach my $pdb_name (@datfile){
            chomp $pdb_name;
            my ($pdbcode, $chain) = split('_', $pdb_name);
            $chain =~ s/\s//g;
            # The function below:
            # (i) copies the PDB file from the right place, depending on whether it is a PDB or mmCIF, obsolete or not
            # (ii) creates a new PDB file which only contains the protein chain of interest
            # (iii) returns an incremented (or not) size of the cluster (i.e. if the PDB cannot be found, it does not count for the cluster actual size)
            $actual_cluster_size = treat_one_cluster_member($pdb_path, $mmcif_path, $obsolete_pdb_path, $obsolete_mmcif_path, $mute_errors, $pdbcode, $chain, $actual_cluster_size);
        }
        chomp $cwd[0];
        chdir($cwd[0]) or die "$cwd[0]: $!"; # XXX

        # Self-explainatory
        if ($actual_cluster_size == 1){
            print "$num_cluster: \e[1;31mSKIP:\e[0m cluster $dat contains only 1 structure\n";
        }
        # Self-explainatory
        elsif ($actual_cluster_size == 0){
            print "$num_cluster: \e[1;31mSKIP:\e[0m cluster $dat contains 0 structure\n";
        }
        else{
            # List the PDB chain files produced above = input for MaxCluster
            system "ls $dat\_dir/*.pdb > $dat.maxclus_in";
    
            # Run MaxCluster
            # As a result, some HHsearch cluster may be divided into several subclusters
            # The members of each (sub)cluster are then aligned with MUSTANG
            system "maxcluster64bit -l $dat.maxclus_in > $dat.maxclus_out 2> $dat.maxclus_err";
            my @subclusters = parse_maxclus("$dat.maxclus_out", "$dat.mustang");
    
            my $subnum = 0;
            my $total_subclus = $#subclusters + 1;

            if ($total_subclus > 0){
                foreach my $subclus (@subclusters){
                    $subnum++;
                    chomp $subclus;
                    #system "sed -i '1s/^/> \\.\\\/\\n\\n/' $dat.mustang";
                    #system "sed -i 's/^/+/' $subclus"; # MUSTANG format # Already added by parse_maxclus()
                    my @size = readfile($subclus);
                    my $subclus_size = $#size + 1;
     
                    run_mustang($mute_errors, $subclus, $num_cluster, $dat, $actual_cluster_size, $theo_cluster_size, $subclus_size, $subnum, $total_subclus, $timelimit, $PDBSEQRES, $mafft_only);
                }
                system "mv $dat.mustang__* $dat\_dir/ $mute_errors";
            }
            else{
                # When no subcluster produced: use HHsearch cluster
                system "sed -i 's/^/+/' $dat.maxclus_in"; # MUSTANG format
                run_mustang($mute_errors, "$dat.maxclus_in", $num_cluster, $dat, $actual_cluster_size, $theo_cluster_size, "NA", "NA", "NA", $timelimit, $PDBSEQRES, $mafft_only);
            }
        }
        system "rm $dat\_dir/*.pdb $mute_errors"; # XXX
        $i++;
    }
 
    print "Done\n";
}

main();

