#! /usr/bin/env python3

#--------------------------------------------------------
# ----------------------- META DATA ---------------------
#--------------------------------------------------------
__author__ = 'IFB - Institut Francaise de Bioinformatique'
__dev__ = 'Reys Victor : reys[at]cbs.cnrs.fr | victor.reys[at]france-bioinformatique.fr'
__version__ = '1.7'

CREDITS = """PROJECT : MS2MODEL - Linear Motif Interaction Prediction
VERSION : {VERSION}
AUTHORS : {AUTHORS}
DEV     : {DEV}
CREDITS : MS2MODEL publications
""".format(VERSION = __version__, AUTHORS = __author__, DEV = __dev__)

README = """###############################################
###############################################
##### LINEAR MOTIF INTERACTION PREDICTION #####
###############################################
###############################################

------------------------------
1. INTRODUCTION :
------------------------------
{CREDITS}

------------------------------
2. MAKE IT WORK :
------------------------------
System requierement :
	-> Linux operating system
	-> python3
	-> Internet connexion (to update databases)

A. Be sure to have all requiered scripts in the same directory :
    -> rLMIP.py
    -> LMIP_script_config.py
    -> DB_UPDATER.py
    -> biogrid_extention.py
    
A'.   Be sure to have requiered directories :
	-> USERS_RESULT
	-> iupred_data

B. Modify Path of Softwares in LMIP_script_config.py to fit your installation parameters

C. Launch DB_UPDATER.py to download/update your databases

D. Launch LiMIP.py
    >python3 LiMIP.py --help

------------------------------
3. OPTIONS AND ARGUMENTS :
------------------------------

>python3 LiMIP.py [-option] <argument1> [-option] <argument2> ...
     [-i || --uniin] <path_to_list_of_unicode>
     [-u || --user] <name_of_your_project>
     [-s || --spec] <indexI,indexX,...,indexN>
     [-a || --anchor2] <float.value>
     [-d || --disorder]
     [-h || --help] 
     [-v || --verbose]

-i or --uniin <path_to_first_file.mol2>
    -> File must be in a valid path
    -> Must contain unicodes (formated as uniprot entry : >sp|Q9Y478|AAKB1_HUMAN 5'-AMP-activated protein kinase subunit beta-1 OS=Homo sapiens GN=PRKAB1 PE=1 SV=4 )
    
-u or --user <name_of_your_project>      
    -> A new directory will be created with your name_of_your_project
    -> If directory allready exists, name_of_your_project will be indexed
    -> Results will be stored in this directory
    
-s or --spec <indexI,indexX,...,index(N-1)>
    -> Index must be an integer
    -> Index value must be written in Python3 Indexing way (form 0 to n-1)
    -> Must be separated by COMMA !!
        e.g. 0,3,4,5,6,54,86,91    
    -> Default : All index will be parsed

-a or --anchor2 <float.value>
    -> Float value must be written with a '.' to separate int with floating numbers
    -> 0 <= float.value < 1
    -> !!! Not recommended to use 1 !!!
    -> Default is 0
    
-d or --disorder
    -> Without argument
    -> Default is without
       without -d option : all connexions are written in connexion table
       with -d option : only strictly disordered ELM motifs are written in connexion table    

-h or --help
    -> Without argument
    -> Shows help

-v or --verbose
    -> Without argument
    -> Default is without :  without -v option, nothing is shown in terminal
    -> -v Display actions in terminal


------------------------------
LAUNCH :
------------------------------
>python3 LiMIP.py -i path_to_list_of_unicode -u my_project_name -v

REQUIERED    [-i || --uniin] <path_to_list_of_unicode>
BETTER IF    [-u || --user] <name_of_your_project>
OPTION       [-s || --spec] <indexI,indexX,...,indexN>
OPTION       [-d || --disorder]
OPTION	     [-h || --help] 
OPTION	     [-v || --verbose]
""".format(CREDITS = CREDITS)
#--------------------------------------------------------
# ----------------- LIBRARY IMPORTATIONS ----------------
#--------------------------------------------------------
import os
import re #RegularExpression lib
import sys
import csv
import time
import copy
import glob
import math # Used by IUpred
import json
import string
import random
import threading
import urllib.request
#import biogrid_extention
import LMIP_script_config # Scripts Configuration

#--------------------------------------------------------
# ----------------------- CLASSE -----------------------
#--------------------------------------------------------
class EndThreadException(Exception):    pass

class BioGridConnexions(threading.Thread):
    def __init__(self, biogrid_list, connexion_dic = {},
                 biogrid_id = False, biogrid_data = False):       
        # Inherit from threading class
        super(BioGridConnexions, self).__init__()
        # Init ending flag
        self.started = False
        self.finished = False
        
        # Load biogrid ( ID and connexions )
        self.biogrid_ID = biogrid_id
        if biogrid_id == False:
            with open(LMIP_script_config.BIOGRID_ID) as filin_id:
                self.biogrid_ID = json.load(filin_id)
                
        self.biogrid_data = biogrid_data
        if biogrid_data == False:
            with open(LMIP_script_config.BIOGRID_DATA) as filin_data:
                self.biogrid_data = json.load(filin_data)
        
        
        # Input list
        self.biogrid_list = biogrid_list
        # Output var
        self.connexion_dic = connexion_dic
        
        
    def run(self):
        self.started = True
        # Loop over entries
        for bio_id in self.biogrid_list:
            if bio_id == False:
                continue
            # Gather uniprot name
            uniprot_name = self.reverse_biogrid_id(bio_id)
                
            # Add new entry in connexion dic
            self.connexion_dic[uniprot_name] = {}
            
            # Loop over interactorsB    
            for indexB, interactorB in enumerate(self.biogrid_data[bio_id]['InteractorB']):
                # Gather uniprot name
                interact_name = self.reverse_biogrid_id(interactorB)
                # Add new entry in connexion dic
                self.connexion_dic[uniprot_name][interact_name] = {'Low':{},'OutFiltered':{}}
                
                # Loop over Low data
                for pmid in self.biogrid_data[bio_id]['PMID_Low'][indexB].keys():
                    if not self.biogrid_data[bio_id]['PMID_Low'][indexB][pmid] in self.connexion_dic[uniprot_name][interact_name]['Low'].keys():
                        self.connexion_dic[uniprot_name][interact_name]['Low'][self.biogrid_data[bio_id]['PMID_Low'][indexB][pmid]]  = []
                    self.connexion_dic[uniprot_name][interact_name]['Low'][self.biogrid_data[bio_id]['PMID_Low'][indexB][pmid]].append(pmid)
                # Loop over Exfiltered data
                for pmid in self.biogrid_data[bio_id]['PMID_Out'][indexB].keys():
                    if not self.biogrid_data[bio_id]['PMID_Out'][indexB][pmid] in self.connexion_dic[uniprot_name][interact_name]['OutFiltered'].keys():
                        self.connexion_dic[uniprot_name][interact_name]['OutFiltered'][self.biogrid_data[bio_id]['PMID_Out'][indexB][pmid]]  = []
                    self.connexion_dic[uniprot_name][interact_name]['OutFiltered'][self.biogrid_data[bio_id]['PMID_Out'][indexB][pmid]].append(pmid)
                    
        self.finished = True
                
    
    def reverse_biogrid_id(self, ID):
        try:
            return list(self.biogrid_ID.keys())[list(self.biogrid_ID.values()).index(ID)]
        except ValueError:
            try:
                return 'BioID_'+ID
            except TypeError:
                return 'Unfound' 
             
            
class EntryPredictor(threading.Thread):
    #--------------------------------------------------------
    # --------------------- ENTRY INIT ----------------------
    #--------------------------------------------------------
    def __init__(self, entry, final_connexion_data, unfound_uniprot_entries,
                 connexion_data_template, uniprot_pfam_headers,
                 input_file_list, biogrid_list, strict_disorder, anchor2,
                 biogrid_id, biogrid_data, uniprot_data, elm_data, valid_elm_instances,
                 elm_predict_tool, iupred_tool):
                 
        # Inherit from threading class
        super(EntryPredictor, self).__init__()
        # Init ending flag
        self.started = False
        self.finished = False
        init_time = time.time()
        
        # Retrieve parameters
        self.entry = entry
        self.strict_disorder = strict_disorder
        self.anchor2_excu_val = anchor2
        
        # Gather of the final connexion data
        self.connexion_data_template = connexion_data_template
        self.uniprot_pfam_headers = uniprot_pfam_headers
        self.final_connexion_data = final_connexion_data
        self.unfound_uniprot_entries = unfound_uniprot_entries
        
    # DATA BASE LOADING
        # Load ELM data
        self.elm_data = elm_data
        self.valid_elm_instances = valid_elm_instances
        
        # Load uniprot_data
        self.uniprot_data = uniprot_data
        
        # Load biogrid ( ID and connexions )
        self.biogrid_ID = biogrid_id
        self.biogrid_data = biogrid_data
        
        
        # Initialize linear motif interaction prediction tool
        self.elm_prediction_tool = elm_predict_tool
        # Initialize IUpred object (IUpred algorithm is used to compute disorder)
        self.IUpred_tool = iupred_tool
        
    # ARGUMENT LOADING
        # Load Uniprot codes list
        self.unicode_list = input_file_list
        
        # Look for BioGrid IDs!
        self.biogrid_list = biogrid_list
        
        # Remember verbose wish
        self.verbose = False
        
        
    #--------------------------------------------------------
    # -------------------- MAIN PROGRAM ---------------------
    #--------------------------------------------------------
    def run(self):    
        self.started = True
        uniprot_entry = self.entry
        k = self.unicode_list.index(uniprot_entry)
        
        try:
         # Request Fasta sequence
            if LMIP_script_config.INTERNAL_UNIPROT == None or not uniprot_entry in list(self.uniprot_data.keys()):
                try:
                    uniprot_return = self.unicode_fasta_query(uniprot_entry)
                    # Retrieve sequence only
                    fasta_seq = ''.join(uniprot_return.split('\n')[1:])
                except:
                    fasta_seq = 'UNFOUND'
                    
            # Gather FASTA from internal db
            else:
                fasta_seq = self.uniprot_data[uniprot_entry]['FASTA']
                
            # Stop condition
            if fasta_seq == 'UNFOUND':
                raise EndThreadException
                
            # Get ELM prediction for this code
            actual_unicode_elm_predictions = self.elm_prediction_tool.predict(fasta_seq)
                
            # Get IUpred predictions
            iupred_score, long_iupred_score, short_disorder_fasta, long_disorder_fasta, anchor2_res = self.IUpred_tool.own_workflow(fasta_seq)
            
            total_matchs = 0
            # Loop over motifs (Generation of new row for each Uniprot/Motif match)
            for i, motif in enumerate(actual_unicode_elm_predictions['MOTIF_NAMES']):
            
                # Unfortunatly, motif can not be in ELM_DATA ...
                if motif in self.elm_data.keys():

                    # Loop over each columns
                    for j, uniprot_pfams in enumerate(self.uniprot_pfam_headers):
                        
                        # Here proceed to BioGrid analysis, as the results of BioGrid are
                        # for Protein-Protein match, and we don't care about the motif...
                        total_biogrid_interactions, exfiltered_interactions = self.biogrid_analysis(self.biogrid_list[k], self.biogrid_list[j])
                        
                        # Loop over PFam IDs of this motif (may be there is more than 1 Pfam)
                            # Might be a many associated Pfam
                            # If pfam MATCH, we associate both (motifs with UniptorCode/gene)
                            # Comprehensive list with condition for speed boost
                        for pfamID in [pf for pf in self.elm_data[motif]['PFAM'] if pf in uniprot_pfams]:
                        
                            for motif_locations in actual_unicode_elm_predictions['MOTIF_LOCATIONS'][i]:
                                mstarts, mends = motif_locations.split('-')
                                
                                # Anchor2 Exclusion condition
                                if not (sum(anchor2_res[(int(mstarts) -1):int(mends)]) / (int(mends)-(int(mstarts) -1))) >= self.anchor2_excu_val:
                                    continue
                                
                                # Check if all resids > 0.5 in iupred
                                disorder_flag = True
                                for res, lres in zip(iupred_score[(int(mstarts) -1):int(mends)], long_iupred_score[(int(mstarts) -1):int(mends)]):
                                    if res < 0.5 or lres < 0.5:
                                        disorder_flag = False
                                        break
                                
                                if not disorder_flag:
                                    disorder_str = '0'
                                else:
                                    disorder_str = '1'            
                                
                                # Checks if we strict disorder flag is set        
                                add_data = True
                                if self.strict_disorder and not disorder_flag:
                                    add_data = False
                                    
                                if add_data:
                                    # Reminder
                                    #['ELM_CLASS', 'MOTIF_UNICODE', 'ASSOCIATED_PFAM', 'MATCHED_PFAM_UNICODE', 'EXPERIMENTAL_EVIDENCE', 'ELM_PROBA', 'STRICT_DISORDER', 'SHORT_IUPRED_AVG_SCORE', 'LONG_IUPRED_AVG_SCORE', 'SHORT_IUPRED_GLOB_DOM', 'LONG_IUPRED_GLOB_DOM', 'ANCHOR2_AVG_SCORE\n']
                                    
                                    # Add data to connexion data
                                    connexion_data = self.connexion_data_template.format(ELM_CLASS = motif,
                                                                                    MATCHED_MOTIF = fasta_seq[(int(mstarts) -1):int(mends)],
                                                                                    MOTIF_UNICODE = uniprot_entry,
                                                                                    MOTIF_STARTS = mstarts,
                                                                                    MOTIF_ENDS = mends,
                                                                                    ASSOCIATED_PFAM = pfamID,
                                                                                    MATCHED_PFAM_UNICODE = self.unicode_list[j],
                                                                                    EXPERIMENTAL_EVIDENCE = self.check_valid_elm_entry(motif, pfamID, uniprot_entry, self.unicode_list[j], mstarts, mends),
                                                                                    ELM_PROBA = str(actual_unicode_elm_predictions['MOTIF_EVALUE'][i]),
    STRICT_DISORDER = disorder_str,
                                                                                    SHORT_IUPRED_AVG_SCORE = (sum(iupred_score[(int(mstarts) -1):int(mends)]) / (int(mends)-(int(mstarts) -1))),
                                                                                    LONG_IUPRED_AVG_SCORE = (sum(long_iupred_score[(int(mstarts) -1):int(mends)]) / (int(mends)-(int(mstarts) -1))),
                                                                                    SHORT_IUPRED_GLOB_DOM = self.get_motif_disorder_score((int(mstarts) -1), int(mends), short_disorder_fasta),
                                                                                    LONG_IUPRED_GLOB_DOM = self.get_motif_disorder_score((int(mstarts) -1), int(mends), long_disorder_fasta),
                                                                                    ANCHOR2_AVG_SCORE = (sum(anchor2_res[(int(mstarts) -1):int(mends)]) / (int(mends)-(int(mstarts) -1))),
                                                                                    BIOGRID_INTERACTIONS = total_biogrid_interactions,
                                                                                    BIOGRID_EXFILTERED = exfiltered_interactions)
                                    
                                      
                                    # Let's not add two times the same data (should not happend)
                                    if not connexion_data in self.final_connexion_data:
                                        self.final_connexion_data.append(connexion_data)
                                        
                                    total_matchs += 1
            
            # Check if ELM_motifs could match any PFam of Uniprot list
            if total_matchs == 0:
                self.unfound_uniprot_entries.append(uniprot_entry)
    
            
        except EndThreadException:
            self.unfound_uniprot_entries.append(uniprot_entry)
        
        finally:
            self.finished = True
            
        
            
            
    def unicode_fasta_query(self, UniprotCode):
        # Initialize query
        query_url = 'https://www.uniprot.org/uniprot/{0}.fasta'.format(UniprotCode.upper())
        
        # Get the RCSB return
        uniprot_query = urllib.request.urlopen(query_url)
        # Wait for all content to be downloaded and cast bytes to string
        uniprot_fasta = uniprot_query.read().decode('UTF-8')
        uniprot_query.close()
        
        return uniprot_fasta
        
    def biogrid_analysis(self, ID1, ID2):
        # Check if referenced in BioGrid
        if ID1 != False and ID2 != False:
            total_low1 = 0
            total_exfiltered1 = 0
            # Analysis for ID1
            try:
                interactorB1_index = self.biogrid_data[ID1]['InteractorB'].index(ID2)
            except KeyError:    
                pass
            except ValueError:
                pass
            else:
                total_low1 += self.biogrid_data[ID1]['Low'][interactorB1_index]
                total_exfiltered1 += self.biogrid_data[ID1]['Low'][interactorB1_index]
             
            total_low2 = 0  
            total_exfiltered2 = 0  
            # Analysis for ID1
            try:
                interactorB2_index = self.biogrid_data[ID2]['InteractorB'].index(ID1)
            except KeyError:    
                pass
            except ValueError:
                pass
            else:
                total_low2 += self.biogrid_data[ID2]['Low'][interactorB2_index]    
                total_exfiltered2 += self.biogrid_data[ID2]['OutFiltered'][interactorB2_index]
            
            # Compute total
            total_low = (total_low2 + total_low1) / 2
            total_exfilter = (total_exfiltered1 + total_exfiltered2) / 2
            # Return values
            return int(total_low), int(total_exfilter)
            
        # If not referenced, means no interactions == 0    
        else:
            return 0, 0
            
            
    def check_valid_elm_entry(self, motif, pfamID, uniprot_entry1, uniprot_entry2, mstarts, mends):
        # Check if in db
        for entry in self.valid_elm_instances:
            if all([entry[0] == motif,
                    entry[1] == pfamID,
                    entry[2] == uniprot_entry1,
                    entry[3] == uniprot_entry2,
                    entry[4] == mstarts,
                    entry[5] == mends]):
                #vprint('ABSOLUTE MATCH FOUND FOR {0}'.format(entry), self.verbose)  
                return 1        
        return 0  
    
    def get_motif_disorder_score(self, m_start, m_end, gap_fasta):
        motif = gap_fasta[m_start:m_end]
        dis_score = 0
        for resid in motif:
            if resid not in ['-', '*']:
                dis_score += 1
        return dis_score / len(motif)       
        
class LiMIP(threading.Thread):
    #--------------------------------------------------------
    # --------------------- PROGRAM INIT --------------------
    #--------------------------------------------------------
    def __init__(self, user_id, input_file_list, biogrid_id, biogrid_data, uniprot_data, elm_data, valid_elm_instances,  elm_predict_tool, iupred_tool, verbose, total_cpu = False, strict_disorder = False, anchor2 = 0, specific_id = False):
        # Inherit from threading class
        super(LiMIP, self).__init__()
        # Init ending flag
        self.started = False
        self.finished = False
        init_time = time.time()
    # DATA BASE LOADING
        # Load ELM data
        self.elm_data = elm_data
        self.valid_elm_instances = valid_elm_instances
        
        # Load uniprot_data
        self.uniprot_data = uniprot_data
        
        # Load biogrid ( ID and connexions )
        self.biogrid_ID = biogrid_id
        self.biogrid_data = biogrid_data
        
        
        # Initialize linear motif interaction prediction tool
        self.elm_prediction_tool = elm_predict_tool
        # Initialize IUpred object (IUpred algorithm is used to compute disorder)
        self.IUpred_tool = iupred_tool
        
    # ARGUMENT LOADING
        # Load and check Uniprot codes list
        valid_unicode_list, unvalidated_list = check_uniprots(input_file_list, self.uniprot_data)
        self.unicode_list = valid_unicode_list
        
        # Look for BioGrid IDs!
        self.biogrid_list = []
        for uniprotcode in self.unicode_list:
            self.biogrid_list.append(self.get_biogrid_ID(uniprotcode))
        # Set output path
        self.output_path = LMIP_script_config.OUTPUT_DIR+user_id+'/'
        # Check if already exists (this should not happend as we check names before)
        # But we never know ...
        if not os.path.exists(self.output_path):
            os.mkdir(self.output_path)
        
        # Check for specific ids
        if specific_id != False and type(specific_id) == type(list()):
            # Apply computation on only specified ID's
            self.specific_id = specific_id
        else:
            # Apply computation on full list
            self.specific_id = list(range(len(self.unicode_list)))
        
        # Stric Disorder Flag
        self.strict_disorder = strict_disorder
        # Anchor2 Exclusion value
        self.anchor2 = anchor2
        
        # Total Available CPU use
        if type(total_cpu) == type(int()):
            if 1 <= total_cpu:
                self.total_cpu = total_cpu
            else:
                if 1 <= LMIP_script_config.TOTAL_CPU:
                    self.total_cpu = LMIP_script_config.TOTAL_CPU
                else:
                    self.total_cpu = 1
        else:
            if 1 <= LMIP_script_config.TOTAL_CPU:
                self.total_cpu = LMIP_script_config.TOTAL_CPU
            else:
                self.total_cpu = 1
        # Remember verbose wish
        self.verbose = verbose
        
        self.generated_files = []
        
    #--------------------------------------------------------
    # -------------------- MAIN PROGRAM ---------------------
    #--------------------------------------------------------
    def run(self):
        self.started = True
        # Generation of the final connexion data
        self.entry_pred_list = []
        final_connexion_data = []
        bio_connexion_dic = {}
        
        connexion_data_template = '{ELM_CLASS}\t{MATCHED_MOTIF}\t{MOTIF_UNICODE}\t{MOTIF_STARTS}\t{MOTIF_ENDS}\t{ASSOCIATED_PFAM}\t{MATCHED_PFAM_UNICODE}\t{EXPERIMENTAL_EVIDENCE}\t{ELM_PROBA}\t{STRICT_DISORDER}\t{SHORT_IUPRED_AVG_SCORE}\t{LONG_IUPRED_AVG_SCORE}\t{SHORT_IUPRED_GLOB_DOM}\t{LONG_IUPRED_GLOB_DOM}\t{ANCHOR2_AVG_SCORE}\t{BIOGRID_INTERACTIONS}\t{BIOGRID_EXFILTERED}\n'
        
        # Generation of header
        final_connexion_data.append(connexion_data_template.format(ELM_CLASS = 'ELM_CLASS',
                                                                   MATCHED_MOTIF = 'MATCHED_MOTIF',
                                                                   MOTIF_UNICODE = 'MOTIF_UNICODE',
                                                                   MOTIF_STARTS = 'MOTIF_STARTS',
                                                                   MOTIF_ENDS = 'MOTIF_ENDS',
                                                                   ASSOCIATED_PFAM = 'ASSOCIATED_PFAM',
                                                                   MATCHED_PFAM_UNICODE = 'MATCHED_PFAM_UNICODE',
                                                                   EXPERIMENTAL_EVIDENCE = 'EXPERIMENTAL_EVIDENCE',
                                                                   ELM_PROBA = 'ELM_PROBA',
                                                                   STRICT_DISORDER = 'STRICT_DISORDER',
                                                                   SHORT_IUPRED_AVG_SCORE = 'SHORT_IUPRED_AVG_SCORE',
                                                                   LONG_IUPRED_AVG_SCORE = 'LONG_IUPRED_AVG_SCORE',
                                                                   SHORT_IUPRED_GLOB_DOM = 'SHORT_IUPRED_GLOB_DOM',
                                                                   LONG_IUPRED_GLOB_DOM = 'LONG_IUPRED_GLOB_DOM',
                                                                   ANCHOR2_AVG_SCORE = 'ANCHOR2_AVG_SCORE',
                                                                   BIOGRID_INTERACTIONS = 'BIOGRID_INTERACTIONS',
                                                                   BIOGRID_EXFILTERED = 'BIOGRID_EXFILTERED'))
        
        
        # This list is used to match PFam ID with rows (ELM_motifs) PFam ID
        uniprot_pfam_headers = [self.uniprot_data[selected_uniprot_entry]['PFAM'] for selected_uniprot_entry in self.unicode_list if selected_uniprot_entry in self.uniprot_data.keys()]
        
        valid_db_list = [self.unicode_list[k] for k in range(len(self.unicode_list)) if self.unicode_list[k] in self.uniprot_data.keys()]
        # Uniprot without PFam data ... :(
        unfound_uniprot_entries = [uniprot for uniprot in valid_db_list if len(self.uniprot_data[uniprot]['PFAM']) == 0]
        del valid_db_list
        
        # Set BioGrid Connexion analysis Object
        self.entry_pred_list.append(BioGridConnexions(biogrid_list = [self.biogrid_list[k] for k in self.specific_id],
                                                      connexion_dic = bio_connexion_dic,
                                                      biogrid_id = self.biogrid_ID,
                                                      biogrid_data = self.biogrid_data))
                                                      
        
        # Loop over each uniprot code
        for k, uniprot_entry in enumerate(self.unicode_list):
            # Check for specific ID
            if k in self.specific_id:
                # Init Entry thread
                entry_pred = EntryPredictor(entry = uniprot_entry,
                                            final_connexion_data = final_connexion_data,
                                            unfound_uniprot_entries = unfound_uniprot_entries,
                                            connexion_data_template = connexion_data_template,
                                            uniprot_pfam_headers = uniprot_pfam_headers,
                                            input_file_list = self.unicode_list,
                                            biogrid_list = self.biogrid_list,
                                            strict_disorder = self.strict_disorder,
                                            anchor2 = self.anchor2,
                                            biogrid_id = self.biogrid_ID,
                                            biogrid_data = self.biogrid_data,
                                            uniprot_data = self.uniprot_data,
                                            elm_data = self.elm_data,
                                            valid_elm_instances = self.valid_elm_instances,
                                            elm_predict_tool = self.elm_prediction_tool,
                                            iupred_tool = self.IUpred_tool)
                
                # Add thread to thead list    
                self.entry_pred_list.append(entry_pred)
        

        vprint('Progression :  0.0 %', self.verbose)
        # Wait for all thread to finish  
        while not self.all_thread_finished():    
            # Printout progression
            if self.verbose:
                delete_last_lines()
            vprint('Progression : {:>4} %'.format(round((self.finished_threads() / len(self.entry_pred_list))*100, 1)), self.verbose)
            
            # launch delta new unlaunched thread
            self.launch_next(self.total_cpu - self.current_actives())
            # Reduce CPU use
            time.sleep(0.02)
            
        vprint('Progression :  100 % -> Finished', self.verbose) 
        # Set outputfile name
        connexion_table_path = self.output_path+LMIP_script_config.CONNEXION_FILE_BASENAME
 
        # Write output file
        with open(connexion_table_path, 'w') as filout:
            for connexion_data in final_connexion_data:
                # Print file in terminal if option -v set (self.verbose == True)
                #vprint(connexion_data, self.verbose)
                filout.write(connexion_data)
 
        # Save generated filepath
        self.generated_files.append(connexion_table_path)


        """
        # BioGrid Partners Extention
        vprint('Launching BioGrid Partners Extention Analysis ...', self.verbose)
        biogrid_extend = biogrid_extention.extend_results(connexion_table_path, self.unicode_list, biogrid_data = self.biogrid_data, biogrid_ID = self.biogrid_ID, uniprot_names = False, uniprot_data = self.uniprot_data)
        
        # Write file
        bio_extend_outputfile = self.output_path+LMIP_script_config.BIO_EXTEND_BASENAME
        with open(bio_extend_outputfile, 'w') as filout:
            filout.write(biogrid_extend)
        self.generated_files.append(bio_extend_outputfile)
        """


        # BioGrid TSV SubDataBase generation
        vprint('Launching BioGrid Partners Extention Analysis ...', self.verbose)
        
        #biogrid_subdb_tsv = biogrid_extention.gen_biogrid_tsv_subdb(self.unicode_list, biogrid_data = self.biogrid_data, biogrid_ID = self.biogrid_ID)
        tsv_row_template = '{0}\t{1}\t{2}\t{3}\t{4}'
        biogrid_subdb_tsv = [tsv_row_template.format('UNIPROT_A', 'UNIPROT_B', 'FILTERED', 'EXP_TYPE', 'PMID')]
        for index_1, uni_1 in enumerate(self.unicode_list):
            if uni_1 in bio_connexion_dic.keys():
                uni_1_key = uni_1
            else:
                try:
                    uni_1_key = 'BioID_'+self.biogrid_list[index_1]
                    if not uni_1_key in bio_connexion_dic.keys():
                        continue
                except:
                    continue
                    
            for uni_2_key in bio_connexion_dic[uni_1_key].keys():
                # Low Througput case
                for exp_type in bio_connexion_dic[uni_1_key][uni_2_key]['Low'].keys():
                    for pmid in bio_connexion_dic[uni_1_key][uni_2_key]['Low'][exp_type]:
                        biogrid_subdb_tsv.append(tsv_row_template.format(uni_1_key, uni_2_key, 'Low', exp_type, pmid))
                # High-T and Exfiltered
                for exp_type in bio_connexion_dic[uni_1_key][uni_2_key]['OutFiltered'].keys():
                    for pmid in bio_connexion_dic[uni_1_key][uni_2_key]['OutFiltered'][exp_type]:
                        biogrid_subdb_tsv.append(tsv_row_template.format(uni_1_key, uni_2_key, 'OutFiltered', exp_type, pmid)) 
        
        # Write file
        bio_subdb_tsvfilepath = self.output_path+LMIP_script_config.BIO_SUBDB_BASENAME
        with open(bio_subdb_tsvfilepath, 'w') as filout:
            filout.write('\n'.join(biogrid_subdb_tsv))
        self.generated_files.append(bio_subdb_tsvfilepath)

        #bio_subdb_jsonfilepath = self.output_path+LMIP_script_config.BIO_SUBDB_JSON_BASENAME
        #with open(bio_subdb_jsonfilepath, 'w', encoding='utf-8') as filout:
        #    json.dump(bio_connexion_dic, filout, ensure_ascii=False, indent=4) 
        
        """ 
        # Check for unfound entries
        vprint('Launching No-ELM-Partners Analysis ...', self.verbose)
        unfound_analysis = []
        # Add header
        unfound_analysis.append(['NoELMPaternsUnicodes/UnicodeList']+self.unicode_list)
        # Loop over rows
        for list_index, uniprot_entry in enumerate(unfound_uniprot_entries):
            # Init new row
            new_row = [uniprot_entry]
            
            # Loop over colunms
            for list_index2, uniprot_entry2 in enumerate(self.unicode_list):
                # Gather data
                total_biogrid_interactions, exfiltered_interactions = self.biogrid_analysis(self.biogrid_list[self.unicode_list.index(uniprot_entry)], self.biogrid_list[list_index2])
                
                # Add data to table
                new_row.append(str(total_biogrid_interactions+exfiltered_interactions))
                
            # Add row to table    
            unfound_analysis.append(new_row)
                
        # Write output file
        with open(self.output_path+'UNFOUND_ANALYSIS.csv', 'w') as filout:
            for row in unfound_analysis:
                # Print file in terminal if option -v set (self.verbose == True)
                vprint(';'.join(row), self.verbose)
                filout.write(';'.join(row)+'\n')
        
        # Save generated filepath
        self.generated_files.append(self.output_path+'UNFOUND_ANALYSIS.csv')
        """
        
        # Set ending variable       
        self.finished = True
        
    #--------------------------------------------------------
    # ---------------------- FUNCTIONS ----------------------
    #--------------------------------------------------------
    def all_thread_finished(self):
        for entry in self.entry_pred_list:
            if not entry.finished:
                return False
        return True
    
    def finished_threads(self):
        ended_threads = 0
        for entry in self.entry_pred_list:
            if entry.finished and entry.started:
                ended_threads += 1
        return ended_threads
        
    def current_actives(self):
        total_act = 0
        for entry in self.entry_pred_list:
            if entry.started and not entry.finished:
                total_act += 1
        return total_act
        
    def launch_next(self, n):
        for i in range(n):
            for entry in self.entry_pred_list:
                if not entry.started and not entry.finished:
                    entry.start()
                    break  
        
    def biogrid_analysis(self, ID1, ID2):
        # Check if referenced in BioGrid
        if ID1 != False and ID2 != False:
            total_low1 = 0
            total_exfiltered1 = 0
            # Analysis for ID1
            try:
                interactorB1_index = self.biogrid_data[ID1]['InteractorB'].index(ID2)
            except KeyError:    
                pass
            except ValueError:
                pass
            else:
                total_low1 += self.biogrid_data[ID1]['Low'][interactorB1_index]
                total_exfiltered1 += self.biogrid_data[ID1]['Low'][interactorB1_index]
             
            total_low2 = 0  
            total_exfiltered2 = 0  
            # Analysis for ID1
            try:
                interactorB2_index = self.biogrid_data[ID2]['InteractorB'].index(ID1)
            except KeyError:    
                pass
            except ValueError:
                pass
            else:
                total_low2 += self.biogrid_data[ID2]['Low'][interactorB2_index]    
                total_exfiltered2 += self.biogrid_data[ID2]['OutFiltered'][interactorB2_index]
            
            # Compute total
            total_low = (total_low2 + total_low1) / 2
            total_exfilter = (total_exfiltered1 + total_exfiltered2) / 2
            # Return values
            return int(total_low), int(total_exfilter)
            
        # If not referenced, means no interactions == 0    
        else:
            return 0, 0
        
    def get_biogrid_ID(self, target_uniprot):
        if target_uniprot in self.biogrid_ID.keys():
            return self.biogrid_ID[target_uniprot]
        return False           
        
        
    

#--------------------------------------------------------
# ----------------------- CLASSE -----------------------
#--------------------------------------------------------
class ELM_Prediction_Tool():
    #--------------------------------------------------------
    # --------------------- PROGRAM INIT --------------------
    #--------------------------------------------------------
    def __init__(self, elm_interaction):
        # Load ELM_classes database
        self.elm_classes = self.load_elm_classes(LMIP_script_config.ELM_CLASSES)
        
        # Load ELM_interaction database
        self.elm_interactions = elm_interaction 
        
    #--------------------------------------------------------
    # -------------------- MAIN PROGRAM ---------------------
    #--------------------------------------------------------
    def predict(self, fasta_seq, specific_class = False):
        # Gather wished data
        ELM_entries = {'MOTIF_NAMES':[], 'MOTIF_PATERN_MATCH':[], 'MOTIF_LOCATIONS':[], 'MOTIF_EVALUE':[], 'PATERN_REGEX':[]}
        
        # Specific class case
        if specific_class:
            motif_matchs = self.elm_classes[specific_class]['CREGEX'].finditer(fasta_seq)
            match_motif_list = []
            match_motif_loc = []
            
            # Loop over matchs
            # This may not loop if no match found
            for match in motif_matchs:
                # Get match_motif seq
                matched_motif = fasta_seq[match.start():match.end()]
                
                match_motif_list.append(matched_motif)
                match_motif_loc.append('{start}-{end}'.format(start = (match.start()+1), end = match.end()))
            
            
            # Add if motif found    
            if len(match_motif_list) > 0:    
                # Add data to variable
                ELM_entries['MOTIF_NAMES'].append(specific_class)
                ELM_entries['MOTIF_EVALUE'].append(self.elm_classes[specific_class]['PROBA'])
                ELM_entries['MOTIF_PATERN_MATCH'].append(match_motif_list)
                ELM_entries['PATERN_REGEX'].append(self.elm_classes[specific_class]['REGEX'])
                ELM_entries['MOTIF_LOCATIONS'].append(match_motif_loc)
                
            return ELM_entries
    
        # Loop over ELM classes
        for elm_class_name in self.elm_classes.keys():   
            # Try to find motif in chain
            # self.elm_classes[elm_class_name]['CREGEX'] is an already compiled regex for class
            motif_matchs = self.elm_classes[elm_class_name]['CREGEX'].finditer(fasta_seq)
            
            match_motif_list = []
            match_motif_loc = []
            
            # Loop over matchs
            # This may not loop if no match found
            for match in motif_matchs:
                # Get match_motif seq
                matched_motif = fasta_seq[match.start():match.end()]
                
                match_motif_list.append(matched_motif)
                match_motif_loc.append('{start}-{end}'.format(start = (match.start()+1), end = match.end()))
            
            
            # Add if motif found    
            if len(match_motif_list) > 0:    
                # Add data to variable
                ELM_entries['MOTIF_NAMES'].append(elm_class_name)
                ELM_entries['MOTIF_EVALUE'].append(self.elm_classes[elm_class_name]['PROBA'])
                ELM_entries['MOTIF_PATERN_MATCH'].append(match_motif_list)
                ELM_entries['PATERN_REGEX'].append(self.elm_classes[elm_class_name]['REGEX'])
                ELM_entries['MOTIF_LOCATIONS'].append(match_motif_loc)
            
        # Save end variable    
        return ELM_entries  
        
    #--------------------------------------------------------
    # ---------------------- FUNCTIONS ----------------------
    #--------------------------------------------------------
    def load_elm_classes(self, elm_classes_db):
        # Load db
        with open(elm_classes_db) as filin:
            elm_classes = json.load(filin)
        
        # Compile regular expressions
        for classe in elm_classes.keys():
            elm_classes[classe]['CREGEX'] = re.compile(elm_classes[classe]['REGEX'])    
            
        return elm_classes        
                              
#--------------------------------------------------------
# ----------------------- CLASSE ------------------------
#--------------------------------------------------------
"""
# IUPred2A: context-dependent prediction of protein disorder as a function of redox state and protein binding
# Balint Meszaros, Gabor Erdos, Zsuzsanna Dosztanyi
# Nucleic Acids Research 2018;46(W1):W329-W337.

Algorithm is the same, but script was reformulated as a Python object, to be used many times
"""
class IUpred():
    # Initialize object
    def __init__(self, anchor2 = True):
        # Save variables
        self.anchor2_calulation = anchor2
        # Load IUpred database
        self.load_iupred_db()
    
    
    def own_workflow(self, seq):
        iupred_score, glob_domains = self.iupred_workflow(seq, 'short')
        long_iupred_score, long_glob_domains = self.iupred_workflow(seq, 'long')
        anchor2_res = self.anchor2(seq, long_iupred_score)
        
        short_disorder_fasta = seq
        for glob_dom in glob_domains:
            short_disorder_fasta = short_disorder_fasta[:glob_dom[0]]+ '*'*(glob_dom[1]-glob_dom[0])+short_disorder_fasta[glob_dom[1]:]
            
        long_disorder_fasta = seq
        for glob_dom in long_glob_domains:
            long_disorder_fasta = short_disorder_fasta[:glob_dom[0]]+ '*'*(glob_dom[1]-glob_dom[0])+short_disorder_fasta[glob_dom[1]:] 
            
                  
        return iupred_score, long_iupred_score, short_disorder_fasta, long_disorder_fasta, anchor2_res       

    def load_iupred_db(self):
        self.iupred_db = {"short":{"LC":1,
                                   "UC":25,
                                   "WC":10,
                                   "MTX":self.read_matrix("{}iupred2_short_energy_matrix".format(LMIP_script_config.IUPRED_DATA))},
                          "long":{"LC":1,
                                  "UC":100,
                                  "WC":10,
                                  "MTX":self.read_matrix("{}iupred2_long_energy_matrix".format(LMIP_script_config.IUPRED_DATA))}}
                                   
        
        # Short historgam    
        shisto, shisto_min, shisto_max, shisto_step = self.read_histo("{}short_histogram".format(LMIP_script_config.IUPRED_DATA))
        self.iupred_db["short"]["HISTO"] = shisto
        self.iupred_db["short"]["HISTO_MIN"] = shisto_min
        self.iupred_db["short"]["HISTO_MAX"] = shisto_max
        self.iupred_db["short"]["HISTO_STEP"] = shisto_step
        
        # Long histogram
        lhisto, lhisto_min, lhisto_max, lhisto_step = self.read_histo("{}long_histogram".format(LMIP_script_config.IUPRED_DATA))
        self.iupred_db["long"]["HISTO"] = lhisto
        self.iupred_db["long"]["HISTO_MIN"] = lhisto_min
        self.iupred_db["long"]["HISTO_MAX"] = lhisto_max
        self.iupred_db["long"]["HISTO_STEP"] = lhisto_step
        self.anchor2_mtx = self.read_matrix('{}anchor2_energy_matrix'.format(LMIP_script_config.IUPRED_DATA))
        self.interface_comp = {}
        with open('{}anchor2_interface_comp'.format(LMIP_script_config.IUPRED_DATA)) as _fn:
            for line in _fn:
                self.interface_comp[line.split()[1]] = float(line.split()[2])    
    
           
            
    def iupred_workflow(self, seq, mode):

        unweighted_energy_score = [0] * len(seq)
        weighted_energy_score = [0] * len(seq)
        iupred_score = [0] * len(seq)

        for idx in range(len(seq)):
            freq_dct = self.aa_freq(seq[max(0, idx - self.iupred_db[mode]["UC"]):max(0, idx - self.iupred_db[mode]["LC"])] + seq[idx + self.iupred_db[mode]["LC"] + 1:idx + self.iupred_db[mode]["UC"] + 1])
            for aa, freq in freq_dct.items():
                try:
                    unweighted_energy_score[idx] += self.iupred_db[mode]["MTX"][seq[idx]][aa] * freq
                except KeyError as err:
                    unweighted_energy_score[idx] += 0
        if mode == 'short':
            for idx in range(len(seq)):
                for idx2 in range(idx - self.iupred_db[mode]["WC"], idx + self.iupred_db[mode]["WC"] + 1):
                    if idx2 < 0 or idx2 >= len(seq):
                        weighted_energy_score[idx] += -1.26
                    else:
                        weighted_energy_score[idx] += unweighted_energy_score[idx2]
                weighted_energy_score[idx] /= len(range(idx - self.iupred_db[mode]["WC"], idx + self.iupred_db[mode]["WC"] + 1))
        else:
            weighted_energy_score = self.smooth(unweighted_energy_score, self.iupred_db[mode]["WC"])
        
        # Compute globular domain
        glob_domains = self.get_globular_domains(weighted_energy_score)

        
        for idx, val in enumerate(weighted_energy_score):
            if val <= self.iupred_db[mode]["HISTO_MIN"] + 2 * self.iupred_db[mode]["HISTO_STEP"]:
                iupred_score[idx] = 1
            elif val >= self.iupred_db[mode]["HISTO_MAX"] - 2 * self.iupred_db[mode]["HISTO_STEP"]:
                iupred_score[idx] = 0
            else:
                iupred_score[idx] = self.iupred_db[mode]["HISTO"][int((weighted_energy_score[idx] - self.iupred_db[mode]["HISTO_MIN"]) * (1 / self.iupred_db[mode]["HISTO_STEP"]))]
                
        return iupred_score, glob_domains
    
    def get_globular_domains(self, weighted_energy_score):
        gr = []
        in_gr = False
        beg, end = 0, 0
        for idx, val in enumerate(weighted_energy_score):
            if in_gr and val <= 0.3:
                gr.append({0: beg, 1: end})
                in_gr = False
            elif in_gr:
                end += 1
            if val > 0.3 and not in_gr:
                beg = idx
                end = idx
                in_gr = True
        if in_gr:
            gr.append({0: beg, 1: end})
        mgr = []
        k = 0
        kk = k + 1
        if gr:
            beg = gr[0][0]
            end = gr[0][1]
        nr = len(gr)
        while k < nr:
            if kk < nr and gr[kk][0] - end < 45:
                beg = gr[k][0]
                end = gr[kk][1]
                kk += 1
            elif end - beg + 1 < 35:
                k += 1
                if k < nr:
                    beg = gr[k][0]
                    end = gr[k][1]
            else:
                mgr.append({0: beg, 1: end})
                k = kk
                kk += 1
                if k < nr:
                    beg = gr[k][0]
                    end = gr[k][1]
        
        return mgr     
            
    def avg(self, lst):
        return (sum(lst) / len(lst))


    def aa_freq(self, _seq):
        _freq = {}
        for _aa in _seq:
            if _aa in _freq:
                _freq[_aa] += 1
            else:
                _freq[_aa] = 1
        for _aa, _ins in _freq.items():
            _freq[_aa] = _ins / len(_seq)
        return _freq


    def read_matrix(self, matrix_file):
        _mtx = {}
        with open(matrix_file, "r") as _fhm:
            for _line in _fhm:
                if _line.split()[0] in _mtx:
                    _mtx[_line.split()[0]][_line.split()[1]] = float(_line.split()[2])
                else:
                    _mtx[_line.split()[0]] = {}
                    _mtx[_line.split()[0]][_line.split()[1]] = float(_line.split()[2])
        return _mtx


    def read_histo(self, histo_file):
        hist = []
        h_min = float("inf")
        h_max = -float("inf")
        with open(histo_file, "r") as fnh:
            for _line in fnh:
                if _line.startswith("#"):
                    continue
                if float(_line.split()[1]) < h_min:
                    h_min = float(_line.split()[1])
                if float(_line.split()[1]) > h_max:
                    h_max = float(_line.split()[1])
                hist.append(float(_line.split()[-1]))
        h_step = (h_max - h_min) / (len(hist))
        return hist, h_min, h_max, h_step


    def smooth(self, energy_list, window):
        weighted_energy_score = [0] * len(energy_list)
        for idx in range(len(energy_list)):
            weighted_energy_score[idx] = self.avg(energy_list[max(0, idx - window):min(len(energy_list), idx + window + 1)])
        return weighted_energy_score


    def anchor2(self, seq, iupred_scores):
    
        local_window_size = 41
        iupred_window_size = 30
        local_smoothing_window = 5
        par_a = 0.0013
        par_b = 0.26
        par_c = 0.43
        iupred_limit = par_c - (par_a / par_b)
        
        local_energy_score = [0] * len(seq)
        interface_energy_score = [0] * len(seq)
        energy_gain = [0] * len(seq)
        for idx in range(len(seq)):
            freq_dct = self.aa_freq(seq[max(0, idx - local_window_size):max(0, idx - 1)] + seq[idx + 2:idx + local_window_size + 1])
            for aa, freq in freq_dct.items():
                try:
                    local_energy_score[idx] += self.anchor2_mtx[seq[idx]][aa] * freq
                except KeyError:
                    local_energy_score[idx] += 0
            for aa, freq in self.interface_comp.items():
                try:
                    interface_energy_score[idx] += self.anchor2_mtx[seq[idx]][aa] * freq
                except KeyError:
                    interface_energy_score[idx] += 0
            energy_gain[idx] = local_energy_score[idx] - interface_energy_score[idx]   
        iupred_scores = self.smooth(iupred_scores, iupred_window_size)
        energy_gain = self.smooth(self.smooth(energy_gain, local_smoothing_window), local_smoothing_window)
        anchor_score = [0] * len(seq)
        for idx in range(len(seq)):
            sign = 1
            if energy_gain[idx] < par_b and iupred_scores[idx] < par_c:
                sign = -1
            corr = 0
            if iupred_scores[idx] > iupred_limit and energy_gain[idx] < 0:
                corr = (par_a / (iupred_scores[idx] - par_c)) + par_b
            anchor_score[idx] = sign * (energy_gain[idx] + corr - par_b) * (iupred_scores[idx] - par_c)
            anchor_score[idx] = 1 / (1 + math.e ** (-22.97968 * (anchor_score[idx] - 0.0116)))
        return anchor_score



############################################
############# HANDLE FUNCTIONS #############
############################################
def delete_last_lines(n=1):
    for erase in range(n):
        sys.stdout.write('\x1b[1A')
        sys.stdout.write('\x1b[2K')

def vprint(what_to_print, verbose_flag=True, refresh=False):
    if verbose_flag == True:
        if refresh == True:
            print(what_to_print, end='\r')
        else:
            print(what_to_print)

def gen_userid(user_id = None):
    if user_id == None:
        new_user_id = ''.join([random.choice(string.ascii_letters+string.digits) for letter in range(12)])
        while os.path.exists(LMIP_script_config.OUTPUT_DIR+new_user_id):
            new_user_id = ''.join([random.choice(string.ascii_letters+string.digits) for letter in range(12)])
    else:
        new_user_id = user_id
        index = 1
        while os.path.exists(LMIP_script_config.OUTPUT_DIR+new_user_id):
            new_user_id = user_id+'_'+str(index)
            index += 1
    return new_user_id           
    
def argument_handler():
    all_args = ' '.join(sys.argv)+' '
# List of Uniprot code out of MS2MODEL
    if all_args.find('-i ') != -1:
        list_uniprot = all_args[all_args.find('-i ')+len('-i '):].split(' ')[0]
    elif all_args.find('--uniin ') != -1:
        list_uniprot = all_args[all_args.find('--uniin ')+len('--uniin '):].split(' ')[0]
    else:
        list_uniprot = " "
        
# List of Specific IDs to compute
    if all_args.find('-s ') != -1:
        list_spec_id = all_args[all_args.find('-s ')+len('-s '):].split(' ')[0]
    elif all_args.find('--spec ') != -1:
        list_spec_id = all_args[all_args.find('--spec ')+len('--spec '):].split(' ')[0]
    else:
        list_spec_id = False
        

# Anchor2 exclusion value
    if all_args.find('-a ') != -1:
        anchor2 = all_args[all_args.find('-a ')+len('-a '):].split(' ')[0]
    elif all_args.find('--anchor2 ') != -1:
        anchor2 = all_args[all_args.find('--anchor2 ')+len('--anchor2 '):].split(' ')[0]
    else:
        anchor2 = False
       
# Total usable CPU 
    if all_args.find('-c ') != -1:
        cpu = all_args[all_args.find('-c ')+len('-c '):].split(' ')[0]
    elif all_args.find('--cpu ') != -1:
        cpu = all_args[all_args.find('--cpu ')+len('--cpu '):].split(' ')[0]
    else:
        cpu = False
        
# Disorder Flag
    if all_args.find('-d ') != -1:
        strict_disorder = True
    elif all_args.find('--disorder ') != -1:
        strict_disorder = True
    else:
        strict_disorder = False
        
# User id arg
    if all_args.find('-u ') != -1:
        userid = all_args[all_args.find('-u ')+len('-u '):].split(' ')[0]
    elif all_args.find('--user ') != -1:
        userid = all_args[all_args.find('--user ')+len('--user '):].split(' ')[0]
    else:
        userid = gen_userid()           
# verbose option
    if all_args.find('-v ') != -1 or all_args.find('-V ') != -1:
        verbose = True
    elif all_args.find('--verbose ') != -1 or all_args.find('--VERBOSE ') != -1:
        verbose = True
    else:
        verbose = False       
# help option
    if all_args.find('-h ') != -1 or all_args.find('-H ') != -1:
        help_display = True
    elif all_args.find('--help ') != -1 or all_args.find('--HELP ') != -1:
        help_display = True
    else:
        help_display = False  
        
    # Return value
    return {"uniprot_list":list_uniprot,
            "specific_id":list_spec_id,
            "anchor2":anchor2,
            "cpu":cpu,
            "strict_disorder":strict_disorder,
            "user_id":userid,
            "verbose":verbose,
            "help":help_display}             

def argument_checker(arg_dic):
    # Return error list of messages initialization
    error_messages = []
    notes_messages = []
    
# UNIPROT list case
    if not os.path.exists(arg_dic["uniprot_list"]):
        verified_uniprot_list =  False
        notes_messages.append('NOTES : NO UNIPROT LIST INPUT ...\n')
        error_messages.append('ERROR : -i path/to/uniprotlist.txt is requiered')
    else:
        input_notes, input_errors = check_uniprot_list_file(os.path.abspath(arg_dic["uniprot_list"]))
        notes_messages.append(input_notes)
        if len(input_errors) == 0:
            verified_uniprot_list = os.path.abspath(arg_dic["uniprot_list"])
        else:
            verified_uniprot_list = 'UNPROPER'
            error_messages.append(input_errors)
            
            
# Specific IDs case    
    try:
        # Load list
        uniprot_list_loaded = MS2MODEl_uniprot_list_loader(arg_dic["uniprot_list"])
        # Set list
        verified_specific_id = []
        failed_specific_id = []
        # Try to split data
        specific_ids = arg_dic["specific_id"].split(',')
        # Loop over ids
        for sid in specific_ids:
            try:
                if 0 <= int(sid) < len(uniprot_list_loaded):
                    verified_specific_id.append(int(sid))
            except:
                failed_specific_id.append(sid)
                
    except:
        verified_specific_id = False
        failed_specific_id = False
        
    finally:
        # Set final value
        if verified_specific_id in [[], False]:
            verified_specific_id = False
        
        # If user asked for specific IDs
        if arg_dic["specific_id"] != False:
            # Retrieved Specifi IDS notes
            if verified_specific_id != False:
                notes_messages.append('NOTES : -s {0} specific IDs where validated\n'.format(len(verified_specific_id)))
                
            if type(failed_specific_id) == type(list()) and len(failed_specific_id) > 0:
                notes_messages.append('ERROR : -s List of specific IDs that where NOT validated and skiped :\n        {0}'.format(','.join(failed_specific_id)))
                    
        else:
            notes_messages.append('NOTES : All index will be analysed\n')
            
# Total CPU available
    # get default CPU in config file
    verified_cpu = LMIP_script_config.TOTAL_CPU
    # Not asked option : 
    if arg_dic["cpu"] != False:
        # Use CPU modification request
        try:
            cpu = int(arg_dic["cpu"])
            if 1 <= cpu:
                verified_cpu = cpu
        # Failure notes
            else:
                notes_messages.append('NOTES : --cpu {} is not valid'.format(arg_dic["cpu"]))
        except:
            notes_messages.append('NOTES : --cpu {} is not valid a integer (1<=cpu)'.format(arg_dic["cpu"]))
    # CPU in use for the run note      
    notes_messages.append('NOTES : {} CPU(s) available for the run \n'.format(verified_cpu))
    
# Strict Disorder case
    if arg_dic["strict_disorder"] == True:
        notes_messages.append('NOTES : --disorder Only Strictly disordered parts will be writtent in Connexion File\n')
                
# User ID case
    if os.path.exists(LMIP_script_config.OUTPUT_DIR+arg_dic["user_id"]):
        notes_messages.append('NOTES : -u path/to/user_id already exists')
        verified_userid = gen_userid(arg_dic["user_id"])
        notes_messages.append('MODIF : USER_ID modified to {0}\n'.format(verified_userid))
    else:
        verified_userid = arg_dic["user_id"]
        notes_messages.append('NOTES : {0} SET AS USER_ID ...\n'.format(verified_userid))

# Anchor2 value exclusion calse
    verified_anchor2_value = 0
    try:
        anchor2 = float(arg_dic["anchor2"])
        if 0 <= anchor2 < 1:
            verified_anchor2_value = anchor2
            notes_messages.append('NOTES : IUpred - Anchor2 exclusion value set to {0}\n'.format(verified_anchor2_value))
    except ValueError:
        try:
            split_anchor = arg_dic["anchor2"].split(',')
            anchor2 = float('.'.join(split_anchor))
            if 0 <= anchor2 < 1:
                verified_anchor2_value = anchor2
                notes_messages.append('NOTES : IUpred - Anchor2 exclusion value set to {0}\n'.format(verified_anchor2_value))
        except:
            pass
            
# Internal DB check    
    db_errors = 0
    if not os.path.exists(LMIP_script_config.ELM_INTERACTION_DOMAIN):
        error_messages.append('DATABASE ERROR -> ELM_interaction_domain MISSING at {0}\n'.format(LMIP_script_config.ELM_INTERACTION_DOMAIN))
        db_errors += 1
    if not os.path.exists(LMIP_script_config.ELM_VALIDE_INSTANCES):
        error_messages.append('DATABASE ERROR -> ELM_verified_instances MISSING at {0}\n'.format(LMIP_script_config.ELM_VALIDE_INSTANCES))
        db_errors += 1  
    if not os.path.exists(LMIP_script_config.ELM_CLASSES):
        error_messages.append('DATABASE ERROR -> ELM_classes MISSING at {0}\n'.format(LMIP_script_config.ELM_CLASSES))
        db_errors += 1   
    if not os.path.exists(LMIP_script_config.UNIPROT_DATA):
        error_messages.append('DATABASE ERROR -> UNIPROT table MISSING at {0}\n'.format(LMIP_script_config.UNIPROT_DATA))  
        db_errors += 1
        
    if db_errors > 0:
        error_messages.append('DATABASE ERRORS : Check path in LMIP_script_config or launch DB_UPDATER.py before retying\n')
    
                
    # Return verified arguments            
    return {"uniprot_list":verified_uniprot_list,
            "specific_id":verified_specific_id,
            "strict_disorder":arg_dic["strict_disorder"],
            "anchor2":verified_anchor2_value,
            "cpu":verified_cpu,
            "user_id":verified_userid,
            "verbose":arg_dic["verbose"],
            "errors":error_messages,
            "notes":notes_messages,
            "help":arg_dic["help"]}

def check_uniprot_list_file(file_path):
    # Return error list of messages initialization
    error_messages = []
    notes_messages = []
    
    # Algorithm to check if file is corrupted
    
    # Return notes and errors
    return '\n'.join(notes_messages), '\n'.join(error_messages)

def display_help():
    if os.path.exists('./README'):
        with open('./README', 'r') as filin:
            print(''.join(filin.readlines()))
    else:
        print(README)

#####################################################
############# DATABASE IMPORT FUNCTIONS #############
#####################################################
def import_databases():
    elm_data = ELM_data_loader(LMIP_script_config.ELM_INTERACTION_DOMAIN)
    valid_elm_instances = ELM_data_valid(LMIP_script_config.ELM_VALIDE_INSTANCES)
    
    # Load uniprot_data
    uniprot_data = UNIPROT_data_loader(LMIP_script_config.UNIPROT_DATA)
    # Load biogrid ( ID and connexions )
    # self.biogrid_ID, self.biogrid_data
    with open(LMIP_script_config.BIOGRID_ID) as filin_id:
        biogrid_ID = json.load(filin_id)
    with open(LMIP_script_config.BIOGRID_DATA) as filin_data:
        biogrid_data = json.load(filin_data)
    
    # Initialize Objects
    # Initialize linear motif interaction prediction tool
    elm_prediction_tool = ELM_Prediction_Tool(elm_data)
    # Initialize IUpred object (IUpred algorithm is used to compute disorder)
    IUpred_tool = IUpred()
    
    return biogrid_ID, biogrid_data, uniprot_data, elm_data, valid_elm_instances,  elm_prediction_tool, IUpred_tool



def ELM_data_loader(elm_interaction_dom_filepath):
    with open(elm_interaction_dom_filepath) as filin:
        elm_data = json.load(filin)
    return elm_data


def UNIPROT_data_loader(uniprot_filepath):
    with open(uniprot_filepath) as filin:
        uniprot_data = json.load(filin)
    return uniprot_data
    
def load_elm_classes(elm_classes_db):
    # Load db
    with open(elm_classes_db) as filin:
        elm_classes = json.load(filin)
    
    # Compile regular expressions
    for classe in elm_classes.keys():
        elm_classes[classe]['CREGEX'] = re.compile(elm_classes[classe]['REGEX'])    
        
    return elm_classes
    
def ELM_data_valid(path):
    # Load ELM_interactions_database
    with open(path) as filin:
        readcsv = csv.reader(filin, delimiter='\t')
        elm_valid_instances = []
        for i, row in enumerate(readcsv):
            if i == 0: 
                # Get header
                headers = row
            else:    
                # Get data
                elm_valid_instances.append(row)
                
    return elm_valid_instances

def MS2MODEl_uniprot_list_loader(ms2model_output_filepath):
    # Generate return list
    uniprot_list = []
    
    # Loop over file
    with open(ms2model_output_filepath, 'r') as filin:
        for line in filin:
            if line.strip() == '':
                continue
            else:
                uniprot_list.append(line.strip())
            """
            if len(line.strip()) == 6:
                uniprot_list.append(line.strip())
            else:    
                try:
                    uniprot_id = line.split('|')[1]
                    uniprot_list.append(uniprot_id)
                except IndexError:
                    print("Couldn't read UniProt in line"+line)
                    continue
            """
    return uniprot_list

def check_uniprots(list, database):
    verified_list = []
    unvalid_list = []
    for entry in list:
        if entry in database.keys():
            verified_list.append(entry)
        else:
            unvalid_list.append(entry)
    return verified_list, unvalid_list
    
####################################################
############## MAIN PROGRAM HANDLER ################
####################################################
if __name__ == '__main__':
    # Launch Script
    try:
        # Starting time var
        start_time = time.time()

        # Get arguments
        arguments_dic = argument_handler()
        
        # Welcome message
        vprint('\n\n{0}- LINEAR MOTIF INTERACTION PREDICTION {0}\n'.format("-"*21), arguments_dic["verbose"])
        vprint(CREDITS, arguments_dic["verbose"])
            
        # Verify arguments
        verified_dic = argument_checker(arguments_dic)
        
        # Print notes
        vprint('\n{0}\n\nLAUNCHING NOTES :\n{1}\n\n{0}\n'.format("-"*80, '\n'.join(verified_dic["notes"])), verified_dic["verbose"])
        
        # if no errors and all inputs were verified
        if len(verified_dic["errors"]) == 0 and verified_dic["help"] == False:
        
            ############################################
            ############## MAIN PROGRAMM ###############
            ############################################
            vprint('Initiating new project ...', verified_dic["verbose"])
            wished_output_path = LMIP_script_config.OUTPUT_DIR+verified_dic["user_id"]+'/'
            os.mkdir(wished_output_path)

            # Load databases
            vprint('Loading Databases ...', verified_dic["verbose"])
            biogrid_id, biogrid_data, uniprot_data, elm_data, valid_elm_instances,  elm_predict_tool, iupred_tool = import_databases()
            
            # Load uniprot list
            imported_uniprot_list = MS2MODEl_uniprot_list_loader(verified_dic["uniprot_list"])
            
            # Verify Uniprots in UniprotDB
            verified_list, unvalidated = check_uniprots(imported_uniprot_list, uniprot_data)
            
            # Print unvalidated
            print('\n    '.join(['WARNING -> Unfound Uniprots in OWN_UniprotDB :\n']+unvalidated), file=sys.stderr)
            vprint('\n    '.join(['WARNING -> Unfound Uniprots in OWN_UniprotDB :\n']+unvalidated))
            
            vprint('Launching LiMIP ...', verified_dic["verbose"])
            # Initialize work
            limip_analysis = LiMIP(user_id = verified_dic["user_id"],
                                   input_file_list = verified_list,
                                   biogrid_id = biogrid_id,
                                   biogrid_data = biogrid_data,
                                   uniprot_data = uniprot_data,
                                   elm_data = elm_data,
                                   valid_elm_instances = valid_elm_instances,
                                   elm_predict_tool = elm_predict_tool,
                                   iupred_tool = iupred_tool,
                                   verbose = verified_dic["verbose"],
                                   total_cpu = verified_dic["cpu"],
                                   strict_disorder = verified_dic["strict_disorder"],
                                   anchor2 = verified_dic["anchor2"],
                                   specific_id = verified_dic["specific_id"])
            # Run programm
            limip_analysis.start()
            
            # Wait for the work to be completed
            while not limip_analysis.finished:
                time.sleep(1)
            # Final printout
            if verified_dic["verbose"]:
                # Computation time used
                print("\n{0} SOFTWARE ENDING MESSAGES {0}\n".format("-"*27))
                # Write connexion table path
                print('OUTPUT ELM CONNEXION FILE PATH :\n{0}\n'.format(limip_analysis.generated_files[0]))
                print('OUTPUT BioGrid CONNEXION FILE PATH :\n{0}\n'.format(limip_analysis.generated_files[1]))
                #print('OUTPUT UNFOUND ANALYSIS FILE PATH :\n{0}\n'.format(limip_analysis.generated_files[-1]))
                # Write computational time used
                print('Total computation time for analysis was of {:.2f} seconds\n'.format(time.time() - start_time))
                # Ending message
                print("\n{0}- THX FOR USING {0}\n".format("-"*32))
                
            # Exit software
            exit() 
            
                                             
        # If some errors were found                                     
        else:
            display_help()
            if verified_dic["help"]:
                exit()
                  
            # Print errors
            print('\n{}\n'.format('-'*80))
            for error in verified_dic["errors"]:
                print(error)
            print('\n{}\n'.format('-'*80))
            
            # Leave programm   
            exit('EXIT STATE : USER INPUT ERROR..')         
        
    except KeyboardInterrupt as err:
        exit('\nUser Keyboard Interruption.. \nOn his last words, the script said : "I did something wrong ?"')