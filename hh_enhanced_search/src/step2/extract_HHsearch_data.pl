#!/usr/bin/env perl

use strict;
use warnings;


# XXX Global dictionnaries XXX
my %MUSTANGDB_pdb2path; # Key = PDB [4CHAR_CHAIN]; Value = path to the corresponding MSA in the MUSTANG database
my %MUSTANGDB_path2pdb;


# time ($0 MOVEIT/proteasome20S/ ../databases/pdb70_clu.tsv ../databases/MUSTANG/ ../databases/pdb_seqatom.txt 2> STDERR > STDOUT)

sub main{
    if ($#ARGV != 4){
        die "Error: incorrect number of arguments\nUsage: $0 <LIST_OF_.out_FILES> <HHSEARCH_OUTPUT_DIR> <HHSEARCH_CLUSTER_FILE> <MUSTANG_DATABASE> <PDB_SEQRES>\n";
    }

    # GET ARGUMENTS
    # commented below is the former version which took the dir location as an argument
    my $hhoutputdir = $ARGV[1]; # Directory where all the .out files produced by HHsearch are located
    #$hhoutputdir =~ s/\/$//; # NOTE: $hhoutputdir dir better be located into the working dir...
    #my @hhout_files = `ls $hhoutputdir/*.out`;

    # new version below
    my @hhout_files = readfile($ARGV[0]);

    my $hhcluster_file = $ARGV[2]; # pdb70_clu.tsv
    my $MUSTANGDB = $ARGV[3];
    $MUSTANGDB =~ s/\/$//;
    my $PDBSEQATOM = $ARGV[4]; # FLAT FILE CONTAINING THE SEQATOM (not SEQRES) FOR EVERY UniProt THAT HAS A 3D STRUCTURE

    unless (-e $MUSTANGDB and -d $MUSTANGDB){
        die "Error: MUSTANG database $MUSTANGDB is not a valid directory\n";
    }

    # LOAD THE TWO GLOBAL DICTIONNARIES
    my @MUSTANGDB_array = `grep -oPr "^>\\d..._\\w+" $MUSTANGDB/`;
    foreach my $line (@MUSTANGDB_array){
        chomp $line;
        my ($path, $pdb) = split(':>', $line);
        if (exists $MUSTANGDB_pdb2path{$pdb}){
            print "\e[1;31mFATAL ERROR: The same PDB $pdb is found in two MUSTANG MSA files\e[0m\n";
        }
        $MUSTANGDB_pdb2path{$pdb} = $path;
        unless (exists $MUSTANGDB_path2pdb{$path}){
            $MUSTANGDB_path2pdb{$path} = [];
        }
        push(@{$MUSTANGDB_path2pdb{$path}}, $pdb);
    }


    # THE FOLLOWING TWO DICTIONNARIES ESTABLISH THE RELATIONSHIP: head_of_HH_cluster <=> member_of_HH_cluster
    my %original_clusters_head_to_members;
    my %original_clusters_members_to_head;
    open(IN, '<', $hhcluster_file) or die "Error while reading $hhcluster_file\n";
    while (my $line = <IN>){
        chomp $line;
        my ($head, $member) = split("\t", $line);
        unless (exists $original_clusters_head_to_members{$head}){
            $original_clusters_head_to_members{$head} = [];
        }
        push(@{$original_clusters_head_to_members{$head}}, $member);
        $original_clusters_members_to_head{$member} = $head;
    }
    close IN;


    # LOAD A DICTIONNARY IN WHICH:
    # - KEY = [4CHAR_CHAIN]
    # - VALUE = SEQATOM SEQUENCE
    my %hash_seqatom;
    open(IN, '<', $PDBSEQATOM) or die "Error while reading $PDBSEQATOM\n";
    while (my $line = <IN>){
        chomp $line;
        my ($pdb6char, $sequence) = split(' ', $line);
        $sequence =~ s/!//g; # XXX GAP INFORMATION IS KEPT IN THIS SEQATOM DATABASE (SYMBOL = '!')
        $hash_seqatom{$pdb6char} = $sequence;
    }
    close IN;


    # DEFINE THE NAMES OF THE OUTPUT FILE AND DIRS
    my $mafft_output_dir = $hhoutputdir.'_MAFFT_OUTPUT';
    #my $output_table_file = $hhoutputdir.'.hhdb';
    my $output_table_file = int(rand(1000000)).'.hhdb';
    #my $HHaln_temporary = $hhoutputdir.'.alntmp';
    my $HHaln_temporary = int(rand(1000000)).'.alntmp';

    unless (-e $mafft_output_dir and -d $mafft_output_dir){
        system "mkdir $mafft_output_dir";
    }

    # Creates a single FASTA file from all the HHsearch results: the .alntmp initialized above
    # 1. ERASE IF ALREADY EXISTS
    #if (-f $HHaln_temporary){
    #    system "rm $HHaln_temporary";
    #}
    # 2. WRITE IT
    get_hhalignments($hhoutputdir, $HHaln_temporary);
    my @allhhaln = readfile($HHaln_temporary); # All the pairwise alignments from HHsearch
    my $allhhaln_str = join("", @allhhaln);
    my @array_hhaln = split(">", $allhhaln_str);


    # IN THE FOLLOWING DICTIONNARY:
    # - KEY = UniProtAC + tab_separator + PDB_and_CHAIN
    # - VALUE = 4 tab-separated columns = '-' + ID% + '-' + ALIGNED_COLS
    # NOTE: THE TWO EMPTY COLUMNS ARE SPACES FOR HHProbability AND Resolution
    # ONCE LOADED, THE DICTIONNARY IS USED TO WRITE THE .hhaln file
    my %hash_results;

    # MAIN LOOP: FOR EACH .out FILE PRODUCED BY findTemplate.py
    foreach my $out_file (@hhout_files){
        chomp $out_file;
        my @hhresults = readfile($out_file);
 
        my $query = find_query_name(\@hhresults);

        # LIST ALL THE TEMPLATES FOUND BY HHSEARCH
        my @templates;
        foreach my $line (@hhresults){
            if ($line =~ m/>(\d\w\w\w_\w+):AUTOPDB; Probability=.+; Identities=.+%; Resolution=.+; MatchedRegionTemplate=/){
                my $template = $1;
                push(@templates, $template);
            }
        }

        # FOR EACH TEMPLATE, SEARCH INTO THE CLUSTERS 
        foreach my $line (@hhresults){
            if ($line =~ m/>(\d\w\w\w_\w+):AUTOPDB; Probability=.+; Identities=.+%; Resolution=.+; MatchedRegionTemplate=/){
                my $template = $1;
                my $hhaln_file = extract_alignment_from_hhsearchresults(\@array_hhaln, $query, $template);
                my @all_msa_files;

                # IF THERE IS A MSA FOR THAT TEMPLATE
                if (exists $MUSTANGDB_pdb2path{$template}){
                    print "\e[34mOK sub-cluster for HH template: $template\e[0m\n";
                    my $msa_file = $MUSTANGDB_pdb2path{$template};
                    my $msa_root = $1 if ($msa_file =~ m/(\d\w\w\w_\w+\.dat)/);
                    @all_msa_files = `ls $MUSTANGDB/$msa_root.*afasta`;
                }

                my $fails = 0;

                foreach my $msa_file (@all_msa_files){
                    chomp $msa_file;
                    if (-f $msa_file and -s $msa_file){ # CHECK FOR EMPTY FILE IN THE MUSTANG DATABASE (WHICH WOULD LEAD TO A CORE DUMP BY MAFFT)
                        my @name_parts = split('/', $msa_file);
                        my $mafft_out = "$query\_$template\_$name_parts[$#name_parts]";
                        system "mafft-profile $hhaln_file $msa_file > $mafft_out.mafftout 2> $mafft_out.maffterr";
                        foreach my $cluster_member (@{$MUSTANGDB_path2pdb{$msa_file}}){
                            my @pairwise_aln = extract_pairwise_aln_from_mafft_output("$mafft_out.mafftout", $cluster_member, $query);
    
                            # CASE: WHEN THE PDB IS PRESENT IN BOTH (i) THE PAIRWISE HHSEARCH ALIGNMENT AND (ii) THE MUSTANG MSA
                            # THEN THERE IS 3 SEQUENCES (INDEX=5), RATHER THAN 2
                            if ($#pairwise_aln == 5){
                                splice @pairwise_aln, 2, 2; # REMOVE THE HHSEARCH ALIGNMENT, WHICH IS XXX SUPPOSED XXX TO BE IN THE MIDDLE
                            }
                            if ($#pairwise_aln != 3){ # XXX NOT ELSIF!!! XXX
                                print "\e[1;33mWARNING_1: unknown error with pairwise alignment extracted from MSA (MAFFT file = $mafft_out.mafftout; PDB2SEARCH = $cluster_member; QUERY = $query; CLUSFILE = $msa_file)\e[0m\n";
                                print "########### ERROR BELOW ###########\n";
                                my @maffterr_array = readfile("$mafft_out.maffterr");
                                print @maffterr_array;
                                $fails++;
                            }
                            else{
                                my ($id_percent, $matched_cols) = compute_id_percent(\@pairwise_aln);
                                if ($id_percent eq 'NA' or $matched_cols == 0 or $matched_cols == 1){
                                    # CHECK THE TWO SEQUENCES: DO THEY ONLY CONTAIN LETTERS AND GAPS?
                                    if ($pairwise_aln[1] !~ m/^[-\w]+$/ or $pairwise_aln[3] !~ m/^[-\w]+$/){
                                        print "\e[1;31mERROR_1: can't compute id percent (MAFFT file = $mafft_out.mafftout; PDB2SEARCH = $cluster_member; QUERY = $query; CLUSFILE = $msa_file.msa)\e[0m\n";
                                        $fails++;
                                    }
                                    else{
                                        if ($id_percent eq 'NA'){
                                            $id_percent = 0;
                                        }
                                        $id_percent = sprintf("%.0f", $id_percent);
                                        $hash_results{"$query\t$cluster_member"} = "-\t$id_percent\t-\t$matched_cols"; # 4 columns: Proba Id Resol Cols
                                    }
                                }
                                else{
                                    $id_percent = sprintf("%.0f", $id_percent);
                                    $hash_results{"$query\t$cluster_member"} = "-\t$id_percent\t-\t$matched_cols"; # 4 columns: Proba Id Resol Cols
                                }
                            }
                        }
                    }
                }
                # ELSE: CREATE ONE!
                # THIS CREATION OF MSA IS ALSO ACTIVATED IN CASE OF ERROR WITH MUSTANG MSA (e.g. 'U' RESIDUES)
                if (!exists $MUSTANGDB_pdb2path{$template} or $fails){
                    print "\e[1;36mNo sub-cluster for HH template: $template\e[0m\n";
                    # XXX XXX XXX THIS
                    # XXX XXX XXX SHOULD
                    # XXX XXX XXX NOT
                    # XXX XXX XXX BE

                    # FIND THE ORIGINAL CLUSTER HEAD
                    my $head = $original_clusters_members_to_head{$template};

                    my $msa_name = "newclus_$head";

                    unless (-e "$msa_name.msa" and -f "$msa_name.msa"){
                        open(OUT, '>>', "$msa_name.multifasta") or die "ERROR: can't write $msa_name.multifasta\n";
                        foreach my $cluster_member (@{$original_clusters_head_to_members{$head}}){
                            if (exists $hash_seqatom{$cluster_member}){
                                print OUT ">$cluster_member\n$hash_seqatom{$cluster_member}\n";
                            }
                            else{
                                # NOTE: most likely the chains named '.' in hhsearch TSV file of clusters
                                print "WARNING: No seqatom found in the flat file for PDB $cluster_member\n";
                            }
                        }
                        close OUT;
    
                        system "mafft --auto $msa_name.multifasta > $msa_name.msa 2> $msa_name.msa_err";
                    }
                    my $mafft_out = "$query\_$template\_$msa_name";
                    system "mafft-profile $hhaln_file $msa_name.msa > $mafft_out.mafftout 2> $mafft_out.maffterr";
                    foreach my $cluster_member (@{$original_clusters_head_to_members{$head}}){
                        # IF IT HAS ALREADY BEEN CALCULATED VIA MUSTANG MSA, DON'T CHANGE IT
                        unless (exists $hash_results{"$query\t$cluster_member"}){
                            my @pairwise_aln = extract_pairwise_aln_from_mafft_output("$mafft_out.mafftout", $cluster_member, $query);
    
                            # CASE: WHEN THE PDB IS PRESENT IN BOTH (i) THE PAIRWISE HHSEARCH ALIGNMENT AND (ii) THE MUSTANG MSA
                            # THEN THERE IS 3 SEQUENCES (INDEX=5), RATHER THAN 2
                            if ($#pairwise_aln == 5){
                                splice @pairwise_aln, 2, 2; # REMOVE THE HHSEARCH ALIGNMENT, WHICH IS XXX SUPPOSED XXX TO BE IN THE MIDDLE
                            }
                            if ($#pairwise_aln != 3){
                                print "\e[1;34mWARNING_2: unknown error with pairwise alignment extracted from MSA (MAFFT file = $mafft_out.mafftout; PDB2SEARCH = $cluster_member; QUERY = $query; CLUSFILE = $msa_name.msa)\e[0m\n";
                                print "########### ERROR BELOW ###########\n";
                                my @maffterr_array = readfile("$mafft_out.maffterr");
                                print @maffterr_array;
                            }
                            else{
                                my ($id_percent, $matched_cols) = compute_id_percent(\@pairwise_aln);
                                if ($id_percent eq 'NA'){
                                    # CHECK THE TWO SEQUENCES: DO THEY ONLY CONTAIN LETTERS AND GAPS?
                                    if ($pairwise_aln[1] !~ m/^[-\w]+$/ or $pairwise_aln[3] !~ m/^[-\w]+$/){
                                        print "\e[1;35mERROR_2: can't compute id percent (MAFFT file = $mafft_out.mafftout; PDB2SEARCH = $cluster_member; QUERY = $query; CLUSFILE = $msa_name.msa)\e[0m\n";
                                    }
                                    else{
                                        if ($id_percent eq 'NA'){
                                            $id_percent = 0;
                                        }
                                        $id_percent = sprintf("%.0f", $id_percent);
                                        $hash_results{"$query\t$cluster_member"} = "-\t$id_percent\t-\t$matched_cols"; # 4 columns: Proba Id Resol Cols
                                    }
                                }
                                else{
                                    $id_percent = sprintf("%.0f", $id_percent);
                                    $hash_results{"$query\t$cluster_member"} = "-\t$id_percent\t-\t$matched_cols"; # 4 columns: Proba Id Resol Cols
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    open(OUT, '>', $output_table_file) or die "Error: cannot create $output_table_file\n\t$!\n"; 
    foreach my $key (sort keys %hash_results){
        print OUT "$key\t$hash_results{$key}\n";
    }
    close OUT;

    #system "readlink -f $output_table_file";

    # THE TASK BELOW IS NOW PERFORMED BY THE WRAPPER
    #system "mv $HHaln_temporary *.maffterr *.mafftout *.hhaln *.msa* *.multifasta $mafft_output_dir/";
}


################################################################################
#                                 SUBROUTINES                                  #
################################################################################

sub extract_alignment_from_hhsearchresults{
# Extract the pairwise alignment between the cluster member (template) and the query (UniProt AC)
    my @array_hhaln = @{$_[0]};
    my $ac2search = $_[1];
    my $template = $_[2];
    my $hhaln_file = '';

    for (my $i=0; $i<= $#array_hhaln; $i++){
        my $j = $i+1;
        if ($array_hhaln[$i] =~ m/$ac2search/ and $array_hhaln[$j] =~ m/$template/){
            $hhaln_file = $ac2search."_vs_".$template.".hhaln";
            unless (-e $hhaln_file and -f $hhaln_file){
                open (OUT, '>', $hhaln_file) or die "Error while writing $hhaln_file:\n\t$!\n";
                print OUT ">$array_hhaln[$i]", ">$array_hhaln[$j]";
                close OUT;
            }
        }
    }

    if (!$hhaln_file){
        print "WARNING: something went wrong with the creation of the .hhaln file of $ac2search and $template\n";
    }

    return $hhaln_file;
}


sub readfile{
    my $filename = shift;
    open(IN, '<', $filename) or die "\e[1;31mError\e[0m while opening $filename:\n\t$!\n";
    my @file = <IN>;
    close IN;
    return @file;
}


sub extract_pairwise_aln_from_mafft_output{
    my $maffoutfile = shift;
    my $pdb6char2search = shift;
    my $ac2search = shift;

    my @mafft_results = readfile($maffoutfile);
    my $mafft_results_str = join("", @mafft_results);
    my @array = split(">", $mafft_results_str);
    
    my @pairwise_aln_temp1;
    for (my $i=0; $i<= $#array; $i++){
        if ($array[$i] =~ m/$ac2search/ or $array[$i] =~ m/$pdb6char2search/){
            push (@pairwise_aln_temp1, ">$array[$i]");
        }
    }
    my $pairwise_aln_str = join("", @pairwise_aln_temp1);
    my @pairwise_aln_temp2 = split("\n", $pairwise_aln_str);
    my @pairwise_aln;
    foreach my $line (@pairwise_aln_temp2){
        if ($line =~ m/^>/ or $pairwise_aln[$#pairwise_aln] =~ m/^>/){
            push (@pairwise_aln, $line);
        }
        else{
            $pairwise_aln[$#pairwise_aln].=$line;
        }
    }
    return @pairwise_aln;
}


sub compute_id_percent{
    my @pairwise_aln = @{$_[0]};


    # XXX DOMAIN FINDER
    my $start = 0;
    my $end = 0;
    my @seqA = split("", $pairwise_aln[3]);
    for (my $j=0; $j <= $#seqA; $j++){
        if ($seqA[$j] ne '-' and !$start){
            $start=$j;
        }
        if (!$end){
            $end = $j;
        }
        if ($end && $seqA[$j] ne '-'){
            $end = $j;
        }
    }
 
    #print "START-1 = $start ; END-1 = $end\n";
 
    my @seqB = split("", $pairwise_aln[1]);
 
    my $id = 0;
    my $mid_gaps = 0;
    my $other_gaps = 0;
    # XXX COMPARE 2 SEQ
    for (my $k = $start; $k <= $end; $k++){
        if ($seqA[$k] eq $seqB[$k]){
            if ($seqA[$k] ne "-"){
                $id++;
            }
            else{
                $mid_gaps++;
            }
        }
        else{
            if ($seqA[$k] eq "-" or $seqB[$k] eq "-"){
                $other_gaps++;
            }
        }
    }
    my $aln_length = $end-$start+1-$mid_gaps-$other_gaps;
 
    #print "ID=$id; LEN=$aln_length\n";

    my $id_percent = 'NA';
    if ($aln_length > 0){
        $id_percent = ($id/$aln_length)*100;
    }

    return ($id_percent, $aln_length);
}

sub find_query_name{
    my @hhresults = @{$_[0]};

    my $query = '';

    if ($hhresults[0] =~ m/^Query/){
        my @A = split(' ', $hhresults[0]);
        $query = join('', @A[1 .. $#A]);
        chomp $query;
        # Get the UniProt AC, if any
        if ($query =~ m/\w\w\|(\w+)-?\d*\|\w+_\w+/){
            $query = $1;
        }
    }
    else{
        die "Error with HHsearch .out file: Query name not found\n";
    }

    return $query;
}


sub create_hash_alignments{
    my @hhr_files = `ls $_[0]/*.out`;

    my %hash;

    foreach my $hhr_file (@hhr_files){
        chomp $hhr_file;
        my @hhr_file_array = readfile($hhr_file);

        my $query = find_query_name(\@hhr_file_array);

        my $stop = 0;
        for (my $No = 1; $No < 500; $No++){ # XXX No more than 500 results in a HHsearch results file
            my @array;
            my $i=0;
            while ($hhr_file_array[$i] !~ m/^No $No/){
                if ($i == $#hhr_file_array){
                    $stop = 1;
                    last;
                }
                $i++;
            }
            last if ($stop);

            $i++;
            my $filename = undef;
            while ($hhr_file_array[$i] !~ m/^No \d+/ && $i < $#hhr_file_array){
                if ($hhr_file_array[$i] =~ m/^>(...._.+):.+Identities=\d+%; Resolution=/){ # NOTE: This regex has been corrected, so that it finds multi-char chain names
                    $filename = $query.':'.$1;
                }
                push(@array, $hhr_file_array[$i]);
                $i++;
                last if ($hhr_file_array[$i] =~ m/^Done/); # XXX This break is specific of the output produced by findTemplate.py
            }

            if ($filename){ # This control is only necessary for the case where .out actually are .out.preprocessed
                $hash{$filename} = \@array;
            }
        }
    }
    return %hash;
}


sub get_hhalignments{
    my $input_dir = shift;
    my $HHaln_temporary = shift;
    
    my %hash_alignments = create_hash_alignments($input_dir); # Key: "Query's UniProt AC:Template's 6 char" (e.g. Q9CX99:6AMV_A); Value: query-template alignment (FASTA)

    open (OUT, '>', $HHaln_temporary) or die "Error: cannot create $HHaln_temporary\n";
    
    foreach my $key (sort keys %hash_alignments){
        #print "KEY:$key\n";
        print OUT @{$hash_alignments{$key}};
    }

    close OUT;
}

main();
