#!/usr/bin/env Rscript

#### FONCTIONS
f = function(vect){
   pos = -as.numeric(vect[1])
   lab = as.character(vect[2])
   x1 = as.numeric(vect[4])
   y1 = as.numeric(vect[5])
   prob = as.numeric(vect[3])
   deb = as.numeric(vect[6])
   fin = as.numeric(vect[7])
   hh_seq = as.numeric(vect[8])

   pt_size=0.5
   if (y1<400 && y1>=100){
    pt_size=0.2
   }
   else if (y1<100 && y1>=60){
    pt_size=0.1
   }else if (y1<60){
   pt_size=0.05
   }


   if (pos == -1){
     points(seq(x1,y1,pt_size),rep(pos,length(seq(x1,y1,pt_size))),pch=21, bg="1", col="1", cex=1.2);
     text((y1+x1)/2,pos,label=toupper(lab),cex=0.6, font=2, col="white");
     text(x1, pos, label=x1, pos=3, cex=0.6,)
     text(y1, pos, label=y1, pos=3, cex=0.6,)
   }else{
     points(seq(x1,y1,pt_size),rep(pos,length(seq(x1,y1,pt_size))) ,pch=23, bg=f_col(prob),col=f_col(prob));
     if( fin == hh_seq) {
        points(y1,pos ,pch=21, bg=f_col(prob), col=f_col(prob));
     }
     if( deb == 1) {
        points(x1,pos ,pch=21, bg=f_col(prob), col=f_col(prob));
     }
        text((y1+x1)/2,pos,label=toupper(lab), col="white", cex=0.6, font=2);
   }
}


f_chev = function(elem1,elem2){
   elem1[1] = as.numeric(elem1[1])
   elem2[1] = as.numeric(elem2[1])
   elem1[4] = as.numeric(elem1[4])
   elem2[4] = as.numeric(elem2[4])
   elem1[5] = as.numeric(elem1[5])
   elem2[5] = as.numeric(elem2[5])
   if (elem1[1] < elem2[1]) return(c(elem2[1],1));
   if (elem1[4] > (elem2[5] + 3) ) return(c(elem2[1],0));
   if (elem1[5] < (elem2[4] - 3) ) return(c(elem2[1],0));
   return(c(elem1[1]+1,1));     
}

f_col = function(num){
  myPal = colorRampPalette( c("red","orange","green","darkblue") )
  mycol = myPal( 8 )
  if (num >= 90) return(mycol[1])
  else if (num >= 80) return(mycol[2])
  else if (num >= 70) return(mycol[3])
  else if (num >= 60) return(mycol[4])
  else if (num >= 50) return(mycol[5])
  else if (num >= 40) return(mycol[6])
  else if (num >= 20) return(mycol[7])
  else return(mycol[8])
}


#### MAIN
args <- commandArgs(TRUE)
t = read.table(args[1], sep=",")
#t = read.table("ATS1_HUMAN.txt", sep=",")
#t = read.table("TAT1_SLC26A8.txt", sep=",")
#t = read.table("s54.txt", sep=",")
#t = read.table("2zaj.txt", sep=",")
#t[,2]=as.character(t[,2])
#t[,9]=t[,1]
#t[2:length(t[,1]),1] = rep(t[2,1],length(t[,1])-1)


#for ( i in 3:length(t[,1])){
#  changed = T
#  while(changed){
#    changed = F
#    for (j in 2:(i-1)){
#      tmp = t[i,1]
#      t[i,1] = f_chev(t[j,],t[i,])
#      if (tmp != t[i,1]) changed = T
#    }
#  }
#}
t[,2]=as.character(t[,2])
#t[,9]=t[,1]
t[,10]=t[,1]
#Ttmp = t
t[2:length(t[,1]),1] = rep(t[2,1],length(t[,1])-1)
t_tmp2 = cbind(t, t[,5]-t[,4]+1)
#colnames(t_tmp2) = c("V1","V2","V3","V4","V5","V6","V7","V8", "V9", "V10")
colnames(t_tmp2) = c("V1","V2","V3","V4","V5","V6","V7","V8", "V9", "V10", "V11")
t_tmp3 = t_tmp2[2:length(t_tmp2[,1]),]
#t_tmp3 = t_tmp3[order(t_tmp3$V3, t_tmp3$V10, decreasing =T), ]
t_tmp3 = t_tmp3[order(t_tmp3$V3, t_tmp3$V11, decreasing =T), ]
t = rbind(t_tmp2[1,],t_tmp3)


for ( i in 2:length(t[,1]) ){
  if ((i+1) < (length(t[,1])+1)){
    for ( j in (i+1):(length(t[,1])) ){
       res = f_chev(t[i,],t[j,])
       res = as.vector(unlist(res))
       t[j,1] = res[1]
    }
  }
}
t = t[order(t$V1),]

cpt = 1
i = 1
while( i <= length(t[,1])){
  if (i == 1) {
    t[i,1] = 1
  }else{
     if (t[i-1,1] == t[i,1]){
       t[i,1] = cpt
     }else{
       cpt = cpt + 1
       t[i,1] = cpt
    }
  }
  i = i + 1
}


my_mai=c(0.2,0,0.2,0)
if ( t[1,5]<20){
  my_mai=c(0.1,0,0.1,0)
}else if(t[1,5]<100 && max(t[,1]) >= 50 ){
  my_mai=c(0.45,0,0.45,0)
}else if ( max(t[,1]) >= 99){
  my_mai=c(0.05,0,0.05,0)
}else if (max(t[,1])>=97){
  my_mai=c(0.1,0,0.1,0)
}

#x = c(15,25,50,100,150,200,400)
#y = c(0.30383,0.507,1.015,2.07,3.065,4.1,8.096)

my_asp = 0.02026*t[1,5]+0.01641

png(args[2],width=800,height=(10+max(t[,1]))*15,pointsize=20)
#png("2zaj.png",width=800,height=(10+max(t[,1]))*15,pointsize=20)
#png("s54.png",width=800,height=(10+max(t[,1]))*15,pointsize=20)
par(mai=my_mai)
plot(c(t[1,3],t[1,5]),c(1,-max(t[,1])),type="n",axe="F",xlab=NA,ylab=NA,asp=my_asp)
apply(t,1, f)
dev.off()


cat(t[2:length(t[,1]),1], file = args[1], append="T")
cat("\n", file = args[1], append="T")
cat(t[2:length(t[,1]),10], file = args[1], append="T")
cat("\n", file = args[1], append="T")


