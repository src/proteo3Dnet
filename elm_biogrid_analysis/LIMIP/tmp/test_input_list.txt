>sp|Q9Y478|AAKB1_HUMAN 5'-AMP-activated protein kinase subunit beta-1 OS=Homo sapiens GN=PRKAB1 PE=1 SV=4
>sp|P53350|PLK1_HUMAN Serine/threonine-protein kinase PLK1 OS=Homo sapiens GN=PLK1 PE=1 SV=1
>sp|Q8IXQ5|KLHL7_HUMAN Kelch-like protein 7 OS=Homo sapiens GN=KLHL7 PE=1 SV=2
>sp|P0DMV8|HS71A_HUMAN Heat shock 70 kDa protein 1A OS=Homo sapiens GN=HSPA1A PE=1 SV=1
>sp|P0DMV9|HS71B_HUMAN Heat shock 70 kDa protein 1B OS=Homo sapiens GN=HSPA1B PE=1 SV=1
>sp|Q04917|1433F_HUMAN 14-3-3 protein eta OS=Homo sapiens GN=YWHAH PE=1 SV=4
>sp|P41240|CSK_HUMAN Tyrosine-protein kinase CSK OS=Homo sapiens GN=CSK PE=1 SV=1
>sp|O95831|AIFM1_HUMAN Apoptosis-inducing factor 1, mitochondrial OS=Homo sapiens GN=AIFM1 PE=1 SV=1
>sp|Q9UGJ0|AAKG2_HUMAN 5'-AMP-activated protein kinase subunit gamma-2 OS=Homo sapiens GN=PRKAG2 PE=1 SV=1
>sp|Q08209|PP2BA_HUMAN Serine/threonine-protein phosphatase 2B catalytic subunit alpha isoform OS=Homo sapiens GN=PPP3CA PE=1 SV=1
>sp|O15144|ARPC2_HUMAN Actin-related protein 2/3 complex subunit 2 OS=Homo sapiens GN=ARPC2 PE=1 SV=1
>sp|O95399|UTS2_HUMAN Urotensin-2 OS=Homo sapiens GN=UTS2 PE=1 SV=1
>sp|O60568|PLOD3_HUMAN Procollagen-lysine,2-oxoglutarate 5-dioxygenase 3 OS=Homo sapiens GN=PLOD3 PE=1 SV=1
>sp|P46108|CRK_HUMAN Adapter molecule crk OS=Homo sapiens GN=CRK PE=1 SV=2
>sp|O95782|AP2A1_HUMAN AP-2 complex subunit alpha-1 OS=Homo sapiens GN=AP2A1 PE=1 SV=3
>sp|Q8N5R6|CCD33_HUMAN Coiled-coil domain-containing protein 33 OS=Homo sapiens GN=CCDC33 PE=1 SV=3
>sp|Q9UJF2|NGAP_HUMAN Ras GTPase-activating protein nGAP OS=Homo sapiens GN=RASAL2 PE=1 SV=2
>sp|Q9H6R7|CB044_HUMAN WD repeat and coiled-coil-containing protein C2orf44 OS=Homo sapiens GN=C2orf44 PE=1 SV=1
>sp|P49023|PAXI_HUMAN Paxillin OS=Homo sapiens GN=PXN PE=1 SV=3
>sp|P63244|GBLP_HUMAN Guanine nucleotide-binding protein subunit beta-2-like 1 OS=Homo sapiens GN=GNB2L1 PE=1 SV=3
>sp|P49356|FNTB_HUMAN Protein farnesyltransferase subunit beta OS=Homo sapiens GN=FNTB PE=1 SV=1
>sp|Q32P28|P3H1_HUMAN Prolyl 3-hydroxylase 1 OS=Homo sapiens GN=P3H1 PE=1 SV=2
>sp|Q9NPL8|TIDC1_HUMAN Complex I assembly factor TIMMDC1, mitochondrial OS=Homo sapiens GN=TIMMDC1 PE=1 SV=2
>sp|Q9UK99|FBX3_HUMAN F-box only protein 3 OS=Homo sapiens GN=FBXO3 PE=1 SV=3
>sp|O00469|PLOD2_HUMAN Procollagen-lysine,2-oxoglutarate 5-dioxygenase 2 OS=Homo sapiens GN=PLOD2 PE=1 SV=2
>sp|Q13627|DYR1A_HUMAN Dual specificity tyrosine-phosphorylation-regulated kinase 1A OS=Homo sapiens GN=DYRK1A PE=1 SV=2
>sp|P84090|ERH_HUMAN Enhancer of rudimentary homolog OS=Homo sapiens GN=ERH PE=1 SV=1
>sp|Q9H2K2|TNKS2_HUMAN Tankyrase-2 OS=Homo sapiens GN=TNKS2 PE=1 SV=1
>sp|Q14145|KEAP1_HUMAN Kelch-like ECH-associated protein 1 OS=Homo sapiens GN=KEAP1 PE=1 SV=2
>sp|P61962|DCAF7_HUMAN DDB1- and CUL4-associated factor 7 OS=Homo sapiens GN=DCAF7 PE=1 SV=1
>sp|Q7L576|CYFP1_HUMAN Cytoplasmic FMR1-interacting protein 1 OS=Homo sapiens GN=CYFIP1 PE=1 SV=1
>sp|Q96F07|CYFP2_HUMAN Cytoplasmic FMR1-interacting protein 2 OS=Homo sapiens GN=CYFIP2 PE=1 SV=2
>sp|Q9Y2A7|NCKP1_HUMAN Nck-associated protein 1 OS=Homo sapiens GN=NCKAP1 PE=1 SV=1
>sp|P61160|ARP2_HUMAN Actin-related protein 2 OS=Homo sapiens GN=ACTR2 PE=1 SV=1
>sp|P61158|ARP3_HUMAN Actin-related protein 3 OS=Homo sapiens GN=ACTR3 PE=1 SV=3
>sp|O14874|BCKD_HUMAN [3-methyl-2-oxobutanoate dehydrogenase [lipoamide]] kinase, mitochondrial OS=Homo sapiens GN=BCKDK PE=1 SV=2
>sp|P38646|GRP75_HUMAN Stress-70 protein, mitochondrial OS=Homo sapiens GN=HSPA9 PE=1 SV=2
>sp|P17066|HSP76_HUMAN Heat shock 70 kDa protein 6 OS=Homo sapiens GN=HSPA6 PE=1 SV=2
>sp|P59998|ARPC4_HUMAN Actin-related protein 2/3 complex subunit 4 OS=Homo sapiens GN=ARPC4 PE=1 SV=3
>sp|P11021|GRP78_HUMAN 78 kDa glucose-regulated protein OS=Homo sapiens GN=HSPA5 PE=1 SV=2
>sp|P11142|HSP7C_HUMAN Heat shock cognate 71 kDa protein OS=Homo sapiens GN=HSPA8 PE=1 SV=1
>sp|Q13557|KCC2D_HUMAN Calcium/calmodulin-dependent protein kinase type II subunit delta OS=Homo sapiens GN=CAMK2D PE=1 SV=3
>sp|Q13131|AAPK1_HUMAN 5'-AMP-activated protein kinase catalytic subunit alpha-1 OS=Homo sapiens GN=PRKAA1 PE=1 SV=4
>sp|P54619|AAKG1_HUMAN 5'-AMP-activated protein kinase subunit gamma-1 OS=Homo sapiens GN=PRKAG1 PE=1 SV=1
>sp|P54652|HSP72_HUMAN Heat shock-related 70 kDa protein 2 OS=Homo sapiens GN=HSPA2 PE=1 SV=1
>sp|Q92598|HS105_HUMAN Heat shock protein 105 kDa OS=Homo sapiens GN=HSPH1 PE=1 SV=1
>sp|O95757|HS74L_HUMAN Heat shock 70 kDa protein 4L OS=Homo sapiens GN=HSPA4L PE=1 SV=3
>sp|Q96C36|P5CR2_HUMAN Pyrroline-5-carboxylate reductase 2 OS=Homo sapiens GN=PYCR2 PE=1 SV=1
>sp|O95271|TNKS1_HUMAN Tankyrase-1 OS=Homo sapiens GN=TNKS PE=1 SV=2
>sp|O75037|KI21B_HUMAN Kinesin-like protein KIF21B OS=Homo sapiens GN=KIF21B PE=1 SV=2
>sp|Q13362|2A5G_HUMAN Serine/threonine-protein phosphatase 2A 56 kDa regulatory subunit gamma isoform OS=Homo sapiens GN=PPP2R5C PE=1 SV=3
>sp|P62993|GRB2_HUMAN Growth factor receptor-bound protein 2 OS=Homo sapiens GN=GRB2 PE=1 SV=1
>sp|O43255|SIAH2_HUMAN E3 ubiquitin-protein ligase SIAH2 OS=Homo sapiens GN=SIAH2 PE=1 SV=1
>sp|Q8IUQ4|SIAH1_HUMAN E3 ubiquitin-protein ligase SIAH1 OS=Homo sapiens GN=SIAH1 PE=1 SV=2
>sp|P46109|CRKL_HUMAN Crk-like protein OS=Homo sapiens GN=CRKL PE=1 SV=1
>sp|Q6ZWJ1|STXB4_HUMAN Syntaxin-binding protein 4 OS=Homo sapiens GN=STXBP4 PE=1 SV=2
>sp|Q8IZP0|ABI1_HUMAN Abl interactor 1 OS=Homo sapiens GN=ABI1 PE=1 SV=4
>sp|P32322|P5CR1_HUMAN Pyrroline-5-carboxylate reductase 1, mitochondrial OS=Homo sapiens GN=PYCR1 PE=1 SV=2
>sp|P14373|TRI27_HUMAN Zinc finger protein RFP OS=Homo sapiens GN=TRIM27 PE=1 SV=1
>sp|Q96HS1|PGAM5_HUMAN Serine/threonine-protein phosphatase PGAM5, mitochondrial OS=Homo sapiens GN=PGAM5 PE=1 SV=2
>sp|Q9Y2S7|PDIP2_HUMAN Polymerase delta-interacting protein 2 OS=Homo sapiens GN=POLDIP2 PE=1 SV=1
