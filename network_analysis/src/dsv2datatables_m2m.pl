#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;

# ./dsv2datatables.pl --input PACK_02dec2252/complexes.tsv --output AAA.html --title MonTitre --separator tab --species HUMAN

#----------------------------

my $VERSION = "2019Dec02_01";

#----------------------------

sub main{
    my $input_filename = '';
    my $help = 0;
    my $output_name = '';
    my $page_title = '';
    my $separator = '';
    my $species = '';

    if ($#ARGV == -1){
        helper();
    }

    GetOptions(
        'input=s' => \$input_filename,
        'output=s' => \$output_name,
        'title=s' => \$page_title,
        'separator=s' => \$separator,
        'species=s' => \$species,
        'help' => \$help
    );

    check_options($help, $output_name, $input_filename, $separator, $page_title, $species);

    my $header = 1;

    # Processing the input, depending on the project type
    my @input_file = process_input($input_filename);

    # Indentation levels
    my $level1 = "  ";
    my $level2 = "    ";
    my $level3 = "      ";

    # Separator parsing
    $separator = parse_separator($separator);

    open(my $outfh, '>', $output_name) or die "Error: cannot create $output_name:\n\t$!\n";


    print $outfh "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN'\n",
                 "  'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>\n",
                 "<html xmlns='http://www.w3.org/1999/xhtml'>\n";

    # In between the head tags is the javascript code for datatables.js
    print_head($outfh, $page_title, $input_filename);

    # Print the body, which contains the table
    print $outfh "<body>\n";
    #print $outfh "<table id='scores_table' class='display compact' cellspacing='0' width='100%'>\n";
    print $outfh "<table id='scores_table' class='display compact' cellspacing='0' style='white-space: nowrap;'>\n";
    foreach my $line (@input_file){
        chomp $line;
        if ($header){
            print $outfh "$level1<thead>\n";
        }
        print $outfh "$level2<tr>\n";
        my @columns = split($separator, $line);
        my $found_species = '';
        for (my $i=0; $i<=$#columns; $i++){
            if ($header){
                print $outfh "$level3<th><span data-toggle='tooltip' title='$columns[$i]'>$columns[$i]</span></th>\n";
            }
            else{
                if ($columns[$i] =~ m/^\d\w\w\w$/){
                    print $outfh "$level3<td><a href=https://www.rcsb.org/structure/$columns[$i] target=_blank>$columns[$i]</a></td>\n";
                }
                elsif ($columns[$i] =~ m/^\w+ \w+ \((\w+)\)$/){
                    $found_species = $1;
                    print $outfh "$level3<td>$found_species</td>\n";
                }
                elsif ($columns[$i] =~ m/^\w+:\w+ \(\d+\)/){
                    my $line_with_links = add_uniprot_links_to_input($columns[$i], $species);
                    print $outfh "$level3<td>$line_with_links</td>\n";
                }
                elsif ($columns[$i] =~ m/^\w+:\w+/){
                    my $line_with_links = add_uniprot_links($columns[$i], $found_species);
                    print $outfh "$level3<td>$line_with_links</td>\n";
                }
                #elsif ($columns[$i] =~ m/^c\d\d\d$/ and $i == 0){ # NOTE: only for column 0
                elsif ($columns[$i] =~ m/^c\d\d\d$/){
                    my $complexes_title = get_partners($columns[$i]);
                    print $outfh "$level3<td><a href=complexes/$columns[$i].html target=_blank title='$complexes_title'>$columns[$i]</a></td>\n";
                }
                elsif ($columns[$i] =~ m/^c\d\d\d,/){
                    print $outfh "$level3<td>";
                    my @complex_list = split(',', $columns[$i]);
                    for (my $j=0; $j<=$#complex_list; $j++){
                        $complex_list[$j] =~ s/ //g;
                        my $complexes_title =  get_partners($complex_list[$j]);
                        print $outfh "<a href=complexes/$complex_list[$j].html target=_blank title='$complexes_title'>$complex_list[$j]</a>";
                        unless ($j == $#complex_list){
                            print $outfh ", ";
                        }
                    }
                    print $outfh "</td>\n";
                }
                elsif ($i == 1 and $columns[$i] =~ m/^(\w+_\w+)$/){
                    print $outfh "$level3<td><a href=vendor/molart.html?$1 target=_blank>$1</a></td>\n";
                }
                elsif (($i == 4 or $i == 3) and $columns[$i] =~ m/^(\d\w\w\w\_\w+)$/){
                    my $pdb6char = $1;
                    my ($pdbcode, $chain) = split('_', $pdb6char);
                    print $outfh "$level3<td><a href=https://www.rcsb.org/structure/$pdbcode target=_blank>$pdb6char</a></td>\n";
                }
                elsif ($i == 2 and $columns[$i] =~ m/[A-Z]+/){
                    print $outfh "$level3<td><a href=https://www.uniprot.org/uniprot/$columns[$i] target=_blank>$columns[$i]</a></td>\n";
                }
                else{
                    print $outfh "$level3<td>$columns[$i]</td>\n";
                }
            }
        }
        print $outfh "$level2</tr>\n";
        if ($header){
            print $outfh "$level1</thead>\n";
            print $outfh "$level1<tbody>\n";
        }
        $header = 0;
    }
    print $outfh "$level1</tbody>\n</table>\n</body>\n</html>\n";
    close $outfh;
}


sub get_partners{
    my $complex_num = shift;
    my @output;
    my @file = readfile("$complex_num.tsv");
    my $line = $file[1]; # NOTE: NOT THE HEADER
    my @A = split("\t", $line);
    my @eles = split(" ", $A[4]);
    foreach my $ele (@eles){
        if ($ele !~ m/^\(/){
            my ($partner, $chain) = split(':', $ele);
            push(@output, $partner);
        }
    }
    my $str = join(" ", @output);
    return $str;
}


sub print_head{
    my $outfh = shift;
    my $page_title = shift;
    my $input_filename = shift;

    my $order;
    my $pageLength = 10;
    if ($input_filename =~ m/c\d\d\d\.tsv$/){
        $order = 2; # Rank by avg id %
        $pageLength = 50;
    }


    print $outfh "<head>\n",
    "    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>\n",
    "    <link rel='stylesheet' href='/portal/css/mobyle.css' type='text/css'/>\n",
    "    <link rel='stylesheet' href='/portal/css/jquery.dataTables.css' type='text/css'/>\n",
    "    <script type='text/javascript' src='/portal/js/jquery.js'></script>\n",
    "    <script type='text/javascript' src='https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-flash-1.5.4/b-html5-1.5.4/datatables.min.js'></script>\n",
    "    <script type='text/javascript'>\n",
    "    \$(document).ready(function() {\n",
    "        \$('#scores_table').dataTable( {\n",
    "            dom: 't<pB>',\n",
    "            buttons: ['copy', 'csv', 'excel'],\n";


    if ($order){
        print $outfh "            order: [ [$order, 'desc'] ],\n";
    }


    print $outfh "            pageLength: $pageLength,\n",
    "            lengthChange: false,\n",
    "            searching : false,\n",
    "            columnDefs: [{\n",
    #"                targets: [2, 3],\n", # FIXME What does it do?
    "                render: function (data, type, row) {\n",
    "                    return '<span data-toggle=\"tooltip\" title=\"' + data + '\">' + (data.length > 35 ? data.substr(0,35) + \"...\" : data) + '</span>';\n",
    "                }\n",
    "            }]\n",
    "        } );\n",
    "    } );\n",
    "    </script>\n",
    "    <title>$page_title</title>\n",
    "</head>\n";
}

sub parse_separator{
    my $separator = shift;

    if ($separator eq 'tab'){
        $separator = "\t";
    }
    elsif ($separator eq 'comma'){
        $separator = ",";
    }
    elsif ($separator eq 'colon'){
        $separator = ":";
    }
    elsif ($separator eq 'semicolon'){
        $separator = ";";
    }
    elsif ($separator eq 'space'){
        $separator = " ";
    }
    else{
        die "Error: $separator is not a valid separator\n";
    }

    return $separator;
}

sub process_input{
    my ($input_filename) = splice(@_, 0, 1);

    my @input_file;

    @input_file = readfile($input_filename);

    return @input_file;
}

sub check_options{
    my ($help, $output_name, $input_filename, $separator, $page_title, $species) = splice( @_, 0, 6 );

    if ($help){
        helper();
    }

    if (!$species){
        die "Error: please provide a species name\n";
    }
    if (!$output_name){
        die "Error: please provide an output name\n";
    }
    if (!$separator){
        die "Error: please provide a separator\n";
    }
    if (!$page_title){
        die "Error: please provide a title for the HTML page\n";
    }
    if (!$input_filename){
        die "Error: please provide an input filename\n";
    }
}

sub add_uniprot_links_to_input{
    my $line = shift;
    my $species = shift;
    my $newline = '';
    my @array = split(/\) /, $line);
    foreach my $ele (@array){
        if ($ele =~ m/^(\w+):(\w+) \((\d+)/){
            my $uniprot = $1;
            my $chain = $2;
            my $seqid = $3;
            $newline.="<a href=https://www.uniprot.org/uniprot/$uniprot\_$species target=_blank>$uniprot</a>:$chain ($seqid) ";
        }
    }
    $newline =~ s/ $//;
    return $newline;
}

sub add_uniprot_links{
    my $line = shift;
    my $found_species = shift;
    my $newline = '';
    my @array = split(" ", $line);
    foreach my $ele (@array){
        if ($ele =~ m/^(\w+):(\w+)/){
            my $chain = $1;
            my $uniprot = $2;
            $newline.="$chain:<a href=https://www.uniprot.org/uniprot/$uniprot\_$found_species target=_blank>$uniprot</a> ";
        }
    }
    $newline =~ s/ $//;
    return $newline;
}

sub readfile{
    my $filename = shift;
    open(IN, '<', $filename) or die "\e[1;31mError\e[0m while opening $filename:\n\t$!\n";
    my @file = <IN>;
    close IN;
    return @file;
}

sub helper {
    print
      "\n",
      "\t$0: Convert delimiter-separated values (DSV) files to jQuery DataTables (HTML pages)\n\n",
      "\tVersion: $VERSION\n\n",
      "\tOptions:\n",
      "\t--input                The x-separated file to convert\n",
      "\t--output               Filename for the output HTML file\n",
      "\t--title                Title of the output HTML page\n",
      "\t--separator            Separator of the x-separated file: 'tab', 'colon', 'semicolon', 'comma', or 'space'\n",
      "\t--help                 This help\n",
      "\n";
    exit 1;
}

main();
