Files in this directory:
- example_UniProtID.dat: the simplest example of input, made of only two UniProtID
- pragmin.dat: flat list of UniProtAC from the Pragmin dataset (DOI: 10.1016/j.str.2018.01.017)
- proteasome.dat: flat list of UniProtAC from the proteasome dataset (DOI: 10.1074/mcp.M112.023317)
- zip2.dat: flat list of UniProtAC from the Zip2 interactome (DOI: 10.1101/gad.308510.117)
- zip2_enrichment.dat: fold change values from the Zip2 interactome (DOI: 10.1101/gad.308510.117)
- Zip2OutputDir.zip: compressed archive containing the results produced by Proteo3Dnet for the Zip2 interactome
