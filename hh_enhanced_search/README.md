hh_enhanced_search: HHsearch + advanced cluster search.
	src: scripts to perform hh_enhanced_search
	data: banks required
	ext: external source code

src: the queries are organized as two steps:
	step1: regular hh_search + some filters in find_template.py
		master script: find_template.py
	step2: advanced hh cluster search (supplement cluster prototypes by all hh cluster members)
		master script: extract_HHsearch_data.pl

ext: 
	hh-suite: the official hh-suite distribution (requires ffindex)
	mafft-7.407-without-extensions: the official source code of MAFFT
	psipred: the official psipred33 distribution (NOTE: 33 version keeps it compatible with hh_banks.

data: 
	databases: 
		MUSTANG: the 3D alignment of each hh_cluster
		pdb70_clu_7158679.tsv: the hhsearch pdb70_clu.tsv with size info in the name
		SEQATM_PASS1.txt: the fasta sequence of the 3D resolved residues of each PDB domain (can differ largely from the seqres sequence).
		hh_banks: the official hh_banks, fixed as for their dssp assignment (see bank_upate) 
		
	bank_update: The procedures to update the different banks.

		pdb_mmcif_and_obsolete: A procedure to identify hh_clusters in MMCIF, and obsoletes.
			Master script:
			Input: Soeding's tsv (pdb70_clu.tsv)
			Output: 4 directoires with mmcif, obsolete_mmcif, mmcif as PDBs (requests pymol) and obsolete PDBs.

		hh_bank_pdbchain_seqatm_dssp_builder: build a PDB with a one file-one chain mapping (filenames can contain mutliple char chain Ids) for FULL pdb (GP: check), fix dssp assignment in hh_banks/pdb70_hhm_db, extract the seq_atm from dssp output. 
			Master script: hh_bank_fix.py
			Input: pdb names from 
			Output: directory with PDB mapping, directory with dssp assignments, SEQATM_PASS1.txt, updated pdb70_hhm_db

		mustang_db_builder: build 3D alignment for each hh cluster, and rescue failures using MAFFT. Some clusters being strange in terms of 3D crds, maxclusters in use to define sub_clusters.
			Master script: mustang_db_builder.py
			Input: Soeding's tsv (pdb70_clu.tsv)
			Output: .afasta alignments (per cluster alignment)
			Dependencies: maxcluster, MAFFT, MUSTANG, PDB data.
		



# Use Docker

docker build -t dev-registry-v2.rpbs.univ-paris-diderot.fr/hh_enhanced_search .

docker run -ti --privileged -v /home/alcor/mounted_banks/:/scratch/banks/ -v $(pwd):/mnt/ dev-registry-v2.rpbs.univ-paris-diderot.fr/hh_enhanced_search
root@a2e005f40604:/#
root@a2e005f40604:/# exit

docker start -a -i a2e005f40604


