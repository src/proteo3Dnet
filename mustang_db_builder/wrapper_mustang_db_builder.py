#!/usr/bin/env python
# -*- coding: utf-8 -*-

def freadfile(infile):
    """ Read an input file and return a list of all lines (without newlines)
    """
    file_name=infile
    try:
        with open( file_name, 'r') as data:
            lines=data.readlines()
        for i,item in enumerate(lines):
            item=item.replace('\n','')
            lines[i]=item
    except:
        print('Error: while reading {}'.format(file_name) )
        sys.exit('Error while reading {}'.format(file_name) )

    return lines

def get_args():
    parser = argparse.ArgumentParser(description = 'Wrapper for the mustang_db_builder Docker image')
    parser.add_argument('-i', dest='infile', type=str, help="the input file (TSV)", required=True)
    parser.add_argument('-n', dest='tasks', type=int, help="the number of parallel tasks", required=True)
    parser.add_argument('-c', dest="rpbs_cluster", action='store_true', help="run on RPBS dev or prod cluster")

    if len(sys.argv) < 2:
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()

    return args.infile, args.tasks, args.rpbs_cluster

def chunk(xs, n):
    ''' Split a list xs into n chunks
    '''
    L = len(xs)
    assert 0 < n <= L
    s = L//n
    return [xs[p:p+s] for p in range(0, L, s)]

def main():
    PDB_PATH = "/scratch/banks/pdb/data/structures/all/pdb/"
    MMCIF_PATH = "/scratch/banks/obsolete_mmCIF/PDB-like/"
    OBSOLETE_PDB_PATH = "/scratch/banks/obsolete_mmCIF/obsolete/"
    OBSOLETE_MMCIF_PATH = "/scratch/banks/obsolete_mmCIF/mmCIF/"

    hhcluster_filename, number_of_tasks, rpbs_cluster = get_args()

    # XXX Part of the obsolete PDB are removed by this wrapper
    os.system("sed -i '/\./d' %s" % hhcluster_filename)

    # From the HHsearch clustering .tsv file:
    # (i) Create a dictionary: key = cluster representative; value = list of cluster members
    # (ii) Create a .dat file foreach key of the above dictionary
    # (iii) Create 'list.txt': the list of all .dat files
    hhcluster_file = freadfile(hhcluster_filename)

    dict_clusters = {}

    for line in hhcluster_file:
        pdb1, pdb2 = line.split("\t", 1)

        if not pdb1 in dict_clusters:
            dict_clusters[pdb1] = []

        dict_clusters[pdb1].append(pdb2)

    os.mkdir("dat_files")

    for key in dict_clusters:
        fh = open("dat_files/"+key+".dat", 'w')
        for cluster_member in dict_clusters[key]:
            fh.write(cluster_member+"\n")
        fh.close()

    os.system("ls dat_files/*.dat > list.txt")

    # Split list.txt into several chunks, which are listed
    output_list_filename = "chunks_list.txt"
    input_list = freadfile("list.txt")

    chunks = chunk(input_list, number_of_tasks)
    total_chunks = len(chunks) # Will be the actual number of tasks

    f1 = open(output_list_filename, 'w')
    for i in range(total_chunks):
        list_name = str(i)+'_list.txt'
        f1.write("%s\n" % list_name)
        f2 = open(list_name, 'w')
        for element in chunks[i]:
            f2.write(element+"\n")
        f2.close()
    f1.close()

    if (rpbs_cluster):
        # Run on prod or dev cluster
        sys.path.append("/service/env")
        import cluster
        cmd = "/home/cluster_align.pl --errors_mute --seqres /home/SEQATM_PASS1.txt --pdb %s --mmcif %s --obsolete_pdb %s --obsolete_mmcif %s --input " % (PDB_PATH, MMCIF_PATH, OBSOLETE_PDB_PATH, OBSOLETE_MMCIF_PATH)
        args = [ '$(sed -n "${TASK_ID}p" %s)' % output_list_filename, ]
        cluster.runTasks(cmd, args, tasks = total_chunks, docker_img = "mustang_db_builder")


if __name__ == '__main__':
    import sys, os, argparse
    main()

