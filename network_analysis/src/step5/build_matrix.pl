#!/usr/bin/env perl

use strict;
use warnings;

if (!$ARGV[0]){
    die "Error: missing argument\n";
}

my @acid_file = readfile($ARGV[0]);
my @head_html;
my @foot_html;

if (-e 'head.html' and -f 'head.html'){
    @head_html = readfile('head.html');
    @foot_html = readfile('foot.html');
}
else{
    @head_html = readfile('/home/step5/head.html');
    @foot_html = readfile('/home/step5/foot.html');
}

open(OUT, '>', "ppi_interactions_view.html") or die "Error: $0: cannot write ppi_interactions_view.html\n";

foreach my $line (@head_html){
    print OUT $line;
}


foreach my $line (@acid_file){
    chomp $line;
    my ($ac, $id) = split("\t", $line);
    print OUT "                <option value=\"$ac\">$id ($ac)</option>\n";
}

my ($firstac, $firstid) = split("\t", $acid_file[0]);
foreach my $line (@foot_html){
    if ($line =~ m/XXXXXX/){
        $line =~ s/XXXXXX/$firstac/;
        print OUT $line;
    }
    else{
        print OUT $line;
    }
}

close OUT;

sub readfile{
    my $filename = shift;
    open(IN, '<', $filename) or die "\e[1;31mError: $0: while opening $filename: $!\e[0m\n";
    my @file = <IN>;
    close IN;
    return @file;
}

