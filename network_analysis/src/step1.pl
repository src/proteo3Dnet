#!/usr/bin/env perl

use strict;
use warnings;

# XXX CAREFUL HERE: no colors for the errors XXX

# This program extracts the UniProt codes from the list provided by the user (list of FASTA sequences or codes)
# If possible, it will extract UniProt ID (entry name) rather than AC (accession number)

# Accepted formats:
# >sp|P49748|ACADV_HUMAN ...
# >tr|P49748|ACADV_HUMAN ...
# ACADV_HUMAN
# P49748

# NOTE: Requires the (decompressed) UniProt mapping file: idmapping_selected.tab.gz (from the UniProt FTP server)
# This mapping file is quite HEAVY! (~25 GB)
# It is therefore not included in the Docker image

sub main{
    if (!$ARGV[0]){
        die "Error: missing argument\nUsage: $0 <INPUT LIST> [SPECIES]\n";
    }


    my @input_file = readfile($ARGV[0]); 
    # NOTE: special readfile() function, which removes leading and trailing \s \t

    my $is_multifasta = is_multifasta(\@input_file);

    my @names;

    my $output_file = $ARGV[0];
    $output_file =~ s/\.\w+$//;
    $output_file.='.acid';
    my $output_file2 = $output_file;
    $output_file2 =~ s/\.acid$/\.ac/;



    # Find the names
    foreach my $line (@input_file){
        chomp $line;
        if ($is_multifasta){
            if ($line =~ m/^>(\w+)$/){
                push(@names, uc($1));
            }
            elsif ($line =~ m/^>\w\w\|(\w+)-?\d*\|(\w+_\w+)/){
                my $ac = uc($1);
                my $id = uc($2);
                push(@names, "$ac\t$id"); # DIRECT EXTRACTION
            }
            elsif ($line =~ m/^>/){
                die "Error: $line is not a valid FASTA identifier\n";
            }
        }
        else{
            if ($line =~ m/^(\w+)$/){
                push(@names, uc($1));
            }
            else{
                die "Error: $line is not a valid UniProt code\n";
            }
        }
    }


    if ($#names > 0){
        for (my $i=0; $i<=$#names; $i++){
            # SEARCH IF: ONLY AC OR ONLY ID
            if ($names[$i] =~ m/^\w+-?\d*$/){
                # IF AC
                if ($names[$i] !~ m/_/){
                    my $uniprotid = get_id_from_ac($names[$i]);
                    $names[$i] = "$names[$i]\t$uniprotid";
                }

                # IF ID
                else{
                    my $redirect = `curl -Ls -o /dev/null -w %{url_effective} https://www.uniprot.org/uniprot/$names[$i]`; # FIXME CREATE FUNCTION
                    my @accession = split('/', $redirect);
                
                    if ($names[$i] =~ m/_/){
                        $names[$i] = "$accession[$#accession]\t$names[$i]";
                    }
                    else{
                        die "Error: $names[$i] does not exist in UniProt\n";
                    }
                }
            }
        }
    }
    else{
        die "Error: no name extracted from the input data\n";
    }


    my $wanted_species = ($ARGV[1]) ? uc($ARGV[1]) : find_major_species(\@names);
    my $adjective = ($ARGV[1]) ? 'selected' : 'major';


    my %hash_redundancy;
    my @filtered_names;
    foreach my $line (@names){
        chomp $line;
        my ($ac, $id) = split(/\t/, $line);
        if ($line =~ m/_$wanted_species$/){
            if (exists $hash_redundancy{$line}){
                print "Warning: input protein $ac ($id) excluded: found multiple times in the dataset (e.g. isoforms)\n";
            }
            else{
                push(@filtered_names, $line);
                $hash_redundancy{$line} = 1;
            }
        }
        else{
            print "Warning: input protein $ac ($id) excluded: does not belong to the $adjective organism\n";
        }
    }

    if ($#filtered_names < 0){
        die "Error: no input protein belongs to the selected organism\n";
    }

    # Final print: .acid file
    open(OUT, '>', $output_file) or die "Error: cannot create $output_file\n\t$!\n";
    foreach my $name (@filtered_names){
        print OUT "$name\n";
    }
    close OUT;

    # Final print bis: .ac file
    open(OUT, '>', $output_file2) or die "Error: cannot create $output_file2\n\t$!\n";
    foreach my $name (@filtered_names){
        my ($ac, $id) = split(/\t/, $name);
        print OUT "$ac\n";
    }
    close OUT;
}

sub is_multifasta{
    foreach my $line (@{$_[0]}){
        if ($line =~ m/^>/ and ${$_[0]}[0] !~ m/^>/){
            die "Error: invalid multi-fasta: first character must be >\n";
        }
    }

    my $previous_line = '';
    foreach my $line (@{$_[0]}){
        if ($line =~ m/^>/ and $previous_line =~ m/^>/){
            die "Error: invalid multi-fasta: two consecutive lines starting with >\n";
        }
        $previous_line = $line;
    }

    # AND NOW...
    if (${$_[0]}[0] =~ m/^>/){
        return 1; # is multi-fasta
    }
    else{
        return 0; # is multi-codes
    }
}

sub find_major_species{
    my %hash;
    foreach my $line (@{$_[0]}){
        my ($ac, $id) = split(/\t/, $line);
        my ($code, $orga) = split('_', $id);
        if (exists $hash{$orga}){
            $hash{$orga}++;
        }
        else{
            $hash{$orga} = 1;
        }
    }

    my @arr = sort {$hash{$b} <=> $hash{$a}} sort keys %hash;

    return $arr[0];
}

sub get_id_from_ac{
    my $ac = $_[0];
    system "wget -q https://www.uniprot.org/uniprot/$ac.txt";
    my $id = '';

    open (IN, '<', "$ac.txt") or die "Error: cannot read $ac.txt\n";
    while (my $line = <IN>){
        if ($line =~ m/^ID\s+(\w+_\w+)/){
            $id = $1;
            last;
        }
    }
    close IN;
    system "rm $ac.txt";

    if (!$id){
        system "wget -q https://www.uniprot.org/uniprot/$ac";
    
        open (IN, '<', "$ac") or die "Error: cannot read $ac\n";
        while (my $line = <IN>){
            if ($line =~ m/alternateName">\((\w+_\w+)\)/){
                $id = $1;
                last;
            }
        }
        close IN;
        system "rm $ac";
        if ($id){
            print "Warning: UniProtAC $ac ($id) seems obsolete\n";
        }
        else{
            die "Error: $ac is an invalid UniProtAC\n";
        }
    }

    return $id;
}

sub readfile{
    my $filename = shift;
    open(IN, '<', $filename) or die "Error while opening $filename:\n\t$!\n";
    my @file = <IN>;
    close IN;
    foreach my $line (@file){
        $line =~ s/^\s+//;
        $line =~ s/\s+$//;
        $line =~ s/^\t+//;
        $line =~ s/\t+$//;
    }
    return @file;
}

main();
