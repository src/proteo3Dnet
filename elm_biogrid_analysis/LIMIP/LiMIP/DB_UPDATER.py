#! /usr/bin/env Python3
#--------------------------------------------------------
# ----------------------- META DATA ---------------------
#--------------------------------------------------------
__author__ = 'IFB - Institut Francaise de Bioinformatique'
__dev__ = 'Reys Victor : reys[at]cbs.cnrs.fr | victor.reys[at]france-bioinformatique.fr'
__version__ = '1.7'
#--------------------------------------------------------
# --------------------- IMPORTATIONS --------------------
#--------------------------------------------------------
import os
import sys
import csv
import json
import time
import copy
import gzip
import codecs
import zipfile
import urllib.request
import urllib.parse
import LMIP_script_config

#--------------------------------------------------------
# ---------------------- FUNCTIONS ----------------------
#--------------------------------------------------------
# Download file from url, backup old file, save new file
def database_updater(db_url, db_internal_path, text = True):
    try:
    # Ask for file
        # Get the ELM return
        bytes_db_request = urllib.request.urlopen(db_url)
        # Wait for all content to be downloaded and cast bytes to string
        bytes_db = bytes_db_request.read()
        bytes_db_request.close()
    except:
        return False
             
    # Check for no overwriting
    old_db_path = backup_old_db(db_internal_path)
    
    # TEXT FILE CASE            
    if text:
        # Make sure files are written in utf-8 ascii characterset
        utf8_db = bytes_db.decode('utf-8')
        utf8_encoded_db = utf8_db.encode('UTF-8')
        # Write db
        with open(db_internal_path, 'w') as filout:
            for letter in utf8_db:
                try:
                    filout.write(letter)
                except UnicodeEncodeError as err:
                    print(str(err), '\nUNICODEERROR ADDED')
                    filout.write('UNICODEERROR')
    else:
        # Write db              
        with open(db_internal_path, 'wb') as filout:
            filout.write(bytes_db)

    return True

# Write database (dic_object) as json
def write_json_db(data_dic, db_path):
    # Check for no overwriting
    old_path = backup_old_db(db_path)
    # Write database as dic/json
    try:
        with open(db_path, 'w', encoding='utf-8') as filout:
            json.dump(data_dic, filout, ensure_ascii=False, indent=4)
    except:
        # If failed during writing, re-Set old database
        os.rename(old_path, db_path)

# Backup any file and add .old.CREATION_TIME extention
def backup_old_db(db_internal_path):
    # Check for no overwriting
    if os.path.exists(db_internal_path):
        # Get creation date file
        last_time_modif = os.path.getmtime(db_internal_path)
        # Old database new filename generation
        old_db_newfilename = db_internal_path+'.old.'+str(last_time_modif).split('.')[0]
        # Modify last db file by adding creation date time
        os.rename(db_internal_path, old_db_newfilename)
        return old_db_newfilename
    else:
        return None


# Compute own db        
def owndb_biogrid_id(uniprot_names_path, own_db_path):   
    # Load uniprot names
    with open(uniprot_names_path) as filin:
        uniprot_names = json.load(filin)
    
    # Generate list of uniprot ID & Compute same string verions 
    full_uniprot_list = list(uniprot_names.keys())
    
    # Set parsing vars                
    uniprot_biogrid_mapping = {}
    
    
    # Access Uniprot Mapping API
    # HELP NOTE : https://www.uniprot.org/help/api_idmapping
    url = 'https://www.uniprot.org/uploadlists/'

    # Set Max Uniprot query length
    max_query_length = 150

    # Loop over uniprot list to not saturate UniProt Server
    max_index = max_query_length 
    min_index = 0
    # Progression print
    print('0.0 %', end='\r')
    while max_index < len(full_uniprot_list) - 1:
        # Increment max and min index    
        if min_index + max_query_length < len(full_uniprot_list):
            max_index = min_index + max_query_length
        # No IndexError case & Out of While
        else:
            max_index = (len(full_uniprot_list) - 1)
        
        uniprot_server_return = False
        while_iter = 0
        while not uniprot_server_return and while_iter < 100:
            # Try to do the query
            try:    
                # Set parameters
                params = {'from': 'ACC+ID',  # IF BUG -> check https://www.uniprot.org/help/api_idmapping
                          'to': 'BIOGRID_ID', 
                          'format': 'tab',
                          'query': ' '.join(full_uniprot_list[min_index:max_index])}
                # Init request          
                data = urllib.parse.urlencode(params)
                data = data.encode('utf-8')
                
                # Request data
                bytes_db_request = urllib.request.urlopen(url, data)
                # Wait for all content to be downloaded and cast bytes to string
                bytes_db = bytes_db_request.read()
                bytes_db_request.close()
                
                # Convert data into list of strings
                string_db = bytes_db.decode('utf-8')
                db = string_db.split('\n')
                
                # Once we could retrive message, we set the flag to True
                uniprot_server_return = True
                
            # Just loop again if query failed :(    
            except:
                while_iter += 1
                continue  
            
        
                
        # Gather data skip header and last empty line [1:-1]
        for lin in db[1:-1]:
            uniprot_code, biogrid_id = lin.strip().split('\t')
            if not uniprot_code in uniprot_biogrid_mapping.keys():
                uniprot_biogrid_mapping[uniprot_code] = str(biogrid_id)
        
        # Modify min_index
        min_index = max_index
        current_progress = round((min_index/len(full_uniprot_list))*100, 2)
        
        # Progression print
        print('{0} %'.format(current_progress), end='\r')
        
    # Write simplified database for our purpose
    write_json_db(uniprot_biogrid_mapping, own_db_path)
    
    """
    #
    # OLD METHOD
    #
    
    # Read archive
    with zipfile.ZipFile(db_path) as unziped:
        file_name = '.'.join(db_path.split('.')[:-1])
        # Load biogrid data   
        with unziped.open(unziped.filelist[0]) as filin:
            full_file = filin.readlines()
    
    # Load uniprot names
    #with open(uniprot_names_path) as filin:
    #    uniprot_names = json.load(filin)        
    
    # Set parsing vars                
    biogrid_entries = {}
    started_file = False
    
    # Progression print
    print('0 %', end='\r')
    
    # Loop over file_lines
    for i, lin in enumerate(full_file):
        # Condtion aquiered if header already found
        if started_file:
            # Progression print
            if (i-file_starts + 1)% int((len(full_file) / 100)) == 0:
                print(round((((i-file_starts + 1)/len(full_file))*100), 0), '%', end='\r')
                
            # Get data
            try:
                #Split line
                row = lin.decode('UTF-8').split('\t')
                # 
                # Map data
                if not row[0] in biogrid_entries.keys():
                    biogrid_entries[row[0]] = []
                
                if row[2] in ['UNIPROT-ACCESSION', 'SWISS-PROT'] and not row[1] in biogrid_entries[row[0]]:
                    biogrid_entries[row[0]].append(row[1])
            
            except:
                continue
        # Else is here so skip header condition once we are importing data only        
        else:        
            # Header gather and data start flag confition
            if lin.decode('UTF-8').strip().split('\t') == ['BIOGRID_ID', 'IDENTIFIER_VALUE', 'IDENTIFIER_TYPE', 'ORGANISM_OFFICIAL_NAME']:
                # Get header
                headers = lin.decode('UTF-8').strip().split('\t')
                # Set data starting FLAGS
                file_starts = i
                started_file = True
            
        
                
    # For user happyness :)
    print('100 % -> DONE', end='\r')
    
    # Write simplified database for our purpose
    write_json_db(biogrid_entries, own_db_path)
    """
    
    
        
def owndb_biogrid_connexions(db_path, own_db_path):
    # Read archive
    with zipfile.ZipFile(db_path) as unziped:
        # Load biogrid data   
        with unziped.open(unziped.filelist[0]) as filin:
            full_file = filin.readlines()
    
    
  # Set parsing variables
    # Entry template var
    new_entry_template = {'InteractorB':[], 'OutFiltered':[], 'Low':[], 'PMID_Low':[], 'PMID_Out':[]}
    # Full database holding var
    biogrid_associations = {}

  # Loop over lines
    print('0 %', end='\r')
    for i, lin in enumerate(full_file):
        # Split data
        row = lin.decode('UTF-8').split('\t')
        
        # Get header
        if i == 0: 
            # Retrieve header indexs
            for index, head in enumerate(row):
                if head == 'BioGRID ID Interactor A':
                    interactorA_index = index
                elif head == 'BioGRID ID Interactor B':
                    interactorB_index = index
                elif head == 'Experimental System':
                    exp_sys_index = index
                elif head == 'Experimental System Type':
                    exp_sys_type_index = index
                elif head == 'Throughput':
                    throughput_index = index
                elif head == 'Pubmed ID':
                    pmid_index = index
        else:
            # Progression print
            if i % int((len(full_file) / 100)) == 0:
                print(round(((i/len(full_file))*100), 0), '%', end='\r')  
        # Data Gathering information
            """the pipeline also integrates an additional source of
            interaction data: the Biological General Repository for Interaction
            Datasets (BioGRID) (Oughtred et al., 2019), from which only physical
            (not genetic) interactions between proteins are retrieved, except
            those associated with the 'Far Western', 'Co-fractionation',
            'Co-localization', 'Biochemical Activity', and 'High Throughput'
            experimental systems.
            """
        # For this reason, We skip many lines
            # from which only physical (not genetic) interactions between proteins are retrieved
            if row[exp_sys_type_index] != 'physical':
                continue
            
            
        # Original interaction analysis
            # New entry check
            if not row[interactorA_index] in biogrid_associations:
                biogrid_associations[row[interactorA_index]] = copy.deepcopy(new_entry_template)
            
            # Add data
            # Case of new connexion
            if not row[interactorB_index] in biogrid_associations[row[interactorA_index]]['InteractorB']:
                biogrid_associations[row[interactorA_index]]['InteractorB'].append(row[interactorB_index])
                biogrid_associations[row[interactorA_index]]['OutFiltered'].append(0)
                biogrid_associations[row[interactorA_index]]['Low'].append(0)
                biogrid_associations[row[interactorA_index]]['PMID_Low'].append({})
                biogrid_associations[row[interactorA_index]]['PMID_Out'].append({})
                
            # Retrieve index of Interactor B
            inter_B_index = biogrid_associations[row[interactorA_index]]['InteractorB'].index(row[interactorB_index])
            # Add values
            if 'Low' in row[throughput_index] and not row[exp_sys_type_index] in ['Far Western', 'Co-fractionation', 'Co-localization', 'Biochemical Activity']:
                # are retrieved, except those associated with the 'Far Western', 'Co-fractionation', 'Co-localization', 'Biochemical Activity', and 'High Throughput'
                # So with this condition I place everything in High
                biogrid_associations[row[interactorA_index]]['Low'][inter_B_index] += 1
                biogrid_associations[row[interactorA_index]]['PMID_Low'][inter_B_index][row[pmid_index]] = row[exp_sys_index]
            else:
                biogrid_associations[row[interactorA_index]]['OutFiltered'][inter_B_index] += 1
                biogrid_associations[row[interactorA_index]]['PMID_Out'][inter_B_index][row[pmid_index]] = row[exp_sys_index]
            
        # Inverse Interaction Analysis    
            # New entry check
            if not row[interactorB_index] in biogrid_associations:
                biogrid_associations[row[interactorB_index]] = copy.deepcopy(new_entry_template)
            
            # Add data
            # Case of new connexion
            if not row[interactorA_index] in biogrid_associations[row[interactorB_index]]['InteractorB']:
                biogrid_associations[row[interactorB_index]]['InteractorB'].append(row[interactorA_index])
                biogrid_associations[row[interactorB_index]]['OutFiltered'].append(0)
                biogrid_associations[row[interactorB_index]]['Low'].append(0)
                biogrid_associations[row[interactorB_index]]['PMID_Low'].append({})
                biogrid_associations[row[interactorB_index]]['PMID_Out'].append({})
                
            # Retrieve index of Interactor B
            inv_inter_A_index = biogrid_associations[row[interactorB_index]]['InteractorB'].index(row[interactorA_index])
            
             # Add values
            if 'Low' in row[throughput_index] and not row[exp_sys_type_index] in ['Far Western', 'Co-fractionation', 'Co-localization', 'Biochemical Activity']:
                # are retrieved, except those associated with the 'Far Western', 'Co-fractionation', 'Co-localization', 'Biochemical Activity', and 'High Throughput'
                # So with this condition I place everything in High
                biogrid_associations[row[interactorB_index]]['Low'][inv_inter_A_index] += 1
                biogrid_associations[row[interactorB_index]]['PMID_Low'][inv_inter_A_index][row[pmid_index]] = row[exp_sys_index]
            else:
                biogrid_associations[row[interactorB_index]]['OutFiltered'][inv_inter_A_index] += 1
                biogrid_associations[row[interactorB_index]]['PMID_Out'][inv_inter_A_index][row[pmid_index]] = row[exp_sys_index]
                
    # For user happyness :)
    print('100 % -> DONE', end='\r')
    
    # Write simplified database for our purpose
    write_json_db(biogrid_associations, own_db_path)     

def owndb_elm_classes(db_path, own_db_path):
    # Init loaded dic variable
    elm_classes = {}
    started_data = False
    # Load ELM_class_database
    with open(db_path, 'r') as filin:
        readcsv = csv.reader(filin, delimiter='\t')
        # Parse file
        for row in readcsv:
            # Skip commentaries
            if row[0].find('#') == 0:
                continue
            else:
                # Get header    
                if not started_data:
                    headers = row
                    # Loop for header keys
                    for index, head in enumerate(headers):
                        
                        if head == 'ELMIdentifier':
                            index_class = index
                        if head == 'Regex':
                            index_regex = index
                        if head == 'Probability':
                            index_proba = index
                    # Set data gathering flag        
                    started_data = True
                # If started_data = True            
                else:    
                    # Get data
                    if not row[index_class] in elm_classes.keys():
                        elm_classes[row[index_class]] = {'CREGEX':'',
                                                         'REGEX':row[index_regex],
                                                         'PROBA':row[index_proba]}

    # Write simplified database for our purpose
    write_json_db(elm_classes, own_db_path)
    
def owndb_elm_interaction(db_path, own_db_path):
    # Load ELM_interactions_database
    with open(db_path) as filin:
        readcsv = csv.reader(filin, delimiter='\t')
        elm_interaction_rows = []
        for i, row in enumerate(readcsv):
            if i == 0: 
                # Get header
                headers = row
            else:    
                # Get data
                elm_interaction_rows.append(row)


    elm_interactions = {}

    for i, elm_interaction_row in enumerate(elm_interaction_rows):
        #print(i, elm_interaction_row)
        
        motif_name = elm_interaction_row[0]
        # Conidtion for first time seen
        if not motif_name in elm_interactions.keys():
            elm_interactions[motif_name] = {'PFAM':[],
                                            'Interact_Domain_Desc':[],
                                            'Interact_Domain_Name':[]}
        
        
        elm_interactions[motif_name]['PFAM'].append(elm_interaction_row[1])
        elm_interactions[motif_name]['Interact_Domain_Desc'].append(elm_interaction_row[2])
            
        #print(splited_elm_interaction_row[0], elm_interactions[splited_elm_interaction_row[0]])
        
        # Manage to get right information with badly formated data at the begining ...
        if len(elm_interaction_row) == 4:
            elm_interactions[motif_name]['Interact_Domain_Name'].append(elm_interaction_row[3])   
        else:
            #print(i, len(elm_interaction_row))
            elm_interactions[motif_name]['Interact_Domain_Name'].append(''.join(elm_interaction_row[3:]))
    
    
    # Write simplified database for our purpose
    write_json_db(elm_interactions, own_db_path)

def owndb_uniprot(db_path, own_db_path):
# Load Uniprot_database
    with open(db_path) as filin:
        readcsv = csv.reader(filin, delimiter='\t')
        uniprot_data_rows = []
        for i, row in enumerate(readcsv):
            if i == 0: 
                # Get header
                headers = row
            else:    
                # Get data
                uniprot_data_rows.append(row)

    # Get index of wished headers 
    # Used to be able to use any downloaded file in any order
    for index, head in enumerate(headers):
        if head == 'Entry':
            entry_index = index
        if head == 'Cross-reference (Pfam)':
            pfam_index = index       
        if head == 'Cross-reference (PDB)':
            pdb_index = index
        if head == 'Sequence':
            fasta_index = index  

    # Now place uniprot data in variable
    uniprot_data = {}

    for i, row in enumerate(uniprot_data_rows):
        if i % int((len(uniprot_data_rows) / 100)) == 0:
            print(round(((i/len(uniprot_data_rows))*100), 0), '%', end='\r')    
        # In our file : Pfam are written : Pfam1;Pfam2;PfamN;
        # Thus we must skip last list element to skip the '' created by the last split of ';'
        # -> row[pfam_index].split(';')[:-1]
        uniprot_data[row[entry_index]] = {'PFAM':row[pfam_index].split(';')[:-1], 'FASTA':row[fasta_index]}
        
        # Same procedure for PDB structures
        
        #print(row[entry_index], uniprot_data[row[entry_index]], '\n')
 
    # Write simplified database for our purpose
    write_json_db(uniprot_data, own_db_path)   

def own_uniprot_names(db_path, own_db_path):
   # Load Uniprot_database
    with open(db_path) as filin:
        readcsv = csv.reader(filin, delimiter='\t')
        uniprot_data_rows = []
        for i, row in enumerate(readcsv):
            if i == 0: 
                # Get header
                headers = row
            else:    
                # Get data
                uniprot_data_rows.append(row)

    # Get index of wished headers 
    # Used to be able to use any downloaded file in any order
    for index, head in enumerate(headers):
        if head == 'Entry':
            entry_index = index
        if head == 'Entry name':
            entry_name_index = index       

    # Now place uniprot data in variable
    uniprot_data = {}

    for i, row in enumerate(uniprot_data_rows):
        uniprot_data[row[entry_index]] = row[entry_name_index]
        
    # Write simplified database for our purpose
    write_json_db(uniprot_data, own_db_path)




def owndb_pfam(db_path, own_db_path):
    # Read archive
    with gzip.open(db_path, 'rb') as ungziped:
        full_file_raw = ungziped.read()
        full_file = full_file_raw.decode('UTF-8').split('\n')
        pfam_rows = []
        for lin in full_file:
            pfam_rows.append(lin.split('\t'))
        
    # Gnerate OWN database
    pfam_dic = {}            
    # For each row
    for row in pfam_rows:
        if row == ['']:
            continue
            
        # Generate lists of PDBIds with same PFam
        if not row[3] in pfam_dic.keys():
            pfam_dic[row[3]] = {}
              
        if not row[4] in pfam_dic[row[3]].keys():
            # Init dic with absurd values that will be changed (easyer algorithm ..)
            pfam_dic[row[3]][row[4]] = {'DOMAIN_START':99999, 'DOMAIN_STOP':0, 'STRUCTURES':[]}
            
        # Add PDBID_CHAIN to pfam_dic[PFAM_ID][UNIPROT_ID]
        pfam_dic[row[3]][row[4]]['STRUCTURES'].append('{0}_{1}'.format(row[2], row[5]))
        
        # Adjust available boundaries
        if int(row[10]) < pfam_dic[row[3]][row[4]]['DOMAIN_START']:
            pfam_dic[row[3]][row[4]]['DOMAIN_START'] = int(row[10])
        if int(row[11]) > pfam_dic[row[3]][row[4]]['DOMAIN_STOP']:
            pfam_dic[row[3]][row[4]]['DOMAIN_STOP'] = int(row[11])
            
    # Write simplified database for our purpose
    write_json_db(pfam_dic, own_db_path)
    
def copy_function(db_path, own_db_path):
    import shutil
    shutil.copy2(db_path, own_db_path) 
#--------------------------------------------------------
# -------------------- MAIN PROGRAM ---------------------
#--------------------------------------------------------
db_update_dic = {'ELM_INTERACTION_DOMAIN':{'LINK':LMIP_script_config.ELM_INTERACTION_DOMAIN_DL,
                                           'DB_PATH':LMIP_script_config.ELM_INTERACTION_DOMAIN_RAW,
                                           'OWN_DB_PATH':LMIP_script_config.ELM_INTERACTION_DOMAIN,
                                           'OWN_DB_ALGORITHM':owndb_elm_interaction},
                 'ELM_CLASSES':{'LINK':LMIP_script_config.ELM_CLASSES_DL,
                                'DB_PATH':LMIP_script_config.ELM_CLASSES_RAW,
                                'OWN_DB_PATH':LMIP_script_config.ELM_CLASSES,
                                'OWN_DB_ALGORITHM':owndb_elm_classes},                          
                 'ELM_EXP_INTERACTIONS':{'LINK':LMIP_script_config.ELM_EXP_INTERACTIONS_DL,
                                         'DB_PATH':LMIP_script_config.ELM_VALIDE_INSTANCES_RAW,
                                         'OWN_DB_PATH':LMIP_script_config.ELM_VALIDE_INSTANCES,
                                         'OWN_DB_ALGORITHM':copy_function},
                 'UNIPROT':{'LINK':LMIP_script_config.UNIPROT_DL,
                            'DB_PATH':LMIP_script_config.UNIPROT_DATA_RAW,
                            'OWN_DB_PATH':LMIP_script_config.UNIPROT_DATA,
                            'OWN_DB_ALGORITHM':owndb_uniprot},
                 'PFAM':{'LINK':LMIP_script_config.PFAM_PDB_DL,
                         'DB_PATH':LMIP_script_config.PFAM_DATA_RAW,
                         'OWN_DB_PATH':LMIP_script_config.PFAM_DATA,
                         'OWN_DB_ALGORITHM':owndb_pfam},
                 'BIOGRID_DATA':{'LINK':LMIP_script_config.BIOGRID_DATA_DL,
                                 'DB_PATH':LMIP_script_config.BIOGRID_DATA_RAW,
                                 'OWN_DB_PATH':LMIP_script_config.BIOGRID_DATA,
                                 'OWN_DB_ALGORITHM':owndb_biogrid_connexions}}

failed_updates = []                    
for database in ['ELM_INTERACTION_DOMAIN', 'ELM_CLASSES', 'ELM_EXP_INTERACTIONS', 'PFAM', 'UNIPROT', 'BIOGRID_DATA']:
    # Monitoring print 
    print('\nDOWNLOADING {0}\nFROM        {1}'.format(database, db_update_dic[database]['LINK']))

# DOWNLOAD
    # Retrieve file extention
    database_extention = db_update_dic[database]['DB_PATH'].split('.')[-1]
    
    # Compressed files case
    if database_extention in ['zip', 'gz', 'targz', 'tar']:
        updated = database_updater(db_update_dic[database]['LINK'],
                                   db_update_dic[database]['DB_PATH'],
                                   text = False)
    # Text files case
    else:
        updated = database_updater(db_update_dic[database]['LINK'],
                                   db_update_dic[database]['DB_PATH'])
                                   
# COMPUTE
    # Error case                                
    if not updated:
        failed_updates.append('ERROR for {0} : No response from server'.format(database))
        # Monitoring print 
        print('FAILED ...')
        
    # No problem case    
    else:
        # Monitoring print 
        print('DONE !\nCOMPUTING OWN {0} SUB_DATABASE'.format(database))
        try:
          # Special case 1 : UNIPROT, need to apply uniprot names generation and BioGrid ID gathering
            if database == 'UNIPROT':
                print('\nCOMPUTING UNIPROT_NAMES SUB_DATABASE'.format(database))
                # Generation of separate db for uniprot names
                own_uniprot_names(db_update_dic[database]['DB_PATH'], LMIP_script_config.UNIPROT_NAMES)
                
                print('\nCOMPUTING BIOGRID_ID MAPPING SUB_DATABASE'.format(database))
                # Generation of BioGrid ID using Uniprot names
                owndb_biogrid_id(LMIP_script_config.UNIPROT_NAMES, LMIP_script_config.BIOGRID_ID)
                
            # Normal cases    
            # Use algorithm to compute own sub_db.json
            db_update_dic[database]['OWN_DB_ALGORITHM'](db_update_dic[database]['DB_PATH'], db_update_dic[database]['OWN_DB_PATH'])
            # Monitoring print 
            print('DONE !')
        except Exception as err:
            # Monitoring print 
            print('FAILED ...\n'+str(err))
            failed_updates.append('ERROR for {0} : Could\'t compute OWN_DATABASE'.format(database))
            
# Failed update check and print
print('\n\n')
for failed in failed_updates:
    print(failed)